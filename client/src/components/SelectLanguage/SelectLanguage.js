import React from 'react';
import { Select, Spinner } from '@chakra-ui/core';
import ErrorAlert from '../ErrorAlert/ErrorAlert';

const SelectLanguage = ({ enums, onChange, value }) => {
    return (
        <Select
            color="black"
            placeholder={"Languages..."}
            variant="flushed"
            name="language"
            onChange={onChange}
        >
            {enums.isLoading ? (
                <Spinner />
            ) : enums.error ? (
                <ErrorAlert />
            ) : (
                        enums.data.languages &&
                        enums.data.languages.map((item) => (
                            <option selected={value === item.id} value={item.id} key={item.id}>
                                {item.name}
                            </option>
                        ))
                    )}
        </Select>
    )
}
export default SelectLanguage;