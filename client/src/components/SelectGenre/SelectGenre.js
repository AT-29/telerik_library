import React from 'react';
import { Select, Spinner } from '@chakra-ui/core';
import ErrorAlert from '../ErrorAlert/ErrorAlert';

const SelectGenre = ({ enums, onChange, value }) => {
    return (
        <Select
            color="black"
            placeholder={value || "Genre..."}
            variant="flushed"
            name="genre"
            onChange={onChange}
        >
            {enums.isLoading ? (
                <Spinner />
            ) : enums.error ? (
                <ErrorAlert />
            ) : (
                        enums.data.genres &&
                        enums.data.genres.map((item) => (
                            <option selected={value === item.id} value={item.id} key={item.id}>
                                {item.name}
                            </option>
                        ))
                    )}
        </Select>
    )
}
export default SelectGenre;