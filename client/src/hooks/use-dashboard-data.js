import { useState, useEffect } from 'react';
import { API_URL } from '../common/common.js';
import axios from 'axios';
import { useAuth } from '../context/auth';

const useDashboardData = () => {
    const { sessionAuth } = useAuth();
    const [dashboard, setDashboard] = useState({
        data: null,
        isLoading: true,
        error: null,
    });

    useEffect(() => {
        axios
            .get(`${API_URL}/home`, {
                headers: {
                    Authorization: `Bearer ${sessionAuth?.token}`,
                },
            })
            .then((res) => {
                setDashboard({
                    data: res.data,
                    isLoading: false,
                    error: null,
                });
            })
            .catch((error) => {
                if (error.response) {
                    setDashboard({
                        data: null,
                        isLoading: false,
                        error: error.response.data.error,
                    });
                }
            })
    }, []);

    return dashboard;
};

export default useDashboardData;