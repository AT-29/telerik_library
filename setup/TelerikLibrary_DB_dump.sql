CREATE DATABASE  IF NOT EXISTS `teleriklibrary` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `teleriklibrary`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 93.123.78.206    Database: teleriklibrary
-- ------------------------------------------------------
-- Server version	5.5.5-10.5.5-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bans`
--

DROP TABLE IF EXISTS `bans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text DEFAULT NULL,
  `from_date` datetime NOT NULL DEFAULT current_timestamp(),
  `to_date` datetime NOT NULL DEFAULT (current_timestamp() + interval 1 day),
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bans_users1_idx` (`user_id`),
  CONSTRAINT `fk_bans_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bans`
--

LOCK TABLES `bans` WRITE;
/*!40000 ALTER TABLE `bans` DISABLE KEYS */;
INSERT INTO `bans` VALUES (1,'I have banned you!','2020-10-09 15:24:28','2020-10-09 15:33:24',3),(2,'I have banned you!','2020-10-09 15:36:17','2020-10-09 15:52:05',3),(3,'I have banned you!','2020-10-09 15:52:13','2020-10-09 15:53:20',3),(4,'Banish, fiend!','2020-10-09 22:55:54','2020-10-09 22:57:25',5),(5,'I have banned you!','2020-10-27 21:52:53','2020-10-27 22:10:48',4),(6,'Banned!','2020-10-30 19:34:48','2020-10-31 18:05:52',28),(7,NULL,'2020-10-31 01:28:55','2020-10-31 01:30:08',67),(8,NULL,'2020-10-31 01:30:14','2020-10-31 01:35:48',67),(9,NULL,'2020-10-31 01:38:45','2020-10-31 01:47:20',67),(10,NULL,'2020-10-31 01:52:19','2020-10-31 01:55:08',67),(11,NULL,'2020-10-31 01:55:15','2020-10-31 01:55:25',67),(12,NULL,'2020-10-31 01:55:20','2020-10-31 01:55:23',15),(13,NULL,'2020-10-31 10:56:05','2020-10-31 10:56:24',24),(14,NULL,'2020-10-31 10:56:35','2020-10-31 10:56:38',24),(15,NULL,'2020-10-31 10:57:41','2020-10-31 10:57:43',67),(16,NULL,'2020-10-31 10:57:50','2020-10-31 10:57:54',67),(17,NULL,'2020-10-31 10:58:27','2020-10-31 10:58:31',67),(18,NULL,'2020-10-31 12:11:23','2020-10-31 12:12:06',67),(19,NULL,'2020-10-31 12:12:13','2020-10-31 12:12:16',67),(20,NULL,'2020-10-31 12:12:25','2020-10-31 12:12:27',67),(21,NULL,'2020-10-31 15:58:54','2020-10-31 15:58:58',15),(22,NULL,'2020-10-31 15:59:05','2020-10-31 15:59:18',15),(23,NULL,'2020-10-31 15:59:24','2020-10-31 15:59:28',15),(24,NULL,'2020-10-31 16:02:22','2020-10-31 16:02:25',15),(25,NULL,'2020-10-31 16:02:47','2020-10-31 16:02:51',15),(26,NULL,'2020-10-31 16:03:01','2020-10-31 16:03:05',15),(27,NULL,'2020-10-31 16:03:30','2020-10-31 16:03:39',15),(28,NULL,'2020-10-31 16:03:44','2020-10-31 16:03:50',15),(29,NULL,'2020-10-31 16:05:28','2020-10-31 16:05:34',15),(30,NULL,'2020-10-31 16:05:52','2020-10-31 16:06:14',15),(31,NULL,'2020-10-31 16:06:18','2020-10-31 16:06:21',15),(32,NULL,'2020-10-31 16:06:28','2020-10-31 16:06:56',67),(33,NULL,'2020-10-31 18:08:52','2020-10-31 18:08:58',28),(34,NULL,'2020-10-31 18:11:51','2020-10-31 18:12:31',28),(35,'you are banned now','2020-10-31 18:13:04','2020-10-31 18:13:19',28),(36,NULL,'2020-10-31 18:13:31','2020-10-31 18:13:37',28),(37,NULL,'2020-10-31 18:15:15','2020-10-31 19:16:19',28),(38,'I don\'t like him','2020-10-31 19:04:26','2020-11-01 19:04:26',54),(39,NULL,'2020-10-31 19:10:10','2020-10-31 19:47:33',20),(40,NULL,'2020-10-31 19:17:40','2020-10-31 19:21:23',28),(41,NULL,'2020-10-31 19:25:46','2020-11-01 13:12:51',28),(42,NULL,'2020-10-31 19:40:31','2020-10-31 19:40:47',24),(43,NULL,'2020-10-31 19:46:40','2020-10-31 19:46:46',24),(44,NULL,'2020-10-31 19:47:35','2020-10-31 19:47:37',20),(45,NULL,'2020-10-31 19:58:59','2020-10-31 19:59:01',29),(46,NULL,'2020-10-31 20:01:36','2020-11-01 16:17:26',18),(47,'I\'m sick of football fans','2020-11-01 12:44:52','2020-11-02 12:44:52',75),(48,NULL,'2020-11-01 12:58:00','2020-11-01 13:05:52',22),(49,NULL,'2020-11-01 13:09:02','2020-11-01 13:09:29',22),(50,'не ме кефиш, брат','2020-11-01 13:09:42','2020-11-02 13:09:42',22),(51,'Misbehaves','2020-11-01 13:13:22','2020-11-01 16:17:43',28),(52,NULL,'2020-11-01 16:17:59','2020-11-02 16:17:59',28),(53,'Return some Dostoyevski','2020-11-02 10:17:36','2020-11-02 19:39:12',89),(54,NULL,'2020-11-02 19:42:44','2020-11-03 19:42:44',28);
/*!40000 ALTER TABLE `bans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(100) NOT NULL,
  `year` int(11) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `genre_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `fk_books_statuses1_idx` (`status_id`),
  KEY `fk_books_languages1_idx` (`language_id`),
  KEY `fk_books_genres1_idx` (`genre_id`),
  CONSTRAINT `fk_books_genres1` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_books_languages1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_books_statuses1` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (1,'Welcome To My Journey','Lou Alvarado',2020,'1604221168581_419-hKJik7L._SX322_BO1,204,203,200_.jpg','A Road That Leads To Victory.\nAt the beginning of my life, God had a plan and a purpose, even when I didn\'t see it. God is no respecter of person. He also has a plan and a purpose for your life. When earthly life tries to determine your destiny and your identity by bringing you confusion and turmoil that leaves you wanting to give up on life, God has a way of visiting you with His gentle touch and true love.','2020-09-30 19:29:55','2020-11-01 10:59:26',0,25,2,1),(2,'Learning JavaScript Design Patterns','Addy Osmani',2012,'1604225083337_images.jpg','With Learning JavaScript Design Patterns, you\'ll learn how to write beautiful, structured, and maintainable JavaScript by applying classical and modern design patterns to the language. If you want to keep your code efficient, more manageable, and up-to-date with the latest best practices, this book is for you.','2020-09-30 19:29:55','2020-11-01 12:04:41',0,1,2,1),(3,'Speaking JavaScript','Axel Rauschmayer',2014,'1604225232415_orm_front_cover.jpg','Like it or not, JavaScript is everywhere these days-from browser to server to mobile-and now you, too, need to learn the language or dive deeper than you have. This concise book guides you into and through JavaScript, written by a veteran programmer who once found himself in the same position.','2020-09-30 19:29:55','2020-11-01 12:07:10',0,1,2,1),(4,'Programming JavaScript Applications','Eric Elliott',2014,'1604225194968_51t0zfcEBVL._SX379_BO1,204,203,200_.jpg','Take advantage of JavaScript\'s power to build robust web-scale or enterprise applications that are easy to extend and maintain. By applying the design patterns outlined in this practical book, experienced JavaScript developers will learn how to write flexible and resilient code that\'s easier-yes, easier-to work with as your code base grows.','2020-09-30 19:29:55','2020-11-01 12:06:32',0,1,2,2),(5,'Understanding ECMAScript 6','Nicholas C. Zakas',2016,'1604229515113_ецмаs.jpg','ECMAScript 6 represents the biggest update to the core of JavaScript in the history of the language. In Understanding ECMAScript 6, expert developer Nicholas C. Zakas provides a complete guide to the object types, syntax, and other exciting changes that ECMAScript 6 brings to JavaScript.','2020-09-30 19:29:55','2020-11-01 13:18:32',0,1,2,1),(6,'You Don\'t Know JS','Kyle Simpson',2015,'1604228694237_41kn-mEpe6L._SX331_BO1,204,203,200_.jpg','No matter how much experience you have with JavaScript, odds are you don’t fully understand the language. As part of the \"You Don’t Know JS\" series, this compact guide focuses on new features available in ECMAScript 6 (ES6), the latest version of the standard upon which JavaScript is built.','2020-09-30 19:29:55','2020-11-01 13:04:51',0,1,2,1),(10,'Making JS Great again','Anthony K. Tonev',2018,'absalom-absalom.jpg','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2020-10-09 17:35:12','2020-10-21 00:24:44',0,1,1,1),(11,'Placeholder','Anthony T',2020,'default_book.png','This book is about placeholders','2020-10-19 15:44:02',NULL,1,1,1,1),(12,'The Silence of the Lambs','Thomas Harris',1991,'1604225368528_Silence3.png','As part of the search for a serial murderer nicknames \"Buffalo Bill,\" FBI trainee Clarice Starling is given an assignment. She must visit a man confined to a high-security facility for the criminally insane and interview him.\n\nThat man, Dr. Hannibal Lecter, is a former psychiatrist with unusual tastes and an intense curiosity about the darker corners of the mind. His intimate understanding of the killer and of Clarice herself form the core of Thomas Harris\' The Silence of the Lambs--an unforgettable classic of suspense fiction.','2020-10-21 20:17:10','2020-11-01 12:09:26',0,4,2,1),(13,'Blindness','Jose Saramago',2017,'1603305812440-blindness.jpg','A book about not being able to see good. At all actually. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ','2020-10-21 21:43:32',NULL,0,3,2,2),(14,'Things Fall Apart','Chinua Achebe',1958,'1603355792402-things-fall-apart.jpg',NULL,'2020-10-22 11:36:56',NULL,0,3,2,1),(15,'Fairy tales','Hans Christian Andersen',1836,'1603357312222-fairy-tales.jpg','Andersen\'s fairy tales, consisting of 156 stories across nine volumes[1] and translated into more than 125 languages,[2] have become culturally embedded in the West\'s collective consciousness, readily accessible to children, but presenting lessons of virtue and resilience in the face of adversity for mature readers as well.[3] His most famous fairy tales include \"The Emperor\'s New Clothes,\" \"The Little Mermaid,\" \"The Nightingale,\" \"The Steadfast Tin Soldier\", \"The Red Shoes\", \"The Princess and the Pea,\" \"The Snow Queen,\" \"The Ugly Duckling,\" \"The Little Match Girl,\" and \"Thumbelina.\" His stories have inspired ballets, plays, and animated and live-action films.[4] One of Copenhagen\'s widest and busiest boulevards, skirting Copenhagen City Hall Square at the corner of which Andersen\'s larger-than-life bronze statue sits, is named \"H. C. Andersens Boulevard','2020-10-22 12:02:16','2020-11-01 12:27:24',0,4,8,1),(16,'Pride and Prejudice','Jane Austen',1813,'1603358646571-pride-and-prejudice.jpg',NULL,'2020-10-22 12:24:30',NULL,0,11,2,2),(17,'Molloy, Malone Dies, The Unnamable, the trilogy','Samuel Beckett',1952,'1603359327859-molloy-malone-dies-the-unnamable.jpg','Molloy is the first of three novels initially written in Paris between 1947 and 1950; this trio, which includes Malone Dies and The Unnamable, is collectively referred to as ‘The Trilogy’ or ‘the Beckett Trilogy.’[1] Beckett deliberately wrote all three books in French and then, aside from some collaborative work on Molloy with Patrick Bowles, served entirely as his own English-language translator; he did the same for most of his plays.[2] As Paul Auster explains, “Beckett’s renderings of his own work are never literal, word-by-word transcriptions. They are free, highly-inventive adaptations of the original text—or, perhaps more accurately, ‘repatriations’ from one language to the other, from one culture to the other. In effect, he wrote every book twice, and each version bears its own indelible mark.”[3] The three thematically-related books are dark existentialist comedies “whose ostensible subject is death,” but, as Salman Rushdie asserts, “are in fact books about life, the lifelong battle of life against its shadow, life shown near battle’s end, bearing its lifetime of scars.”[1] As the books progress, the prose becomes increasingly bare and stripped down,[4] and as Benjamin Kunkel notes, they “[have] become famous in the history of fiction because of what is left out: the usual novelistic apparatus of plot, scenes, and characters. … Here, it seems, is the novelistic equivalent of abstract painting.”[2]','2020-10-22 12:35:51',NULL,0,27,2,1),(18,'The Decameron','Giovanni Boccaccio',1351,'1603359567672-the-decameron.jpg','The book\'s primary title exemplifies Boccaccio\'s fondness for Greek philology: Decameron combines Greek δέκα, déka (\"ten\") and ἡμέρα, hēméra (\"day\") to mean \"ten-day [event]\",[2] referring to the period in which the characters of the frame story tell their tales.\n\nBoccaccio\'s subtitle, Prencipe Galeotto, refers to Galehaut, a fictional king portrayed in the Lancelot-Grail who was sometimes called by the title haut prince \"high prince\". Galehaut was a close friend of Lancelot and an enemy of King Arthur. When Galehaut learned that Lancelot loved Arthur\'s wife, Guinevere, he set aside his own ardor for Lancelot in order to arrange a meeting between his friend and Guinevere. At this meeting the Queen first kisses Lancelot, and so begins their love affair.\n\nIn Canto V of Inferno, Dante compares these fictional lovers with the real-life paramours Francesca da Rimini and Paolo Malatesta, whose relationship he fictionalises. In Inferno, Francesca and Paolo read of Lancelot and Guinevere, and the story impassions them to lovemaking.\n\nDante\'s description of Galehaut\'s munificence and savoir-faire amidst this intrigue impressed Boccaccio. By invoking the name Prencipe Galeotto in the alternative title to Decameron, Boccaccio alludes to a sentiment he expresses in the text: his compassion for women deprived of free speech and social liberty, confined to their homes and, at times, lovesick. He contrasts this life with that of the men free to enjoy hunting, fishing, riding, and falconry.','2020-10-22 12:39:51',NULL,0,11,9,1),(19,'The War of The Worlds','Herbert Wells',1898,'1604225623913_860.jpg','When an army of invading Martians lands in England, panic and terror seize the population. As the aliens traverse the country in huge three-legged machines, incinerating all in their path with a heat ray and spreading noxious toxic gases, the people of the Earth must come to terms with the prospect of the end of human civilization and the beginning of Martian rule.','2020-10-22 14:42:55','2020-11-01 12:13:41',0,11,2,1),(20,'Memoirs of Hadrian','Marguerite Yourcenar',1951,'1603367076728-memoirs-of-hadrian.jpg','Memoirs of Hadrian (French: Mémoires d\'Hadrien) is a novel by the Belgian-born French writer Marguerite Yourcenar about the life and death of Roman Emperor Hadrian. First published in France in French in 1951 as Mémoires d\'Hadrien, the book was an immediate success, meeting with enormous critical acclaim. Although the historical Hadrian wrote an autobiography, it has been lost.\n\nThe book takes the form of a letter to Hadrian\'s adoptive grandson and eventual successor \"Mark\" (Marcus Aurelius). The emperor meditates on military triumphs, love of poetry and music, philosophy, and his passion for his lover Antinous, all in a manner similar to Gustave Flaubert\'s \"melancholy of the antique world.\"\n\nYourcenar noted in her postscript \"Carnet de note\" to the original edition, quoting Flaubert, that she had chosen Hadrian as the subject of the novel in part because he had lived at a time when the Roman gods were no longer believed in, but Christianity was not yet established. This intrigued her for what she saw as parallels to her own post-war European world','2020-10-22 14:44:35','2020-11-01 12:31:14',0,11,4,2),(21,'Testssdsds','sdsadadda',2021,'1603443015040-a-la-recherche-du-temps-perdu.jpg','sssasda','2020-10-23 11:50:14',NULL,0,9,2,1),(22,'Diary of a madman','Lu Xun',2017,'1603644488245_diary-of-a-madman.jpg','A book about a madman A book about a madman A book about a madman A book about a madman A book about a madman A book about a','2020-10-25 18:48:05',NULL,0,28,16,2),(23,'Stories','Anton Chekhov',1886,'1603967720498_stories-of-anton-chekhov.jpg','Chekhov wrote over 500 short stories. This book is a partial collection.','2020-10-29 12:35:22',NULL,0,11,6,1),(24,'Nostromo','Joseph Conrad',1904,'1603967823549_nostromo.jpg','Nostromo: A Tale of the Seaboard is a 1904 novel by Joseph Conrad, set in the fictitious South American republic of \"Costaguana\". It was originally published serially in two volumes of T.P.\'s Weekly.\n\nIn 1998, the Modern Library ranked Nostromo 47th on its list of the 100 best English-language novels of the 20th century. It is frequently regarded as amongst the best of Conrad\'s long fiction; F. Scott Fitzgerald once said, \"I\'d rather have written Nostromo than any other novel.','2020-10-29 12:37:05',NULL,0,11,2,1),(26,'Great Expectations','Charles Dickens',1861,'1603967944190_great-expectations.jpg','Great Expectations is the thirteenth novel by Charles Dickens and his penultimate completed novel, which depicts the education of an orphan nicknamed Pip (the book is a bildungsroman, a coming-of-age story). It is Dickens\'s second novel, after David Copperfield, to be fully narrated in the first person.[N 1] The novel was first published as a serial in Dickens\'s weekly periodical All the Year Round, from 1 December 1860 to August 1861.[1] In October 1861, Chapman and Hall published the novel in three volumes.[2][3][4]\n\nThe novel is set in Kent and London in the early to mid-19th century[5] and contains some of Dickens\'s most celebrated scenes, starting in a graveyard, where the young Pip is accosted by the escaped convict Abel Magwitch.[6] Great Expectations is full of extreme imagery – poverty, prison ships and chains, and fights to the death[6] – and has a colourful cast of characters who have entered popular culture. These include the eccentric Miss Havisham, the beautiful but cold Estella, and Joe, the unsophisticated and kind blacksmith. Dickens\'s themes include wealth and poverty, love and rejection, and the eventual triumph of good over evil.[6] Great Expectations, which is popular both with readers and literary critics,[7][8] has been translated into many languages and adapted numerous times into various media.\n\nUpon its release, the novel received near universal acclaim.[7] Although Dickens\'s contemporary Thomas Carlyle referred to it disparagingly as that \"Pip nonsense,\" he nevertheless reacted to each fresh installment with \"roars of laughter.\"[9] Later, George Bernard Shaw praised the novel, as \"All of one piece and consistently truthful.\"[10] During the serial publication, Dickens was pleased with public response to Great Expectations and its sales;[11] when the plot first formed in his mind, he called it \"a very fine, new and grotesque idea.\"[12]','2020-10-29 12:39:06',NULL,0,11,2,1),(27,'Jacques the Fatalist','Denis Diderot',1796,'1603968019176_jacques-the-fatalist.jpg','Jacques the Fatalist and his Master (French: Jacques le fataliste et son maître) is a novel by Denis Diderot, written during the period 1765–1780. The first French edition was published posthumously in 1796, but it was known earlier in Germany, thanks to Schiller\'s partial translation, which appeared in 1785 and was retranslated into French in 1793, as well as Mylius\'s complete German version of 1792.','2020-10-29 12:40:21',NULL,0,11,4,1),(28,'Crime and Punishment','Fyodor Dostoevsky',1866,'1604304938839_Crime_and_Punishment.jpg','Crime and Punishment (pre-reform Russian: Преступленіе и наказаніе; post-reform Russian: Преступление и наказание, tr. Prestupléniye i nakazániye, IPA: [prʲɪstʊˈplʲenʲɪje ɪ nəkɐˈzanʲɪje]) is a novel by the Russian author Fyodor Dostoevsky. It was first published in the literary journal The Russian Messenger in twelve monthly installments during 1866.[1] It was later published in a single volume. It is the second of Dostoevsky\'s full-length novels following his return from ten years of exile in Siberia. Crime and Punishment is considered the first great novel of his \"mature\" period of writing.[2] The novel is often cited as one of the supreme achievements in literature','2020-10-29 12:41:12','2020-11-02 10:15:42',0,11,6,1),(29,'The Idiot','Fyodor Dostoevsky',1869,'1603968140819_the-idiot.jpg','The Idiot (pre-reform Russian: Идіотъ; post-reform Russian: Идиот, tr. Idiót) is a novel by the 19th-century Russian author Fyodor Dostoevsky. It was first published serially in the journal The Russian Messenger in 1868–69.\n\nThe title is an ironic reference to the central character of the novel, Prince (Knyaz) Lev Nikolayevich Myshkin, a young man whose goodness, open-hearted simplicity and guilelessness lead many of the more worldly characters he encounters to mistakenly assume that he lacks intelligence and insight. In the character of Prince Myshkin, Dostoevsky set himself the task of depicting \"the positively good and beautiful man.\"[1] The novel examines the consequences of placing such a unique individual at the centre of the conflicts, desires, passions and egoism of worldly society, both for the man himself and for those with whom he becomes involved.\n\nJoseph Frank describes The Idiot as \"the most personal of all Dostoevsky\'s major works, the book in which he embodies his most intimate, cherished, and sacred convictions.\"[2] It includes descriptions of some of his most intense personal ordeals, such as epilepsy and mock execution, and explores moral, spiritual and philosophical themes consequent upon them. His primary motivation in writing the novel was to subject his own highest ideal, that of true Christian love, to the crucible of contemporary Russian society.','2020-10-29 12:42:22',NULL,0,11,6,2),(30,'The Possessed','Fyodor Dostoevsky',1872,'1603968191643_the-possessed.jpg','Demons (pre-reform Russian: Бѣсы; post-reform Russian: Бесы, tr. Bésy, IPA: [ˈbʲe.sɨ]; sometimes also called The Possessed or The Devils) is a novel by Fyodor Dostoevsky, first published in the journal The Russian Messenger in 1871–72. It is considered one of the four masterworks written by Dostoevsky after his return from Siberian exile, along with Crime and Punishment (1866), The Idiot (1869) and The Brothers Karamazov (1880). Demons is a social and political satire, a psychological drama, and large-scale tragedy. Joyce Carol Oates has described it as \"Dostoevsky\'s most confused and violent novel, and his most satisfactorily \'tragic\' work.\"[1] According to Ronald Hingley, it is Dostoevsky\'s \"greatest onslaught on Nihilism\", and \"one of humanity\'s most impressive achievements—perhaps even its supreme achievement—in the art of prose fiction.','2020-10-29 12:43:13',NULL,0,11,6,1),(31,'The Brothers Karamazov','Fyodor Dostoevsky',1880,'1603968230521_the-brothers-karamazov.jpg','The Brothers Karamazov (Russian: Бра́тья Карама́зовы, Brat\'ya Karamazovy, pronounced [ˈbratʲjə kərɐˈmazəvɨ]), also translated as The Karamazov Brothers, is the final novel by Russian author Fyodor Dostoevsky. Dostoevsky spent nearly two years writing The Brothers Karamazov, which was published as a serial in The Russian Messenger from January 1879 to November 1880. Dostoevsky died less than four months after its publication.\n\nSet in 19th-century Russia, The Brothers Karamazov is a passionate philosophical novel that enters deeply into questions of God, free will, and morality. It is a theological drama dealing with problems of faith, doubt and reason in the context of a modernizing Russia, with a plot that revolves around the subject of patricide. Dostoevsky composed much of the novel in Staraya Russa, which inspired the main setting.[1] Since its publication, it has been acclaimed as one of the supreme achievements in world literature','2020-10-29 12:43:52',NULL,0,11,6,2),(32,'Middlemarch','George Eliot',1871,'1603968330027_middlemarch.jpg','Middlemarch, A Study of Provincial Life is a novel by the English author Mary Anne Evans, who wrote as George Eliot. It first appeared in eight instalments (volumes) in 1871 and 1872. Set in a fictitious English Midland town in 1829–1832, it follows distinct, intersecting stories with many characters.[1] Issues include the status of women, the nature of marriage, idealism, self-interest, religion, hypocrisy, political reform, and education. Despite comic elements, Middlemarch uses realism to encompass historical events: the 1832 Reform Act, early railways, and the accession of King William IV. It looks at medicine of the time and reactionary views in a settled community facing unwelcome change. Eliot began writing the two pieces that formed the novel in 1869–1870 and completed it in 1871. Initial reviews were mixed, but it is now seen widely as her best work and one of the great English novels.','2020-10-29 12:45:31',NULL,0,11,2,1),(33,'Invisible Man','Ralph Ellison',1952,'1603968381447_invisible-man.jpg','Invisible Man is a novel by Ralph Ellison, published by Random House in 1952. It addresses many of the social and intellectual issues faced by the African Americans in the early twentieth century, including black nationalism, the relationship between black identity and Marxism, and the reformist racial policies of Booker T. Washington, as well as issues of individuality and personal identity.\n\nInvisible Man won the U.S. National Book Award for Fiction in 1953.[2] In 1998, the Modern Library ranked Invisible Man 19th on its list of the 100 best English-language novels of the 20th century.[3] Time magazine included the novel in its TIME 100 Best English-language Novels from 1923 to 2005, calling it \"the quintessential American picaresque of the 20th century,\" rather than a \"race novel, or even a bildungsroman.\"[4] Malcolm Bradbury and Richard Ruland recognize an existential vision with a \"Kafka-like absurdity.\"[5] According to The New York Times, Barack Obama modeled his memoir Dreams from My Father on Ellison\'s novel','2020-10-29 12:46:23',NULL,0,11,2,1),(34,'The Sound and the Fury','William Faulkner',1929,'1603968488926_the-sound-and-the-fury.jpg','The Sound and the Fury is a novel by the American author William Faulkner. It employs several narrative styles, including stream of consciousness. Published in 1929, The Sound and the Fury was Faulkner\'s fourth novel, and was not immediately successful. In 1931, however, when Faulkner\'s sixth novel, Sanctuary, was published—a sensationalist story, which Faulkner later said was written only for money—The Sound and the Fury also became commercially successful, and Faulkner began to receive critical attention.[1]\n\nIn 1998, the Modern Library ranked The Sound and the Fury sixth on its list of the 100 best English-language novels of the 20th century.','2020-10-29 12:48:10',NULL,0,11,2,1),(35,'Madame Bovary','Gustave Flaubert',1857,'1603968679080_madame-bovary.jpg','Madame Bovary (/ˈboʊvəri/;[1] French: [madam bɔvaʁi]), originally published as Madame Bovary: Provincial Manners (French: Madame Bovary: Mœurs de province [madam bɔvaʁi mœʁ(s) də pʁɔvɛ̃s]), is the debut novel of French writer Gustave Flaubert, published in 1856. The eponymous character lives beyond her means in order to escape the banalities and emptiness of provincial life.\n\nWhen the novel was first serialized in Revue de Paris between 1 October 1856 and 15 December 1856, public prosecutors attacked the novel for obscenity. The resulting trial in January 1857 made the story notorious. After Flaubert\'s acquittal on 7 February 1857, Madame Bovary became a bestseller in April 1857 when it was published in two volumes. A seminal work of literary realism, the novel is now considered Flaubert\'s masterpiece, and one of the most influential literary works in history. The British critic James Wood writes: \"Flaubert established, for good or ill, what most readers think of as modern realist narration, and his influence is almost too familiar to be visible.','2020-10-29 12:51:20',NULL,0,11,4,1),(36,'Sentimental Education','Gustave Flaubert',1869,'1603968834463_l-education-sentimentale.jpg','Sentimental Education (French: L\'Éducation sentimentale, 1869) is a novel by Gustave Flaubert. Considered one of the most influential novels of the 19th century, it was praised by contemporaries such as George Sand[1] and Émile Zola,[2] but criticised by Henry James.[3] The story focuses on the romantic life of a young man at the time of the French Revolution of 1848.','2020-10-29 12:53:56',NULL,0,11,4,1),(37,'Gypsy Ballads','Federico Garcia Lorca',1928,'1603968916131_gypsy-ballads.jpg','The Romancero gitano (often translated into English as Gypsy Ballads) is a poetry collection by Spanish writer Federico García Lorca. First published in 1928, it is composed of eighteen romances with subjects like the night, death, the sky, and the moon. All of the poems deal with the Romani people (gypsies) and their culture, but only as a theme used to carry the larger message that the poet was trying to convey.\n\nThe Romancero gitano was instantly popular and remains García Lorca\'s best known book of poetry.[1] It was a highly stylised imitation of the ballads and poems that were still being told throughout the Spanish countryside. García Lorca himself described the work as a \"carved altar piece\" of Andalusia with \"gypsies, horses, archangels, planets, its Jewish and Roman breezes, rivers, crimes, the everyday touch of the smuggler and the celestial note of the naked children of Córdoba. A book that hardly expresses visible Andalusia at all, but where the hidden Andalusia trembles\".[1] The book brought him fame across Spain and the Hispanic world; it would only be until much later in his life that he gained notability as a playwright.','2020-10-29 12:55:18',NULL,0,11,5,1),(38,'One Hundred Years of Solitude','Gabriel Garcia Marquez',1967,'1603969028219_one-hundred-years-of-solitude.jpg','One Hundred Years of Solitude (Spanish: Cien años de soledad, American Spanish: [sjen ˈaɲos ðe soleˈðað]) is a landmark 1967 novel by Colombian author Gabriel García Márquez that tells the multi-generational story of the Buendía family, whose patriarch, José Arcadio Buendía, founded the (fictitious) town of Macondo. The novel is often cited as one of the supreme achievements in literature.[1][2][3][4]\n\nThe magical realist style and thematic substance of One Hundred Years of Solitude established it as an important representative novel of the literary Latin American Boom of the 1960s and 1970s,[5] which was stylistically influenced by Modernism (European and North American) and the Cuban Vanguardia (Avant-Garde) literary movement.\n\nSince it was first published in May 1967 in Buenos Aires by Editorial Sudamericana, One Hundred Years of Solitude has been translated into 46 languages and sold more than 50 million copies.[6][7][8][9] The novel, considered García Márquez\'s magnum opus, remains widely acclaimed and is recognized as one of the most significant works both in the Hispanic literary canon[10] and in world literature.','2020-10-29 12:57:10',NULL,0,11,5,1),(39,'Love in the Time of Cholera','Gabriel Garcia Marquez',1985,'1603969107720_love-in-the-time-of-cholera.jpg','Love in the Time of Cholera (Spanish: El amor en los tiempos del cólera) is a novel by Colombian Nobel prize winning author Gabriel García Márquez. The novel was first published in Spanish in 1985. Alfred A. Knopf published an English translation in 1988, and an English-language movie adaptation was released in 2007.','2020-10-29 12:58:29',NULL,0,11,5,1),(40,'Dead Souls','Nikolai Gogol',1842,'1603969151707_dead-souls.jpg','Dead Souls (Russian: «Мёртвые души», Mjórtvyje dúshi) is a novel by Nikolai Gogol, first published in 1842, and widely regarded as an exemplar of 19th-century Russian literature. The novel chronicles the travels and adventures of Pavel Ivanovich Chichikov (Russian: Павел Иванович Чичиков) and the people whom he encounters. These people typify the Russian middle-class of the time. Gogol himself saw his work as an \"epic poem in prose\", and within the book characterised it as a \"novel in verse\". Despite supposedly completing the trilogy\'s second part, Gogol destroyed it shortly before his death. Although the novel ends in mid-sentence (like Sterne\'s Sentimental Journey), it is usually regarded[by whom?] as complete in the extant form.','2020-10-29 12:59:13',NULL,1,11,6,3),(41,'The Tin Drum','Günter Grass',1959,'1603969208157_the-tin-drum.jpg','The Tin Drum (German: Die Blechtrommel, pronounced [diː ˈblɛçˌtʁɔml̩] (About this soundlisten)) is a 1959 novel by Günter Grass. The novel is the first book of Grass\'s Danziger Trilogie (Danzig Trilogy). It was adapted into a 1979 film, which won both the Palme d\'Or, in the same year, and the Academy Award for Best Foreign Language Film the following year.','2020-10-29 13:00:10',NULL,0,11,3,1),(42,'Hunger','Knut Hamsun',1890,'1603969305711_hunger.jpg','One of the most important and controversial writers of the 20th century, Knut Hamsun made literary history with the publication in 1890 of this powerful, autobiographical novel recounting the abject poverty, hunger and despair of a young writer struggling to achieve self-discovery and its ultimate artistic expression. The book brilliantly probes the psychodynamics of alienation, obsession, and self-destruction, painting an unforgettable portrait of a man driven by forces beyond his control to the edge of the abyss. Hamsun influenced many of the major 20th-century writers who followed him, including Kafka, Joyce and Henry Miller. Required reading in world literature courses, the highly influential, landmark novel will also find a wide audience among lovers of books that probe the \"unexplored crannies in the human soul\" (George Egerton).','2020-10-29 13:01:47',NULL,0,11,3,1),(43,'The Old Man and the Sea','Ernest Hemingway',1952,'1603969365105_the-old-man-and-the-sea.jpg','The Old Man and the Sea is a short novel written by the American author Ernest Hemingway in 1951 in Cayo Blanco (Cuba), and published in 1952.[1] It was the last major work of fiction written by Hemingway that was published during his lifetime. One of his most famous works, it tells the story of Santiago, an aging Cuban fisherman who struggles with a giant marlin far out in the Gulf Stream off the coast of Cuba.[2]\n\nIn 1953, The Old Man and the Sea was awarded the Pulitzer Prize for Fiction, and it was cited by the Nobel Committee as contributing to their awarding of the Nobel Prize in Literature to Hemingway in 1954','2020-10-29 13:02:46',NULL,0,11,2,1),(44,'A Doll\'s House','Henrik Ibsen',1879,'1603969605158_a-Dolls-house.jpg','A Doll\'s House (Danish and Bokmål: Et dukkehjem; also translated as A Doll House) is a three-act play written by Norwegian playright Henrik Ibsen. It premiered at the Royal Theatre in Copenhagen, Denmark, on 21 December 1879, having been published earlier that month.[1] The play is set in a Norwegian town circa 1879.\n\nThe play is significant for the way it deals with the fate of a married woman, who at the time in Norway lacked reasonable opportunities for self-fulfillment in a male-dominated world, despite the fact that Ibsen denies it was his intent to write a feminist play. It aroused a great sensation at the time,[2] and caused a \"storm of outraged controversy\" that went beyond the theatre to the world newspapers and society.[3]\n\nIn 2006, the centennial of Ibsen\'s death, A Doll\'s House held the distinction of being the world\'s most performed play that year.[4] UNESCO has inscribed Ibsen\'s autographed manuscripts of A Doll\'s House on the Memory of the World Register in 2001, in recognition of their historical value.[5]\n\nThe title of the play is most commonly translated as A Doll\'s House, though some scholars use A Doll House. John Simon says that A Doll\'s House is \"the British term for what [Americans] call a \'dollhouse\'\".[6] Egil Törnqvist says of the alternative title: \"Rather than being superior to the traditional rendering, it simply sounds more idiomatic to Americans.','2020-10-29 13:06:47',NULL,0,11,13,1),(45,'Ulysses','James Joyce',1922,'1603969666812_ulysses.jpg','Ulysses is a modernist novel by Irish writer James Joyce. It was first serialized in parts in the American journal The Little Review from March 1918 to December 1920 and then published in its entirety in Paris by Sylvia Beach on 2 February 1922, Joyce\'s 40th birthday. It is considered one of the most important works of modernist literature[1] and has been called \"a demonstration and summation of the entire movement.\"[2] According to Declan Kiberd, \"Before Joyce, no writer of fiction had so foregrounded the process of thinking\".[3]\n\nUlysses chronicles the peripatetic appointments and encounters of Leopold Bloom in Dublin in the course of an ordinary day, 16 June 1904.[4][5] Ulysses is the Latinised name of Odysseus, the hero of Homer\'s epic poem the Odyssey, and the novel establishes a series of parallels between the poem and the novel, with structural correspondences between the characters and experiences of Bloom and Odysseus, Molly Bloom and Penelope, and Stephen Dedalus and Telemachus, in addition to events and themes of the early 20th-century context of modernism, Dublin, and Ireland\'s relationship to Britain. The novel is highly allusive and also imitates the styles of different periods of English literature.','2020-10-29 13:07:48',NULL,0,11,2,1),(46,'Stories','Franz Kafka',1924,'1603969720923_stories-of-franz-kafka.jpg','Franz Kafka, a German-language writer of novels and short stories, regarded by critics as one of the most influential authors of the 20th century, was trained as a lawyer and was employed by an insurance company, writing only in his spare time.','2020-10-29 13:08:42',NULL,0,11,2,1),(47,'The Trial','Franz Kafka',1924,'1603969770435_the-trial.jpg','The Trial (German: Der Process,[1] later Der Proceß, Der Prozeß and Der Prozess) is a novel written by Franz Kafka between 1914 and 1915 and published posthumously in 1925. One of his best-known works, it tells the story of Josef K., a man arrested and prosecuted by a remote, inaccessible authority, with the nature of his crime revealed neither to him nor to the reader. Heavily influenced by Dostoevsky\'s Crime and Punishment and The Brothers Karamazov, Kafka even went so far as to call Dostoevsky a blood relative.[2] Like Kafka\'s other novels, The Trial was never completed, although it does include a chapter which appears to bring the story to an intentionally abrupt ending.\n\nAfter Kafka\'s death in 1924 his friend and literary executor Max Brod edited the text for publication by Verlag Die Schmiede. The original manuscript is held at the Museum of Modern Literature, Marbach am Neckar, Germany. The first English-language translation, by Willa and Edwin Muir, was published in 1937.[3] In 1999, the book was listed in Le Monde\'s 100 Books of the Century and as No. 2 of the Best German Novels of the Twentieth Century.','2020-10-29 13:09:32',NULL,0,11,3,1),(48,'The Castle','Franz Kafka',1926,'1603969810109_the-castle.jpg','The Castle (German: Das Schloss, also spelled Das Schloß [das ˈʃlɔs]) is a 1926 novel by Franz Kafka. In it a protagonist known only as \"K.\" arrives in a village and struggles to gain access to the mysterious authorities who govern it from a castle. Kafka died before he could finish the work, but suggested it would end with K. dying in the village, the castle notifying him on his death bed that his \"legal claim to live in the village was not valid, yet, taking certain auxiliary circumstances into account, he was permitted to live and work there.\" Dark and at times surreal, The Castle is often understood to be about alienation, unresponsive bureaucracy, the frustration of trying to conduct business with non-transparent, seemingly arbitrary controlling systems, and the futile pursuit of an unobtainable goal.','2020-10-29 13:10:11',NULL,0,11,3,1),(49,'The Sound of the Mountain','Yasunari Kawabata',1954,'1603969869237_the-sound-of-the-mountain.jpg','The Sound of the Mountain (Yama no Oto) is a novel by Japanese writer Yasunari Kawabata, serialized between 1949 and 1954. The Sound of the Mountain is unusually long for a Kawabata novel, running to 276 pages in its English translation. Like much of his work, it is written in short, spare prose akin to poetry, which its English-language translator Edward Seidensticker likened to a haiku in the introduction to his translation of Kawabata\'s best-known novel, Snow Country.\n\nSound of the Mountain was adapted as a film of the same name (Toho, 1954), directed by Mikio Naruse and starring Setsuko Hara, So Yamamura, Ken Uehara and Yatsuko Tanami.\n\nThe book is included in the Bokklubben World Library\'s list of the 100 greatest works of world literature.','2020-10-29 13:11:11',NULL,0,11,14,1),(50,'Zorba the Greek','Nikos Kazantzakis',1946,'1603969918713_zorba-the-greek.jpg','Zorba the Greek (Greek: Βίος και Πολιτεία του Αλέξη Ζορμπά, Víos kai Politeía tou Aléxē Zorbá, Life and Times of Alexis Zorbas) is a novel written by the Cretan author Nikos Kazantzakis, first published in 1946. It is the tale of a young Greek intellectual who ventures to escape his bookish life with the aid of the boisterous and mysterious Alexis Zorba. The novel was adapted into the successful 1964 film of the same name directed by Michael Cacoyannis as well as a stage musical and a BBC radio play.','2020-10-29 13:12:00',NULL,0,11,7,1),(51,'Poems','Giacomo Leopardi',1818,'1603969975500_poems-giacomo-leopardi.jpg','Italian poetry.','2020-10-29 13:12:57',NULL,0,11,9,1),(52,'The Golden Notebook','Doris Lessing',1962,'1603970033725_the-golden-notebook.jpg','The Golden Notebook is a 1962 novel by Doris Lessing. It, like the two books that followed it, enters the realm of what Margaret Drabble in The Oxford Companion to English Literature called Lessing\'s \"inner space fiction\"; her work that explores mental and societal breakdown. It contains powerful anti-war and anti-Stalinist messages, an extended analysis of communism and the Communist Party in England from the 1930s to the 1950s, and an examination of the budding sexual and women\'s liberation movements.','2020-10-29 13:13:55',NULL,0,11,2,1),(53,'Pippi Longstocking','Astrid Lindgren',1945,'1603970067274_pippi-longstocking.jpg','The Golden Notebook is a 1962 novel by Doris Lessing. It, like the two books that followed it, enters the realm of what Margaret Drabble in The Oxford Companion to English Literature called Lessing\'s \"inner space fiction\"; her work that explores mental and societal breakdown. It contains powerful anti-war and anti-Stalinist messages, an extended analysis of communism and the Communist Party in England from the 1930s to the 1950s, and an examination of the budding sexual and women\'s liberation movements.','2020-10-29 13:14:29',NULL,0,11,15,2),(54,'Children of Gebelawi','Naguib Mahfouz',1959,'1603970120889_children-of-gebelawi.jpg','Children of Gebelawi, (أولاد حارتنا) is a novel by the Egyptian writer and Nobel laureate Naguib Mahfouz. It is also known by its Egyptian dialectal transliteration, Awlad Haretna, formal Arabic transliteration, Awlaadu Haaratena and by the alternative translated transliteral Arabic title of Children of Our Alley.','2020-10-29 13:15:22',NULL,0,11,11,2),(55,'Buddenbrooks','Thomas Mann',1901,'1603970173759_buddenbrooks.jpg','Buddenbrooks (German: [ˈbʊdn̩ˌbʁoːks] (About this soundlisten)) is a 1901 novel by Thomas Mann, chronicling the decline of a wealthy north German merchant family over the course of four generations, incidentally portraying the manner of life and mores of the Hanseatic bourgeoisie in the years from 1835 to 1877. Mann drew deeply from the history of his own family, the Mann family of Lübeck, and their milieu.\n\nIt was Mann\'s first novel, published when he was twenty-six years old. With the publication of the second edition in 1903, Buddenbrooks became a major literary success. Its English translation by Helen Tracy Lowe-Porter was published in 1924. The work led to a Nobel Prize in Literature for Mann in 1929; although the Nobel award generally recognises an author\'s body of work, the Swedish Academy\'s citation for Mann identified \"his great novel Buddenbrooks\" as the principal reason for his prize','2020-10-29 13:16:15',NULL,0,11,3,1),(56,'The Magic Mountain','Thomas Mann',1924,'1603970250518_the-magic-mountain.jpg','The Magic Mountain (German: Der Zauberberg, pronounced [deːɐ̯ ˈt͡saʊ̯bɐˌbɛʁk] (About this soundlisten)) is a novel by Thomas Mann, first published in German in November 1924. It is widely considered to be one of the most influential works of twentieth-century German literature.','2020-10-29 13:17:32',NULL,0,11,3,2),(57,'Moby Dick','Herman Melville',1851,'1603970300031_moby-dick.jpg','Moby-Dick; or, The Whale is an 1851 novel by American writer Herman Melville. The book is the sailor Ishmael\'s narrative of the obsessive quest of Ahab, captain of the whaling ship Pequod, for revenge on Moby Dick, the giant white sperm whale that on the ship\'s previous voyage bit off Ahab\'s leg at the knee. A contribution to the literature of the American Renaissance, Moby-Dick was published to mixed reviews, was a commercial failure, and was out of print at the time of the author\'s death in 1891. Its reputation as a \"Great American Novel\" was established only in the 20th century, after the centennial of its author\'s birth. William Faulkner said he wished he had written the book himself,[1] and D. H. Lawrence called it \"one of the strangest and most wonderful books in the world\" and \"the greatest book of the sea ever written\".[2] Its opening sentence, \"Call me Ishmael\", is among world literature\'s most famous.','2020-10-29 13:18:21',NULL,0,11,2,1),(58,'The Tale of Genji','Murasaki Shikibu',1006,'1603970365972_the-tale-of-genji.jpg','The Tale of Genji (源氏物語, Genji monogatari, pronounced [ɡeɲdʑi monoɡaꜜtaɾi]) is a classic work of Japanese literature written in the early 11th century by the noblewoman and lady-in-waiting Murasaki Shikibu. The original manuscript, created around the peak of the Heian period, no longer exists. It was made in \"concertina\" or orihon style:[1] several sheets of paper pasted together and folded alternately in one direction then the other. The work is a unique depiction of the lifestyles of high courtiers during the Heian period. It is written in archaic language and a poetic yet confusing style that make it unreadable to the average Japanese person without dedicated study.[2] It was not until the early 20th century that Genji was translated into modern Japanese by the poet Akiko Yosano. The first English translation was attempted in 1882 but was of poor quality and incomplete.\n\nThe work recounts the life of Hikaru Genji, or \"Shining Genji\", the son of an ancient Japanese emperor, known to readers as Emperor Kiritsubo, and a low-ranking concubine called Kiritsubo Consort. For political reasons, the emperor removes Genji from the line of succession, demoting him to a commoner by giving him the surname Minamoto, and he pursues a career as an imperial officer. The tale concentrates on Genji\'s romantic life and describes the customs of the aristocratic society of the time. It may be the world\'s first novel,[3] the first modern novel, the first psychological novel or the first novel still to be considered a classic.','2020-10-29 13:19:27',NULL,0,11,14,1),(59,'Aku the deciever','Samurai Jack',102033,'1604144776515_shirt-1574020773-78d2edbdd3fe9ad383574f34573db7b7.jpg','Long ago in a distant land, I, Aku, the shapeshifting master of darkness, unleashed an unspeakable evil. But, a foolish samurai warrior wielding a magic sword stepped forth to opposed me.\nBefore the final blow was struck, I tore open a portal in time and flung him into the future where my evil is law. Now, the fool seeks to return to the past and undo the future that is Aku.','2020-10-30 03:43:32','2020-10-31 13:46:25',1,28,14,3),(60,'Strange Planet','Nathan W. Pyle',2020,'1604022556902_shirt-1574020773-78d2edbdd3fe9ad383574f34573db7b7.jpg','A book about alien beigns.','2020-10-30 03:49:16',NULL,1,24,6,3),(61,'A Doll\'s House','Henrik Ibsen',1879,'http://localhost:3000/images/1603969605158_a-Dolls-house.jpg','A Doll\'s House (Danish and Bokmål: Et dukkehjem; also translated as A Doll House) is a three-act play written by Norwegian playright Henrik Ibsen. It premiered at the Royal Theatre in Copenhagen, Denmark, on 21 December 1879, having been published earlier that month.[1] The play is set in a Norwegian town circa 1879.\n\nThe play is significant for the way it deals with the fate of a married woman, who at the time in Norway lacked reasonable opportunities for self-fulfillment in a male-dominated world, despite the fact that Ibsen denies it was his intent to write a feminist play. It aroused a great sensation at the time,[2] and caused a \"storm of outraged controversy\" that went beyond the theatre to the world newspapers and society.[3]\n\nIn 2006, the centennial of Ibsen\'s death, A Doll\'s House held the distinction of being the world\'s most performed play that year.[4] UNESCO has inscribed Ibsen\'s autographed manuscripts of A Doll\'s House on the Memory of the World Register in 2001, in recognition of their historical value.[5]\n\nThe title of the play is most commonly translated as A Doll\'s House, though some scholars use A Doll House. John Simon says that A Doll\'s House is \"the British term for what [Americans] call a \'dollhouse\'\".[6] Egil Törnqvist says of the alternative title: \"Rather than being superior to the traditional rendering, it simply sounds more idiomatic to Americans.','2020-10-31 11:37:38',NULL,1,11,13,3),(62,'A Doll\'s House','Henrik Ibsen',1879,'1603969605158_a-Dolls-house.jpg','A Doll\'s House (Danish and Bokmål: Et dukkehjem; also translated as A Doll House) is a three-act play written by Norwegian playright Henrik Ibsen. It premiered at the Royal Theatre in Copenhagen, Denmark, on 21 December 1879, having been published earlier that month.[1] The play is set in a Norwegian town circa 1879.\n\nThe play is significant for the way it deals with the fate of a married woman, who at the time in Norway lacked reasonable opportunities for self-fulfillment in a male-dominated world, despite the fact that Ibsen denies it was his intent to write a feminist play. It aroused a great sensation at the time,[2] and caused a \"storm of outraged controversy\" that went beyond the theatre to the world newspapers and society.[3]\n\nIn 2006, the centennial of Ibsen\'s death, A Doll\'s House held the distinction of being the world\'s most performed play that year.[4] UNESCO has inscribed Ibsen\'s autographed manuscripts of A Doll\'s House on the Memory of the World Register in 2001, in recognition of their historical value.[5]\n\nThe title of the play is most commonly translated as A Doll\'s House, though some scholars use A Doll House. John Simon says that A Doll\'s House is \"the British term for what [Americans] call a \'dollhouse\'\".[6] Egil Törnqvist says of the alternative title: \"Rather than being superior to the traditional rendering, it simply sounds more idiomatic to Americans.','2020-10-31 11:50:35',NULL,1,11,13,3),(63,'Frenchsdaas','dasdasdasdas',1222222,'1604143582695_big_5539e2d61de04456e1d88dd6f3c201c8.jpg','djfkodjfdfhksdhjkshfkjsdhjksdhfksdhjsdhjsd','2020-10-31 13:26:21',NULL,1,5,17,3),(64,'The Metamorphosis','Franz Kafka',1915,'1604229766571_41ZVSlBzbsL.jpg','\"As Gregor Samsa awoke one morning from uneasy dreams he found himself transformed in his bed into a gigantic insect. He was laying on his hard, as it were armor-plated, back and when he lifted his head a little he could see his domelike brown belly divided into stiff arched segments on top of which the bed quilt could hardly keep in position and was about to slide off completely. His numerous legs, which were pitifully thin compared to the rest of his bulk, waved helplessly before his eyes.\"\n\nWith it\'s startling, bizarre, yet surprisingly funny first opening, Kafka begins his masterpiece, The Metamorphosis. It is the story of a young man who, transformed overnight into a giant beetle-like insect, becomes an object of disgrace to his family, an outsider in his own home, a quintessentially alienated man. A harrowing—though absurdly comic—meditation on human feelings of inadequacy, guilt, and isolation, The Metamorphosis has taken its place as one of the most widely read and influential works of twentieth-century fiction. As W.H. Auden wrote, \"Kafka is important to us because his predicament is the predicament of modern man.\"','2020-11-01 13:22:44',NULL,0,19,3,1),(65,'Catch 22','Joseph Heller',1970,'1604235571563_Catch22.jpg','The novel is set during World War II, from 1942 to 1944. It mainly follows the life of Captain John Yossarian, a U.S. Army Air Forces B-25 bombardier. Most of the events in the book occur while the fictional 256th Squadron is based on the island of Pianosa, in the Mediterranean Sea, west of Italy. The novel looks into the experiences of Yossarian and the other airmen in the camp, who attempt to maintain their sanity while fulfilling their service requirements so that they may return home.','2020-11-01 14:01:52','2020-11-01 14:59:29',0,27,2,1),(66,'To Kill a Mocking Bird','Harper Lee',1960,'1604233244014_to-kill-a-mockingbird-book-cover-movie-poster-art-2-nishanth-gopinathan.jpg','The unforgettable novel of a childhood in a sleepy Southern town and the crisis of conscience that rocked it, To Kill A Mockingbird became both an instant bestseller and a critical success when it was first published in 1960. It went on to win the Pulitzer Prize in 1961 and was later made into an Academy Award-winning film, also a classic.\n\nCompassionate, dramatic, and deeply moving, To Kill A Mockingbird takes readers to the roots of human behavior - to innocence and experience, kindness and cruelty, love and hatred, humor and pathos. Now with over 18 million copies in print and translated into forty languages, this regional story by a young Alabama woman claims universal appeal. Harper Lee always considered her book to be a simple love story. Today it is regarded as a masterpiece of American literature.','2020-11-01 14:20:41','2020-11-01 14:21:31',0,9,2,1),(67,'Beloved','Toni Morrison',1987,'1604235312534_beloved.jpg','Beloved is a 1987 novel by the American writer Toni Morrison. Set after the American Civil War, it tells the story of a family of former slaves whose Cincinnati home is haunted by a malevolent spirit. Beloved is inspired by a true life incident involving Margaret Garner, an escaped slave from Kentucky who fled to the free state of Ohio in 1856, but was captured in accordance with the Fugitive Slave Act of 1850. When U.S. Marshals burst into the cabin where Garner and her husband had barricaded themselves, they found that she had killed her two-year-old daughter and was attempting to kill her other children to spare them from being returned to slavery.\n\nMorrison had come across an account of Garner titled \"A Visit to the Slave Mother who Killed Her Child\" in an 1856 newspaper article published in the American Baptist, and reproduced in The Black Book; a miscellaneous compilation of black history and culture that Morrison edited in 1974.[1]\n\nThe novel won the Pulitzer Prize for Fiction in 1988[2] and was a finalist for the 1987 National Book Award.[3] It was adapted as a 1998 movie of the same name, starring Oprah Winfrey. A survey of writers and literary critics compiled by The New York Times ranked it as the best work of American fiction from 1981 to 2006.','2020-11-01 14:55:16',NULL,0,11,2,1),(68,'The Man Without Qualities','Robert Musil',1931,'1604235388094_the-man-without-qualities.jpg','The Man Without Qualities (German: Der Mann ohne Eigenschaften; 1930–1943) is an unfinished modernist novel in three volumes and various drafts, by the Austrian writer Robert Musil.\n\nThe novel is a \"story of ideas\", which takes place in the time of the Austro-Hungarian monarchy\'s last days, and the plot often veers into allegorical digressions on a wide range of existential themes concerning humanity and feelings. It has a particular concern with the values of truth and opinion and how society organizes ideas about life and society, though the book is well over a thousand pages long in its entirety, and so no one single theme dominates.','2020-11-01 14:56:32',NULL,0,11,3,1),(69,'Lolita','Vladimir Nabokov',1955,'1604235432830_lolita.jpg','Lolita is a 1955 novel written by Russian-American novelist Vladimir Nabokov. The novel is notable for its controversial subject: the protagonist and unreliable narrator, a middle-aged literature professor under the pseudonym Humbert Humbert, is obsessed with a 12-year-old girl, Dolores Haze, with whom he becomes sexually involved after he becomes her stepfather. \"Lolita\" is his private nickname for Dolores. The novel was originally written in English and first published in Paris in 1955 by Olympia Press. Later it was translated into Russian by Nabokov himself and published in New York City in 1967 by Phaedra Publishers.\n\nLolita quickly attained a classic status. The novel was adapted into a film by Stanley Kubrick in 1962, and another film by Adrian Lyne in 1997. It has also been adapted several times for the stage and has been the subject of two operas, two ballets, and an acclaimed, but commercially unsuccessful, Broadway musical. The term \"Lolita\" has been assimilated into popular culture as a description of a young girl who is \"precociously seductive...without connotations of victimization\".[2] Many authors consider it the greatest work of the 20th century,[3] and it has been included in several lists of best books, such as Time\'s List of the 100 Best Novels, Le Monde\'s 100 Books of the Century, Bokklubben World Library, Modern Library\'s 100 Best Novels, and The Big Read.','2020-11-01 14:57:17',NULL,0,11,2,1),(70,'Nineteen Eighty-Four','George Orwell',1949,'1604235516303_nineteen-eighty-four.jpg','Nineteen Eighty-Four: A Novel, often published as 1984, is a dystopian social science fiction novel by English novelist George Orwell. It was published on 8 June 1949 by Secker & Warburg as Orwell\'s ninth and final book completed in his lifetime. Thematically, Nineteen Eighty-Four centres on the consequences of totalitarianism, mass surveillance, and repressive regimentation of persons and behaviours within society.[2][3] Orwell, himself a democratic socialist, modeled the authoritarian government in the novel after Stalinist Russia.[2][3][4] More broadly, the novel examines the role of truth and facts within politics and the ways in which they are manipulated.','2020-11-01 14:58:40',NULL,0,11,2,1),(71,'The Book of Disquiet','Fernando Pessoa',1928,'1604235578817_the-book-of-disquiet.jpg','The Book of Disquiet (Livro do Desassossego: Composto por Bernardo Soares, ajudante de guarda-livros na cidade de Lisboa) is a work by the Portuguese author Fernando Pessoa (1888–1935). Published posthumously, The Book of Disquiet is a fragmentary lifetime project, left unedited by the author, who introduced it as a \"factless autobiography.\" The publication was credited to Bernardo Soares, one of the author\'s alternate writing names, which he called semi-heteronyms, and had a preface attributed to Fernando Pessoa, another alternate writing name or orthonym.','2020-11-01 14:59:43',NULL,0,11,12,1),(72,'Tales','Edgar Allan Poe',1950,'1604235620830_tales-and-poems-of-edgar-allan-poe.jpg','The works of American author Edgar Allan Poe (January 19, 1809 – October 7, 1849) include many poems, short stories, and one novel. His fiction spans multiple genres, including horror fiction, adventure, science fiction, and detective fiction, a genre he is credited with inventing.[1] These works are generally considered part of the Dark romanticism movement, a literary reaction to Transcendentalism.[2] Poe\'s writing reflects his literary theories: he disagreed with didacticism[3] and allegory.[4] Meaning in literature, he said in his criticism, should be an undercurrent just beneath the surface; works whose meanings are too obvious cease to be art.[5] Poe pursued originality in his works, and disliked proverbs.[6] He often included elements of popular pseudosciences such as phrenology[7] and physiognomy.[8] His most recurring themes deal with questions of death, including its physical signs, the effects of decomposition, concerns of premature burial, the reanimation of the dead, and mourning.[9] Though known as a masterly practitioner of Gothic fiction, Poe did not invent the genre; he was following a long-standing popular tradition.[10]','2020-11-01 15:00:25',NULL,0,11,2,1),(73,'In Search of Lost Time','Marcel Proust',1920,'1604235706066_a-la-recherche-du-temps-perdu.jpg','In Search of Lost Time (French: À la recherche du temps perdu), also translated as Remembrance of Things Past, is a novel in seven volumes by Marcel Proust (1871–1922). It is his most prominent work, known both for its length and its theme of involuntary memory; the most famous example of this is the \"episode of the madeleine,\" which occurs early in the first volume. It gained fame in English in translations by C. K. Scott Moncrieff and Terence Kilmartin as Remembrance of Things Past, but the title In Search of Lost Time, a literal rendering of the French, became ascendant after D. J. Enright adopted it for his revised translation published in 1992.\n\nIn Search of Lost Time follows the narrator\'s recollections of childhood and experiences into adulthood in the late 19th century and early 20th century aristocratic France, while reflecting on the loss of time and lack of meaning to the world.[1] The novel began to take shape in 1909. Proust continued to work on it until his final illness in the autumn of 1922 forced him to break off. Proust established the structure early on, but even after volumes were initially finished he kept adding new material and edited one volume after another for publication. The last three of the seven volumes contain oversights and fragmentary or unpolished passages, as they existed only in draft form at the death of the author; the publication of these parts was overseen by his brother Robert.\n\nThe work was published in France between 1913 and 1927. Proust paid for the publication of the first volume (by the Grasset publishing house) after it had been turned down by leading editors who had been offered the manuscript in longhand. Many of its ideas, motifs and scenes were anticipated in Proust\'s unfinished novel Jean Santeuil (1896–99), though the perspective and treatment there are different, and in his unfinished hybrid of philosophical essay and story, Contre Sainte-Beuve (1908–09).','2020-11-01 15:01:50',NULL,0,11,4,1),(74,'Pedro Páramo','Juan Rulfo',1955,'1604235757600_pedro-paramo.jpg','Pedro Páramo is a novel written by Juan Rulfo about a man named Juan Preciado who travels to his recently deceased mother\'s hometown, Comala, to find his father, only to come across a literal ghost town─populated, that is, by spectral figures. Initially, the novel was met with cold critical reception and sold only two thousand copies during the first four years; later, however, the book became highly acclaimed. Páramo was a key influence on Latin American writers such as Gabriel García Márquez. Pedro Páramo has been translated into more than 30 different languages and the English version has sold more than a million copies in the United States.\n\nGabriel García Márquez has said that he felt blocked as a novelist after writing his first four books and that it was only his life-changing discovery of Pedro Páramo in 1961 that opened his way to the composition of his masterpiece, One Hundred Years of Solitude. Moreover, García Márquez claimed that he \"could recite the whole book, forwards and backwards.\"[1] Jorge Luis Borges considered Pedro Páramo to be one of the greatest texts written in any language','2020-11-01 15:02:41',NULL,0,11,5,1),(75,'Midnight\'s Children','Salman Rushdie',1981,'1604235817362_midnights-children.jpg','Midnight\'s Children is a 1981 novel by author Salman Rushdie. It deals with India\'s transition from British colonialism to independence and the partition of India. It is considered an example of postcolonial, postmodern, and magical realist literature. The story is told by its chief protagonist, Saleem Sinai, and is set in the context of actual historical events. The style of preserving history with fictional accounts is self-reflexive.\n\nMidnight\'s Children won both the Booker Prize and the James Tait Black Memorial Prize in 1981.[1] It was awarded the \"Booker of Bookers\" Prize and the best all-time prize winners in 1993 and 2008 to celebrate the Booker Prize 25th and 40th anniversary.[2][3] In 2003, the novel was listed on the BBC\'s The Big Read poll of the UK\'s \"best-loved novels\".[4] It was also added to the list of Great Books of the 20th Century, published by Penguin Books.','2020-11-01 15:03:41',NULL,0,11,2,1),(76,'Das Boot','Lothar-Günther Buchheim',1973,'1604235829379_299596.jpg','It is autumn, 1941, and a German U-boat commander and his crew set out on yet another hazardous patrol in the Battle of the Atlantic. Over the coming weeks they must brave the stormy waters of the Atlantic in their mission to seek out and destroy British supply ships. But the tide is beginning to turn against the Germans in the war for the North Atlantic. Their targets now travel in convoys, fiercely guarded by Royal Navy destroyers, and when contact is finally made the hunters rapidly become the hunted. As the U-boat is forced to hide beneath the surface of the sea a cat-and-mouse game begins, where the increasing claustrophobia of the submarine becomes an enemy just as frightening as the depth charges that explode around it. Of the 40,000 men who served on German submarines, 30,000 never returned. Written by a survivor of the U-boat fleet, Das Boot is a psychological drama merciless in its intensity, and a classic novel of World War II.','2020-11-01 15:03:47',NULL,0,5,3,1),(77,'Season of Migration to the North','Tayeb Salih',1966,'1604235866145_season-of-migration-to-the-north.jpg','Season of Migration to the North (Arabic: موسم الهجرة إلى الشمال‎ Mawsim al-Hijrah ilâ al-Shamâl) is a classic postcolonial Arabic novel by the Sudanese novelist Tayeb Salih. In 1966, Salih published his novel, the one for which he is best known. It was first published in the Beirut journal Hiwâr. The main concern of the novel is with the impact of British colonialism and European modernity on rural African societies in general and Sudanese culture and identity in particular.[1] His novel reflects the conflicts of modern Sudan and depicts the brutal history of European colonialism as shaping the reality of contemporary Sudanese society. Damascus-based Arab Literary Academy named it one of the best novels in Arabic of the twentieth century. Mawsim al-Hijrah ilâ al-Shamâl is considered to be an important turning point in the development of postcolonial narratives that focus on the encounter between East and West.[2]\n\nThe novel has been translated into more than twenty languages.[3] Salih was fluent in both English and Arabic, but chose to pen this novel in Arabic.[4] The English translation by Denys Johnson-Davis was published in 1969 as part of the influential Heinemann African Writers Series. The novel is a counternarrative to Heart of Darkness. It was described by Edward Said as one of the six great novels in Arabic literature.[5] In 2001 it was selected by a panel of Arab writers and critics as the most important Arab novel of the twentieth century.','2020-11-01 15:04:30',NULL,0,11,11,1),(78,'Hamlet','William Shakespeare',1603,'1604235909536_hamlet.jpg','The Tragedy of Hamlet, Prince of Denmark, often shortened to Hamlet (/ˈhæmlɪt/), is a tragedy written by William Shakespeare sometime between 1599 and 1601. It is Shakespeare\'s longest play with 30,557 words. Set in Denmark, the play depicts Prince Hamlet and his revenge against his uncle, Claudius, who has murdered Hamlet\'s father in order to seize his throne and marry Hamlet\'s mother.\n\nHamlet is considered among the most powerful and influential works of world literature, with a story capable of \"seemingly endless retelling and adaptation by others\".[1] It was one of Shakespeare\'s most popular works during his lifetime[2] and still ranks among his most performed, topping the performance list of the Royal Shakespeare Company and its predecessors in Stratford-upon-Avon since 1879.[3] It has inspired many other writers—from Johann Wolfgang von Goethe and Charles Dickens to James Joyce and Iris Murdoch—and has been described as \"the world\'s most filmed story after Cinderella\".','2020-11-01 15:05:13',NULL,0,11,2,1),(79,'King Lear','William Shakespeare',1608,'1604235969327_king-lear.jpg','King Lear is a tragedy written by William Shakespeare. It tells the tale of a king who bequeaths his power and land to two of his three daughters, after they declare their love for him in a fawning and obsequious manner. His third daughter gets nothing, because she will not flatter him as her sisters had done. When he feels he has been treated with disrespect by the two daughters who now have his wealth and power, he becomes furious to the point of madness. He eventually becomes tenderly reconciled to his third daughter, just before tragedy strikes her and then the king.\n\nDerived from the legend of Leir of Britain, a mythological pre-Roman Celtic king, the play has been widely adapted for the stage and motion pictures, with the title role coveted by accomplished actors.\n\nThe first attribution to Shakespeare of this play, originally drafted in 1605 or 1606 at the latest with its first known performance on St. Stephen\'s Day in 1606, was a 1608 publication in a quarto of uncertain provenance, in which the play is listed as a history; it may be an early draft or simply reflect the first performance text. The Tragedy of King Lear, a revised version that is better tailored for performance, was included in the 1623 First Folio. Modern editors usually conflate the two, though some insist that each version has its own individual integrity that should be preserved.\n\nAfter the English Restoration, the play was often revised with a happy ending for audiences who disliked its dark and depressing tone, but since the 19th century Shakespeare\'s original version has been regarded as one of his supreme achievements. The tragedy is noted for its probing observations on the nature of human suffering and kinship. George Bernard Shaw wrote \"No man will ever write a better tragedy than Lear.\"','2020-11-01 15:06:13',NULL,0,11,2,1),(80,'The Call of Cthulhu','Howard  Lovecraft',1928,'1604236002002_15730101._SX318_.jpg','One of the feature stories of the Cthulhu Mythos, H.P. Lovecraft\'s \'the Call of Cthulhu\' is a harrowing tale of the weakness of the human mind when confronted by powers and intelligences from beyond our world.','2020-11-01 15:06:39',NULL,0,18,2,1),(81,'Othello','William Shakespeare',1609,'1604236007495_othello.jpg','Othello (The Tragedy of Othello, the Moor of Venice) is a tragedy by William Shakespeare, believed to have been written in 1603. It is based on the story Un Capitano Moro (\"A Moorish Captain\") by Cinthio (a disciple of Boccaccio\'s), first published in 1565.[1] The story revolves around its two central characters: Othello, a Moorish general in the Venetian army, and his treacherous ensign, Iago. Given its varied and enduring themes of racism, love, jealousy, betrayal, revenge, and repentance, Othello is still often performed in professional and community theatre alike, and has been the source for numerous operatic, film, and literary adaptations.','2020-11-01 15:06:51',NULL,0,11,2,1),(82,'Oedipus the King','Sophocles',-430,'1604236056235_oedipus-the-king.jpg','Oedipus Rex, also known by its Greek title, Oedipus Tyrannus (Ancient Greek: Οἰδίπους Τύραννος, pronounced [oidípoːs týrannos]), or Oedipus the King, is an Athenian tragedy by Sophocles that was first performed around 429 BC.[1] Originally, to the ancient Greeks, the title was simply Oedipus (Οἰδίπους), as it is referred to by Aristotle in the Poetics. It is thought to have been renamed Oedipus Tyrannus to distinguish it from another of Sophocles\'s plays, Oedipus at Colonus. In antiquity, the term \"tyrant\" referred to a ruler with no legitimate claim to rule, but it did not necessarily have a negative connotation.[2][3][4]\n\nOf Sophocles\' three Theban plays that have survived, and that deal with the story of Oedipus, Oedipus Rex was the second to be written. However, in terms of the chronology of events that the plays describe, it comes first, followed by Oedipus at Colonus and then Antigone.\n\nPrior to the start of Oedipus Rex, Oedipus has become the king of Thebes while unwittingly fulfilling a prophecy that he would kill his father, Laius (the previous king), and marry his mother, Jocasta (whom Oedipus took as his queen after solving the riddle of the Sphinx). The action of Sophocles\'s play concerns Oedipus\'s search for the murderer of Laius in order to end a plague ravaging Thebes, unaware that the killer he is looking for is none other than himself. At the end of the play, after the truth finally comes to light, Jocasta hangs herself while Oedipus, horrified at his patricide and incest, proceeds to gouge out his own eyes in despair.\n\nOedipus Rex is regarded by many scholars as the masterpiece of ancient Greek tragedy[citation needed]. In his Poetics, Aristotle refers several times to the play in order to exemplify aspects of the genre.','2020-11-01 15:07:40',NULL,0,11,7,1),(83,'The Red and the Black','Stendhal',1830,'1604236116128_le-rouge-et-le-noir.jpg','Le Rouge et le Noir (French pronunciation: ​[lə ʁuʒ e l(ə) nwaʁ]; meaning The Red and the Black) is a historical psychological novel in two volumes by Stendhal, published in 1830.[1] It chronicles the attempts of a provincial young man to rise socially beyond his modest upbringing through a combination of talent, hard work, deception, and hypocrisy. He ultimately allows his passions to betray him.\n\nThe novel\'s full title, Le Rouge et le Noir: Chronique du XIXe siècle (The Red and the Black: A Chronicle of the 19th Century),[2] indicates its twofold literary purpose as both a psychological portrait of the romantic protagonist, Julien Sorel, and an analytic, sociological satire of the French social order under the Bourbon Restoration (1814–30). In English, Le Rouge et le Noir is variously translated as Red and Black, Scarlet and Black, and The Red and the Black, without the subtitle.[3]\n\nThe title is taken to refer to the tension between the clerical (black) and secular (red)[4] interests of the protagonist, but this interpretation is but one of many','2020-11-01 15:08:40',NULL,0,11,4,1),(84,'The Fellowship of the Ring','John Tolkien',1954,'1604236472906_The_Fellowship_of_the_Ring_cover.gif','One Ring to rule them all, One Ring to find them, One Ring to bring them all and in the darkeness bind them\n\nIn ancient times the Rings of Power were crafted by the Elven-smiths, and Sauron, The Dark Lord, forged the One Ring, filling it with his own power so that he could rule all others. But the One Ring was taken from him, and though he sought it throughout Middle-earth, it remained lost to him. After many ages it fell into the hands of Bilbo Baggins, as told in The Hobbit.\n\nIn a sleepy village in the Shire, young Frodo Baggins finds himself faced with an immense task, as his elderly cousin Bilbo entrusts the Ring to his care. Frodo must leave his home and make a perilous journey across Middle-earth to the Cracks of Doom, there to destroy the Ring and foil the Dark Lord in his evil purpose.','2020-11-01 15:09:22','2020-11-01 15:14:30',0,20,2,1),(85,'The Life And Opinions of Tristram Shandy','Laurence Sterne',1760,'1604236172027_the-life-and-opinions-of-tristram-shandy.jpg','The Life and Opinions of Tristram Shandy, Gentleman, also known as just Tristram Shandy, is a novel by Laurence Sterne. It was published in nine volumes, the first two appearing in 1759, and seven others following over the next seven years (vols. 3 and 4, 1761; vols. 5 and 6, 1762; vols. 7 and 8, 1765; vol. 9, 1767). It purports to be a biography of the eponymous character. Its style is marked by digression, double entendre, and graphic devices.\n\nSterne had read widely, which is reflected in Tristram Shandy. Many of his similes, for instance, are reminiscent of the works of the metaphysical poets of the 17th century,[1] and the novel as a whole, with its focus on the problems of language, has constant regard for John Locke\'s theories in An Essay Concerning Human Understanding.[2] Arthur Schopenhauer called Tristram Shandy one of \"the four immortal romances.\"','2020-11-01 15:09:36',NULL,0,11,2,1),(86,'Gulliver\'s Travels','Jonathan Swift',1726,'1604236211546_gullivers-travels.jpg','Gulliver\'s Travels, or Travels into Several Remote Nations of the World. In Four Parts. By Lemuel Gulliver, First a Surgeon, and then a Captain of Several Ships is a 1726 prose satire[1][2] by the Irish writer and clergyman Jonathan Swift, satirising both human nature and the \"travellers\' tales\" literary subgenre. It is Swift\'s best known full-length work, and a classic of English literature. Swift claimed that he wrote Gulliver\'s Travels \"to vex the world rather than divert it\".\n\nThe book was an immediate success. The English dramatist John Gay remarked \"It is universally read, from the cabinet council to the nursery.\"[3] In 2015, Robert McCrum released his selection list of 100 best novels of all time in which Gulliver\'s Travels is listed as \"a satirical masterpiece\".','2020-11-01 15:10:15',NULL,0,11,2,1),(87,'War and Peace','Leo Tolstoy',1867,'1604236250425_war-and-peace.jpg','War and Peace (pre-reform Russian: Война и миръ; post-reform Russian: Война и мир, romanized: Voyna i mir [vɐjˈna i ˈmʲir]) is a novel by the Russian author Leo Tolstoy, first published serially, then published in its entirety in 1869. It is regarded as one of Tolstoy\'s finest literary achievements and remains a classic of world literature.[1][2][3]\n\nThe novel chronicles the French invasion of Russia and the impact of the Napoleonic era on Tsarist society through the stories of five Russian aristocratic families. Portions of an earlier version, titled The Year 1805,[4] were serialized in The Russian Messenger from 1865 to 1867, then the novel was published in its entirety in 1869.[5]\n\nTolstoy said War and Peace is \"not a novel, even less is it a poem, and still less a historical chronicle.\" Large sections, especially the later chapters, are philosophical discussions rather than narrative.[6] Tolstoy also said that the best Russian literature does not conform to standards and hence hesitated to call War and Peace a novel. Instead, he regarded Anna Karenina as his first true novel.','2020-11-01 15:10:54',NULL,0,11,6,1),(88,'Anna Karenina','Leo Tolstoy',1877,'1604236287259_anna-karenina.jpg','Anna Karenina (Russian: «Анна Каренина», IPA: [ˈanːə kɐˈrʲenʲɪnə])[1] is a novel by the Russian author Leo Tolstoy, first published in book form in 1878. Many writers consider Anna Karenina the greatest work of literature ever,[2] and Tolstoy himself called it his first true novel. It was initially released in serial installments from 1873 to 1877 in the periodical The Russian Messenger.\n\nA complex novel in eight parts, with more than a dozen major characters, it is spread over more than 800 pages (depending on the translation and publisher), typically contained in two volumes. It deals with themes of betrayal, faith, family, marriage, Imperial Russian society, desire, and rural vs. city life. The plot centers on an extramarital affair between Anna and dashing cavalry officer Count Alexei Kirillovich Vronsky that scandalizes the social circles of Saint Petersburg and forces the young lovers to flee to Italy in a search for happiness. After they return to Russia, their lives further unravel.\n\nTrains are a recurring motif throughout the novel, which takes place against the backdrop of rapid transformations as a result of the liberal reforms initiated by Emperor Alexander II of Russia, with several major plot points taking place either on passenger trains or at stations in Saint Petersburg or elsewhere in Russia. The novel has been adapted into various media including theatre, opera, film, television, ballet, figure skating, and radio drama. The first of many film adaptations was released in 1911 but has not survived.','2020-11-01 15:11:31',NULL,0,11,6,1),(89,'The Two Towers','John Tolkien',1954,'1604236331937_The_Two_Towers_cover.gif','One Ring to rule them all, One Ring to find them, One Ring to bring them all and in the darkness bind them\n\nFrodo and his Companions of the Ring have been beset by danger during their quest to prevent the Ruling Ring from falling into the hands of the Dark Lord by destroying it in the Cracks of Doom. They have lost the wizard, Gandalf, in a battle in the Mines of Moria. And Boromir, seduced by the power of the Ring, tried to seize it by force. While Frodo and Sam made their escape, the rest of the company was attacked by Orcs. Now they continue the journey alone down the great River Anduin—alone, that is, save for the mysterious creeping figure that follows wherever they go.','2020-11-01 15:12:09',NULL,0,20,2,1),(90,'The Death of Ivan Ilyich','Leo Tolstoy',1886,'1604236339124_the-death-of-ivan-ilyich.jpg','The Death of Ivan Ilyich (Russian: Смерть Ивана Ильича, Smert\' Ivána Ilyichá), first published in 1886, is a novella by Leo Tolstoy, considered one of the masterpieces of his late fiction, written shortly after his religious conversion of the late 1870s.[1]\n\nWidely considered to be one of the finest novellas ever written,[2] The Death of Ivan Ilyich tells the story of a high-court judge in 19th-century Russia and his sufferings and death from a terminal illness.','2020-11-01 15:12:23',NULL,0,11,6,1),(91,'The Adventures of Huckleberry Finn','Mark Twain',1884,'1604236378873_the-adventures-of-huckleberry-finn.jpg','Adventures of Huckleberry Finn (or, in more recent editions, The Adventures of Huckleberry Finn) is a novel by Mark Twain, first published in the United Kingdom in December 1884 and in the United States in February 1885. Commonly named among the Great American Novels, the work is among the first in major American literature to be written throughout in vernacular English, characterized by local color regionalism. It is told in the first person by Huckleberry \"Huck\" Finn, the narrator of two other Twain novels (Tom Sawyer Abroad and Tom Sawyer, Detective) and a friend of Tom Sawyer. It is a direct sequel to The Adventures of Tom Sawyer.\n\nThe book is noted for its colorful description of people and places along the Mississippi River. Set in a Southern antebellum society that had ceased to exist over 20 years before the work was published, Adventures of Huckleberry Finn is an often scathing satire on entrenched attitudes, particularly racism.\n\nPerennially popular with readers, Adventures of Huckleberry Finn has also been the continued object of study by literary critics since its publication. The book was widely criticized upon release because of its extensive use of coarse language. Throughout the 20th century, and despite arguments that the protagonist and the tenor of the book are anti-racist,[2][3] criticism of the book continued due to both its perceived use of racial stereotypes and its frequent use of the racial slur \"nigger\".','2020-11-01 15:13:03',NULL,0,11,2,1),(92,'The Return of The King','John Tolkien',1955,'1604236445681_The_Return_of_the_King_cover.gif','One Ring to rule them all, One Ring to find them, One Ring to bring them all and in the darkness bind them\n\nIn ancient times the Rings of Power were crafted by the Elven-smiths, and Sauron, the Dark Lord, forged the One Ring, filling it with his own power so that he could rule all others. But the One Ring was taken from him, and though he sought it throughout Middle-earth, it remained lost to him. After many ages it fell by chance into the hands of the hobbit Bilbo Baggins.\n\nFrom Sauron\'s fastness in the Dark Tower of Mordor, his power spread far and wide. Sauron gathered all the Great Rings to him, but always he searched for the One Ring that would complete his dominion.\n\nWhen Bilbo reached his eleventy-first birthday he disappeared, bequeathing to his young cousin Frodo the Ruling Ring and a perilous quest: to journey across Middle-earth, deep into the shadow of the Dark Lord, and destroy the Ring by casting it into the Cracks of Doom.\n\nThe Lord of the Rings tells of the great quest undertaken by Frodo and the Fellowship of the Ring: Gandalf the Wizard; the hobbits Merry, Pippin, and Sam; Gimli the Dwarf; Legolas the Elf; Boromir of Gondor; and a tall, mysterious stranger called Strider. ','2020-11-01 15:14:03',NULL,0,20,2,1),(93,'Mahabharata','Vyasa',-700,'1604236465419_the-mahab-harata.jpg','The Mahābhārata (US: /məhɑːˈbɑːrətə/,[1] UK: /ˌmɑːhəˈbɑːrətə/;[2] Sanskrit: महाभारतम्, Mahābhāratam, pronounced [mɐɦaːˈbʱaːrɐtɐm]) is one of the two major Sanskrit epics of ancient India, the other being the Rāmāyaṇa.[3] It narrates the struggle between two groups of cousins in the Kurukshetra War and the fates of the Kaurava and the Pāṇḍava princes and their successors.\n\nIt also contains philosophical and devotional material, such as a discussion of the four \"goals of life\" or puruṣārtha (12.161). Among the principal works and stories in the Mahābhārata are the Bhagavad Gita, the story of Damayanti, story of Savitri and Satyavan, story of Kacha and Devyani, the story of Ṛṣyasringa and an abbreviated version of the Rāmāyaṇa, often considered as works in their own right.\n\n\nKrishna and Arjuna at Kurukshetra, 18th–19th-century painting\nTraditionally, the authorship of the Mahābhārata is attributed to Vyāsa. There have been many attempts to unravel its historical growth and compositional layers. The bulk of the Mahābhārata was probably compiled between the 3rd century BCE and the 3rd century CE, with the oldest preserved parts not much older than around 400 BCE.[4][5] The original events related by the epic probably fall between the 9th and 8th centuries BCE.[5] The text probably reached its final form by the early Gupta period (c. 4th century CE).[6][7]','2020-11-01 15:14:29',NULL,0,11,17,1),(94,'Leaves of Grass','Walt Whitman',1855,'1604236517099_leaves-of-grass.jpg','Leaves of Grass is a poetry collection by American poet Walt Whitman (1819–1892), each poem of which is loosely connected and represents the celebration of his philosophy of life and humanity. Though first published in 1855, Whitman spent most of his professional life writing and rewriting Leaves of Grass,[1] revising it multiple times until his death. This resulted in vastly different editions over four decades—the first edition being a small book of twelve poems, and the last, a compilation of over 400.\n\nInfluenced by Ralph Waldo Emerson and the Transcendentalist movement, itself an offshoot of Romanticism, Whitman\'s poetry praises nature and the individual human\'s role in it. Rather than relying on symbolism, allegory, and meditation on the religious and spiritual, like much of the poetry (especially English poetry) to come before it, Leaves of Grass (particularly the first edition) exalts the body and the material world instead. Much like Emerson, however, Whitman does not diminish the role of the mind or the spirit; rather, he elevates the human form and the human mind, deeming both worthy of poetic praise.\n\nWith one exception, its poems do not rhyme or follow standard rules for meter and line length. Among the works in this collection are \"Song of Myself\", \"I Sing the Body Electric\", and \"Out of the Cradle Endlessly Rocking\". Later editions would include Whitman\'s elegy to the assassinated President Abraham Lincoln, \"When Lilacs Last in the Dooryard Bloom\'d\".\n\nLeaves of Grass is notable for its discussion of delight in sensual pleasures during a time when such candid displays were considered immoral. Accordingly, the book was highly controversial during its time for its explicit sexual imagery, and Whitman was subject to derision by many contemporary critics. Over time, however, the collection has infiltrated popular culture and been recognized as one of the central works of American poetry.','2020-11-01 15:15:21',NULL,0,11,2,1),(95,'Mrs Dalloway','Virginia Woolf',1925,'1604236559820_mrs-dalloway.jpg','Mrs Dalloway (published on 14 May 1925[1]) is a novel by Virginia Woolf that details a day in the life of Clarissa Dalloway, a fictional high-society woman in post–First World War England. It is one of Woolf\'s best-known novels.\n\nCreated from two short stories, \"Mrs Dalloway in Bond Street\" and the unfinished \"The Prime Minister\", the novel addresses Clarissa\'s preparations for a party she will host that evening. With an interior perspective, the story travels forward and back in time and in and out of the characters\' minds to construct an image of Clarissa\'s life and of the inter-war social structure. In October 2005, Mrs Dalloway was included on Time\'s list of the 100 best English-language novels written since Time debuted in 1923.','2020-11-01 15:16:04',NULL,0,11,2,1),(96,'To the Lighthouse','Virginia Woolf',1927,'1604236604085_to-the-lighthouse.jpg','To the Lighthouse is a 1927 novel by Virginia Woolf. The novel centres on the Ramsay family and their visits to the Isle of Skye in Scotland between 1910 and 1920.\n\nFollowing and extending the tradition of modernist novelists like Marcel Proust and James Joyce, the plot of To the Lighthouse is secondary to its philosophical introspection. Cited as a key example of the literary technique of multiple focalization, the novel includes little dialogue and almost no action; most of it is written as thoughts and observations. The novel recalls childhood emotions and highlights adult relationships. Among the book\'s many tropes and themes are those of loss, subjectivity, the nature of art and the problem of perception.\n\nIn 1998, the Modern Library named To the Lighthouse No. 15 on its list of the 100 best English-language novels of the 20th century.[1] In 2005, the novel was chosen by TIME magazine as one of the one hundred best English-language novels since 1923','2020-11-01 15:16:48',NULL,0,11,2,1),(97,'The Stranger','Albert Camus',1942,'1604236781627_l-etranger.jpg','L\'Étranger (French: [l‿e.tʁɑ̃.ʒe]) is a 1942 novel by French author Albert Camus. Its theme and outlook are often cited as examples of Camus\'s philosophy, absurdism coupled with that of existentialism, though Camus personally rejected the latter label.[1]\n\nThe title character is Meursault, an indifferent French Algerian described as \"a citizen of France domiciled in North Africa, a man of the Mediterranean, an homme du midi yet one who hardly partakes of the traditional Mediterranean culture.\"[2] He attends his mother\'s funeral. A few days later, he kills an Arab man in French Algiers, who was involved in a conflict with one of Meursault\'s neighbors. Meursault is tried and sentenced to death. The story is divided into two parts, presenting Meursault\'s first-person narrative view before and after the murder, respectively.\n\nIn January 1955, Camus wrote:\n\nI summarized The Stranger a long time ago, with a remark I admit was highly paradoxical: \"In our society any man who does not weep at his mother\'s funeral runs the risk of being sentenced to death.\" I only meant that the hero of my book is condemned because he does not play the game.[3]\n\nThe Stranger\'s first edition consisted of only 4,400 copies, which was so few that it could not be a best-seller. Since it was published during the Nazi occupation of France, there was a possibility that the Propaganda-Staffel would censor it, but a representative of the Occupation authorities felt it contained nothing damaging to their cause, so it was published without omissions. However, the novel was well received in anti-Nazi circles in addition to Jean-Paul Sartre\'s article \"Explication de L\'Étranger\".[4]\n\nTranslated four times into English, and also into numerous other languages, the novel has long been considered a classic of 20th-century literature. Le Monde ranks it as number one on its 100 Books of the Century.','2020-11-01 15:19:45',NULL,0,11,4,1),(98,'Don Quijote De La Mancha','Miguel de Cervantes',1610,'1604236864191_don-quijote-de-la-mancha.jpg','The Ingenious Gentleman Don Quixote of La Mancha (Modern Spanish: El ingenioso hidalgo (in Part 2, caballero) Don Quijote de la Mancha, pronounced [el iŋxeˈnjoso iˈðalɣo ðoŋ kiˈxote ðe la ˈmantʃa] (About this soundlisten)), or just Don Quixote (/ˌdɒn kiːˈhoʊti/, US: /-teɪ/;[1] Spanish: [doŋ kiˈxote] (About this soundlisten)), is a Spanish novel by Miguel de Cervantes. It was published in two parts, in 1605 and 1615. A founding work of Western literature, it is often labeled \"the first modern novel\"[2][3] and many authors consider it to be the best literary work ever written.[4][5]\n\nThe plot revolves around the adventures of a noble (hidalgo) from La Mancha named Alonso Quixano, who reads so many chivalric romances that he loses his mind and decides to become a knight-errant (caballero andante) to revive chivalry and serve his nation, under the name Don Quixote de la Mancha. He recruits a simple farmer, Sancho Panza, as his squire, who often employs a unique, earthy wit in dealing with Don Quixote\'s rhetorical monologues on knighthood, already considered old-fashioned at the time. Don Quixote, in the first part of the book, does not see the world for what it is and prefers to imagine that he is living out a knightly story.\n\nThe book had a major influence on the literary community, as evidenced by direct references in Alexandre Dumas\' The Three Musketeers (1844), Mark Twain\'s Adventures of Huckleberry Finn (1884), and Edmond Rostand\'s Cyrano de Bergerac (1897), as well as the word quixotic and the epithet Lothario; the latter refers to a character in \"El curioso impertinente\" (\"The Impertinently Curious Man\"), an intercalated story that appears in Part One, chapters 33–35. The 19th-century German philosopher Arthur Schopenhauer cited Don Quixote as one of the four greatest novels ever written.[6]','2020-11-01 15:21:08',NULL,0,11,5,2),(99,'The Canterbury Tales','Geoffrey Chaucer',1450,'1604236910673_the-canterbury-tales.jpg','The Canterbury Tales (Middle English: Tales of Caunterbury[2]) is a collection of 24 stories that runs to over 17,000 lines written in Middle English by Geoffrey Chaucer between 1387 and 1400.[3] In 1386, Chaucer became Controller of Customs and Justice of Peace and, in 1389, Clerk of the King\'s work.[4] It was during these years that Chaucer began working on his most famous text, The Canterbury Tales. The tales (mostly written in verse, although some are in prose) are presented as part of a story-telling contest by a group of pilgrims as they travel together from London to Canterbury to visit the shrine of Saint Thomas Becket at Canterbury Cathedral. The prize for this contest is a free meal at the Tabard Inn at Southwark on their return.\n\nAfter a long list of works written earlier in his career, including Troilus and Criseyde, House of Fame, and Parliament of Fowls, The Canterbury Tales is near-unanimously seen as Chaucer\'s magnum opus. He uses the tales and descriptions of its characters to paint an ironic and critical portrait of English society at the time, and particularly of the Church. Chaucer\'s use of such a wide range of classes and types of people was without precedent in English. Although the characters are fictional, they still offer a variety of insights into customs and practices of the time. Often, such insight leads to a variety of discussions and disagreements among people in the 14th century. For example, although various social classes are represented in these stories and all of the pilgrims are on a spiritual quest, it is apparent that they are more concerned with worldly things than spiritual. Structurally, the collection resembles Boccaccio\'s Decameron, which Chaucer may have read during his first diplomatic mission to Italy in 1372.','2020-11-01 15:21:55',NULL,0,11,2,1),(100,'Stories','Anton Chekhov',1886,'1604236953100_stories-of-anton-chekhov.jpg','Anton Chekhov was a Russian playwright and short-story writer who is considered to be among the greatest writers of short fiction in history. He wrote hundreds of short stories, one novel, and seven full-length plays.','2020-11-01 15:22:37',NULL,1,11,6,3),(101,'Uncle Ganio','Aleko Konstantinov',1895,'1604237014857_1652.jpg','The collection of feuilletons consists of two parts. Each feuilleton is presented by a different narrator, a member of a group of like-minded Bulgarians telling of Bay Ganyo\'s adventures; this group of narrators is itself a direct opposition to Bay Ganyo: they are educated, courteous and civilized people with European manners and clothing.','2020-11-01 15:23:32',NULL,0,27,1,1),(102,'Gordon Ramsay\'s Ultimate Fit Food','Gordon Ramsay',2018,'1604237162905_51MnAt5BuWL.jpg','\'These are my go-to recipes when I want to eat well at home. My great hope is that they will inspire you to get cooking to improve your own health whatever your personal goal.\'\nGORDON RAMSAY\n\nThe dream combination - a Michelin-starred superchef who is also a committed athlete. Gordon knows how important it is to eat well, whether you\'re training for a triathlon or just leading a busy active life. And just because it\'s healthy food you don\'t have to compromise on taste and flavour.\n\nThe book is divided into three sections, each one offering breakfasts, lunches, suppers, sides and snacks with different health-boosting benefits. The Healthy section consists of nourishing recipes for general wellbeing; the Lean recipes encourage healthy weight loss; and the Fit section features pre- and post-workout dishes to build strength and energise.\n\nThis is the ultimate collection of recipes that you\'ll enjoy cooking and eating, and will leave you in great shape whatever your fitness goals.','2020-11-01 15:26:00','2020-11-01 18:10:39',0,23,2,2),(103,'The Jungle Book','John Lockwood Kipling',1894,'1604237372873_JunglebookCover.jpg','The Jungle Book key characters are Mowgli, a boy raised by wolves and Sher Khan, biggest tiger in India. As Baloo the sleepy brown bear, Bagheera the cunning black panther, Kaa the python, and his other animal friends teach their beloved “man-cub” the ways of the jungle, Mowgli gains the strength and wisdom he needs for his frightful fight with Shere Khan, the tiger who robbed him of his human family. But there are also the tales of Rikki-tikki-tavi the mongoose and his “great war” against the vicious cobras Nag and Nagaina; of Toomai, who watches the elephants dance; and of Kotick the white seal, who swims in the Bering Sea.','2020-11-01 15:29:30',NULL,0,11,2,1),(104,'The Hound of the Baskervilles','Arthur Doyle',1902,'1604238125065_330px-Cover_(Hound_of_Baskervilles,_1902).jpg','We owe The Hound of the Baskervilles (1902) to Arthur Conan Doyle\'s good friend Fletcher \"Bobbles\" Robinson, who took him to visit some scary English moors and prehistoric ruins, and told him marvelous local legends about escaped prisoners and a 17th-century aristocrat who fell afoul of the family dog. Doyle transmogrified the legend: generations ago, a hound of hell tore out the throat of devilish Hugo Baskerville on the moonlit moor. Poor, accursed Baskerville Hall now has another mysterious death: that of Sir Charles Baskerville. Could the culprit somehow be mixed up with secretive servant Barrymore, history-obsessed Dr. Frankland, butterfly-chasing Stapleton, or Selden, the Notting Hill murderer at large? Someone\'s been signaling with candles from the mansion\'s windows. Nor can supernatural forces be ruled out. Can Dr. Watson--left alone by Sherlock Holmes to sleuth in fear for much of the novel--save the next Baskerville, Sir Henry, from the hound\'s fangs?\nMany Holmes fans prefer Doyle\'s complete short stories, but their clockwork logic doesn\'t match the author\'s boast about this novel: it\'s \"a real Creeper!\" What distinguishes this particular Hound is its fulfillment of Doyle\'s great debt to Edgar Allan Poe--it\'s full of ancient woe, low moans, a Grimpen Mire that sucks ponies to Dostoyevskian deaths, and locals digging up Neolithic skulls without next-of-kins\' consent. \"The longer one stays here the more does the spirit of the moor sink into one\'s soul,\" Watson realizes. \"Rank reeds and lush, slimy water-plants sent an odour of decay ... while a false step plunged us more than once thigh-deep into the dark, quivering mire, which shook for yards in soft undulations around our feet ... it was as if some malignant hand was tugging us down into those obscene depths.\" Read on--but, reader, watch your step! --Tim Appelo','2020-11-01 15:42:02',NULL,0,17,2,1),(105,'Mozart','Robert Gutman',1999,'1604238349522_isdasdasdmages.jpg','This major work, the result of years of careful study and analysis, places Wolfgang Amadeus Mozart\'s life and music in the context of the intellectual, political and artistic currents of eighteenth-century Europe. The result is a fresh interpretation of Mozart\'s genius, as Robert Gutman shows the great composer in a new light. With an informed and sensitive handling, Mozart emerges as an affectionate and generous man with family and friends, self-deprecating, witty, and winsome but also an austere moralist, incisive and purposeful. The major genres in which Mozart worked-chamber music, liturgical, theater and keyboard compositions, concertos, operas, symphonies, and oratorios-are unfolded to reveal a man of luminous intellect. Mozart is an extraordinary portrait of a man and his times and a brilliant distillation of musical thought.','2020-11-01 15:45:47',NULL,0,3,2,1),(106,'Enemy at The Gates','William Craig',1973,'1604247943835_enemy-at-the-gates-1.jpg','Stalingrad, the bloodiest battle in the history of warfare, cost the lives of nearly two million men and women. It signaled the beginning of the end for the Third Reich of Adolf Hitler; it foretold the Russian juggernaut that would destroy Berlin and make the Soviet Union a superpower. As Winston Churchill characterized the result of the conflict at Stalingrad: \" the hinge of fate had turned.\"\n\nWilliam Craig, author and historian, has painstakingly recreated the details of this great battle: from the hot summer of August 1942, when the German armies smashed their way across southern Russia toward the Volga River, through the struggle for Stalingrad-a city Hitler had never meant to capture and Stalin never meant to defend-on to the destruction of the supposedly invincible German Sixth Army and the terror of the Russian prison camps in frozen Siberia. Craig has interviewed hundreds of survivors of the battle-both Russian and German soldiers and civilians-and has woven their incredible experiences into the fabric of hitherto unknown documents. The resulting mosaic is epic in scope, and the human tragedy that unfolds is awesome.','2020-11-01 18:25:41',NULL,0,5,2,1);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrows`
--

DROP TABLE IF EXISTS `borrows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `borrows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_date` datetime DEFAULT current_timestamp(),
  `to_date` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_borrows_users1_idx` (`user_id`),
  KEY `fk_borrows_books1_idx` (`book_id`),
  CONSTRAINT `fk_borrows_books1` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_borrows_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrows`
--

LOCK TABLES `borrows` WRITE;
/*!40000 ALTER TABLE `borrows` DISABLE KEYS */;
INSERT INTO `borrows` VALUES (2,'2020-10-09 14:55:10','2020-10-09 15:06:31',3,3),(3,'2020-10-09 15:22:24','2020-10-09 15:22:32',2,3),(4,'2020-10-09 15:26:38','2020-10-09 15:34:15',2,3),(5,'2020-10-09 15:34:27','2020-10-09 15:55:26',3,3),(6,'2020-10-09 15:54:27','2020-10-09 15:55:03',3,1),(7,'2020-10-09 17:39:05','2020-10-09 17:40:14',3,10),(8,'2020-10-09 19:45:35','2020-10-09 19:46:35',4,4),(9,'2020-10-09 20:47:07','2020-10-09 20:48:18',4,1),(10,'2020-10-09 21:28:00','2020-10-09 21:28:21',5,1),(11,'2020-10-17 16:56:01','2020-10-17 16:56:17',1,1),(12,'2020-10-17 16:57:08','2020-10-17 16:57:12',1,1),(13,'2020-10-26 00:15:37','2020-10-26 00:16:24',4,10),(14,'2020-10-26 00:18:48','2020-10-26 00:18:49',4,10),(15,'2020-10-26 00:18:51','2020-10-26 00:18:52',4,10),(16,'2020-10-26 00:18:53','2020-10-26 00:18:55',4,10),(17,'2020-10-26 00:24:51','2020-10-26 00:27:23',4,10),(18,'2020-10-26 00:27:23','2020-10-26 00:28:20',4,10),(19,'2020-10-26 00:28:29','2020-10-26 00:28:31',4,10),(20,'2020-10-26 00:29:10','2020-10-26 00:29:11',4,10),(21,'2020-10-26 00:29:14','2020-10-26 00:29:14',4,10),(22,'2020-10-26 00:29:16','2020-10-26 00:29:18',4,10),(23,'2020-10-26 00:46:08','2020-10-26 00:46:22',4,17),(24,'2020-10-26 02:43:57','2020-10-26 02:57:21',4,10),(25,'2020-10-26 04:16:29','2020-10-26 04:16:33',4,22),(26,'2020-10-26 12:24:11','2020-10-26 12:24:27',1,20),(27,'2020-10-26 12:28:01',NULL,1,20),(28,'2020-10-26 12:28:23',NULL,1,16),(29,'2020-10-26 15:03:53','2020-10-26 15:04:06',4,10),(30,'2020-10-26 15:32:27','2020-10-26 15:32:30',4,1),(31,'2020-10-26 15:33:40','2020-10-26 15:33:54',4,1),(32,'2020-10-26 18:04:54','2020-10-26 18:04:56',4,10),(33,'2020-10-26 18:41:03','2020-10-26 18:42:25',1,22),(34,'2020-10-26 18:45:01','2020-10-26 18:45:02',4,10),(35,'2020-10-26 19:44:28','2020-10-26 19:44:42',4,17),(36,'2020-10-26 19:45:54','2020-10-26 19:45:56',4,17),(37,'2020-10-26 19:46:56','2020-10-26 19:46:59',4,17),(38,'2020-10-26 19:58:13','2020-10-26 19:58:16',4,17),(39,'2020-10-26 20:07:25','2020-10-26 20:14:38',1,17),(40,'2020-10-26 20:16:35','2020-10-26 20:16:37',4,11),(41,'2020-10-26 20:17:59','2020-10-26 20:18:04',4,15),(42,'2020-10-26 22:31:21','2020-10-26 22:31:25',4,10),(43,'2020-10-26 22:36:21','2020-10-26 22:36:25',4,10),(44,'2020-10-26 22:36:44','2020-10-26 22:36:47',4,10),(45,'2020-10-26 22:39:12','2020-10-26 22:39:15',4,10),(46,'2020-10-26 22:40:55','2020-10-26 22:41:12',4,10),(47,'2020-10-26 22:41:42','2020-10-26 22:41:44',4,10),(48,'2020-10-26 22:41:48','2020-10-26 22:41:51',4,10),(49,'2020-10-26 22:46:13','2020-10-26 22:46:15',4,10),(50,'2020-10-26 22:48:21','2020-10-26 22:48:23',4,10),(51,'2020-10-26 22:49:02','2020-10-26 22:49:10',4,10),(52,'2020-10-26 22:49:25','2020-10-26 22:50:01',4,10),(53,'2020-10-26 22:50:02','2020-10-26 22:56:07',4,10),(54,'2020-10-26 22:56:10','2020-10-26 22:56:12',4,10),(55,'2020-10-26 22:56:21','2020-10-26 22:56:23',4,10),(56,'2020-10-26 22:57:23','2020-10-26 22:57:27',4,10),(57,'2020-10-26 22:59:18','2020-10-26 22:59:20',4,10),(58,'2020-10-26 22:59:24','2020-10-26 22:59:27',4,10),(59,'2020-10-26 23:01:53','2020-10-26 23:01:56',4,10),(60,'2020-10-26 23:01:59','2020-10-26 23:02:01',4,10),(61,'2020-10-26 23:03:24','2020-10-26 23:03:25',4,10),(62,'2020-10-26 23:03:28','2020-10-26 23:03:32',4,10),(63,'2020-10-26 23:04:01','2020-10-26 23:04:06',4,22),(64,'2020-10-27 01:38:43','2020-10-27 01:38:48',4,1),(65,'2020-10-27 11:16:20','2020-10-27 11:16:24',1,11),(66,'2020-10-27 11:21:38','2020-10-27 11:29:43',1,11),(67,'2020-10-27 11:30:58','2020-10-28 11:51:07',1,17),(68,'2020-10-27 11:34:04','2020-10-27 11:34:06',4,15),(69,'2020-10-27 11:47:32','2020-10-27 11:47:33',4,15),(70,'2020-10-27 11:48:48','2020-10-27 11:48:50',4,4),(71,'2020-10-27 13:12:04','2020-10-27 13:12:07',1,4),(72,'2020-10-27 14:27:25',NULL,4,4),(73,'2020-10-27 14:27:25',NULL,4,4),(74,'2020-10-27 14:27:25',NULL,4,4),(75,'2020-10-27 14:27:25',NULL,4,4),(76,'2020-10-27 14:27:25',NULL,4,4),(77,'2020-10-27 17:53:54','2020-10-27 17:54:58',4,11),(78,'2020-10-27 18:10:28','2020-10-27 18:10:30',4,11),(79,'2020-10-27 18:11:29','2020-10-27 18:11:30',4,11),(80,'2020-10-27 18:13:47','2020-10-27 18:13:49',4,11),(81,'2020-10-27 18:14:26','2020-10-27 18:14:27',4,11),(82,'2020-10-27 18:14:36','2020-10-27 18:14:39',4,11),(83,'2020-10-27 18:17:39','2020-10-27 18:25:21',4,11),(84,'2020-10-27 18:25:54','2020-10-27 18:26:23',4,11),(85,'2020-10-27 18:26:25','2020-10-27 18:26:26',4,11),(86,'2020-10-27 18:27:45','2020-10-27 18:27:47',4,11),(87,'2020-10-27 18:27:58','2020-10-27 18:28:01',4,11),(88,'2020-10-27 18:39:57','2020-10-27 18:39:59',4,11),(89,'2020-10-27 18:40:22','2020-10-27 18:40:23',4,11),(90,'2020-10-27 18:41:13','2020-10-27 18:41:16',4,11),(91,'2020-10-27 18:42:06','2020-10-27 18:42:08',4,11),(92,'2020-10-27 18:42:18','2020-10-27 18:42:21',4,11),(93,'2020-10-27 18:43:48','2020-10-27 18:43:50',4,11),(94,'2020-10-27 18:44:38','2020-10-27 18:44:41',4,11),(95,'2020-10-27 18:45:05','2020-10-27 18:45:07',4,11),(96,'2020-10-27 18:45:53','2020-10-27 18:45:56',4,11),(97,'2020-10-27 18:46:49','2020-10-27 18:46:51',4,11),(98,'2020-10-27 18:47:17','2020-10-27 18:47:18',4,11),(99,'2020-10-27 18:48:31','2020-10-27 18:48:33',4,11),(100,'2020-10-27 19:03:30','2020-10-27 19:03:32',4,11),(101,'2020-10-27 19:03:47','2020-10-27 19:03:58',4,11),(102,'2020-10-27 19:18:35','2020-10-27 19:18:37',4,11),(103,'2020-10-27 21:12:05','2020-10-27 21:12:08',1,22),(104,'2020-10-27 21:21:01','2020-10-27 21:21:04',1,22),(105,'2020-10-27 22:15:12','2020-10-27 22:15:14',4,11),(106,'2020-10-27 22:17:10','2020-10-27 22:17:14',4,11),(107,'2020-10-27 23:38:15','2020-10-27 23:38:17',4,11),(108,'2020-10-27 23:38:26','2020-10-27 23:38:28',4,11),(109,'2020-10-27 23:39:19','2020-10-27 23:39:20',4,11),(110,'2020-10-27 23:41:56','2020-10-27 23:41:57',4,11),(111,'2020-10-28 00:01:14','2020-10-28 00:01:16',4,11),(112,'2020-10-28 00:18:00','2020-10-28 00:18:01',4,11),(113,'2020-10-28 00:19:19','2020-10-28 00:19:20',4,11),(114,'2020-10-28 00:20:53','2020-10-28 00:20:55',4,11),(115,'2020-10-28 00:29:12','2020-10-28 00:29:13',4,11),(116,'2020-10-28 00:40:09','2020-10-28 00:40:11',4,11),(117,'2020-10-28 01:29:01','2020-10-28 01:29:03',4,11),(118,'2020-10-28 01:48:25','2020-10-28 01:48:27',4,11),(119,'2020-10-28 02:19:12','2020-10-28 02:19:13',4,10),(120,'2020-10-28 02:21:22','2020-10-28 02:21:24',4,10),(121,'2020-10-28 02:26:26','2020-10-28 02:26:30',4,10),(122,'2020-10-28 03:17:57','2020-10-28 03:18:06',4,10),(123,'2020-10-28 11:48:58','2020-10-28 11:49:00',65,1),(124,'2020-10-28 11:49:05','2020-10-28 11:49:07',4,10),(125,'2020-10-28 11:49:09','2020-10-28 11:49:12',4,10),(126,'2020-10-28 11:51:09','2020-10-28 11:51:36',1,17),(127,'2020-10-28 11:51:55','2020-10-28 11:51:57',65,17),(128,'2020-10-28 12:27:29','2020-10-28 12:27:31',4,17),(129,'2020-10-28 12:30:47','2020-10-28 12:31:00',65,22),(130,'2020-10-28 12:31:07',NULL,65,22),(131,'2020-10-28 12:31:11','2020-10-28 12:31:13',4,17),(132,'2020-10-28 12:31:33','2020-10-28 12:31:35',65,11),(133,'2020-10-28 12:31:54','2020-10-28 12:31:55',4,17),(134,'2020-10-28 12:32:21','2020-10-28 12:32:23',4,17),(135,'2020-10-28 12:32:42','2020-10-28 12:32:44',4,17),(136,'2020-10-28 12:33:00','2020-10-28 12:33:02',4,17),(137,'2020-10-28 12:33:19','2020-10-28 12:33:21',65,11),(138,'2020-10-28 12:33:35','2020-10-28 12:33:36',4,17),(139,'2020-10-28 12:34:05','2020-10-28 12:34:06',4,17),(140,'2020-10-28 12:36:47','2020-10-28 12:36:48',4,17),(141,'2020-10-28 12:36:51','2020-10-28 12:36:53',4,17),(142,'2020-10-28 12:43:19','2020-10-28 12:43:34',4,17),(143,'2020-10-28 12:44:11','2020-10-28 12:44:14',4,17),(144,'2020-10-28 12:44:39','2020-10-28 12:44:48',4,17),(145,'2020-10-28 12:47:34','2020-10-28 12:49:07',4,17),(146,'2020-10-28 12:49:11','2020-10-28 12:52:03',4,17),(147,'2020-10-28 12:52:20','2020-10-28 12:52:24',4,17),(148,'2020-10-28 13:17:03','2020-10-28 13:18:39',4,17),(149,'2020-10-28 13:18:49','2020-10-28 13:18:59',4,17),(150,'2020-10-28 13:19:55','2020-10-28 13:24:59',4,17),(151,'2020-10-28 13:25:00','2020-10-28 13:30:28',4,17),(152,'2020-10-28 13:52:33','2020-10-28 13:55:51',4,17),(153,'2020-10-28 13:57:04','2020-10-28 13:57:16',4,17),(154,'2020-10-28 13:57:48','2020-10-28 13:57:52',4,17),(155,'2020-10-28 13:58:49','2020-10-28 13:58:52',4,17),(156,'2020-10-28 14:00:26','2020-10-28 14:03:42',66,17),(157,'2020-10-28 14:03:52','2020-10-28 14:03:59',66,17),(158,'2020-10-28 14:45:35','2020-10-28 14:47:25',4,17),(159,'2020-10-28 14:47:30','2020-10-28 14:47:44',4,17),(160,'2020-10-28 14:50:28','2020-10-28 14:51:40',4,17),(161,'2020-10-28 14:51:45','2020-10-28 15:12:55',4,17),(162,'2020-10-28 16:42:42','2020-10-28 16:42:46',2,1),(163,'2020-10-28 17:58:53','2020-10-28 17:58:55',4,17),(164,'2020-10-28 18:33:31','2020-10-28 18:33:33',4,17),(165,'2020-10-28 18:33:57','2020-10-28 18:33:58',4,10),(166,'2020-10-28 18:34:12','2020-10-28 18:34:17',4,3),(167,'2020-10-28 19:19:32','2020-10-28 19:19:35',1,6),(168,'2020-10-28 20:07:54','2020-10-28 20:20:05',4,15),(169,'2020-10-28 20:20:07','2020-10-28 20:20:37',4,15),(170,'2020-10-28 20:20:44','2020-10-28 20:20:53',4,15),(171,'2020-10-28 20:21:34','2020-10-28 20:24:17',4,15),(172,'2020-10-28 20:28:05','2020-10-28 20:28:06',4,15),(173,'2020-10-28 21:13:36','2020-10-28 21:13:38',4,11),(174,'2020-10-28 21:14:03','2020-10-28 21:14:05',4,11),(175,'2020-10-28 21:14:07','2020-10-28 21:16:21',4,11),(176,'2020-10-28 21:16:26','2020-10-28 21:16:29',4,11),(177,'2020-10-28 21:16:34','2020-10-28 21:16:36',4,15),(178,'2020-10-28 21:16:39','2020-10-28 21:16:55',4,15),(179,'2020-10-28 21:16:50','2020-10-28 21:16:52',4,11),(180,'2020-10-28 21:17:53','2020-10-28 21:18:07',4,11),(181,'2020-10-28 21:19:25','2020-10-28 21:19:30',4,11),(182,'2020-10-28 21:29:08','2020-10-28 21:29:19',4,11),(183,'2020-10-28 21:29:36','2020-10-28 21:29:50',4,10),(184,'2020-10-28 21:29:41','2020-10-28 21:29:55',4,17),(185,'2020-10-28 21:30:57','2020-10-28 21:30:59',4,17),(186,'2020-10-28 21:31:17','2020-10-28 21:31:23',4,10),(187,'2020-10-28 21:32:37','2020-10-28 21:49:02',4,11),(188,'2020-10-28 22:26:40','2020-10-28 22:26:42',4,11),(189,'2020-10-29 16:19:43','2020-10-29 16:19:47',4,57),(190,'2020-10-29 19:25:06','2020-10-29 19:25:08',5,15),(191,'2020-10-29 19:29:19','2020-10-29 19:29:21',5,40),(192,'2020-10-29 19:30:51','2020-10-29 19:30:54',5,44),(193,'2020-10-30 23:36:07','2020-10-30 23:36:16',1,10),(194,'2020-10-31 16:18:45','2020-10-31 16:20:08',4,55),(195,'2020-10-31 16:20:11','2020-10-31 16:20:38',4,55),(196,'2020-10-31 16:20:39','2020-10-31 16:21:21',4,55),(197,'2020-10-31 16:21:22','2020-10-31 16:22:01',4,55),(198,'2020-10-31 16:23:51','2020-10-31 16:23:53',4,59),(199,'2020-10-31 16:24:56','2020-10-31 16:24:58',4,40),(200,'2020-10-31 16:40:43','2020-10-31 16:40:44',4,54),(201,'2020-10-31 16:43:49','2020-10-31 16:43:51',4,37),(202,'2020-10-31 16:45:11','2020-10-31 16:45:13',4,42),(203,'2020-10-31 16:49:43','2020-10-31 16:49:47',4,26),(204,'2020-10-31 16:51:31','2020-10-31 16:51:33',4,2),(205,'2020-10-31 16:52:28','2020-10-31 16:52:33',4,34),(206,'2020-10-31 16:53:31','2020-10-31 16:53:33',4,28),(207,'2020-10-31 16:56:54','2020-10-31 16:57:15',4,3),(208,'2020-10-31 17:06:03','2020-10-31 17:06:05',4,30),(209,'2020-10-31 17:22:09',NULL,4,56),(210,'2020-10-31 17:37:17','2020-10-31 17:38:31',4,15),(211,'2020-10-31 17:38:35','2020-10-31 17:38:38',4,15),(212,'2020-10-31 17:40:33','2020-10-31 17:40:36',4,14),(213,'2020-10-31 17:41:29','2020-10-31 17:41:34',4,14),(214,'2020-10-31 18:06:56','2020-10-31 19:11:46',28,59),(215,'2020-10-31 18:36:29','2020-10-31 18:41:48',4,10),(216,'2020-10-31 18:44:58','2020-10-31 19:09:47',4,10),(217,'2020-10-31 19:04:35','2020-10-31 19:09:29',4,44),(218,'2020-10-31 19:08:32','2020-10-31 19:09:41',4,57),(219,'2020-10-31 19:09:33','2020-10-31 19:09:35',4,44),(220,'2020-10-31 19:12:54','2020-10-31 19:14:09',4,44),(221,'2020-10-31 19:14:17','2020-10-31 19:14:22',4,44),(222,'2020-10-31 19:14:35','2020-10-31 19:14:40',4,55),(223,'2020-10-31 19:15:43','2020-10-31 19:15:45',4,59),(224,'2020-10-31 19:15:51','2020-10-31 19:15:53',4,44),(225,'2020-10-31 19:17:00','2020-10-31 19:18:46',28,44),(226,'2020-10-31 20:58:22','2020-10-31 20:58:29',4,59),(227,'2020-10-31 21:24:31','2020-10-31 21:24:36',4,31),(228,'2020-11-01 00:27:57','2020-11-01 00:28:05',4,59),(229,'2020-11-01 11:31:58','2020-11-01 11:32:02',5,33),(230,'2020-11-01 13:18:21','2020-11-01 13:18:29',4,58),(231,'2020-11-01 15:39:42','2020-11-01 15:40:58',83,57),(232,'2020-11-01 15:45:46','2020-11-01 15:46:25',83,15),(233,'2020-11-01 15:49:43','2020-11-01 15:49:52',83,44),(234,'2020-11-01 15:51:55','2020-11-01 15:51:59',84,57),(235,'2020-11-01 15:54:01','2020-11-01 15:54:07',83,101),(236,'2020-11-01 15:55:29','2020-11-01 15:55:34',83,102),(237,'2020-11-01 16:00:05','2020-11-01 16:00:08',83,105),(238,'2020-11-01 16:04:27','2020-11-01 16:04:31',83,53),(239,'2020-11-01 16:07:45','2020-11-01 16:07:48',83,58),(240,'2020-11-01 16:10:57','2020-11-01 16:10:59',84,65),(241,'2020-11-01 16:11:07','2020-11-01 16:11:14',83,1),(242,'2020-11-01 16:15:45','2020-11-01 16:15:48',84,103),(243,'2020-11-01 16:19:37','2020-11-01 16:19:39',84,98),(244,'2020-11-01 16:20:39','2020-11-01 16:20:41',84,104),(245,'2020-11-01 16:28:41','2020-11-01 16:28:43',85,104),(246,'2020-11-01 16:32:21','2020-11-01 16:32:22',84,77),(247,'2020-11-01 17:06:57','2020-11-01 17:06:58',84,53),(248,'2020-11-01 18:12:35','2020-11-01 18:12:41',74,101),(249,'2020-11-01 18:13:31','2020-11-01 18:13:32',4,76),(250,'2020-11-01 18:14:01','2020-11-01 18:14:10',84,76),(251,'2020-11-01 18:15:28','2020-11-01 18:15:30',85,76),(252,'2020-11-01 18:16:01','2020-11-01 18:16:09',74,88),(253,'2020-11-01 18:16:48','2020-11-01 18:16:52',74,76),(254,'2020-11-01 18:18:05',NULL,74,98),(255,'2020-11-01 18:24:08',NULL,5,53),(256,'2020-11-01 18:24:25',NULL,5,102),(257,'2020-11-01 18:26:22','2020-11-01 18:26:28',5,88),(258,'2020-11-01 18:27:42','2020-11-01 18:27:43',85,106),(259,'2020-11-01 18:31:02','2020-11-01 18:31:07',5,106),(260,'2020-11-01 18:34:42','2020-11-01 18:37:33',5,106),(261,'2020-11-01 18:54:12','2020-11-01 18:54:14',85,65),(262,'2020-11-01 19:07:04','2020-11-01 19:07:06',85,19),(263,'2020-11-01 19:11:33','2020-11-01 19:11:36',5,101),(264,'2020-11-01 19:12:01','2020-11-01 19:12:02',5,104),(265,'2020-11-01 19:12:46','2020-11-01 19:12:48',85,105),(266,'2020-11-01 19:12:58','2020-11-01 19:13:00',85,77),(267,'2020-11-01 19:13:15','2020-11-01 19:13:18',85,88),(268,'2020-11-01 19:14:49','2020-11-01 19:14:51',85,33),(269,'2020-11-01 19:14:56','2020-11-01 19:14:57',85,55),(270,'2020-11-01 19:15:02','2020-11-01 19:15:03',85,28),(271,'2020-11-01 19:15:13','2020-11-01 19:15:15',85,58),(272,'2020-11-01 19:15:42','2020-11-01 19:15:43',85,84),(273,'2020-11-01 19:15:56','2020-11-01 19:15:58',85,89),(274,'2020-11-01 19:16:28','2020-11-01 19:16:29',85,92),(275,'2020-11-01 19:16:55','2020-11-01 19:16:56',85,24),(276,'2020-11-01 19:17:27','2020-11-01 19:17:29',84,19),(277,'2020-11-01 19:19:07','2020-11-01 19:19:09',84,84),(278,'2020-11-01 19:19:18','2020-11-01 19:19:19',84,89),(279,'2020-11-01 19:19:46','2020-11-01 19:19:47',84,12),(280,'2020-11-01 19:25:25','2020-11-01 19:25:26',5,105),(281,'2020-11-01 19:26:15','2020-11-01 19:26:16',5,12),(282,'2020-11-01 19:27:41','2020-11-01 19:27:43',5,66),(283,'2020-11-01 19:31:51','2020-11-01 19:31:53',1,30),(284,'2020-11-01 19:33:42','2020-11-01 19:33:44',84,105),(285,'2020-11-01 19:34:12','2020-11-01 19:34:14',84,64),(286,'2020-11-01 19:44:26','2020-11-01 19:44:28',5,64),(287,'2020-11-01 19:45:11','2020-11-01 19:45:13',85,57),(288,'2020-11-01 19:47:13','2020-11-01 19:47:15',84,101),(289,'2020-11-01 19:47:37','2020-11-01 19:47:39',84,55),(290,'2020-11-01 19:47:57','2020-11-01 19:47:59',84,106),(291,'2020-11-01 19:48:11','2020-11-01 19:48:13',84,41),(292,'2020-11-01 19:48:30','2020-11-01 19:48:31',84,47),(293,'2020-11-01 19:52:02','2020-11-01 19:52:04',84,44),(294,'2020-11-01 19:54:14','2020-11-01 19:54:16',84,83),(295,'2020-11-01 19:54:31','2020-11-01 19:54:33',84,96),(296,'2020-11-01 19:54:47','2020-11-01 19:54:48',84,97),(297,'2020-11-01 19:55:04','2020-11-01 19:55:06',84,49),(298,'2020-11-01 19:55:18','2020-11-01 19:55:20',84,68),(299,'2020-11-01 19:55:28','2020-11-01 19:55:30',84,65),(300,'2020-11-01 20:35:24','2020-11-01 20:35:26',85,23),(301,'2020-11-01 20:47:08','2020-11-01 20:47:09',2,12),(302,'2020-11-01 20:47:31','2020-11-01 20:47:32',2,106),(303,'2020-11-01 20:53:30','2020-11-01 20:53:32',76,12),(304,'2020-11-01 21:29:23','2020-11-01 21:29:24',88,12),(305,'2020-11-01 22:13:33','2020-11-01 22:13:35',85,12),(306,'2020-11-02 00:03:21','2020-11-02 00:03:23',2,2),(307,'2020-11-02 00:54:09','2020-11-02 00:54:11',2,6),(308,'2020-11-02 01:05:07','2020-11-02 01:05:09',84,84),(309,'2020-11-02 10:07:50','2020-11-02 10:09:00',89,76),(310,'2020-11-02 10:13:59','2020-11-02 10:19:25',89,28),(311,'2020-11-02 10:14:06',NULL,89,31),(312,'2020-11-02 10:14:11',NULL,89,29),(313,'2020-11-02 10:14:16','2020-11-02 10:14:25',89,30),(314,'2020-11-02 10:30:42','2020-11-02 10:30:44',90,101),(315,'2020-11-02 11:08:54','2020-11-02 11:09:45',91,26),(316,'2020-11-02 11:28:23','2020-11-02 11:28:28',1,101),(317,'2020-11-02 12:24:44','2020-11-02 12:24:57',92,2),(318,'2020-11-02 12:27:49','2020-11-02 12:27:51',92,80),(319,'2020-11-02 12:29:27','2020-11-02 12:29:31',93,88),(320,'2020-11-02 12:31:40','2020-11-02 12:31:42',93,55),(321,'2020-11-02 12:42:34','2020-11-02 12:42:45',94,92),(322,'2020-11-02 13:33:03',NULL,95,54),(323,'2020-11-02 13:33:12','2020-11-02 13:33:18',95,55),(324,'2020-11-02 14:47:22','2020-11-02 14:47:25',92,28),(325,'2020-11-02 19:41:13','2020-11-02 19:41:16',89,24),(326,'2020-11-10 20:55:22','2020-11-10 20:55:31',5,76);
/*!40000 ALTER TABLE `borrows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL,
  `edit_reason` tinytext DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `review_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_reviews1_idx` (`review_id`),
  KEY `fk_comments_users1_idx` (`user_id`),
  CONSTRAINT `fk_comments_reviews1` FOREIGN KEY (`review_id`) REFERENCES `reviews` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'I hate you','2020-10-09 15:18:08','2020-10-09 15:19:19','i hate the user',0,3,2),(2,'Actualy not very much','2020-10-09 15:18:37',NULL,NULL,1,3,2),(3,'HAHAHAH','2020-10-09 15:20:52',NULL,NULL,0,3,2),(4,'FUNNY','2020-10-09 16:02:26',NULL,NULL,0,3,2),(5,'Interisting','2020-10-09 16:02:57',NULL,NULL,1,4,2),(6,'I mean nice review','2020-10-09 18:02:32','2020-10-09 18:03:24','was refering to book...',0,4,3),(7,'Nice review man!','2020-10-09 18:06:16',NULL,NULL,0,4,3),(8,'I agree - pretty good review','2020-10-09 18:39:55',NULL,NULL,0,4,3),(9,'NOT cool','2020-10-09 21:56:03','2020-10-09 21:56:13','nope not cool',1,9,5),(10,'Very nice but can you fix your grammar?','2020-10-09 22:58:28',NULL,NULL,0,9,5),(11,'New Comment','2020-10-25 22:22:22',NULL,NULL,0,9,2),(12,'Wow, Ok!','2020-10-25 22:22:22',NULL,NULL,0,7,2),(13,'Are you kidding, lifestyle. This book is amazing!! Simply put.','2020-10-28 22:22:42','2020-10-28 22:32:07',NULL,0,20,4),(14,'HEHE!!!!!!!','2020-10-28 22:42:39','2020-10-28 23:12:05','!!!!!',0,20,4),(15,'I like the weather today\n','2020-10-28 22:57:59',NULL,NULL,0,20,4),(16,'This is a cool comment bro','2020-10-28 23:16:01',NULL,NULL,1,5,4),(17,'Cool, I like your profile picture!','2020-10-28 23:16:49','2020-11-01 19:38:26',NULL,0,5,4),(18,'I agree once more. And edited','2020-10-28 23:17:53','2020-10-28 23:18:26','just editing',1,5,4),(19,'Pretty good!','2020-10-28 23:18:09',NULL,NULL,1,5,4),(20,'Good job.','2020-10-28 23:52:36',NULL,NULL,1,5,4),(21,'Excellent. Once again','2020-10-28 23:53:04',NULL,NULL,0,15,4),(22,'I like it','2020-10-29 02:31:10',NULL,NULL,1,15,1),(23,'No, no, no','2020-10-29 02:31:40',NULL,NULL,0,15,1),(24,'asdadad','2020-10-29 02:54:46',NULL,NULL,1,14,1),(25,'asdddd','2020-10-29 02:56:41',NULL,NULL,1,14,1),(26,'It happens to the best of us. Even the best ones','2020-10-29 16:43:09','2020-10-31 19:27:01','edited extra stuff',0,9,4),(27,'Me too!','2020-10-29 19:24:57',NULL,NULL,0,16,5),(28,'678910','2020-10-29 23:06:31',NULL,NULL,0,42,5),(29,'Indeed','2020-10-30 01:11:30',NULL,NULL,0,12,5),(30,'LOL, nice one','2020-10-30 23:35:16',NULL,NULL,0,5,1),(31,'That\'s funny','2020-10-30 23:35:44',NULL,NULL,1,5,1),(32,'Nice very nice are you awriter too?','2020-10-31 17:13:31',NULL,NULL,1,45,4),(33,'Interesting','2020-10-31 17:15:57',NULL,NULL,0,45,4),(34,'Hah interesting','2020-10-31 17:26:06',NULL,NULL,1,44,4),(35,'Fun aaaaa','2020-10-31 17:26:23','2020-10-31 17:30:06','glad that im alive',0,44,4),(36,'Ok This is a good thing !!!!','2020-10-31 17:47:14','2020-10-31 17:47:31','interesting',1,48,4),(37,'!!!!!!!!!!!!!!!!!!!!!!!!!!','2020-10-31 17:47:47',NULL,NULL,0,48,4),(38,'!!!!!!!!!!!!!!!!!!','2020-10-31 17:47:53',NULL,NULL,0,48,4),(39,'Haha funny things','2020-10-31 19:23:21',NULL,NULL,0,51,28),(40,'Helloooo','2020-10-31 19:23:58',NULL,NULL,0,50,28),(41,'sdasdsdasdadad','2020-10-31 19:33:32','2020-10-31 19:33:40',NULL,1,23,4),(42,'How could not understand it - it\'s so obvious.','2020-10-31 21:10:22',NULL,NULL,0,46,4),(43,'I like your style\n','2020-11-01 12:14:41',NULL,NULL,0,44,1),(44,'Interesting point','2020-11-01 12:24:56',NULL,NULL,0,43,1),(45,'I want to learn french too! We have so much in common!!!','2020-11-01 16:23:19',NULL,NULL,0,94,5),(46,'You have inspired me to read this book again!','2020-11-01 16:24:09',NULL,NULL,0,93,5),(47,'Great review as aways!','2020-11-01 16:24:35',NULL,NULL,0,92,5),(48,'You\'re right about that!','2020-11-01 18:14:10',NULL,NULL,0,84,74),(49,'You win this time. Amazing review!','2020-11-01 18:17:44',NULL,NULL,0,100,84),(50,'Glad you like it. War books are my speciality. You can\'t beat me here. hehe :P ','2020-11-01 18:20:10',NULL,NULL,0,100,85),(51,'You should borrow Enemy at The Gates. Battle for Stalingrad. Just came out! ','2020-11-01 18:27:08',NULL,NULL,0,102,85),(52,'Thanks for this review, it really helped me.','2020-11-01 18:32:22',NULL,NULL,0,103,5),(53,'Admin, it seems that someone is still holding this book. It has been two weeks now. Please take action - I want to read it. Thanks.','2020-11-01 18:37:01',NULL,NULL,0,12,85),(54,'100% agree! Your review is a masterpiece as the book itself.','2020-11-01 19:26:52','2020-11-01 21:02:50',NULL,0,108,5),(55,'Wow - you read Kafka too? Which book of his is your favorite','2020-11-01 19:44:19',NULL,NULL,0,111,5),(56,'Give me a break, Cleopatra. His review is not that great. You just like everything he writes. Marry him already!','2020-11-01 20:39:01',NULL,NULL,0,108,85),(57,'Laureate breathes... Cleopatra - likes, comments, subscribes!','2020-11-01 20:44:58',NULL,NULL,0,108,2),(58,'Cleopatra and Laureate sitting on a tree... K. I. S. S. I. N. G.','2020-11-01 21:25:48',NULL,NULL,0,108,87),(59,'Then why did you borrow it?','2020-11-01 21:40:56',NULL,NULL,0,117,2),(60,'Because I don\'t have a family...','2020-11-01 21:41:20',NULL,NULL,0,117,88),(61,'v1.2.1 released - there only one type now and it\'s called \"duck\" type.','2020-11-02 00:05:48','2020-11-02 00:07:13',NULL,0,119,87),(62,'v1.3.6 released - with more bugs.','2020-11-02 00:08:49',NULL,NULL,0,119,4),(63,'v1.4.8 released - variables declared with \"var\" will be accessible outside of your computer.','2020-11-02 00:15:39',NULL,NULL,0,119,5),(64,'v1.6.1 released - delete is a reserved word again.','2020-11-02 00:17:57',NULL,NULL,0,119,88),(65,'v1.7.2 released - Array.prototype.splice has 14 arguments now.','2020-11-02 00:32:34',NULL,NULL,0,119,1),(66,'1.8.3 released - when called \"alert\" will also make annoying alarm sounds.','2020-11-02 00:36:01','2020-11-02 00:37:19',NULL,0,119,76),(67,'v1.9.0 released - every time you declare a variable with \"let\" - a puppy dies.','2020-11-02 00:52:07',NULL,NULL,0,119,84),(68,'v1.9.8 released - new bugs will also work on older versions.','2020-11-02 00:53:16',NULL,NULL,0,119,85),(69,'Indeed...','2020-11-02 01:07:48',NULL,NULL,0,121,85),(70,'Superb! 123','2020-11-02 10:10:30','2020-11-02 10:11:03','12345',0,100,89);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genres` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres`
--

LOCK TABLES `genres` WRITE;
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
INSERT INTO `genres` VALUES (1,'Programming'),(2,'Sci-Fi'),(3,'Biography'),(4,'Thriller'),(5,'History'),(6,'Romance'),(7,'Finance'),(8,'Crime'),(9,'Drama'),(10,'Children\'s'),(11,'Classic'),(12,'Encyclopedia'),(13,'Poetry'),(14,'Science'),(15,'Travel'),(16,'Self help'),(17,'Mystery'),(18,'Horror'),(19,'Fantasy'),(20,'Adventure'),(21,'Economics'),(22,'Hobbies'),(23,'Health and Fitness'),(24,'Home and Garden'),(25,'Philosophy'),(26,'Religion'),(27,'Comedy'),(28,'Art'),(29,'Politics');
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'Bulgarian'),(2,'English'),(3,'German'),(4,'French'),(5,'Spanish'),(6,'Russian'),(7,'Greek'),(8,'Danish'),(9,'Italian'),(10,'Hebrew'),(11,'Arabic'),(12,'Portuguese'),(13,'Norwegian'),(14,'Japanese'),(15,'Swedish'),(16,'Chinese'),(17,'Sanskrit');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `levels`
--

DROP TABLE IF EXISTS `levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `required_points` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `levels`
--

LOCK TABLES `levels` WRITE;
/*!40000 ALTER TABLE `levels` DISABLE KEYS */;
INSERT INTO `levels` VALUES (1,'Beginner',0),(2,'Intermediate',50),(3,'Advanced',200),(4,'Pro',500),(5,'Guru',1000);
/*!40000 ALTER TABLE `levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_deleted` tinyint(1) DEFAULT 0,
  `review_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_likes_reviews1_idx` (`review_id`),
  KEY `fk_likes_users1_idx` (`user_id`),
  CONSTRAINT `fk_likes_reviews1` FOREIGN KEY (`review_id`) REFERENCES `reviews` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_likes_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (1,1,3,2),(2,0,3,2),(3,0,5,3),(4,0,3,4),(5,1,7,5),(6,0,7,3),(7,1,9,4),(8,1,9,4),(9,1,9,4),(10,1,9,4),(11,1,9,4),(12,1,9,4),(13,1,9,4),(14,1,9,4),(15,1,9,4),(16,1,9,4),(17,1,9,4),(18,1,9,4),(19,1,9,4),(20,1,9,4),(21,1,9,4),(22,1,9,4),(23,1,9,4),(24,1,9,4),(25,1,9,4),(26,1,9,4),(27,1,9,4),(28,1,9,4),(29,1,9,4),(30,1,9,4),(31,1,7,4),(32,1,8,4),(33,1,7,4),(34,1,9,2),(35,1,7,2),(36,0,8,2),(37,1,7,2),(38,1,7,2),(39,1,9,2),(40,1,7,2),(41,0,9,2),(42,1,7,2),(43,1,16,1),(44,1,16,1),(45,1,16,1),(46,1,16,1),(47,1,7,1),(48,1,22,1),(49,0,21,4),(50,0,17,4),(51,0,23,1),(52,1,8,1),(53,1,16,4),(54,1,5,4),(55,1,14,4),(56,1,20,4),(57,0,14,4),(58,0,8,4),(59,0,14,1),(60,0,7,1),(61,1,9,1),(62,1,12,1),(63,1,12,1),(64,1,12,1),(65,1,12,1),(66,1,12,1),(67,1,12,1),(68,1,12,1),(69,1,12,1),(70,1,12,1),(71,1,12,1),(72,1,12,1),(73,1,12,1),(74,1,12,1),(75,1,12,1),(76,1,12,1),(77,1,12,1),(78,1,12,1),(79,1,12,1),(80,1,12,1),(81,1,12,1),(82,1,12,1),(83,1,12,1),(84,1,12,1),(85,1,12,1),(86,1,12,1),(87,1,12,1),(88,1,12,1),(89,1,12,1),(90,1,12,1),(91,1,12,1),(92,1,12,1),(93,1,12,1),(94,1,12,1),(95,1,12,1),(96,1,6,1),(97,1,18,1),(98,0,12,1),(99,1,13,1),(100,0,13,1),(101,0,21,1),(102,1,42,1),(103,1,42,1),(104,1,42,1),(105,1,42,1),(106,1,16,5),(107,0,16,1),(108,1,44,1),(109,0,42,5),(110,0,5,1),(111,1,47,1),(112,1,16,4),(113,0,16,4),(114,1,44,4),(115,1,44,4),(116,0,75,4),(117,0,76,5),(118,0,9,1),(119,1,75,1),(120,1,75,1),(121,1,44,1),(122,0,43,1),(123,1,43,83),(124,0,80,83),(125,0,81,83),(126,0,84,83),(127,0,85,83),(128,0,86,83),(129,0,87,83),(130,0,89,83),(131,0,94,5),(132,0,92,5),(133,0,93,5),(134,0,94,85),(135,0,91,1),(136,0,83,1),(137,1,91,4),(138,0,93,4),(139,0,83,4),(140,0,91,4),(141,0,68,86),(142,0,85,1),(143,0,84,74),(144,1,98,74),(145,0,99,85),(146,0,100,85),(147,0,100,84),(148,0,100,74),(149,0,100,1),(150,0,103,5),(151,0,103,85),(152,0,106,85),(153,0,106,5),(154,0,108,84),(155,0,108,5),(156,0,111,84),(157,0,100,5),(158,0,99,5),(159,0,103,4),(160,1,104,4),(161,1,100,4),(162,1,99,4),(163,0,100,4),(164,0,109,4),(165,0,86,4),(166,1,83,84),(167,0,111,5),(168,0,110,2),(169,0,116,76),(170,0,118,85),(171,0,104,85),(172,0,107,85),(173,1,119,2),(174,0,119,4),(175,0,119,3),(176,0,100,2),(177,0,83,2),(178,0,108,2),(179,0,108,85),(180,0,108,87),(181,0,119,76),(182,0,119,84),(183,1,122,89),(184,0,100,89),(185,0,76,89),(186,1,101,93),(187,0,101,93),(188,1,127,93),(189,1,127,93),(190,0,124,1),(191,0,122,89),(192,1,102,89),(193,0,125,1),(194,0,126,1);
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT 0,
  `book_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ratings_books1_idx` (`book_id`),
  KEY `fk_ratings_users1_idx` (`user_id`),
  CONSTRAINT `fk_ratings_books1` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ratings_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (1,2,'2020-10-09 15:11:39','2020-10-09 15:12:19',0,3,3),(2,4,'2020-10-09 17:40:23',NULL,1,10,3),(3,1,'2020-10-09 19:47:08','2020-10-27 11:48:44',0,4,4),(4,3,'2020-10-09 20:48:34','2020-11-01 20:58:44',0,1,4),(5,3,'2020-10-09 21:28:30',NULL,0,1,5),(6,2,'2020-10-26 04:19:30','2020-10-26 10:12:12',0,22,4),(7,3,'2020-10-26 12:24:43','2020-11-01 20:56:56',0,20,1),(8,3,'2020-10-26 14:45:11','2020-11-01 16:07:59',0,10,4),(9,5,'2020-10-26 18:42:35',NULL,0,22,1),(10,3,'2020-10-27 11:16:34','2020-10-29 03:02:02',0,11,1),(11,5,'2020-10-27 11:30:24','2020-11-02 18:50:23',0,17,1),(12,4,'2020-10-27 11:34:24','2020-10-27 11:47:38',0,15,4),(13,3,'2020-10-27 13:12:12','2020-11-01 12:05:10',0,4,1),(14,4,'2020-10-27 22:21:54','2020-10-28 01:49:21',0,11,4),(15,5,'2020-10-28 01:57:04','2020-10-28 18:03:55',0,17,4),(16,3,'2020-10-28 12:34:36',NULL,0,11,65),(17,1,'2020-10-28 12:36:51',NULL,0,17,65),(18,5,'2020-10-28 16:43:32','2020-10-28 17:31:57',0,1,2),(19,5,'2020-10-28 17:58:51','2020-10-28 18:01:20',0,1,1),(20,5,'2020-10-29 16:22:16','2020-10-29 16:39:03',0,57,4),(21,4,'2020-10-29 19:25:53','2020-11-01 12:29:31',0,15,5),(22,3,'2020-10-29 19:29:48',NULL,0,40,5),(23,4,'2020-10-29 19:31:29','2020-11-01 11:52:40',0,44,5),(24,5,'2020-10-30 23:37:14',NULL,0,10,1),(25,4,'2020-10-31 16:40:57','2020-11-01 13:21:27',0,54,4),(26,4,'2020-10-31 16:44:06',NULL,0,37,4),(27,4,'2020-10-31 16:45:21',NULL,0,42,4),(28,4,'2020-10-31 16:49:59',NULL,0,26,4),(29,3,'2020-10-31 16:51:41',NULL,0,2,4),(30,3,'2020-10-31 16:52:58',NULL,0,34,4),(31,3,'2020-10-31 16:53:47',NULL,0,28,4),(32,2,'2020-10-31 17:01:12',NULL,0,3,4),(33,1,'2020-10-31 17:06:19','2020-10-31 17:09:42',0,30,4),(35,3,'2020-10-31 21:24:50',NULL,0,31,4),(36,2,'2020-11-01 11:35:23',NULL,0,33,5),(37,5,'2020-11-01 12:29:11',NULL,0,10,3),(38,3,'2020-11-01 13:17:35',NULL,0,55,4),(39,2,'2020-11-01 13:18:52','2020-11-01 20:58:58',0,58,4),(40,5,'2020-11-01 15:41:38',NULL,0,57,83),(41,4,'2020-11-01 15:48:17',NULL,0,15,83),(42,5,'2020-11-01 15:51:42',NULL,0,44,83),(43,4,'2020-11-01 15:54:44',NULL,0,57,84),(44,5,'2020-11-01 15:54:50',NULL,0,101,83),(45,5,'2020-11-01 15:56:49',NULL,0,102,83),(46,4,'2020-11-01 16:01:55',NULL,0,105,83),(47,5,'2020-11-01 16:05:48',NULL,0,53,83),(48,4,'2020-11-01 16:09:24',NULL,0,58,83),(49,5,'2020-11-01 16:12:32',NULL,0,1,83),(50,4,'2020-11-01 16:13:39',NULL,0,65,84),(51,3,'2020-11-01 16:16:48',NULL,0,103,84),(52,5,'2020-11-01 16:19:46','2020-11-01 21:00:01',0,98,84),(53,4,'2020-11-01 16:22:00',NULL,0,104,84),(54,5,'2020-11-01 16:30:04',NULL,0,104,85),(55,3,'2020-11-01 16:33:09','2020-11-01 20:57:56',0,77,84),(56,4,'2020-11-01 17:07:52','2020-11-01 20:58:13',0,53,84),(57,5,'2020-11-01 18:13:15',NULL,0,101,74),(58,4,'2020-11-01 18:15:12',NULL,0,76,84),(59,5,'2020-11-01 18:16:17',NULL,0,76,85),(60,3,'2020-11-01 18:16:35',NULL,0,88,74),(61,5,'2020-11-01 18:17:31',NULL,0,76,74),(62,4,'2020-11-01 18:28:10',NULL,0,106,85),(63,5,'2020-11-01 18:32:10',NULL,0,106,5),(64,3,'2020-11-01 19:00:19',NULL,0,65,85),(65,5,'2020-11-01 19:07:57',NULL,0,19,85),(66,4,'2020-11-01 19:18:41',NULL,0,19,84),(67,5,'2020-11-01 19:21:43',NULL,0,12,84),(68,5,'2020-11-01 19:26:00',NULL,0,105,5),(69,4,'2020-11-01 19:27:17',NULL,0,12,5),(70,4,'2020-11-01 19:34:47',NULL,0,64,84),(71,3,'2020-11-01 19:46:13',NULL,0,57,85),(72,4,'2020-11-01 19:49:46',NULL,0,47,84),(73,4,'2020-11-01 19:53:08',NULL,0,44,84),(74,1,'2020-11-01 20:48:47',NULL,0,106,2),(75,5,'2020-11-01 20:55:55','2020-11-01 21:01:21',0,12,76),(76,3,'2020-11-01 21:30:22',NULL,0,12,88),(77,4,'2020-11-01 22:13:49',NULL,0,12,85),(78,3,'2020-11-02 00:04:07',NULL,0,2,2),(79,3,'2020-11-02 00:54:39',NULL,0,6,2),(80,3,'2020-11-02 01:05:22',NULL,0,84,84),(81,5,'2020-11-02 10:09:31',NULL,0,76,89),(82,5,'2020-11-02 10:30:57',NULL,0,101,90),(83,5,'2020-11-02 11:10:31',NULL,0,26,91),(84,4,'2020-11-02 12:26:39',NULL,0,2,92),(85,5,'2020-11-02 12:28:26',NULL,0,80,92),(86,4,'2020-11-02 12:30:10',NULL,0,88,93),(87,4,'2020-11-02 14:47:38',NULL,0,28,92),(88,4,'2020-11-02 19:41:27',NULL,0,24,89),(89,3,'2020-11-10 20:55:45',NULL,0,76,5);
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL,
  `edit_reason` tinytext DEFAULT NULL,
  `is_deleted` tinyint(1) unsigned zerofill DEFAULT 0,
  `book_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reviews_books1_idx` (`book_id`),
  KEY `fk_reviews_users1_idx` (`user_id`),
  CONSTRAINT `fk_reviews_books1` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reviews_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (3,'On second thought - nice book','2020-10-09 15:07:06','2020-10-09 15:08:54','I have changed my mind again',0,3,3),(4,'AMAZING','2020-10-09 15:23:11','2020-10-28 17:31:57',NULL,0,3,2),(5,'I like books. I have cried a bit','2020-10-09 17:40:48','2020-10-09 17:43:05','Review was too romantic for my taste',0,10,3),(6,'Great Book','2020-10-09 19:46:50','2020-10-28 17:42:20',NULL,0,4,4),(7,'Uplifting - simply amazing! Cool','2020-10-09 20:49:08','2020-10-31 19:32:08',NULL,0,1,4),(8,'It\'s somewhat ok','2020-10-09 21:28:55','2020-10-09 21:31:03','Edited',1,1,5),(9,'I thought this was a comment but it was a review','2020-10-09 21:47:40','2020-10-09 21:55:43','Edited',0,1,5),(10,'This book is great, really hehe!!','2020-10-26 03:31:09','2020-10-28 03:15:43','book is good',1,10,4),(11,'Great Book man','2020-10-26 04:16:43',NULL,NULL,0,22,4),(12,'Very interesting, brought me to tears at times.....','2020-10-26 12:24:43','2020-11-01 12:23:09','Well, I reconsidered',0,20,1),(13,'Excellent','2020-10-26 18:42:35',NULL,NULL,1,22,1),(14,'Great Book!\n\nEdit: Maybe not so much...Wellll','2020-10-27 11:16:34','2020-10-28 23:45:20',NULL,1,11,1),(15,'Excellent, deep','2020-10-27 11:30:24','2020-10-31 14:25:23','Too big title',0,17,1),(16,'Love fairy tales! !!!','2020-10-27 11:34:24','2020-10-27 11:47:38','changed my mind',0,15,4),(17,'Cool book','2020-10-27 11:43:40','2020-10-28 18:03:55',NULL,1,17,4),(18,'Great book for beginners!','2020-10-27 13:12:12','2020-11-01 12:05:10',NULL,1,4,1),(19,'Az updeitnah revieto si','2020-10-27 22:21:54','2020-10-28 01:49:21','adddddddddd',1,11,4),(20,'OKish','2020-10-28 12:34:36',NULL,NULL,0,11,65),(21,'Disappointing, I expected so much more','2020-10-28 12:36:51',NULL,NULL,0,17,65),(22,'This book is nice!','2020-10-28 16:43:19',NULL,NULL,0,1,2),(23,'On second thought, I like it very much','2020-10-28 17:57:55','2020-10-28 18:01:20',NULL,1,1,1),(24,'This is a stupid book','2020-10-28 18:13:06',NULL,NULL,1,17,4),(25,'This is a stupid book','2020-10-28 18:14:32',NULL,NULL,1,17,4),(26,'Great book!','2020-10-28 18:15:29',NULL,NULL,1,17,4),(27,'Great Book','2020-10-28 18:17:13',NULL,NULL,1,17,4),(28,'Great!','2020-10-28 18:18:01',NULL,NULL,1,17,4),(29,'aaaaaaaaaa','2020-10-28 18:22:02',NULL,NULL,1,17,4),(30,'Great review!','2020-10-28 18:24:19',NULL,NULL,1,17,4),(31,'!!!!!!!!!','2020-10-28 18:27:24',NULL,NULL,1,17,4),(32,'sssssssssssssssssssssssssssssssss','2020-10-28 18:28:33',NULL,NULL,1,17,4),(33,'!!!!!!!!!!!!','2020-10-28 18:29:11',NULL,NULL,1,17,4),(34,'Great!','2020-10-28 18:30:07',NULL,NULL,1,17,4),(35,'!!!!!!!!!','2020-10-28 18:32:18',NULL,NULL,1,17,4),(36,'Great review','2020-10-28 18:33:04',NULL,NULL,1,17,4),(37,'Gdasdas','2020-10-28 18:33:14',NULL,NULL,1,17,4),(38,'Gooood!','2020-10-28 18:33:25',NULL,NULL,1,17,4),(39,'Interesting - I speack JAVA too.','2020-10-28 18:34:35',NULL,NULL,1,3,4),(40,'AAAAAAAAAAAAA','2020-10-28 19:04:32',NULL,NULL,1,11,4),(41,'Nice one','2020-10-29 03:01:22','2020-10-29 03:01:32',NULL,1,11,1),(42,'12345','2020-10-29 03:01:54','2020-10-29 03:02:02',NULL,0,11,1),(43,'Moby Dick is very big. Huge! However, the the size of the whale doesn\'t matter. It matters how you use it.','2020-10-29 16:21:52','2020-10-29 16:39:03',NULL,0,57,4),(44,'Classic tales from the olden days.','2020-10-29 19:25:41','2020-11-01 12:29:31',NULL,0,15,5),(45,'Gogol is amazing - one of my favorites.','2020-10-29 19:29:48',NULL,NULL,0,40,5),(46,'I don\'t understand this book. Is there some hiden meaning or something? Never mind - I got it - it\'s about a house for a doll....','2020-10-29 19:31:28','2020-11-01 11:52:40',NULL,0,44,5),(47,'Great book! Highly recommended!','2020-10-30 23:37:14',NULL,NULL,1,10,1),(48,'Nice book','2020-10-31 16:23:28',NULL,NULL,1,55,4),(49,'Nice book','2020-10-31 16:23:38',NULL,NULL,1,55,4),(50,'Nice book!','2020-10-31 16:23:58',NULL,NULL,0,59,4),(51,'I like this book alot!','2020-10-31 16:24:35',NULL,NULL,0,59,4),(52,'Nice book!','2020-10-31 16:25:07',NULL,NULL,1,40,4),(53,'Nice book!','2020-10-31 16:25:49',NULL,NULL,1,40,4),(54,'Niceeeeeeeeeeeee','2020-10-31 16:26:05',NULL,NULL,1,40,4),(55,'Niceeeeeeeeeeeee!','2020-10-31 16:26:29',NULL,NULL,1,40,4),(56,'I like this book!','2020-10-31 16:26:44',NULL,NULL,0,40,4),(57,'Nice I like it!','2020-10-31 16:28:43',NULL,NULL,0,40,4),(58,'sdadasdasdasdasdasdada','2020-10-31 16:30:50',NULL,NULL,0,40,4),(59,'asdasdasdasdasdasdasda','2020-10-31 16:31:08',NULL,NULL,0,40,4),(60,'sadasdasdasdasdada','2020-10-31 16:31:21',NULL,NULL,0,40,4),(61,'asdasdasdadda','2020-10-31 16:31:45',NULL,NULL,0,40,4),(62,'sadadasdasdadas','2020-10-31 16:32:50',NULL,NULL,0,40,4),(63,'sadsadasdasdasdasdasdada','2020-10-31 16:33:34',NULL,NULL,0,40,4),(64,'Cool very much','2020-10-31 16:40:57',NULL,NULL,1,54,4),(65,'I love gypsy ballads - I\'m from Sliven.','2020-10-31 16:44:06',NULL,NULL,0,37,4),(66,'Interesting...','2020-10-31 16:45:20',NULL,NULL,0,42,4),(67,'Somehow I\'ve expected more from this...','2020-10-31 16:49:59',NULL,NULL,0,26,4),(68,'I have to learn this','2020-10-31 16:51:41',NULL,NULL,0,2,4),(69,'What does it say on the cover? Does anybody know?','2020-10-31 16:52:58',NULL,NULL,0,34,4),(70,'Write a review here you go.','2020-10-31 16:53:47',NULL,NULL,0,28,4),(71,'I like it!','2020-10-31 17:01:12',NULL,NULL,0,3,4),(72,'Interesting book','2020-10-31 17:06:18',NULL,NULL,1,30,4),(73,'Interesting book again','2020-10-31 17:06:38','2020-10-31 17:09:42',NULL,0,30,4),(74,'I like this book !!!!!!!!!','2020-10-31 17:43:14','2020-10-31 17:44:01',NULL,1,14,4),(75,'fun - very fun!','2020-10-31 21:24:50',NULL,NULL,0,31,4),(76,'Invisible Man? Ok But how did they wrote a book about him if he was invisible? I mean - they can\'t see him. How do they know who he was and what he did. ','2020-11-01 11:35:23',NULL,NULL,0,33,5),(77,'It\'s not to my taste, too much drama','2020-11-01 13:17:35',NULL,NULL,0,55,4),(78,'I like eastern cultures.','2020-11-01 13:18:52','2020-11-01 20:59:20',NULL,0,58,4),(79,'Heart warming story','2020-11-01 13:21:27',NULL,NULL,0,54,4),(80,'Very interesting book. I like it very much','2020-11-01 15:41:38','2020-11-01 15:42:33','Some more text',0,57,83),(81,'Very good book!','2020-11-01 15:48:17',NULL,NULL,0,15,83),(82,'Good book!','2020-11-01 15:51:42',NULL,NULL,0,44,83),(83,'What can I say about this great American novel that hasn’t already been said by generations of readers and academics?\n\nMoby-Dick is as mammoth, mysterious and elusive as the enormous white whale that gives the book its name. The opening line (“Call me Ishmael”) is one of the most famous in all literature. And even people who’ve never read it are familiar with the peg-legged, vengeance-seeking Captain Ahab, the archetype for any maniacally obsessed leader.\n\nWhat makes the novel so fascinating is how modern it feels. It’s an adventure tale about a man who’s driven to hunt down the beast who maimed him, but it’s also a treatise on whales and the whaling industry, a sharp look at class and culture (the sailors hail from all around the world), and a bold literary experiment, for 1851 or even today.\n\nIt’s hugely digressive, contains dialogue that at times sounds Shakespearean, and there’s not really much action until the end. But somehow it’s still very entertaining. Melville (who, of course, knew all about whaling) is such a clever, genial writer, that you’ll be smiling and chuckling throughout and gasping at his powers of description and observation.\n\nYou’ll smell the salty air, feel the churning waves and your heart will beat a little faster when one of the crew cries “There she blows!”\n\nI wasn’t especially moved by the story, but I don’t think we’re meant to be. Each of the characters is distinct, and Melville is savvy in the way that he uses silence to reveal dissent, particularly in the growing animosity between Ahab and Starbuck, the responsible first mate. (Yup, that’s where the coffee chain got its name.)\n\nBut what I do feel about the book is awe and respect. Like the ocean itself, it is vast and has unknowable depths, and I can see myself in another couple of years venturing back out for another rewarding trip.','2020-11-01 15:54:44',NULL,NULL,0,57,84),(84,'Funny book.','2020-11-01 15:54:50',NULL,NULL,0,101,83),(85,'Yummyyyy booooook!!!  :)','2020-11-01 15:56:49',NULL,NULL,0,102,83),(86,'Like this book!','2020-11-01 16:01:55',NULL,NULL,0,105,83),(87,'I love  this book!','2020-11-01 16:05:48',NULL,NULL,0,53,83),(88,'Cool book but I don\'t understand the title.','2020-11-01 16:07:59',NULL,NULL,1,10,4),(89,'I love this book .','2020-11-01 16:09:24',NULL,NULL,0,58,83),(90,'Cool!','2020-11-01 16:12:32',NULL,NULL,0,1,83),(91,'When the title of a book enters the English language that puts it on my reading list right away. What constitutes \'sanity\' for men in war is problematic on two levels: \n\n1). - who put them in this situation (war)\nand\n2). - what would a \"sane\" person do to get out of the situation. \n\nAnother book I think should be on a \'congressional reading list.\'','2020-11-01 16:13:39','2020-11-01 16:14:52','added new lines',0,65,84),(92,'Rudyard Kipling is the worst example of the quintessential British Imperialist and Colonialist. His attitude towards India is contemptuous and condescending. As a person, I dislike him intensely.\n\nKipling writes beautifully. His stories are simple, engaging and profound at the same time. As a writer, I love him.\n\nThis is a childhood favourite. I read it first in translation, and then in the original. This is a true classic - it works for one as a child as well as an adult.','2020-11-01 16:16:48',NULL,NULL,0,103,84),(93,'The stars move toward an infinity not to be counted. Yet so accessible. They shift over making a space for me around the campfire as we listen to Cervantes’ narrator tell us tales nested within tales. The emergence of modern tropes. I follow the Don who will not cease to follow his life source; his imagination; his seeing the actual reality agreed upon lacking. Robed in humor there is a biting edge of honing books as the source of life. The inner life over the outer life? Breeding laughter while breeding thought.\n\nWhat a reading experience! I am so glad that I now have this volume as a part of me. What I am perplexed about is that Volume 2 was written without permission by someone else and therefore a decade later Cervantes writes his; a reaction or a creative urge. Does anyone know the answer. It would help me greatly in deciding whether to go onto Volume 2','2020-11-01 16:19:46',NULL,NULL,0,98,84),(94,'I\'d been toying with the idea of reading books in French. I can understand the language - but as for speaking it, well here\'s another ball game. I read part of this edition in my class when I was 13 years old. I read when the hound was racing towards its would be victim.\n\nWould be victim...due to Sherlock Holmes\' intervention. Holmes is a very fantastic, very popular character. Sir Arthur Conan Doyle, though he claimed to loathe the character, had a hidden fondness for Holmes. The author was furious with the treatment of his creation in the hands of Maurice Leblanc, who was the creator of Arsene Lupin.\n\nMovie and TV adaptations come and go, but the purity of Sherlock Holmes is like vapor to the uninitiated. Us fans know secretly what makes him tick. But we cannot transcribe ourselves in certain terms. We lack the knowledge how to pin the exactitude of Homes as portrayed in the original 4 novels and 56 short stories. Same for the directors and wannabe authors who wish to ape Doyle.\n\nThis was an experiment. And I think I succeeded in enjoying the story, known as it was to me in an earlier reading in English. The Hound of the Baskervilles was really wildly successful only in retrospective. Now it is part of the legacy of the detective. The meerschaum pipe and the hunting apparel are nearly part of folklore, and I enjoyed visiting it thoroughly. Highly recommended.','2020-11-01 16:21:59',NULL,NULL,0,104,84),(95,'My second time reading this story and I still felt the chill and goosebumps. It helped that since I last read it three years ago, I had forgotten some of the particular details of the story and hence, every new revelation was as new to me as when I read it the first time around.\n\nArthur Conan Doyle combines the mystery, bleakness, horror and danger of the moorland and the Grimpen Mire within it, the fear and terror of the dreaded Hound of the legend of Baskerville, and a murder to create one of the most original detective stories.','2020-11-01 16:30:04',NULL,NULL,0,104,85),(96,'Heart of darkness backwards. ','2020-11-01 16:33:09','2020-11-01 20:57:56',NULL,0,77,84),(97,'Since I love Scandinavian authors and review many Swedish and Norwegian novels, I\'m often asked what the best language is if you\'re planning to read one in translation. It\'s early days yet, but I\'m starting to feel more and more certain that the answer is German. Just like Komet im Mumintal , which I read last year, Pippi Langstrumpf was an absolute winner and felt 100% authentic. It was exactly like reading it in Swedish: the melody of the sentences was the same, the word-play was the same, and, most important, Pippi\'s voice was the same. In English, it somehow doesn\'t quite work, and she often comes across as bratty or insane. Here, the spell is never broken. She is the coolest, bravest, funniest person in the world; Tommy and Annika can\'t help loving her with all their little hearts, and neither could I. If you can\'t appreciate this wonderful book in the original, read it in German and you\'ll hardly miss anything at all.\n\nHeja Pippi! And, by the way, thank you for telling all those amazing barefaced lies. It\'s totally put me in the right frame of mind for writing the project proposal I\'m supposed to be finishing this evening. I guess I\'d better get back to doing that.','2020-11-01 17:07:52',NULL,NULL,0,53,84),(98,'Witty old school gangsta! There is a lot to learn from him','2020-11-01 18:13:15',NULL,NULL,0,101,74),(99,'Excellent true account of a young German sailor\'s time serving on a submarine during WWII.\nI can\'t praise this book enough. This was made into a miniseries which was one of the most hypnotic and thrilling I\'ve ever seen. There was one lighthearted scene where the young sailors lustily sing their favorite song \"It\'s a Long Way to Tipperary\" that I found hilarious.','2020-11-01 18:15:12',NULL,NULL,0,76,84),(100,'Probably the most existential and realistic book of submarine warfare from the point of view of a German crew that I have read. It reveals an experience most would rather not have had to live through.\n\nSubmarines need buoyancy to function. Salt water makes that extremely difficult because the specific gravity of salt water varies with depth, temperature, the amount of plankton, salinity, even the time of year. (Apparently fresh water is much easier.) Now let’s assume that the specific gravity changes by 1/1000, a small enough amount. If the weight of the sub is 800 tons, the weight must change by 1/1000 or 1600 lbs., not an insignificant amount. The weight has to be increased or decreased as the case may be.\n\nOr let’s say the cook moves a 100 lb. sack of potatoes from the stern forward. That amount of weight has to be redistributed by pumping an equivalent amount of water back to the stern in the trim tanks. You will never think of submarines in quite the same way after you have read the description of their sub in the midst of a storm having to run on the surface for speed (if you could call it that) and to charge the batteries, the boat plunging and heaving through the waves. Can submarines capsize?\n\nThis kind of fascinating information adds such verisimilitude to one of the submarine classics to come out of WW II. The author served as a naval correspondent on U-Boots during the war and experienced much of what he then wrote about. I have seen the movie (in German) and listened to the audiobook in addition to reading the book (in English). Not recommended in any form if you have a heart condition.\n\n“All doubts are silenced by the concept of duty.” Think about that.','2020-11-01 18:16:17',NULL,NULL,0,76,85),(101,'I don\'t quite understand this story, there\'s too much drama.','2020-11-01 18:16:35',NULL,NULL,0,88,74),(102,'Now this is excellent book! War time stories are what get and keep my attention. I\'d like to see some more similar books around here.','2020-11-01 18:17:31',NULL,NULL,0,76,74),(103,'This title was the catalyst for my enduring fascination with books covering the fighting on the Eastern Front during World War Two. This is a great story of the fighting at Stalingrad endured by the German and Russian armies. Although not as deeply researched as Glantz’s titles this book offers an insight into the soldier’s war and does it brilliantly. This is still one of my top ten books ever which isn’t bad considering it was first published in the early 1970’s. Recommended for anyone who loves a good historical account.','2020-11-01 18:28:10',NULL,NULL,0,106,85),(104,'I don\'t read war books in principle, but I really liked a review here and decided to give it a chance. I was not disappointed! There is so much life and character in this book. It\'s a must read!','2020-11-01 18:32:10','2020-11-01 18:32:42','Typo error',0,106,5),(105,'Is it tragic, absurd, or funny?\nWHO CARES!\nThis beats out almost every book that purports to be funny and I\'m not particularly unfamiliar with funny books.\nCatch-22 grabs you by the skinny hairs and shocks you into the most wonderful and horrible bureaucratic nightmare ever devised. It\'s not even the clarity that strikes you. It\'s not the convoluted insanity of a huge cast of truly unforgettable and brilliant characters as they stumble from one mismatched contradiction after another or as they game the system to truly amazing proportions. (Milo.) :)\nIt\'s the timing, the clever buildups, the sheer insanity of one damnable event after another and the realization that the only clear solution, the only way out of this trap, is...\nNo. Wait. That IS the realization. There is no way out.\n\nWe can put the book down, but the absurdities live on. Not just the absurdities inside the book, but in our own lives as we deal with one more piece of nonsense after another. There is no escape. None.\n\nAnd yet, I kept laughing throughout this novel. This brilliant, brilliant novel.\n\nI\'m going out on a limb here to say it\'s in the upper 20 books of all time. Maybe higher. There\'s absolutely nothing about this book I didn\'t love. I\'m gonna have to read this 4-5 times just for the sheer perverse pleasure of it.\n\nSure, some Italian whore might come at me with a steak knife or other piece of cutlery, but that\'s the cost of doing business with the military.\nTotally amazing.','2020-11-01 19:00:19',NULL,NULL,0,65,85),(106,'H.G Wells is trying to make a point about British Imperialism through his book. His goal is to show people how England\'s colonies might feel by having the peaceful English countryside razed and innocent people slaughtered and the peoples inability to fight back against an immense foreign power. The normality the Narrator feels with all of the violence holds a parallel to the violence used in colonies to keep the people under control, and how it became a common occurrence','2020-11-01 19:07:57',NULL,NULL,0,19,85),(107,'This classic 1898 science fiction novel has teeth to it, and it’s not just the Martians. The War of the Worlds is a lot more thoughtfully written than I had remembered. In between deadly heat rays, huge tripod machines striding around the country killing everything in their path, and bloodthirsty Martians trying to take over Earth (starting with Great Britain), there\'s also critique of colonialism, religious hypocrisy, and even how humans treat animals. The ways in which people react in a crisis is given just as much attention as the Martians\' actions.\n\nI read this when I was a teenager, but for whatever reason I didn’t get much out of it at the time. But I let myself get roped into a GR group read of it, partly because it\'s so short. And also because my literary diet needs more classics. And you know? I\'m glad I did.\nUpping my rating from 3 stars to 4.5 on reread, partly in recognition of how advanced this book was for its time in some of its concepts, and the influence it\'s had on the SF genre.\nGroup read with the Non-Crunchy Classics Pantaloonless crew.','2020-11-01 19:18:41',NULL,NULL,0,19,84),(108,'The character of Clarice Starling was great in this book! She’s more fierce and cunning then what Jodie Foster does with the character in the movie. Jodie Foster still does a damn fine job with this character but it’s hard to be exactly like a character in a book. You can’t see what she thinks or how she interprets the Buffalo Bill case and the prisoner, Hannibal Lecter.\nSpeaking of Dr. Hannibal Lecter, he’s a monster, sadistic, way too intelligent, and will scare the pants off you. Anthony Hopkins does such a great job with this character. Let’s just take a moment to appreciate how he took Thomas Harris’ creation and made Lecter unforgettable!\nBesides the great characterization, the police procedural and research of serial killers that Harris does in this series is fantastic! You’ve got to realize that this is wrote in the 80’s and he likely has influenced the crime detective book genre more than anyone out there! Kudos Thomas Harris!!\nIf you have been living under a rock for the last 30 something years and have not seen this movie, go read the book first!! You won\'t be disappointed.\nAnd if you love this movie and you\'re worried about not getting anything from the book, take it from me. I felt the same and I just gave this 5 stars!','2020-11-01 19:21:43',NULL,NULL,0,12,84),(109,'I love Mozart and his music. I know how to play Eine Kleine Nacht Musik on the piano.','2020-11-01 19:26:00',NULL,NULL,0,105,5),(110,'Chilling! Read it before watching the film. Both are great!','2020-11-01 19:27:17',NULL,NULL,0,12,5),(111,'Gregor Samsa wakes up one morning to discover he\'s been transformed into a giant beetle-like creature. Can he and his family adjust to his new form?\n\nThe Metamorphosis is one of those books that a lot of people get dragooned into reading during high school and therefore are predisposed to loath. I managed to escape this fate and I\'m glad. The Metamorphosis is quite a strange little book.\n\nTranslated from German, The Metamorphosis is the story of how Gregor Samsa\'s transformation tears his family apart. I feel like there are hidden meanings that are just beyond my grasp. I suspect it\'s a commentary about how capitalism devours its workers when they\'re unable to work or possibly about how the people who deviate from the norm are isolated. However, I mostly notice how Samsa\'s a big frickin\' beetle and his family pretends he doesn\'t exist.\n\nThere\'s some absurdist humor at the beginning. Samsa\'s first thoughts upon finding out he\'s a beetle is how he\'s going to miss work. Now, I\'m as dedicated to my job as most people but if I woke up to find myself a giant beetle, I don\'t think I\'d have to mull over the decision to take a personal day or two.\n\nAside from that, the main thing that sticks out is what a bunch of bastards Samsa\'s family is. He\'s been supporting all of them for years in his soul-crushing traveling salesman job and now they\'re pissed that they have to carry the workload. Poor things. It\'s not like Gregor\'s sitting on the couch drinking beer while they\'re working. He\'s a giant damn beetle! Cut him some slack.\n\nAll kidding aside, the ending is pretty sad. I\'ll bet Mr. Samsa felt like a prick later. The Metamorphosis gets four stars, primarily for being so strange and also because it\'s the ancestor of many weird or bizarro tales that came afterwords. It\'s definitely worth an hour or two of your time.','2020-11-01 19:34:47',NULL,NULL,0,64,84),(112,'Moby-Dick; or, The Whale is a novel by American writer Herman Melville, published in 1851 during the period of the American Renaissance.\n\nSailor Ishmael tells the story of the obsessive quest of Ahab, captain of the whaler Pequod, for revenge on Moby Dick, the white whale that on the previous whaling voyage bit off Ahab\'s leg at the knee.\n\nThe novel was a commercial failure and out of print at the time of the author\'s death in 1891, but during the 20th century, its reputation as a Great American Novel was established.\n\nWilliam Faulkner confessed he wished he had written it himself, and D. H. Lawrence called it \"one of the strangest and most wonderful books in the world\", and \"the greatest book of the sea ever written\". \"Call me Ishmael\" is among world literature\'s most famous opening sentences.\n','2020-11-01 19:46:13',NULL,NULL,0,57,85),(113,'This book haunts me. I can’t stop thinking about it because I have questions, questions and more questions; I have so many unanswered questions that I will never know the answer to, and it’s slowly killing me!\nWhat is the trial? Is K actually guilty or is he innocent? Is this novel a nightmare sequence or a paranormal encountering? Why are so many characters never heard from again? And who is that mysterious figure at the end of the novel that witnesses K\'s fate? There are just so many questions, but no damned answers!\nThis is frustrating, so frustrating. The novel leaves the reader with an overwhelming sense of perplexity. There is no definitive explanation as to what has actually happened; there is no logical sense of the events. But, then K doesn’t know either; he is just as confused by the strange happenings as the reader. The events are completely unexplainable and unfathomable; thus, Kafka’s trial will stay with the me for the rest of my life, as I ponder this bizarre novel again, and again.\nThere are no answers!','2020-11-01 19:49:46',NULL,NULL,0,47,84),(114,'Ibsen’s famous A Doll’s House is a landmark in the development of truly independent female heroines, rejecting the patriarchy they were socialised to accept unconditionally.\n\nNora, the main character, fails to make her husband understand that their perception of reality is incompatible as he keeps seeing her as a doll, acting out a pretty life for his pleasure and reputation.\nIn the original version, Nora shows the path to independence by opting for the uncertain future of a life lived alone and independently, but Ibsen was confronted with dominant misogyny and power play when German theatres in 1880 asked for “an alternative ending” (yes!), one in which Nora is emotionally blackmailed into staying with her family for the sake of the children. Curtain falls on that “barbaric act of violence”, as Ibsen himself put it when commenting on the \"politically correct\" alternative a rewriting of literature to suit a misogynistic society protective of all documentation of the role of women.\nWell, unfortunately we are watching an all too real alternative ending to a century of increasing women’s rights at the moment as well. Across the world, \"alternatives\" to freedom of speech, movement, and choice are implemented in “so-called democratic processes”, hijacked by the resurrected mindsets of 19th century white, male, heterosexual, pseudo-Christian figures. Domestic violence, rape culture, law-making against family planning and abortion, the alternatives to women’s rights are scarily real.','2020-11-01 19:53:08','2020-11-01 19:53:52','removed a link',0,44,84),(115,'I\'ve watched the film and I like it but reading this is soooo hard. I can\'t even understand when it is about the Germans and when it is about the Russians. Movies are so much better than books...','2020-11-01 20:48:46','2020-11-01 20:49:25','fixed my grammar',0,106,2),(116,'“I ate his liver with some fava beans and a nice chianti” - HISS HISS HISS HISS','2020-11-01 20:55:55',NULL,NULL,0,12,76),(117,'This book is not family friendly!','2020-11-01 21:30:22',NULL,NULL,0,12,88),(118,'The book is so, so much better than the movie, but I didn\'t like the direction this story line went after this. Yes, have compassion for Hannibal, and the trauma of his childhood, but he is a vile murderer. He did not limit his killing spree to equally vile people. Sorry, I just couldn\'t buy the twist. Still, an engrossing read, and I still give it 4 stars. (I read this years and years ago, when I still read this genre. Now, I avoid them as much as I can. Stay on the light side. These serial killer books are way too dark for me now.)','2020-11-01 22:13:49',NULL,NULL,0,12,85),(119,'v1.1.0 released - delete is no longer a reserved word and some bug fixes.','2020-11-02 00:04:07','2020-11-02 00:07:45',NULL,0,2,2),(120,'There - I\'ve read it.','2020-11-02 00:54:39',NULL,NULL,0,6,2),(121,'Ash nazg durbatulûk, ash nazg gimbatul,\nash nazg thrakatulûk, agh burzum-ishi krimpatul.','2020-11-02 01:05:22',NULL,NULL,0,84,84),(122,'Great book, wartime stories are the best! More words...','2020-11-02 10:09:31','2020-11-02 10:10:00','More words',0,76,89),(123,'Уникална!','2020-11-02 10:30:57',NULL,NULL,0,101,90),(124,'Ъмейзинг бук','2020-11-02 11:10:31',NULL,NULL,0,26,91),(125,'Needs to be updated to the new standards of writing JavaScript applications. Some of the patterns are outdated and/or have ES6 equivalent that is much easier to read and use.','2020-11-02 12:26:39',NULL,NULL,0,2,92),(126,'This book is a must for fans of horror fiction.','2020-11-02 12:28:26',NULL,NULL,0,80,92),(127,'ok book!','2020-11-02 12:30:10','2020-11-02 12:30:27',NULL,0,88,93),(128,'Fairly good.','2020-11-02 14:47:38',NULL,NULL,0,28,92),(129,'Great book','2020-11-02 19:41:27',NULL,NULL,0,24,89),(130,'A war book... not my type.','2020-11-10 20:55:45',NULL,NULL,0,76,5);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_date` datetime NOT NULL DEFAULT current_timestamp(),
  `auth_token` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ip` varchar(46) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sessions_users_idx` (`user_id`),
  CONSTRAINT `fk_sessions_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,'2020-10-09 12:23:38','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDIyMzU0MjAsImV4cCI6MTYwMjI0MTAyMH0.RlKMfkhDGypW1XHUowvXlkeK-K-QVQU1s4sBWdcivTg',1,'192.168.0.101'),(2,'2020-10-09 13:01:58','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDIyMzc3MDUsImV4cCI6MTYwMjI0MTMwNX0.qLN234gvF04BSgLiBPJdtULuwhRAWDvWULlaffTnXFA',1,'192.168.11.130'),(3,'2020-10-09 14:29:57','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNDI5OTksImV4cCI6MTYwMjI0ODU5OX0.GuBTv8UHB59OZCJ-kldl_pr2i--h1mNQwdQ5qp-TipA',2,'192.168.0.101'),(4,'2020-10-09 14:33:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNDMyMzUsImV4cCI6MTYwMjI0ODgzNX0.0Mgwzh4ReVBvzE2QxRptnimh5EfujgELMv7RzUBLXFY',2,'192.168.0.101'),(5,'2020-10-09 14:47:29','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNDQwNTEsImV4cCI6MTYwMjI0OTY1MX0.MBfx4VCjs9nToMw04gmuUsg01How1dzDOymCwrPlyfg',3,'192.168.0.101'),(6,'2020-10-09 15:13:34','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNDU2MTUsImV4cCI6MTYwMjI1MTIxNX0.D4BKK5CiOG3GXalzJiQucrU5LveWQMhSC4V22-4BE7E',2,'192.168.0.101'),(7,'2020-10-09 15:23:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDIyNDYyMTgsImV4cCI6MTYwMjI1MTgxOH0.rJcxe3gJpiu1wLigwUDTA5S3o3cHMR9Rn7uBkVjBHfY',1,'192.168.0.101'),(8,'2020-10-09 15:26:24','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNDYzODYsImV4cCI6MTYwMjI1MTk4Nn0.E-G09WhMOUXE0__KAEUW-_5TW4OYwvFQu9iDmMFtQyQ',2,'192.168.0.101'),(9,'2020-10-09 15:27:00','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNDY0MjIsImV4cCI6MTYwMjI1MjAyMn0.41ifTNVPhqXD_LVaMysOro9d2KJaD8_EWy4OOpLTBIU',3,'192.168.0.101'),(10,'2020-10-09 15:33:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDIyNDY3ODQsImV4cCI6MTYwMjI1MjM4NH0.oWZ2dQkUaWw9uyBlIex6XvkKPUc08LTKXElMZFzDZXg',1,'192.168.0.101'),(11,'2020-10-09 15:51:15','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNDc4NzYsImV4cCI6MTYwMjI1MzQ3Nn0.SyqCrMwElMuvLAmnTc9g_qPawRimnl-Ic8XcHrcfLt0',3,'192.168.0.101'),(12,'2020-10-09 16:11:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNDkwODksImV4cCI6MTYwMjI1NDY4OX0.MKRtt-oD9506BkzN-sDKq7KuiLpwUuOQ66fucAjXgFg',3,'192.168.0.101'),(13,'2020-10-09 16:24:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNDk4OTUsImV4cCI6MTYwMjI1NTQ5NX0.GcecpAQ1FcBQt5qG2NHL6IvcUO-H5h0TrYHnIlXdN9w',3,'192.168.0.101'),(14,'2020-10-09 16:34:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDIyNTA0MzQsImV4cCI6MTYwMjI1NDAzNH0.8FXkg3wkqcqKoqRH1byogUVdiVIdeeFfPjMzec9-UDM',1,'192.168.11.130'),(15,'2020-10-09 16:36:55','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNTA2MTcsImV4cCI6MTYwMjI1NjIxN30.iZHSQ60z4lsUhP_vRSy6UeLY3T_1noVm7djl5i_OaZE',3,'192.168.0.101'),(16,'2020-10-09 16:39:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNTA3OTUsImV4cCI6MTYwMjI1NjM5NX0.FHb2QC5T3FRin6NAVt3_LWSqL_b1chkpojdQBW8o0ao',3,'192.168.0.101'),(17,'2020-10-09 16:40:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNTA4NDEsImV4cCI6MTYwMjI1NjQ0MX0.aqIx4N76p8yyoO9WvcmHwV6TCFXvAJarROUbcIgNTyU',3,'192.168.0.101'),(18,'2020-10-09 16:41:30','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNTA4OTIsImV4cCI6MTYwMjI1NjQ5Mn0._4o1C6o2J4znvxSC1RlmGCJPAzUGYQWsE8YwP6rbF_w',3,'192.168.0.101'),(19,'2020-10-09 16:47:58','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDIyNTEyNjUsImV4cCI6MTYwMjI1Njg2NX0.PnX0tlMPV8kPiiqDqtVDK6W0rmQg_EKOIZZNhHqKXj4',1,'192.168.11.130'),(20,'2020-10-09 17:31:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNTM4ODQsImV4cCI6MTYwMjI1OTQ4NH0.IFv6FGwWvw7rzNYTF54SQ6XRHW_xtV3WzAO-dEz2Q3A',3,'192.168.0.101'),(21,'2020-10-09 17:34:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDIyNTQwNjAsImV4cCI6MTYwMjI1OTY2MH0.nElaXfT_deSzhCXCM5o3IEUlCrwSiOVXvCJRWfEZFP0',1,'192.168.0.101'),(22,'2020-10-09 17:38:33','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNTQzMTQsImV4cCI6MTYwMjI1OTkxNH0.UVAQC0eQMozpgdkxKeBYgnPsSVnL1V-ONG7QBrJyGf0',3,'192.168.0.101'),(23,'2020-10-09 18:39:15','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDIyNTc5NTcsImV4cCI6MTYwMjI2MzU1N30.hZqpvm9EtYc1DmLm3QOWF3NjC997zwvC5sM9HBlSDmM',3,'192.168.0.101'),(24,'2020-10-09 19:43:34','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMjI2MTgxNSwiZXhwIjoxNjAyMjY3NDE1fQ.Ega0e6iEqYDTVDeutQEENsvw8YqatWi4TJTo8QVknjc',4,'192.168.0.101'),(25,'2020-10-09 20:40:23','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMjI2NTIyNCwiZXhwIjoxNjAyMjcwODI0fQ.M9gjPTVU7rMNpzm9Mj1TnAsiBfhaI4CGloeuq0yj2Rw',4,'192.168.0.101'),(26,'2020-10-09 20:41:06','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMjI2NTI2OCwiZXhwIjoxNjAyMjcwODY4fQ.TvnOcu9VVvLkd4LARGhl4nDyEP_Qq7dqnE9dkhKSfD0',4,'192.168.0.101'),(27,'2020-10-09 20:41:11','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMjI2NTI3MywiZXhwIjoxNjAyMjcwODczfQ.WCdxL-5SdXCZ9f04tUw5JjRtfpB5Qgund7nWf9Mw2Qg',4,'192.168.0.101'),(28,'2020-10-09 20:43:06','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMjI2NTM4OCwiZXhwIjoxNjAyMjcwOTg4fQ.7DvCHtz-EhdHjDiBkqBkQ88yKHouY2JrQIRmCu7Tl54',4,'192.168.0.101'),(29,'2020-10-09 20:43:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMjI2NTQwNywiZXhwIjoxNjAyMjcxMDA3fQ.DUIAGohasbpOqT2oHFRqi9z7bLEBLW7yL03GGnZ2Qpo',4,'192.168.0.101'),(30,'2020-10-09 20:43:35','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMjI2NTQxNiwiZXhwIjoxNjAyMjcxMDE2fQ.L_C6veJyo89skXLIEm8mVsbAxzlyCuP6m7Y9rAttozg',4,'192.168.0.101'),(31,'2020-10-09 20:45:30','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMjI2NTUzMiwiZXhwIjoxNjAyMjcxMTMyfQ.1t4zGaSbl5wqTUVbYDuc1L9ncMASKEoWL6SaEzErkfU',4,'192.168.0.101'),(32,'2020-10-09 20:46:58','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMjI2NTYyMCwiZXhwIjoxNjAyMjcxMjIwfQ.FJXJs8GV7UXg8FhDUl82dOgKJJiLfuDq7j9616eWSek',4,'192.168.0.101'),(33,'2020-10-09 21:22:32','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAyMjY3NzU0LCJleHAiOjE2MDIyNzMzNTR9.gs1JE_Zrt8os2zMutgdE_ggNR-i-2mfKsXBQ5MLZk00',5,'192.168.0.101'),(34,'2020-10-09 22:44:58','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDIyNzI2OTksImV4cCI6MTYwMjI3ODI5OX0.mq_GSeipRPYUsfaD0QlFqHih2CvMXBhaaI7K7bfhWR8',1,'192.168.0.101'),(35,'2020-10-09 22:56:18','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAyMjczMzgwLCJleHAiOjE2MDIyNzg5ODB9.x4dBKjpFIcC8nAtKPKldSfJRF7AVJ1N5HqYCskokhXs',5,'192.168.0.101'),(36,'2020-10-14 16:35:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDI2ODI0OTcsImV4cCI6MTYwMjY4ODA5N30.lS-ZD4FfvhnkjrVkgzgAVzM6FrSL9VG-bbIqW30ibtw',1,'192.168.11.130'),(37,'2020-10-14 16:35:35','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDI2ODI1MTgsImV4cCI6MTYwMjY4ODExOH0.fY2KlIi1IuUFv61Q6etzGUlLmPGt0YyKaOrtlhA-15k',1,'192.168.11.130'),(38,'2020-10-14 16:36:59','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDI2ODI2MDIsImV4cCI6MTYwMjY4ODIwMn0.8jDGz5Kcdy6Rl8gkuxluIOex29lAQtk1OT-XrjEpxrE',1,'192.168.11.130'),(39,'2020-10-14 16:45:45','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDI2ODMxNDYsImV4cCI6MTYwMjY4ODc0Nn0.PfJSqnPjcrhyoyQ9K_MCiHhbt6V7Ic_0euCDARBANn8',1,'192.168.0.102'),(40,'2020-10-17 16:06:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDI5Mzk5NzQsImV4cCI6MTYwMjk0NTU3NH0.qRbmNw9ALm7xFo9yTrk556jIfR0sqlHdccVY1BBgOAo',1,'192.168.0.102'),(41,'2020-10-17 17:20:27','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDI5NDQ0MjksImV4cCI6MTYwMjk1MDAyOX0.7eNvD52zcfitEM63fBCVuySBVtgGkcziFirTgJdUNaA',1,'192.168.0.102'),(42,'2020-10-19 15:29:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjYsInVzZXJuYW1lIjoiSm9obiIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzExMDU1OSwiZXhwIjoxNjAzMTE2MTU5fQ.n_9QgcqIsynlkw0K8HyHf-5b_1aR23mBFJwOqTZ4sbg',6,'192.168.0.102'),(43,'2020-10-19 15:42:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDMxMTEzNTksImV4cCI6MTYwMzExNjk1OX0.0Cyx86F88F_X3NAvygP_BAtx6_nhZh35mh3Cge10DVE',1,'192.168.0.102'),(44,'2020-10-19 16:09:17','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDMxMTI5NTcsImV4cCI6MTYwMzExODU1N30.R2GIMaNaMPjLK9exJ4O47OHy0SX_H3V6zyesKWF07X4',1,'192.168.0.102'),(45,'2020-10-25 14:20:34','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzIwMDksImV4cCI6MTYwMzYzNzYwOX0.u-X5ollHyDyseh7cEiyFKAtD2hkm9ArLyZVvJkRPpx4',1,'192.168.11.130'),(46,'2020-10-25 14:48:18','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzM2NzIsImV4cCI6MTYwMzYzOTI3Mn0.xD48dpkHNCzzy3tWi9ZmU354pMyf9pwwUcKIr0hT_yA',1,'192.168.11.130'),(47,'2020-10-25 14:52:49','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzM5NDMsImV4cCI6MTYwMzYzOTU0M30.pjJi7hTW83rLltstpX83QbNqFe7zzn09pyKSjFlLvTw',1,'192.168.11.130'),(48,'2020-10-25 14:56:11','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzQxNDUsImV4cCI6MTYwMzYzOTc0NX0.CZwUtgZaSlAbVy39xJ8wO6nBjUOaV8E4hLwkejhvNQ4',1,'192.168.11.130'),(49,'2020-10-25 14:56:33','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzQxNjcsImV4cCI6MTYwMzYzOTc2N30.6n4xUomCK0Y8gbkBXbQy_HPT-h89lXsukPJ35jFk93I',1,'192.168.11.130'),(50,'2020-10-25 14:58:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzQyOTQsImV4cCI6MTYwMzYzOTg5NH0.zsSL1fBVSKBYjgsrAD9WMyhglCopsSifgWqjd9HokKo',1,'192.168.11.130'),(51,'2020-10-25 15:00:21','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzQzOTUsImV4cCI6MTYwMzYzOTk5NX0.PwGTc6f4i8Z8imElcgzf_z9aLSTJM_jCt-5Fto3Df4A',1,'192.168.11.130'),(52,'2020-10-25 15:01:11','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzQ0NDUsImV4cCI6MTYwMzY0MDA0NX0.G159vULFVkP7FAUtdhOPqiLlUjnlc7XslBe7nwSGMVY',1,'192.168.11.130'),(53,'2020-10-25 15:02:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzQ0OTksImV4cCI6MTYwMzY0MDA5OX0.D2jK5Zug1LxdoSkd01-_cvxRKSe6NIDnkBwKYNlAIBc',1,'192.168.11.130'),(54,'2020-10-25 15:02:56','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzQ1NTAsImV4cCI6MTYwMzY0MDE1MH0.0EJ9dxDUYf17H1eB7EfJSspYFK1b7HEVdRGNXU2BV8U',1,'192.168.11.130'),(55,'2020-10-25 15:03:08','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzQ1NjIsImV4cCI6MTYwMzY0MDE2Mn0.Wuo3FRgS5I4_d3Fa3Xx_d9cV1nlIY_luGuBtPA9JDAs',1,'192.168.11.130'),(56,'2020-10-25 15:09:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzQ5MTgsImV4cCI6MTYwMzY0MDUxOH0.1so0GxSI01kLiddohM1jFHGqsFcMyTqTiB3FV0X4H4A',1,'192.168.11.130'),(57,'2020-10-25 15:09:41','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzQ5NTYsImV4cCI6MTYwMzY0MDU1Nn0.UxH0dsVFbnftmZVfvC3m2nv-MpyQzH4YDmlN9zoV21c',1,'192.168.11.130'),(58,'2020-10-25 15:11:25','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzUwNTksImV4cCI6MTYwMzY0MDY1OX0.75be8iobU-qvD-CjLmVBWZVTw2InHCOko0R3LiRMb7Y',1,'192.168.11.130'),(59,'2020-10-25 15:11:35','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzUwNjksImV4cCI6MTYwMzY0MDY2OX0.sq2yuxUQoREuSudfCn7hcu_htGUfKwoUXFgHz1tcmwE',1,'192.168.11.130'),(60,'2020-10-25 15:36:06','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzY1NDAsImV4cCI6MTYwMzY0MjE0MH0.HYkoMX7ftrZB-Nu0mv1MHUujEvpAzykb6bOJuxxIhpE',1,'192.168.11.130'),(61,'2020-10-25 15:36:40','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzY1NzQsImV4cCI6MTYwMzY0MjE3NH0.9I7qUPhWKMag9fbtzaYNYV-s2Yh4FsQKmfMWRhmxV_8',1,'192.168.11.130'),(62,'2020-10-25 15:41:00','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzY4MzQsImV4cCI6MTYwMzY0MjQzNH0.0bWJ2tgiUou_pJC5pNZS7bK7wwAiURxwqGcdN_xPq8w',1,'192.168.11.130'),(63,'2020-10-25 15:42:18','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzY5MTMsImV4cCI6MTYwMzY0MjUxM30.aS44Jw6pEa_MBL5Ok2y7NHPAqitKlsuVrEDeOebEnmY',1,'192.168.11.130'),(64,'2020-10-25 15:43:21','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzY5NzUsImV4cCI6MTYwMzY0MjU3NX0.psdYFPmykuOmaz0UGsc9IO_9CeyVpT01y5OerQ_xtVk',1,'192.168.11.130'),(65,'2020-10-25 15:47:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2MzcyNDgsImV4cCI6MTYwMzY0Mjg0OH0.Auc661g_0CdwSn7TUzpaNwWYxgcEcdZNuFa2s7C4cQY',1,'192.168.11.130'),(66,'2020-10-25 16:24:00','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2Mzk0MTQsImV4cCI6MTYwMzY0NTAxNH0.4RDCDFupcCSeDCB46sd6ifZQbjgT5_RC9wz1EssxGG8',1,'192.168.11.130'),(67,'2020-10-25 16:31:58','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2Mzk4OTIsImV4cCI6MTYwMzY0NTQ5Mn0.dPfRrL2zk4IUw471yZf5e8ZFBpLMp_3ZdBpENSgXvvY',1,'192.168.11.130'),(68,'2020-10-25 16:49:59','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDA5NzMsImV4cCI6MTYwMzY0NjU3M30.aZ1zfYRujiOIjVAVfCtEJkRGtFA-C1UkIimlH0N5rr8',1,'192.168.11.130'),(69,'2020-10-25 16:50:12','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDA5ODYsImV4cCI6MTYwMzY0NjU4Nn0.vh12GBXrKHz12R9y2k3FKB6EQoG-FjfNPVRvSHhqL7M',1,'192.168.11.130'),(70,'2020-10-25 16:52:45','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDExMzksImV4cCI6MTYwMzY0NjczOX0.p-gGQKQW8P_h7OCq1Tl6LKv_KYHiWq3QX4hcDu4XiL0',1,'192.168.11.130'),(71,'2020-10-25 16:59:42','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDE1NTYsImV4cCI6MTYwMzY0NzE1Nn0.7aPsnKH8qQ9O0HxT4irI3oNSrIW4Df2dPyfU7gpM6i8',1,'192.168.11.130'),(72,'2020-10-25 17:09:09','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2Mzg1NTEsImV4cCI6MTYwMzY0NDE1MX0.hY0tDdjJWIuReMZ-dPNnTdRWdyugmJIEu317ldcOYkk',1,'192.168.0.102'),(73,'2020-10-25 17:09:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDIxMzMsImV4cCI6MTYwMzY0NzczM30.qUn-U06v3ulvw610qovZLFWhv5ckoYaMlrwN0peD-Ig',1,'192.168.11.130'),(74,'2020-10-25 17:12:50','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMyLCJ1c2VybmFtZSI6IlZlbmVyYSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzYzODc3MywiZXhwIjoxNjAzNjQ0MzczfQ.QCqSlTRKrwlhm5NA4dNbmT3PP_9WFpPN2wswfZklp-k',32,'192.168.0.102'),(75,'2020-10-25 17:13:55','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMyLCJ1c2VybmFtZSI6IlZlbmVyYSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzYzODgzNywiZXhwIjoxNjAzNjQ0NDM3fQ.xNfx-RIPqd_XNUoGDAyOiknN_rxOJ6oiCFPVu2keoAE',32,'192.168.0.102'),(76,'2020-10-25 17:14:06','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2Mzg4NDksImV4cCI6MTYwMzY0NDQ0OX0.rUfSjDLMu_FBdWy1ZeEw5l16yvGsjaTx62Nbztf3MJg',1,'192.168.0.102'),(77,'2020-10-25 17:41:04','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDA0NjYsImV4cCI6MTYwMzY0NjA2Nn0.AlTOKZgFbusGQvujbN5V2prm4DGpIlZT2H20YzzfEak',1,'192.168.0.102'),(78,'2020-10-25 17:45:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDA3MjQsImV4cCI6MTYwMzY0NjMyNH0.SeMDyi-FSLMRxk8mrzZ7fXS3U941LmtzSPG5zSCdkVg',1,'192.168.0.102'),(79,'2020-10-25 17:46:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDA3OTEsImV4cCI6MTYwMzY0NjM5MX0.Y81ZAAn2XaepVVCjn7aWEoO8hEpNiau69zYeUGn65eI',1,'192.168.0.102'),(80,'2020-10-25 17:47:40','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDA4NjMsImV4cCI6MTYwMzY0NjQ2M30.FmlaFNWejonF6XnuhCA4t8szVWywO88VtUW88EnQ5fc',1,'192.168.0.102'),(81,'2020-10-25 17:55:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDEzMDcsImV4cCI6MTYwMzY0NjkwN30.4jpYNajxcwOzKNZUcKyCsOec4cZNB7i3YlZkyTgW8zQ',1,'192.168.0.102'),(82,'2020-10-25 18:44:50','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMyLCJ1c2VybmFtZSI6IlZlbmVyYSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY0NDI5MiwiZXhwIjoxNjAzNjQ5ODkyfQ.hpqsC89RYMEb_qCHl0iHIuwgtzFAEXH67keZ3XOLv3k',32,'192.168.0.102'),(83,'2020-10-25 18:47:33','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NDQ0NTUsImV4cCI6MTYwMzY1MDA1NX0.T4FWEg5HAgmsEExYAKrkPMN3sAuOhms4h7JlkBacRds',1,'192.168.0.102'),(84,'2020-10-25 19:31:29','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NTA2NjMsImV4cCI6MTYwMzY1NjI2M30.XF0bw6NUhERHmvpOEaCuNgMB557Z73S0lrrPnTvvWpI',1,'192.168.11.130'),(85,'2020-10-25 19:37:44','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NTEwMzgsImV4cCI6MTYwMzY1NjYzOH0.dorIe0cdYv5I5FMihMLSkGSSp-xTS7QTlrZ1qwVfSxc',1,'192.168.11.130'),(86,'2020-10-25 19:40:36','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NTEyMTAsImV4cCI6MTYwMzY1NjgxMH0.2Yr3wk4mrWUTtgRHaJLj9VAtORDc1eY0asJjIMK8vAk',1,'192.168.11.130'),(87,'2020-10-25 20:02:33','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQwLCJ1c2VybmFtZSI6ImxpbWE1NTUiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM2NTI1MjcsImV4cCI6MTYwMzY1ODEyN30.PZJt0HEUG2hmK-cssvR4iAJgUOuxUgsXUczBE-K-v4E',40,'192.168.11.130'),(88,'2020-10-25 20:07:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQxLCJ1c2VybmFtZSI6ImxpbWE2NjYiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM2NTI4MzEsImV4cCI6MTYwMzY1ODQzMX0.xE0GAhcH-lsSGeJnrmLdpbGk2Yhwcrnu1ANCJms567w',41,'192.168.11.130'),(89,'2020-10-25 20:08:55','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQyLCJ1c2VybmFtZSI6ImxpbWE3NzciLCJpc0FkbWluIjowLCJpYXQiOjE2MDM2NTI5MDksImV4cCI6MTYwMzY1ODUwOX0.YrRmVmq_ujpUCx4-D_Q6j093XKlaZ1us92Nn_GVhmMo',42,'192.168.11.130'),(90,'2020-10-25 20:15:35','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQzLCJ1c2VybmFtZSI6ImxpbWE4ODgiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM2NTMzMDksImV4cCI6MTYwMzY1ODkwOX0.J7QspinhIEsB8Ji4G_9aB83yVyrjlaNGeAesKxmPAGY',43,'192.168.11.130'),(91,'2020-10-25 20:20:08','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQzLCJ1c2VybmFtZSI6ImxpbWE4ODgiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM2NTM1ODIsImV4cCI6MTYwMzY1OTE4Mn0.LLBpzqP5dkBR5hS0a4gNDsrdQ1S4LQ-_rXOOYhhxd_Y',43,'192.168.11.130'),(92,'2020-10-25 20:24:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY1MDI0NSwiZXhwIjoxNjAzNjU1ODQ1fQ.hAxcgw2j8GBOdaKaqLiSP9iAszLJB6XT4j0l29yeUQI',4,'192.168.0.102'),(93,'2020-10-25 20:27:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQzLCJ1c2VybmFtZSI6ImxpbWE4ODgiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM2NTQwMTMsImV4cCI6MTYwMzY1OTYxM30.mc_2jTJfREL4uL3G5i_BTrSVQqO9pdAg5PyYa4RTT9M',43,'192.168.11.130'),(94,'2020-10-25 20:27:43','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQ1LCJ1c2VybmFtZSI6ImxpbWExMSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY1NDAzNywiZXhwIjoxNjAzNjU5NjM3fQ.j46YSmzgOJurG3dnbYADJcBlshzRa3b8pKgvvxaANqA',45,'192.168.11.130'),(95,'2020-10-25 20:34:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQ4LCJ1c2VybmFtZSI6ImxpbWExMTExIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzNjU0NDMzLCJleHAiOjE2MDM2NjAwMzN9.a0mKDeumXZGN_RNWhGPSLbM337wqvaeYU4ZkeY30ws0',48,'192.168.11.130'),(96,'2020-10-25 20:46:45','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NTUxNzksImV4cCI6MTYwMzY2MDc3OX0.wrjRzUUdOpmR10dd9M0iuq8BLHC5LrQy3vmw2vm4GUE',1,'192.168.11.130'),(97,'2020-10-25 20:47:14','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQ5LCJ1c2VybmFtZSI6ImxpbWE5OTk5IiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzNjU1MjA3LCJleHAiOjE2MDM2NjA4MDd9.h4bPHMV3oQLxsizlBkeNHfEEHIYILV_074sf9EK0aas',49,'192.168.11.130'),(98,'2020-10-25 20:57:40','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NTU4MzQsImV4cCI6MTYwMzY2MTQzNH0.S3QT53GsMWSHXRAeKLaYp7eQnjdHEH2lPSDMRO77bCs',1,'192.168.11.130'),(99,'2020-10-25 20:57:51','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQzLCJ1c2VybmFtZSI6ImxpbWE4ODgiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM2NTU4NDUsImV4cCI6MTYwMzY2MTQ0NX0.YOuNNHnLO2Ed6zm2ZI9r4DYJnKllaWt69EgXOC5n1Mo',43,'192.168.11.130'),(100,'2020-10-25 20:58:15','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NTU4NjksImV4cCI6MTYwMzY2MTQ2OX0.pz8EhloUVuaHYrUm2dwaxAeJXTDvWorxYuOWYHV8wIg',1,'192.168.11.130'),(101,'2020-10-25 20:58:25','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQzLCJ1c2VybmFtZSI6ImxpbWE4ODgiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM2NTU4NzksImV4cCI6MTYwMzY2MTQ3OX0.9KtCXaVdmuPu8HGDLoeiF-QuQ-h5ou4spgaJ8oI3u1Y',43,'192.168.11.130'),(102,'2020-10-25 21:00:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NTU5ODcsImV4cCI6MTYwMzY2MTU4N30.Pz0XlG3MwnOeqx2gL60CVLA86canvvQpDbxfH4wDcf0',1,'192.168.11.130'),(103,'2020-10-25 21:00:21','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NTU5OTUsImV4cCI6MTYwMzY2MTU5NX0.AI-Zu2gREqECa65Ln7jqA7bsda78GJFERA4HlfuGaMM',1,'192.168.11.130'),(104,'2020-10-25 22:01:48','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY1NjExMCwiZXhwIjoxNjAzNjYxNzEwfQ.2wEuKIORLDAcNKR23amSReHNlOZCt5OtN2n4view7Vo',4,'192.168.0.102'),(105,'2020-10-25 22:17:56','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM2NjA2NTAsImV4cCI6MTYwMzY2NjI1MH0.f3ta8EiZCw4EAc6ogw8OzYHiypNTTQfA-a8cf_I142M',1,'192.168.11.130'),(106,'2020-10-25 23:35:52','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY2MTc1MywiZXhwIjoxNjAzNjY3MzUzfQ.ur0zR-w8TH1UcB_477WvF0ul0UjuT11q9Hq1aAr4J7w',4,'192.168.0.102'),(107,'2020-10-26 00:06:32','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY2MzU5MiwiZXhwIjoxNjAzNjY5MTkyfQ.WWTvrC-XL8b8VeD5FtICiuB_k2Y0M3O6Q7pp5iL8f-E',4,'192.168.0.102'),(108,'2020-10-26 00:11:56','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY2MzkxNywiZXhwIjoxNjAzNjY5NTE3fQ.60sXR4dg0VMV9M2a06lij56o4QA-IRojf3Eteg_JZQg',4,'192.168.0.102'),(109,'2020-10-26 02:08:46','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY3MDkyNiwiZXhwIjoxNjAzNjc2NTI2fQ.9RHroMLRTPSrKChND5wIUCMucpt0IAuW3jdWs5rSJVk',4,'192.168.0.102'),(110,'2020-10-26 03:35:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY3NjEwMywiZXhwIjoxNjAzNjgxNzAzfQ.85mFkJkHMtMVoSk-CCmHFIfs48LR4roj0jKIuoaKV4o',4,'192.168.0.102'),(111,'2020-10-26 04:11:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY3ODI4NywiZXhwIjoxNjAzNjgzODg3fQ.NDJN0K67shw389xRqpKWA9JITcC0Fs7XI-RC4xqi2vg',4,'192.168.0.102'),(112,'2020-10-26 10:11:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzY5OTg5OCwiZXhwIjoxNjAzNzA1NDk4fQ.qOb6VUFjxh1GOGYG9pANQ8eMyRrN6LuMzPbI48niJow',4,'192.168.0.102'),(113,'2020-10-26 10:54:20','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUzLCJ1c2VybmFtZSI6ImxvdmVqb3kiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM3MDI0NjEsImV4cCI6MTYwMzcwODA2MX0.TZeVPiYXG-9-RmI5YuPp5HU85sliuNuRU3gCPGz4PQQ',53,'192.168.11.130'),(114,'2020-10-26 11:45:08','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzcwNTUwOSwiZXhwIjoxNjAzNzExMTA5fQ.tuWddiOCZwJZlN1Vqafw-xvbpbJyrmVoo5agfwpcl4c',4,'192.168.0.102'),(115,'2020-10-26 11:55:00','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3MDYxMDEsImV4cCI6MTYwMzcxMTcwMX0.08_u00pSSFkReGfUdXZNpEudLsVg3JQLkWT0UOhaPHs',1,'192.168.11.130'),(116,'2020-10-26 11:55:11','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3MDYxMTIsImV4cCI6MTYwMzcxMTcxMn0.1mQ2zlhEb4sAiuZWpA0uKRp8Vw9ki_Td9wvOG7Db8UE',1,'192.168.11.130'),(117,'2020-10-26 11:55:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjU4LCJ1c2VybmFtZSI6ImFsYWJhbGExMTEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM3MDYxMjgsImV4cCI6MTYwMzcxMTcyOH0.ABfW0mcXnSfzHnBAJruj-u0m9jamItZf5bjvmqCq-7I',58,'192.168.11.130'),(118,'2020-10-26 11:56:57','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjU5LCJ1c2VybmFtZSI6ImFsYWJhbGExMTExMSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzcwNjIxNywiZXhwIjoxNjAzNzExODE3fQ.3BWZmd5Vpkj1n3hFnT63r8q9JVwIMivnF-eecZY7hr8',59,'192.168.11.130'),(119,'2020-10-26 11:57:59','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjYwLCJ1c2VybmFtZSI6ImFzZGYxIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzNzA2MjgwLCJleHAiOjE2MDM3MTE4ODB9.g3MTk1tSXKjsbQUzIkSz7s5gJx0CFof_rXWLPrxEpfM',60,'192.168.11.130'),(120,'2020-10-26 12:15:25','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3MDczMjYsImV4cCI6MTYwMzcxMjkyNn0.y80miDmOdmiist13p-cw2xAfk81ZLifo-_5g_dZc5aU',1,'192.168.11.130'),(121,'2020-10-26 12:15:42','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjYxLCJ1c2VybmFtZSI6ImxpbWExMTEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM3MDczNDMsImV4cCI6MTYwMzcxMjk0M30.JnlhubZvgOpYgOlpqqeRYGV7dDYlLY-gTvRkNnCLxKY',61,'192.168.11.130'),(122,'2020-10-26 12:16:15','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjYyLCJ1c2VybmFtZSI6ImxpbWE3ODkiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM3MDczNzYsImV4cCI6MTYwMzcxMjk3Nn0.Eh2Xrq5wIb73Pj6z0GLtHkb-VppzCQFcuHbSdHaWR-c',62,'192.168.11.130'),(123,'2020-10-26 12:21:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3MDc2ODgsImV4cCI6MTYwMzcxMzI4OH0.norGh2EfGM0ManlZuzUGZ0iVvaRAByKQqthGKnx72GQ',1,'192.168.11.130'),(124,'2020-10-26 12:24:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3MDc4NDMsImV4cCI6MTYwMzcxMzQ0M30.zaojArhHwsS6Jp_Yd9B5CqWK4BeEeK-qBTC9EE6_ELE',1,'192.168.11.130'),(125,'2020-10-26 13:52:56','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzcxMzE3NywiZXhwIjoxNjAzNzE4Nzc3fQ.L25lh7TcLeFXBWf7STkGJNaq2OaKEP_5uahHY961XtE',4,'192.168.0.102'),(126,'2020-10-26 13:54:10','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzcxMzI1MCwiZXhwIjoxNjAzNzE4ODUwfQ.h6sz5habD5tI4fpUfXJp0-ljP3wIiiK51VzHy7q6rQ0',4,'192.168.0.102'),(127,'2020-10-26 14:31:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzcxNTQ4OSwiZXhwIjoxNjAzNzIxMDg5fQ.EuhODhqhsqNRkeiOJsLtyN48ZREPm1RkeOh8asvYCb0',4,'192.168.0.102'),(128,'2020-10-26 15:03:08','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzcxNzM4OSwiZXhwIjoxNjAzNzIyOTg5fQ.d3Vv6SNFbc4NezLx1PLAZq9uKM_u6jvAg4DatgoS-Zk',4,'192.168.0.102'),(129,'2020-10-26 16:19:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzcyMTk0NiwiZXhwIjoxNjAzNzI3NTQ2fQ.X-dHIqj7UX-CFLecbGoVZacuxnQs9v530Jg-ts929C0',4,'192.168.0.102'),(130,'2020-10-26 16:40:35','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzcyMzIzNiwiZXhwIjoxNjAzNzI4ODM2fQ.6B9Atctaq3k9M8_7r8fF0j4_y1yJzuiRMyfqvUDvETI',4,'192.168.0.102'),(131,'2020-10-26 18:25:16','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3Mjk1MTYsImV4cCI6MTYwMzcyOTU0Nn0.MSS5sN6UUftnTEpmfguzkl-muc_ZZF7xM8ZsBGndJy8',1,'192.168.11.130'),(132,'2020-10-26 18:25:48','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjYzLCJ1c2VybmFtZSI6ImFkbWluMTIzNDUiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM3Mjk1NDksImV4cCI6MTYwMzcyOTU3OX0.ksP6gk95_tue__y_hmfoFaE7cZ8r72YcP8omMQohf8E',63,'192.168.11.130'),(133,'2020-10-26 18:25:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzcyOTU1NCwiZXhwIjoxNjAzNzM1MTU0fQ.GqoyUsfTf7oAu-Tseeivj7f00B2tKBvN9lcLfCKV0ZA',4,'192.168.0.102'),(134,'2020-10-26 18:26:17','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3Mjk1NzgsImV4cCI6MTYwMzcyOTYwOH0.HZiUtcWk16L9i1kInR2xHpkeA4OxG32MwITqu1UGAiE',1,'192.168.11.130'),(135,'2020-10-26 18:28:14','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3Mjk2OTQsImV4cCI6MTYwMzcyOTcyNH0.aMNCdhCNRPp28hSUVfI5kCCuDgGnkHlzRKjRN0jqPlw',1,'192.168.11.130'),(136,'2020-10-26 18:30:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3Mjk4MDgsImV4cCI6MTYwMzcyOTgzOH0.ooJ0MbiFkw-Rznt89i0aw9kPtpY9KtCYzVgtau1OcyQ',1,'192.168.11.130'),(137,'2020-10-26 18:33:34','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3MzAwMTUsImV4cCI6MTYwMzczMDA0NX0.-GDgQPbvKAdKfMuIPX32r85sUIiA-KIra0vYQ_hgiZ4',1,'192.168.11.130'),(138,'2020-10-26 18:38:21','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3MzAzMDIsImV4cCI6MTYwMzczMDMzMn0.yfmWaOn48FgSztmf2YR4_9pj-t9edVDyCdlDfP3YePk',1,'192.168.11.130'),(139,'2020-10-26 18:40:43','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3MzA0NDQsImV4cCI6MTYwMzczNDA0NH0.uhJSZu_ANqNdm741SlWKQ_j7YkyoIFgQT2o1_vqZCJ0',1,'192.168.11.130'),(140,'2020-10-26 18:59:55','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3MzE1OTUsImV4cCI6MTYwMzczNTE5NX0.Q9ZvaDGrxWWYapSzxhhKu_098Oi79W-bt1ltxjZRs4k',1,'192.168.11.130'),(141,'2020-10-26 19:38:48','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzczMzkyOSwiZXhwIjoxNjAzNzM5NTI5fQ.e7Tz1Hft3w_MZqVT2_eTa4NiZdDjeralc32E42h217k',4,'192.168.0.102'),(142,'2020-10-26 19:44:27','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3MzQyNjgsImV4cCI6MTYwMzczNzg2OH0.ENJeAXe_6oC8X-AM7F36Q8Q9Ejjo83t6IDSjDXW5rPs',1,'192.168.11.130'),(143,'2020-10-26 20:11:51','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzczNTkxMSwiZXhwIjoxNjAzNzQxNTExfQ.bkdlabECoiy3kkMA86spdSf-dY5UOxJRGfD8LdwiO3g',4,'192.168.0.102'),(144,'2020-10-26 20:44:53','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3Mzc4OTMsImV4cCI6MTYwMzc0MTQ5M30.xiYfLnYFqD62QfZ6-uo1adl4W8R2ClUl0XnVj5ztbi4',1,'192.168.11.130'),(145,'2020-10-26 21:45:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3NDE1MDMsImV4cCI6MTYwMzc0NTEwM30.u58zxQWKj75uC8omtSl9v4M78x-JdOres0r-EHYIIWg',1,'192.168.11.130'),(146,'2020-10-26 22:28:59','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzc0NDEzOSwiZXhwIjoxNjAzNzQ5NzM5fQ.-Xjrt2yvYG8oxkGcSqeOcwvmrABQg3pJFyuexKMizKM',4,'192.168.0.102'),(147,'2020-10-27 00:17:06','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3NTA2MjcsImV4cCI6MTYwMzc1NjIyN30.TorGeNaqAkC1gipKUzYOKcT8-zb75oEHEg3WISm42BA',1,'192.168.0.102'),(148,'2020-10-27 00:38:48','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzc1MTkyOSwiZXhwIjoxNjAzNzU3NTI5fQ.2pf7Qkxh4kffxt2LTf-YN0vh1-5OoRsi4lF2bdMtU28',4,'192.168.0.102'),(149,'2020-10-27 11:02:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzc4OTMyNywiZXhwIjoxNjAzNzk0OTI3fQ.5h1FC3hzZj2eYOdwYFamewIk3XXYFPgOveXkbb1UtUc',4,'192.168.0.102'),(150,'2020-10-27 11:12:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3ODk5MjcsImV4cCI6MTYwMzc5MzUyN30.MYPpblxEPqfp6Ot_ydGT0bHuWxDFBtQTrrdLB_qVeBE',1,'192.168.11.130'),(151,'2020-10-27 11:27:31','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3OTA4NTEsImV4cCI6MTYwMzc5NDQ1MX0.aiY5pVIJsj_9gpHu14iZGyZbTkCinL7QZ-7NuZXqhpc',1,'192.168.11.130'),(152,'2020-10-27 12:46:14','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3OTU1NzQsImV4cCI6MTYwMzc5OTE3NH0.vXEWgfSJo7-F_AXNZU1yPompYjAJJLpKZRlhJuBFnEY',1,'192.168.11.130'),(153,'2020-10-27 13:46:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM3OTkxOTksImV4cCI6MTYwMzgwMjc5OX0.sb9OtHirKlxMFjEpW02I9Qn8dm_fQDMxZziqq5Br4X4',1,'192.168.11.130'),(154,'2020-10-27 13:50:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzc5OTQwNiwiZXhwIjoxNjAzODA1MDA2fQ.izHvePBA2oM2yiSdVpW01qaYQ-26CmASwDIDTYnSpD0',4,'192.168.0.102'),(155,'2020-10-27 15:20:25','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MDQ4MjUsImV4cCI6MTYwMzgwODQyNX0.q0GexymhfZHLuYcO8A7yzOOQoL0SO3ygRtSvIMzv_s8',1,'192.168.11.130'),(156,'2020-10-27 15:31:51','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzgwNTUxMSwiZXhwIjoxNjAzODExMTExfQ.GsI5fLqBJmLyGAzmVvLTxJ9ql9AAgGqGlQJ-p5YAdgU',4,'192.168.0.102'),(157,'2020-10-27 16:33:44','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MDkyMjQsImV4cCI6MTYwMzgxMjgyNH0.ijFKW-PSPVcIh8ONhoF5xF59ckx_3sNhCKX1qO702Ws',1,'192.168.11.130'),(158,'2020-10-27 17:34:23','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MTI4NjMsImV4cCI6MTYwMzgxNjQ2M30.wJ173pvvyq1wU-QQeplCV6A1VQdHXimmOewJ9AyeZAU',1,'192.168.11.130'),(159,'2020-10-27 17:47:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzgxMzY2MCwiZXhwIjoxNjAzODE5MjYwfQ.hIfnN3HNOQ1DC0V0d4-SoCDerKKZJffGko0OXdQPrko',4,'192.168.0.102'),(160,'2020-10-27 18:34:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MTY0NzksImV4cCI6MTYwMzgyMDA3OX0.PrYYqx4IvxvYARpgpWf8BvK97ng3yZYLdnD38__7qQ8',1,'192.168.11.130'),(161,'2020-10-27 19:21:52','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzgxOTMxMiwiZXhwIjoxNjAzODI0OTEyfQ.8GJjZ8Jjtg8-bswBsKj7-JcTT3VeDItw-Gj6-wC90wY',4,'192.168.0.102'),(162,'2020-10-27 20:24:44','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MjMwODQsImV4cCI6MTYwMzgyMzExNH0.jOKqIAnjoFHSIxJhzhx-y0CD7bEL1ZO6PeLvDKCzxRM',1,'192.168.11.130'),(163,'2020-10-27 20:27:34','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MjMyNTQsImV4cCI6MTYwMzgyMzI4NH0.qxnM98a7tQRAglj9ORh250wFqoPNuxlXsfi95ezwb3s',1,'192.168.11.130'),(164,'2020-10-27 20:44:55','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MjQyOTUsImV4cCI6MTYwMzgyNDMyNX0.kKG9nKO5ZoorBRoWSrLbvxjdnUYGc1wUnPH6FjJrt_Q',1,'192.168.11.130'),(165,'2020-10-27 20:45:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MjQzNTQsImV4cCI6MTYwMzgyNDM4NH0.ODviqGkkEwy1Msm7DSWrieGNw54L2G5EZenuIwDpEAc',1,'192.168.11.130'),(166,'2020-10-27 20:46:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MjQ0MTQsImV4cCI6MTYwMzgyNDQ0NH0.GHOVOYDzhlDoOfKzLYOqiJhp_eO6ziFI9CR8RgvRb-8',1,'192.168.11.130'),(167,'2020-10-27 20:55:36','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzgyNDkzNywiZXhwIjoxNjAzODMwNTM3fQ.cv-rIitlZUAJx3h2YmgFNHcshXJx3b9fIXwQdI4zFP4',4,'192.168.0.102'),(168,'2020-10-27 21:05:56','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzgyNTU1NywiZXhwIjoxNjAzODMxMTU3fQ.Dl0Yh35uDLq2-laLgHFyiJpQc7LB65hpJEOfVC2aD8E',4,'192.168.0.102'),(169,'2020-10-27 21:11:57','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MjU5MTcsImV4cCI6MTYwMzgyOTUxN30.HLKZuqnI1jvHnMyLpm8ncem2vWRuWu5CnuEHrjHZRk0',1,'192.168.11.130'),(170,'2020-10-27 21:31:32','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzgyNzA5MiwiZXhwIjoxNjAzODMyNjkyfQ.zwVvXt7Y9wC8C2sVQZ-aQqGqMqYOUuQAZn5os1dHUgQ',4,'192.168.0.102'),(171,'2020-10-27 21:43:23','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4Mjc4MDMsImV4cCI6MTYwMzgzMzQwM30.Vvgi-fClAapNbqhuIywzj0C6NAnO7vIK0FGAeNQLDi0',1,'192.168.0.102'),(172,'2020-10-27 21:47:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MjgwNDAsImV4cCI6MTYwMzgzMzY0MH0.cqRJJiKJjfi9g2dV1rWzIBX4kitUochoQ58YgCaS14M',1,'192.168.0.102'),(173,'2020-10-27 21:52:27','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4MjgzNDYsImV4cCI6MTYwMzgzMTk0Nn0.tZCFYIKpqLrwaXy5CGYm0cMfmZP2tNDJLqCy-S4929U',1,'192.168.11.130'),(174,'2020-10-27 21:59:01','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4Mjg3NDEsImV4cCI6MTYwMzgzNDM0MX0.rZXUCu9WKMvBSdTjnpHaoPr-bSO3sQjRl0ppVcBnSFY',1,'192.168.0.102'),(175,'2020-10-27 22:57:25','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzgzMjI0NSwiZXhwIjoxNjAzODM3ODQ1fQ.m-JT4kL8lTHwa4RWh8LgM_DelfUSAeJryKrU6NKkoVo',4,'192.168.0.102'),(176,'2020-10-28 00:31:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzgzNzg4NiwiZXhwIjoxNjAzODQzNDg2fQ.L6wQt-nroMx8KAzCv8-pqPXKLDBQjF1jVD7kJbZXvRE',4,'192.168.0.102'),(177,'2020-10-28 02:00:29','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4NDMyMjksImV4cCI6MTYwMzg0ODgyOX0.RqcEw3BpViLP4qgtiyT5KxfGM8UcnkJ1sP-UB5B7weA',1,'192.168.0.102'),(178,'2020-10-28 02:09:50','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzg0Mzc5MCwiZXhwIjoxNjAzODQ5MzkwfQ.lbj2rOqxguw6YX_GgqZ5qA-qeE5qlMxeu3TqqJB3XRQ',4,'192.168.0.102'),(179,'2020-10-28 03:09:47','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzg0NzM4NywiZXhwIjoxNjAzODUyOTg3fQ.vH6EZ6qccP6G0RVHh0M73Iu0yGpB3aWIMwnwRxerRD4',4,'192.168.0.102'),(180,'2020-10-28 10:10:46','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4NzI2NDcsImV4cCI6MTYwMzg3ODI0N30.2jXgoKrXUWCzlh1KnuKtBWECy4c1KTv-qaxJoqK_Aas',1,'192.168.0.102'),(181,'2020-10-28 10:21:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzg3MzI4NiwiZXhwIjoxNjAzODc4ODg2fQ.ID3ZteR3XCvMgQDVWEexl8-6IC5cAis9RKjHMw1Q4vE',4,'192.168.0.102'),(182,'2020-10-28 11:09:25','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4NzYxNjQsImV4cCI6MTYwMzg3OTc2NH0.FPv1o7IRkDHhCxxDnxZCxeRnm5Bnx7dxId0XoFGc-pw',1,'192.168.11.130'),(183,'2020-10-28 11:39:36','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjY0LCJ1c2VybmFtZSI6ImxpZmVzdHlsZTEyMyIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzg3Nzk3NCwiZXhwIjoxNjAzODgxNTc0fQ.cBSW3vkmybypnsN3bQ2mOvtrkEsEs2cf3b-bmz0aq1I',64,'192.168.11.130'),(184,'2020-10-28 11:40:32','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjY1LCJ1c2VybmFtZSI6ImxpZmVzdHlsZTExIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzODc4MDMwLCJleHAiOjE2MDM4ODE2MzB9.51gD5AJK2RX_z7VGPYgsGRBXzWq9QGbGaVsSHajHCb0',65,'192.168.11.130'),(185,'2020-10-28 11:50:31','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4Nzg2MzIsImV4cCI6MTYwMzg4NDIzMn0.MbeUelqGglv0aTcezOVqFlTRJLU9sBd0cFFBc9T5Ttw',1,'192.168.0.102'),(186,'2020-10-28 12:19:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzg4MDM1OSwiZXhwIjoxNjAzODg1OTU5fQ.sACmQTWFgEgWe0l4QvGHaEZ5aCcUGhWfQljwnhbmalw',4,'192.168.0.102'),(187,'2020-10-28 12:27:04','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjY1LCJ1c2VybmFtZSI6ImxpZmVzdHlsZTExIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzODgwODIyLCJleHAiOjE2MDM4ODQ0MjJ9.tSTxYxmGZJIuP0vwfD14aH1zwSYxg933Pt4pqcr7xpw',65,'192.168.11.130'),(188,'2020-10-28 13:38:10','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzg4NTA5MSwiZXhwIjoxNjAzODkwNjkxfQ.P_IkzdpylgnDYqqjOFtyvlDte1UCVCvmUJxt5GhKtx4',4,'192.168.0.102'),(189,'2020-10-28 13:59:53','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjY2LCJ1c2VybmFtZSI6IkJ1YmFtYXJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzODg2MzkzLCJleHAiOjE2MDM4OTE5OTN9.jVWH-jNrLLh2RDELcMxFo1XaKPDaqpFoDu3KnJmdoRY',66,'192.168.0.102'),(190,'2020-10-28 14:41:00','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzg4ODg2MSwiZXhwIjoxNjAzODk0NDYxfQ.DocM5LzIWc8168BoMEes6izW7aD4xIzn7RYKu5W6wZ4',4,'192.168.0.102'),(191,'2020-10-28 15:30:42','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzg5MTg0MiwiZXhwIjoxNjAzODk3NDQyfQ.tCMrtuz6IRGEsWllKEmQnf8mX8Xpo9RGbLhph_QCBSk',4,'192.168.0.102'),(192,'2020-10-28 15:47:21','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4OTI4MzksImV4cCI6MTYwMzg5NjQzOX0.iKMVUX7BTfphpOnUbB5RtIpBkboH4C4hp0d2kTwjA0w',1,'192.168.11.130'),(193,'2020-10-28 16:17:17','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzg5NDYzOCwiZXhwIjoxNjAzOTAwMjM4fQ.UXsEBmWnNoqh65xQmyMj2jAoXuJRwk4fboC6oDicOuU',4,'192.168.0.102'),(194,'2020-10-28 16:19:57','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDM4OTQ3OTgsImV4cCI6MTYwMzkwMDM5OH0.ToxNNsHbC9WdzjqpclvOvX8dFiJcgg7SW4cOZ2PTUMo',2,'192.168.0.102'),(195,'2020-10-28 17:35:21','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzg5OTMyMSwiZXhwIjoxNjAzOTA0OTIxfQ.HU893kDpAeRWhn5vwSVslyiqJ01EZmU03cMJzxoD3hQ',4,'192.168.0.102'),(196,'2020-10-28 17:44:18','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM4OTk4NTYsImV4cCI6MTYwMzkwMzQ1Nn0.k0F8XcwDTgA0yJLzre0k4xmDwfK5t0u4pZ-8pWi98yQ',1,'192.168.11.130'),(197,'2020-10-28 18:44:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MDM0NjQsImV4cCI6MTYwMzkwNzA2NH0.Kou_NsxMl1oOEAZj4aI_8BhPRY8AqEtbX1kU5argkxg',1,'192.168.11.130'),(198,'2020-10-28 18:55:32','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjY3LCJ1c2VybmFtZSI6ImFzZDEyMzQ1NiIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzkwNDEzMSwiZXhwIjoxNjAzOTA3NzMxfQ.wxis0WwazqUkkueI7tXempRXKOCN8-B4lOcP4iv4b0M',67,'192.168.11.130'),(199,'2020-10-28 19:07:08','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MDQ4MjcsImV4cCI6MTYwMzkwODQyN30.U_teXmG98k3_Sf1VKdLnVH0zh-D3uWND6UrHrH5Rb5w',1,'192.168.11.130'),(200,'2020-10-28 19:08:47','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzkwNDkyNywiZXhwIjoxNjAzOTEwNTI3fQ.sDikNKoxq8GbS04QcHow7WKuwGy2VQoKIH-1O-aJ_DY',4,'192.168.0.102'),(201,'2020-10-28 19:22:52','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MDU3NzIsImV4cCI6MTYwMzkxMTM3Mn0.sma8jCEfIv1bvgRAbbgKrtgRrUqqzfT8-gGf0JKR1qM',1,'192.168.0.102'),(202,'2020-10-28 19:24:33','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzkwNTg3MywiZXhwIjoxNjAzOTExNDczfQ.vctVGL5kgLQE9TNeFLOq9NqCsqoy5DeUu5yeYAN8DnI',4,'192.168.0.102'),(203,'2020-10-28 20:10:44','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MDg2NDMsImV4cCI6MTYwMzkxMjI0M30.ahsJ1L97UiGc25jqx85hOn86d8bGgi2MYn1qtmxtAG8',1,'192.168.11.130'),(204,'2020-10-28 20:21:29','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MDkyODksImV4cCI6MTYwMzkxNDg4OX0.2ubZwcKXOok4vP8_gS-MhfTgEWqHRWC3kEHdWOZESPw',1,'192.168.0.102'),(205,'2020-10-28 20:24:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzkwOTQ1MywiZXhwIjoxNjAzOTE1MDUzfQ.Ihl2NttX6EqZlLUAUBt2oZAGutdzAMCaeoA_Fc5wflg',4,'192.168.0.102'),(206,'2020-10-28 21:13:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MTIzODEsImV4cCI6MTYwMzkxNTk4MX0.2bv3LOxQIKp-rB2zrQSm3Dq9Mrn4cmgkaV7gxUg5jd4',1,'192.168.11.130'),(207,'2020-10-28 21:18:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzkxMjY4MiwiZXhwIjoxNjAzOTE4MjgyfQ.ZnC588NZUCpPEPY8zcrdtMBvmkOncYT9_LQ7LvmPdX8',4,'192.168.0.102'),(208,'2020-10-28 21:32:46','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MTM1NjYsImV4cCI6MTYwMzkxOTE2Nn0.EkWt0w73a7qdxrujah7HgwvHHPWbUIGmddFN7H81vPY',1,'192.168.0.102'),(209,'2020-10-28 21:33:18','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzkxMzU5OSwiZXhwIjoxNjAzOTE5MTk5fQ.R0dYDM5w-CqHEeLDa2ZPRlyYP1dl96ARo_FrMiCnRFY',4,'192.168.0.102'),(210,'2020-10-28 21:59:47','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzkxNTE4NywiZXhwIjoxNjAzOTIwNzg3fQ.EeNup2QfxM-rMGyTtCM7XJ-6mV1nvgIkX594Nm07CKw',4,'192.168.0.102'),(211,'2020-10-28 22:16:31','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MTYxODksImV4cCI6MTYwMzkxOTc4OX0.tzAtiqR2mn3lCjI41SokQ9gL4f2h6HKwvuTQb3worEE',1,'192.168.11.130'),(212,'2020-10-28 23:16:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MTk3OTcsImV4cCI6MTYwMzkyMzM5N30.Tt12HesCLk59TJx4n0yt19RPgzshXH_S6_EQSD5wTC4',1,'192.168.11.130'),(213,'2020-10-28 23:30:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDM5MjA2MjAsImV4cCI6MTYwMzkyNjIyMH0.D14Gbnev8z4bJgX45iPDByFETDrj6EphJX6TRl2nq4c',2,'192.168.0.102'),(214,'2020-10-28 23:51:08','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzkyMTg2OSwiZXhwIjoxNjAzOTI3NDY5fQ.G7FT2hu9vjXAFXYC7T1sgpsHEOb5IQZVmFbKRN8IBaE',4,'192.168.0.102'),(215,'2020-10-29 00:16:46','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MjM0MDQsImV4cCI6MTYwMzkyNzAwNH0.8KMH11k8gcjD8iTy31JU_btMD2P97gphGMSDV8VWoOw',1,'192.168.11.130'),(216,'2020-10-29 00:36:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzkyNDU2NCwiZXhwIjoxNjAzOTMwMTY0fQ.QVR8ABsFDUpO2mMi-NMgrQH0SebDEV54t9zotmYaJVo',4,'192.168.0.102'),(217,'2020-10-29 01:17:11','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MjcwMjksImV4cCI6MTYwMzkzMDYyOX0.BBqZX7vICuzN-Gd598yW3HSUg4kpfbIBFKmOd440QsI',1,'192.168.11.130'),(218,'2020-10-29 02:17:17','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MzA2MzUsImV4cCI6MTYwMzkzNDIzNX0.qbeeKx62FMuXys69Cdb212GZr1D5_0UFLcuTWjbwBIM',1,'192.168.11.130'),(219,'2020-10-29 03:17:38','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5MzQyNTYsImV4cCI6MTYwMzkzNzg1Nn0.zr2C7u3W-yNtUgS49RBrYoY_JetAaox_cd1SN6XX6PM',1,'192.168.11.130'),(220,'2020-10-29 10:50:49','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk2MTQ0OSwiZXhwIjoxNjAzOTY3MDQ5fQ.Clkfc3gW2T3X3Fl0EYN6k10e114F6wo5wJvUNkK-yfE',4,'192.168.0.102'),(221,'2020-10-29 10:51:24','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5NjE0ODIsImV4cCI6MTYwMzk2NTA4Mn0.8ZgVidzhPfpSzwFOjF29esDZLLoTo-TFv_N10Abm_W0',1,'192.168.11.130'),(222,'2020-10-29 12:22:00','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5NjY5MTksImV4cCI6MTYwMzk3MDUxOX0.ZTC4rEEuRUaAk0nCYAhsc5OlrypL9DAtY-9vJOx7tBU',1,'192.168.11.130'),(223,'2020-10-29 12:31:58','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5Njc1MTYsImV4cCI6MTYwMzk3MTExNn0.Dt6C8jVtYn5s4ZyqOs16Jd9eIabH_RfJNiAkcL1FGCc',1,'192.168.11.130'),(224,'2020-10-29 12:51:11','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk2ODY3MSwiZXhwIjoxNjAzOTc0MjcxfQ.RYrNrwx3y5U5axmaREHGD6393LSe25iEJf3a4cclJ5I',4,'192.168.0.102'),(225,'2020-10-29 13:39:51','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5NzE1ODksImV4cCI6MTYwMzk3NTE4OX0.BJOo5wHYbk3_W0_3fh-JlIPKwbUhggK8gAvn3qMv5Is',1,'192.168.11.130'),(226,'2020-10-29 14:00:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5NzI4MTEsImV4cCI6MTYwMzk3NjQxMX0.4hB7Vpog2JZpsX2loK0jhHh2SO7hTMY-KuMsOe4PxAs',1,'192.168.11.130'),(227,'2020-10-29 14:29:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5NzQ1NTgsImV4cCI6MTYwMzk3ODE1OH0.__1ty2AxZR8fDTAgCqzf-pFTtQNtYCFkElvGij4Zkk0',1,'192.168.11.130'),(228,'2020-10-29 14:51:11','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk3NTg3MSwiZXhwIjoxNjAzOTgxNDcxfQ.j25HDK95kRbC14W_w9DYayhciUfhIqkdN9yv_2rp27U',4,'192.168.0.102'),(229,'2020-10-29 15:29:43','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5NzgxODEsImV4cCI6MTYwMzk4MTc4MX0.QsDjF90jicI_KSEZE56FK2_e34Ln-LMGbG38npM_FZQ',1,'192.168.11.130'),(230,'2020-10-29 15:39:41','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk3ODc4MSwiZXhwIjoxNjAzOTg0MzgxfQ.-8nQIjZ8v4W0BBCASWZ7zRDwN_XQ_6ctHSqrmuEro_w',4,'192.168.0.102'),(231,'2020-10-29 15:50:12','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk3OTQxMiwiZXhwIjoxNjAzOTg1MDEyfQ.scYP6LY01dAs6kTeroLgjn9NHJVfcQOnQDGqyXUhMyQ',4,'192.168.0.102'),(232,'2020-10-29 15:55:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjY4LCJ1c2VybmFtZSI6Ik1hcmlpYVBldHJvdmFNaW1haWxvdmEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDM5Nzk3MDMsImV4cCI6MTYwMzk4NTMwM30.Y6ija9AmNYGCncPV2debn7oQDKYXU46DngyAH9BcXY4',68,'192.168.0.102'),(233,'2020-10-29 15:55:48','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk3OTc0OCwiZXhwIjoxNjAzOTg1MzQ4fQ.O4E3bHFayGgVDJ5HUWnkZWeHlwO5IA2o-my-p_Mcilc',4,'192.168.0.102'),(234,'2020-10-29 16:03:31','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5ODAyMTEsImV4cCI6MTYwMzk4NTgxMX0.idG_typoTYqNRGSXhDuOyP9AbXImiaM9GYK04QkftuY',1,'192.168.0.102'),(235,'2020-10-29 16:05:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk4MDMyMiwiZXhwIjoxNjAzOTg1OTIyfQ.qyVL6FcUgQOQ_IQ5Pthhvl8FIAprsVPMu0Vkcqa-SRQ',4,'192.168.0.102'),(236,'2020-10-29 16:19:36','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk4MTE3NiwiZXhwIjoxNjAzOTg2Nzc2fQ.cnvQRsO_3oM3sbfYkMB0KkFoc9PQtWG2HFlEsOdkE9w',4,'192.168.0.102'),(237,'2020-10-29 17:09:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzOTg0MTk0LCJleHAiOjE2MDM5ODk3OTR9.xZTGlB0v5PocklmwAB459jvJbFGLisy1lcqJgQhJo_c',5,'192.168.0.102'),(238,'2020-10-29 17:22:25','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk4NDk0NSwiZXhwIjoxNjAzOTkwNTQ1fQ.GvDPjUIKyyrsVTQtpMAp8N66LrWodFnvDUfcCQWicPQ',4,'192.168.0.102'),(239,'2020-10-29 17:32:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5ODU1NTcsImV4cCI6MTYwMzk5MTE1N30.6XxLICT30VQTjpAn59TnVIITgW7Pmer6MWhzMNq29ko',1,'192.168.0.102'),(240,'2020-10-29 17:32:56','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzOTg1NTc2LCJleHAiOjE2MDM5OTExNzZ9.2x4e-m1x_lsV-83qiQ8xQAHJWT-wtoti-ZNrOpUGBfo',5,'192.168.0.102'),(241,'2020-10-29 17:43:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzOTg2MjM0LCJleHAiOjE2MDM5OTE4MzR9.vWrhquLcXOQScP00yQ7y_hbTUZDJBJrka2Gg6LyvQfk',5,'192.168.0.102'),(242,'2020-10-29 17:53:00','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzOTg2NzgwLCJleHAiOjE2MDM5OTIzODB9.jKR15zsx6yHi0r0Yt0Jgc5ysN2ZFRiW0KlzYNDiBmMQ',5,'192.168.0.102'),(243,'2020-10-29 18:05:47','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk4NzU0NywiZXhwIjoxNjAzOTkzMTQ3fQ.CrhUGXJsrWsNd3Xq7loo8STfujx5f6OuKEUoJKXkS1k',4,'192.168.0.102'),(244,'2020-10-29 18:06:14','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk4NzU3NCwiZXhwIjoxNjAzOTkzMTc0fQ.3KMVx9NZ8ySVD2z44HKkLBbX3mHYsm2EugSbwte2avo',4,'192.168.0.102'),(245,'2020-10-29 18:32:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5ODkxMjMsImV4cCI6MTYwMzk5NDcyM30.LMKXdtBz8EpU6x8IrYVNLQLdHF6BKqPJdVFQNvPFvOc',1,'192.168.0.102'),(246,'2020-10-29 18:34:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5ODkyNzksImV4cCI6MTYwMzk5NDg3OX0.4yQ7QVoBk5dqRm7mz7DmLDwbE1Dh-RW-KhqNu4TuJ04',1,'192.168.0.102'),(247,'2020-10-29 18:34:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwMzk4OTI5NCwiZXhwIjoxNjAzOTk0ODk0fQ.PxIJiBXhNNf4AL_DKT37RwPevovSwb9TwiFWkZ6lkiI',4,'192.168.0.102'),(248,'2020-10-29 18:53:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDM5OTA0MTksImV4cCI6MTYwMzk5NjAxOX0.IzuYpW6TF3JhT9Kar389A7SUq5M2MG-Do7Np0jySIhg',3,'192.168.0.102'),(249,'2020-10-29 19:22:20','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjAzOTkyMTQwLCJleHAiOjE2MDM5OTc3NDB9.CbDeYv18CQ8NKFI9ToJlh6oUxDzAy6CbPrI3iZpEmBQ',5,'192.168.0.102'),(250,'2020-10-29 20:29:42','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDM5OTYxODAsImV4cCI6MTYwMzk5OTc4MH0.n4jieMYfFaSbs2GhxOBDWpVTUy9SXQa_SUE7PT9AReU',1,'192.168.11.130'),(251,'2020-10-29 23:00:52','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDAwNTI1MiwiZXhwIjoxNjA0MDEwODUyfQ.peUJTZW4pMEg99dqI1g2NqLA1Jpb0CofkaPdnFozE5k',4,'192.168.0.102'),(252,'2020-10-29 23:01:34','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MDA1Mjk0LCJleHAiOjE2MDQwMTA4OTR9.877u2igop-l3fICLq5YKxFc32qLuYQaRxXXgdgtVbPQ',5,'192.168.0.102'),(253,'2020-10-30 00:00:35','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwMDg4MzUsImV4cCI6MTYwNDAxNDQzNX0.JN_Rf-cnd9sCkyZ2yYMQ8D9Dgp3O-ztF14vvQMsMMr0',1,'192.168.0.102'),(254,'2020-10-30 00:01:25','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MDA4ODg1LCJleHAiOjE2MDQwMTQ0ODV9.tpsc2Zl39-R5rHlczk1Jd8VvtWfaJbmrC6vV8lufHnA',5,'192.168.0.102'),(255,'2020-10-30 02:14:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwMTY4NDcsImV4cCI6MTYwNDAyMjQ0N30.uUiseVbbcy9WHSSomfHAeRKZqFTiZnWOamO2RkLp5Y0',1,'192.168.0.102'),(256,'2020-10-30 03:47:35','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwMjI0NTUsImV4cCI6MTYwNDAyODA1NX0.mRuaLEIoJKG5bLxg-6kTvClkY7aRHQqVSTeJeRgEW8g',1,'192.168.0.102'),(257,'2020-10-30 03:54:52','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwMjI4OTMsImV4cCI6MTYwNDAyODQ5M30.XLDvrCPqY3c_R2zRVqrSRVui61mZOs9mOUPrDHC6jLM',1,'192.168.0.102'),(258,'2020-10-30 10:09:24','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNDUzNjIsImV4cCI6MTYwNDA0ODk2Mn0.jiQIj_-QVqFYOp84VtqjjBO-iROh3hX8BKpenPZSh9Y',1,'192.168.11.130'),(259,'2020-10-30 10:17:35','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNDU4NTMsImV4cCI6MTYwNDA0OTQ1M30.KHUyN5QKMX_9jwdLdMD9Zj7qXETTV0UG8jd-rEq5W7s',1,'192.168.11.130'),(260,'2020-10-30 10:36:24','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNDY5ODMsImV4cCI6MTYwNDA1MjU4M30.AUuI_JqbLqri53bw8DECfQH335cnfl48hzWKaJGh54s',1,'192.168.0.102'),(261,'2020-10-30 10:49:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDA0Nzc1MywiZXhwIjoxNjA0MDUzMzUzfQ.0ucsfE29GQ5T8E8noHNDn6qAGyYNWN27l2TGFC-iCPM',4,'192.168.0.102'),(262,'2020-10-30 11:18:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNDk0ODUsImV4cCI6MTYwNDA1MzA4NX0.U-8PDEyUpY2YAfUirXu-wIgKuoUvfj71wbnRFJQeoiA',1,'192.168.11.130'),(263,'2020-10-30 12:18:50','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNTMxMjgsImV4cCI6MTYwNDA1NjcyOH0.srTNMF1DNCPVrzcGMcd9PmCsDevpJEc47qkyHdPY6wA',1,'192.168.11.130'),(264,'2020-10-30 13:08:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNTYxMDEsImV4cCI6MTYwNDA2MTcwMX0.MGcze5of752OhIn3znBd3WYa_gZziUR9Hujqm21BBQ8',1,'192.168.0.102'),(265,'2020-10-30 13:20:10','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNTY4MDgsImV4cCI6MTYwNDA2MDQwOH0.YkeMdpSarlXXFA6_Ldy0G-IW7PDbtBmxzNtLpILjPw4',1,'192.168.11.130'),(266,'2020-10-30 13:54:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNTg4NDYsImV4cCI6MTYwNDA2NDQ0Nn0.jvx5mFdbU2VmvQ4Q23Xw05d0wp2fJSIwwGr7YbPG_WA',1,'192.168.0.102'),(267,'2020-10-30 14:20:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNjA0MjMsImV4cCI6MTYwNDA2NDAyM30.TAH-7uAPAItZFhDNSNwJQAaZrGHNKR8G98WLIsba9UA',1,'192.168.11.130'),(268,'2020-10-30 15:27:32','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNjQ0NTEsImV4cCI6MTYwNDA3MDA1MX0.S_7Fm_V533rVg2SViyDCzAg-PwDAXQ__qKvURMKBfRc',1,'192.168.0.102'),(269,'2020-10-30 15:40:14','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNjUyMTEsImV4cCI6MTYwNDA2ODgxMX0.N3b50oLwFLfTJ3P78LfXMLPBFuVq5TeJy_rPC0tVG3w',1,'192.168.11.130'),(270,'2020-10-30 16:41:38','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNjg4OTUsImV4cCI6MTYwNDA3MjQ5NX0.-tpRuwZ-5zLxjwk9kYsMepF7Lli6ieprJ9uuNab2dbQ',1,'192.168.11.130'),(271,'2020-10-30 16:44:23','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNjkwNjIsImV4cCI6MTYwNDA3NDY2Mn0.KwqOztKeoph007L4sTZxshSByNl48TeJKRbSrub2Fro',1,'192.168.0.102'),(272,'2020-10-30 17:07:24','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNzA0NDQsImV4cCI6MTYwNDA3NjA0NH0.KR9G4HgCYN0JMExN1p4bt6R9CCjoQ9W2etL6l6MEbmw',1,'192.168.0.102'),(273,'2020-10-30 17:47:06','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNzI4MjQsImV4cCI6MTYwNDA3NjQyNH0.YO_kPN7xYpn0qFtS-2538-R6MjUTaSCCb8ccKCRcR60',1,'192.168.11.130'),(274,'2020-10-30 18:11:06','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNzQyNjYsImV4cCI6MTYwNDA3OTg2Nn0.IyGSxDFBLHPAMTkUtuj1m-8PhsGlgxcyVw9IFu5DTHQ',1,'192.168.0.102'),(275,'2020-10-30 18:17:48','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNzQ2NjcsImV4cCI6MTYwNDA4MDI2N30.PrbEKfeqBJKmcU6sa0acLr9elxQcOqPU_8glgUnykwM',1,'192.168.0.102'),(276,'2020-10-30 18:47:23','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNzY0NDAsImV4cCI6MTYwNDA4MDA0MH0.I7wYjXJZ3C6SDuO_P30LE6Ntbo3UpyonqcZcROcJcHA',1,'192.168.11.130'),(277,'2020-10-30 19:25:06','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNzg3MDMsImV4cCI6MTYwNDA4MjMwM30.93uHDXZ1DhYZdY8rBFNhn5joTZoqOSiVKxJuQ8kUcyU',1,'192.168.11.130'),(278,'2020-10-30 19:25:27','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNzg3MjUsImV4cCI6MTYwNDA4MjMyNX0.Xak5UWZi5NJDqyNgpUZ1rfDtRfhpitQzbbUEClyf33w',1,'192.168.11.130'),(279,'2020-10-30 19:33:40','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQwNzkyMTgsImV4cCI6MTYwNDA4MjgxOH0.Ver9fHsC4YQwolTChUcr4nCnYXqzo5P82guXFjeO858',28,'192.168.11.130'),(280,'2020-10-30 19:35:04','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQwNzkzMDEsImV4cCI6MTYwNDA4MjkwMX0.LzqrGzzMOx6fcWE7YkHQVJFRubinYakxiqb59-zMddA',28,'192.168.11.130'),(281,'2020-10-30 19:40:31','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwNzk2MjksImV4cCI6MTYwNDA4MzIyOX0.aVNOG0yoxG2McxfRQ4aOBPqMBOSkgpC7rzS2oOlpwzc',1,'192.168.11.130'),(282,'2020-10-30 20:12:08','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwODE1MjcsImV4cCI6MTYwNDA4NzEyN30.BWrzIt9rLH12ec2oTRJe9Nr5RCbqPQ3xvmof0HIb9iE',1,'192.168.0.102'),(283,'2020-10-30 20:40:46','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwODMyNDMsImV4cCI6MTYwNDA4Njg0M30.cm0DNIN8cYV25YMtYT5uozsNy3ifA3dxGIKpsKztFjE',1,'192.168.11.130'),(284,'2020-10-30 21:14:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQwODUyNDQsImV4cCI6MTYwNDA4ODg0NH0.q4PwqBbCpBcSJDSMp6y45Pe-mEoR9Pmtsp-QMQ6M-P8',28,'192.168.11.130'),(285,'2020-10-30 21:33:04','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwODYzODIsImV4cCI6MTYwNDA4OTk4Mn0.9vSsLPTLiH45AZnGIvnCEA4htZtskCqaRHrsFLUfMUs',1,'192.168.11.130'),(286,'2020-10-30 22:14:48','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwODg4ODksImV4cCI6MTYwNDA5NDQ4OX0.kcsvy3UMklBZoBFUiFHY7fyuPCcuAuV7tobXKlHVt94',1,'192.168.0.102'),(287,'2020-10-30 22:16:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDA4ODk2OCwiZXhwIjoxNjA0MDk0NTY4fQ.ReQ6-mIEr-YxiJFZncHsbJHVkUgRvB0uZrS4jcW4XnM',4,'192.168.0.102'),(288,'2020-10-30 22:16:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwODkwMTUsImV4cCI6MTYwNDA5NDYxNX0.YjW8sQrE2iWsFZibSKSlhonRXCsRrpklcBYFxTjMc3o',1,'192.168.0.102'),(289,'2020-10-30 22:23:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQwODk0MDAsImV4cCI6MTYwNDA5NTAwMH0.q9JfW5bHIhxG2IXjgZUs-YMs_vnGi-a7cb3fnyHRzfY',3,'192.168.0.102'),(290,'2020-10-30 22:27:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDA4OTY1MCwiZXhwIjoxNjA0MDk1MjUwfQ.ScArXNzWX306FbyB41XLzZ_is7LCDIe8d6onCxvIU2s',4,'192.168.0.102'),(291,'2020-10-30 22:29:30','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQwODk3NjgsImV4cCI6MTYwNDA5MzM2OH0.9gDutFZ1T5rubQ407Vl9Z0D4a1Fz8rNcBZjciJ-f7oM',3,'192.168.11.130'),(292,'2020-10-30 22:36:41','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwOTAxOTgsImV4cCI6MTYwNDA5Mzc5OH0.DTqEy1FpDzjJVYk5TMbCAwr8F6Mca1XO3CghJZc1JTk',1,'192.168.11.130'),(293,'2020-10-30 22:52:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwOTExNTgsImV4cCI6MTYwNDA5Njc1OH0.4RJd6pBzhzpyJnHzeIIVgaXUEjHuo52h-ehioGE7LRE',1,'192.168.0.102'),(294,'2020-10-30 23:32:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDA5MzUyNSwiZXhwIjoxNjA0MDk5MTI1fQ.gs78iNbaCr5E4D99uhAPmktljqRThKi0K9kMXpNLUOU',4,'192.168.0.102'),(295,'2020-10-30 23:32:23','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQwOTM1NDAsImV4cCI6MTYwNDA5NzE0MH0.lAh0aOLwIVeBPQJDnxm0fPs97yTCIfTSHzlGWfSEu1Y',28,'192.168.11.130'),(296,'2020-10-30 23:33:45','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwOTM2MjMsImV4cCI6MTYwNDA5NzIyM30.QrJ1EbfRVGb0AbzgS7-dc1JU5qE0ymR4oEzRETCBxTA',1,'192.168.11.130'),(297,'2020-10-30 23:34:52','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwOTM2OTAsImV4cCI6MTYwNDA5NzI5MH0.bwrH35SAyjA1d7XRCXgRzhaN5pmT10MRlXz1DopdTwc',1,'192.168.11.130'),(298,'2020-10-30 23:36:45','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwOTM4MDMsImV4cCI6MTYwNDA5NzQwM30.ZjYcyUdQqCOjzihdG-6FO5e8YI1_eVZYaxyhTpvV2I8',1,'192.168.11.130'),(299,'2020-10-31 00:53:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQwOTg0MTgsImV4cCI6MTYwNDEwNDAxOH0.M4e9V7AxqPNhT7v7Z5WXdrXp0V0JIanuxnoqOWbLwKk',1,'192.168.0.102'),(300,'2020-10-31 10:55:14','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxMzQ1MTYsImV4cCI6MTYwNDE0MDExNn0.XsgUqhyfCusZUWek9mVLrr3wS3FTUqTJO-Yvphg4iik',1,'192.168.0.102'),(301,'2020-10-31 12:13:12','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDEzOTE5NCwiZXhwIjoxNjA0MTQ0Nzk0fQ.utRWlXhiewIV8TkA08zPJVEjUBnuSzOz4hzyCpGzhIU',4,'192.168.0.102'),(302,'2020-10-31 12:15:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxMzkzMDcsImV4cCI6MTYwNDE0NDkwN30.j8Da1ZdgBhkJkc8V8DhZLZtbOPFTiFVRD2eAT-SVmCE',1,'192.168.0.102'),(303,'2020-10-31 12:35:18','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE0MDUyMCwiZXhwIjoxNjA0MTQ2MTIwfQ.SRV05rT-nl702bxLVOdmw98h_C94apz_u4saCCVImp4',4,'192.168.0.102'),(304,'2020-10-31 13:25:44','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNDM1NDYsImV4cCI6MTYwNDE0OTE0Nn0.qbdsuXliga6QR-7GL8M0DGvtzihG_oq-6JjzRVn9e2I',1,'192.168.0.102'),(305,'2020-10-31 13:42:00','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNDQ1MTcsImV4cCI6MTYwNDE0ODExN30.nQPi-GBU7l3oSU3JAgTMpRO5TebiDCLUvn5irtFmtVM',1,'192.168.11.130'),(306,'2020-10-31 14:11:41','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNDYzMDMsImV4cCI6MTYwNDE1MTkwM30.zYk846aw0qQkgysQMuCQPYk2gKcnuOJJFz-RoxvTPLU',1,'192.168.0.102'),(307,'2020-10-31 14:17:31','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE0NjY1MywiZXhwIjoxNjA0MTUyMjUzfQ.LryfbrgpuN7R6zmd6YkOjdCe2li4WNx2KaedvkXS8Mg',4,'192.168.0.102'),(308,'2020-10-31 14:17:47','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNDY2NjgsImV4cCI6MTYwNDE1MjI2OH0.UH5d1OYwhDEk1uAlOyTO-j9uAFGUoSYT5fI2CBPbg2Q',1,'192.168.0.102'),(309,'2020-10-31 14:27:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE0NzIyNCwiZXhwIjoxNjA0MTUyODI0fQ.B-ZgyTrOwZSl4A2KgWkUp5vp52F0PMtxbED2gholRU4',4,'192.168.0.102'),(310,'2020-10-31 14:28:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNDcyODYsImV4cCI6MTYwNDE1Mjg4Nn0.aBeYemJtwFJbzV0QLS861yi4WFoILKzXWAIGfrEdd3o',1,'192.168.0.102'),(311,'2020-10-31 14:41:31','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE0ODA5MywiZXhwIjoxNjA0MTUzNjkzfQ.hhMJRXQqGuh94xBcB5bDfKksmOkRNI-Bk26ZcaKSpEo',4,'192.168.0.102'),(312,'2020-10-31 14:41:46','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNDgxMDgsImV4cCI6MTYwNDE1MzcwOH0.axtVQYNgJIQzdDDvu6L0r97Q3zNTALLrLjrRSRLknoE',1,'192.168.0.102'),(313,'2020-10-31 15:18:16','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNTAyOTMsImV4cCI6MTYwNDE1Mzg5M30.Kb0mwxzthAzvbEj3IX1xbhWQ1u1sV8_LkC0emJk_W-8',1,'192.168.11.130'),(314,'2020-10-31 15:51:46','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNTIzMDMsImV4cCI6MTYwNDE1NTkwM30.FCey7ekowXIyQ-w0jAFL_cJq1zO4BAV7n-JEcVyCUZk',1,'192.168.11.130'),(315,'2020-10-31 16:15:24','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNTM3MjUsImV4cCI6MTYwNDE1OTMyNX0.0uFwRv0lKPe01fZJMO09njhcCtr_VFu0xZSTuBD6fK0',1,'192.168.0.102'),(316,'2020-10-31 16:15:38','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE1Mzc0MCwiZXhwIjoxNjA0MTU5MzQwfQ.dVtxpVTAezlbmoJ4kt4hJJDHTa2M_DCJx6ncREtvVgA',4,'192.168.0.102'),(317,'2020-10-31 16:45:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNTU1MDIsImV4cCI6MTYwNDE1OTEwMn0.lrX81L-ygJj3ZkKkHi5L3I9Zz7IPl02m_rSXI9pOkYg',1,'192.168.11.130'),(318,'2020-10-31 16:53:41','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNTYwMTgsImV4cCI6MTYwNDE1OTYxOH0.bPxqnEdhvIRU9dkGcQacoVy4rWyxsf1Z9ZCkNSyjtmU',1,'192.168.11.130'),(319,'2020-10-31 17:49:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNTkzNDQsImV4cCI6MTYwNDE2NDk0NH0.lWRkLkQG0aU4TnOckljiie6k5zZ6uKLsf0fwdviqviI',1,'192.168.0.102'),(320,'2020-10-31 17:59:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNTk5ODEsImV4cCI6MTYwNDE2NTU4MX0.FgCTPu9kaQUiHP-Px2fLsiMg3EXCTf7NOrlDCLdZ6O0',28,'192.168.0.102'),(321,'2020-10-31 18:00:15','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjAwMTcsImV4cCI6MTYwNDE2NTYxN30.LZhQq8GR_X8nMWn4XevKgDMri-vnRrA_8ksMjVsn0tk',28,'192.168.0.102'),(322,'2020-10-31 18:03:04','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE2MDE4NiwiZXhwIjoxNjA0MTY1Nzg2fQ.kEc4Ii9viEku3K-3eOLLPNH09q2TyrDqy-WDIHY77kw',4,'192.168.0.102'),(323,'2020-10-31 18:03:15','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjAxOTcsImV4cCI6MTYwNDE2NTc5N30.HN2wdha9IKeWYEni0nTihRnPFqjCIBoaZQiIr1Zo0ho',28,'192.168.0.102'),(324,'2020-10-31 18:05:30','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNjAzMzIsImV4cCI6MTYwNDE2NTkzMn0.dFp5nvmmPzGuv0_rMlfKbpMiqrxjrb2AIzHIXnoQuGE',1,'192.168.0.102'),(325,'2020-10-31 18:06:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjAzODgsImV4cCI6MTYwNDE2NTk4OH0.PhqMQspnYcIfRFjQaaP8FMJO55WokUZa4BG3ZCGGgLk',28,'192.168.0.102'),(326,'2020-10-31 18:07:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNjA0NDQsImV4cCI6MTYwNDE2NjA0NH0.YtDVX1lIJKD10SVmGVsk2Fne4j6pW3HxZLTpb2eXazI',1,'192.168.0.102'),(327,'2020-10-31 18:07:38','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjA0NTksImV4cCI6MTYwNDE2NjA1OX0.Xv1c4oKh_wSq3ce8epR75ee2UG5aDaIbrAudUSo_GO0',28,'192.168.0.102'),(328,'2020-10-31 18:07:59','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNjA0NzYsImV4cCI6MTYwNDE2NDA3Nn0.h7UazxduZPBhUIthAwLurfjjQnAYC0wexnvSQ0FGccM',1,'192.168.11.130'),(329,'2020-10-31 18:08:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNjA1MTksImV4cCI6MTYwNDE2NjExOX0.hIMcY2jtZZO3_kKXmculFZ4ku078wifN6GcOuhsLiV8',1,'192.168.0.102'),(330,'2020-10-31 18:14:59','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjA5MDAsImV4cCI6MTYwNDE2NjUwMH0.hZuDL9wUjs_hEIYx4JYNxqokZvvPjRPUTBYwez6nFAc',28,'192.168.0.102'),(331,'2020-10-31 18:17:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjEwMzUsImV4cCI6MTYwNDE2NjYzNX0.oSHIvNe-FLO54PFE6qFGtQuwm5yPTjWzUKx3zo0Oz6Y',28,'192.168.0.102'),(332,'2020-10-31 18:24:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE2MTQ2OCwiZXhwIjoxNjA0MTY3MDY4fQ.dK4NJpY6TMmY3wkCHwBjMBe0WHllIqEvKH_mwQw8zAM',4,'192.168.0.102'),(333,'2020-10-31 18:25:59','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE2MTU2MCwiZXhwIjoxNjA0MTY3MTYwfQ.Ll1SK0DANz3qSwE_KIao-6FKrxxJdSHV0CqFnL27Kc4',4,'192.168.0.102'),(334,'2020-10-31 18:30:40','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjE4NDIsImV4cCI6MTYwNDE2NzQ0Mn0.MO7NcmyIGvwAQLcuJmNQSdgg3VRk4hVru7ZlMY12HG4',28,'192.168.0.102'),(335,'2020-10-31 18:32:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE2MTkzNSwiZXhwIjoxNjA0MTY3NTM1fQ.SBgPeA1W54sL0Qo975pNr3kHg6Y4sPvnCpSLjq1SZVM',4,'192.168.0.102'),(336,'2020-10-31 18:47:14','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjI4MzYsImV4cCI6MTYwNDE2ODQzNn0.uRuVJsH4M0WPYn0MDVyIvLtXffTxX3Op73uyB73uSAc',28,'192.168.0.102'),(337,'2020-10-31 18:51:53','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE2MzExNSwiZXhwIjoxNjA0MTY4NzE1fQ.E7IawWSwKK8tbOazLJ43mkqF_pRLctjEf6fJhI0VtmA',4,'192.168.0.102'),(338,'2020-10-31 19:09:21','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNjQxNTgsImV4cCI6MTYwNDE2Nzc1OH0.rCcuxIyUqkzKb2PEnOVBHCmevZXfsacNnupXahERFBc',1,'192.168.11.130'),(339,'2020-10-31 19:10:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjQyMjQsImV4cCI6MTYwNDE2OTgyNH0.P26cuBOs3MPlt7vBvEWCGRFJHkNOj1pqztTFMMxRxxw',28,'192.168.0.102'),(340,'2020-10-31 19:12:48','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE2NDM3MCwiZXhwIjoxNjA0MTY5OTcwfQ.phKj8-B-rfu1qPNuZSvKddY0PINR9rewZHmO2iuiK1k',4,'192.168.0.102'),(341,'2020-10-31 19:16:04','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjQ1NjYsImV4cCI6MTYwNDE3MDE2Nn0.tyZ-IsZuzcjgiXots_63q4nTYCo4vfJvRgSw1s3OwkY',28,'192.168.0.102'),(342,'2020-10-31 19:16:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjQ1OTgsImV4cCI6MTYwNDE3MDE5OH0.lBikNgqOalku6jip2opLhdMB2v0b6i76QNArOmpGOWs',28,'192.168.0.102'),(343,'2020-10-31 19:18:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjQ3MTAsImV4cCI6MTYwNDE3MDMxMH0.JBTukKSogBPwuDFTTcf6IwuG0YnohHsBAAt0IHWbukM',28,'192.168.0.102'),(344,'2020-10-31 19:23:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjQ5ODcsImV4cCI6MTYwNDE3MDU4N30.aItgdpUpLE8dQK2gV-8O1mRKqZUtrR1Q0v3KM_KxaG8',28,'192.168.0.102'),(345,'2020-10-31 19:26:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNjUxNjUsImV4cCI6MTYwNDE3MDc2NX0.dl0WkfGlUlpW8_yGChDWN0FkH7ad6Ad93-gaNqbp6VA',28,'192.168.0.102'),(346,'2020-10-31 19:26:30','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE2NTE5MiwiZXhwIjoxNjA0MTcwNzkyfQ.WJ8nagT376Aux8NUakgo1hDnSxUZ6q7Aeuj63NO5T4k',4,'192.168.0.102'),(347,'2020-10-31 19:40:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNjYwMjQsImV4cCI6MTYwNDE3MTYyNH0.NVWB0wRSfWr-oX8lcTcPTb3wgnec-su8CcZ_zla4r0I',1,'192.168.0.102'),(348,'2020-10-31 20:24:53','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNjg2OTAsImV4cCI6MTYwNDE3MjI5MH0.xEM-a0D68Z_YlilyHs7qy_k8_h6Jday1Xp5Y7kkE2uU',1,'192.168.11.130'),(349,'2020-10-31 20:29:46','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNjg5ODgsImV4cCI6MTYwNDE3NDU4OH0.q0AeAPnXrN07JcXWoOIVqD4wtNYa85VUfWe-1g6_Xww',1,'192.168.0.102'),(350,'2020-10-31 20:48:49','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjcxLCJ1c2VybmFtZSI6Ik1hcmlvIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MTcwMTMxLCJleHAiOjE2MDQxNzU3MzF9.xt1MXmaC1AReqjRKTSDhibxCMSq1JjPeK4t72YFcgkc',71,'192.168.0.102'),(351,'2020-10-31 20:49:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjcyLCJ1c2VybmFtZSI6Ik1hcnJzc3MiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNzAxNjEsImV4cCI6MTYwNDE3NTc2MX0.IKK39dAi6zXqgnJZZtv_VWf83iJ4akPv-jLdGQKFLNg',72,'192.168.0.102'),(352,'2020-10-31 20:50:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNzAyMDUsImV4cCI6MTYwNDE3NTgwNX0.VOiChPX8vFNiUdASpdGUeABvjdGrb1yUIC8foulYEW8',1,'192.168.0.102'),(353,'2020-10-31 20:52:29','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjE4LCJ1c2VybmFtZSI6ImFudGNoZXYiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQxNzAzNDYsImV4cCI6MTYwNDE3Mzk0Nn0.adWbZ6M4kMX8UgZyIEbvfKWbuFmQzBHiqztKPfXXslA',18,'192.168.11.130'),(354,'2020-10-31 20:58:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE3MDcwMSwiZXhwIjoxNjA0MTc2MzAxfQ.fBdgDMQJdKCRiue_hNc4PQGSc8RcHL8vag1JHJbmx1E',4,'192.168.0.102'),(355,'2020-10-31 22:01:21','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxNzQ0ODMsImV4cCI6MTYwNDE4MDA4M30.CbMlKh7OiYwSmluohCCG0zo2LDqxPkLsdCcihWAJTGc',1,'192.168.0.102'),(356,'2020-11-01 00:17:23','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQxODI2NDUsImV4cCI6MTYwNDE4ODI0NX0.08ZBjnp_GKxstheD8HZXUPtoBBRknT7awJ2JnARzn-0',1,'192.168.0.102'),(357,'2020-11-01 00:27:50','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDE4MzI3MiwiZXhwIjoxNjA0MTg4ODcyfQ.7LqsGEPJc66C3i4PN2u_KxQBW_3gQyfYhMonl61cu7g',4,'192.168.0.102'),(358,'2020-11-01 09:56:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyMTczODEsImV4cCI6MTYwNDIyMDk4MX0.iQCjRo0Ovj8CBWcu4GIp0erMFRQe_SEdtnYRubfktAM',28,'192.168.11.130'),(359,'2020-11-01 10:06:58','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMTgwMTMsImV4cCI6MTYwNDIyMTYxM30.EwfItv1C_6DQrRyYPED-bY81Vn45YCjMhjR7C__JHqc',1,'192.168.11.130'),(360,'2020-11-01 10:37:23','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDIxOTg0NiwiZXhwIjoxNjA0MjI1NDQ2fQ.wCgPPDUESLH6O9XhB7aj_xNcw3YAiUuRtib6sWikudA',4,'192.168.0.102'),(361,'2020-11-01 10:41:12','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyMjAwNzUsImV4cCI6MTYwNDIyNTY3NX0.hPCySiIktHKMnFoGOSVj9_VBKGhRgOAaU9nF9Qr9kss',28,'192.168.0.102'),(362,'2020-11-01 10:45:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMjAzMzksImV4cCI6MTYwNDIyNTkzOX0.gs8HlHclXOeta9AgLDMXygODPMmEKeY7V1vpnzGWghY',1,'192.168.0.102'),(363,'2020-11-01 11:15:17','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMjIxMTMsImV4cCI6MTYwNDIyNTcxM30.b2AVl2y6G7xvAMXdPHqN5Gf5CkvILOEjE6Bsu2GygX0',1,'192.168.11.130'),(364,'2020-11-01 11:30:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjIzMDMwLCJleHAiOjE2MDQyMjg2MzB9.O4NbKQsg1gHV1crcB9Yje4kx67wp7TeSR78VdugKdC0',5,'192.168.0.102'),(365,'2020-11-01 12:04:14','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMjUwNTYsImV4cCI6MTYwNDIzMDY1Nn0.oBaaRe5kISzxrOmw01MUUzjdYyWYS-q0ncjxaWbuA3g',1,'192.168.0.102'),(366,'2020-11-01 12:15:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMjU3MTgsImV4cCI6MTYwNDIyOTMxOH0._iitYwbl9BAx9xDWItewgfsPfmSzKAl1vkL3Nb6Jk6A',1,'192.168.11.130'),(367,'2020-11-01 12:17:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjczLCJ1c2VybmFtZSI6IlRoZSBMYXVyZWF0ZSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDIyNTgzNSwiZXhwIjoxNjA0MjMxNDM1fQ.Gr3WkVLnAw9NXRunwMuUpGSzWX-1mcBeUlAhYHDIRbs',73,'192.168.0.102'),(368,'2020-11-01 12:25:17','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjI2MzIwLCJleHAiOjE2MDQyMzE5MjB9.LAdNGnyQ33VVDDMJ65VTHqeUj6W7QAVdjLUEmKkT5s0',5,'192.168.0.102'),(369,'2020-11-01 12:29:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyMjY1MzksImV4cCI6MTYwNDIzMDEzOX0.0kBaKkdd8NW3gUGjPs1-JjGNuTktCc9CBJi2CTrkLqw',3,'192.168.11.130'),(370,'2020-11-01 12:29:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMjY1NzMsImV4cCI6MTYwNDIzMDE3M30.0h7GIVvGEyGSw154fXsHEigImeS52RjYmrsITB-xM94',1,'192.168.11.130'),(371,'2020-11-01 12:44:23','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMjc0NjYsImV4cCI6MTYwNDIzMzA2Nn0.ctrndihDFXtfhfiNYYVyX8ZEidoQ5HcSozR4CyYjC-g',1,'192.168.0.102'),(372,'2020-11-01 12:46:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyMjc1ODUsImV4cCI6MTYwNDIzMzE4NX0.kbasHM2-iMu2EevfU-ex1qK5aOFPYb6fVSF8BZv1O9E',28,'192.168.0.102'),(373,'2020-11-01 12:50:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMjc4MDUsImV4cCI6MTYwNDIzMzQwNX0.aRwGq1O3znUQpTbYPz1BT1OjzJgFRnk2_UNFL8HAabk',1,'192.168.0.102'),(374,'2020-11-01 13:03:56','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMjg2MzksImV4cCI6MTYwNDIzNDIzOX0.MlVqXVBiTr8Uj0lToJsQ1EnaEB6Tp20Ej2vFKTiQlWY',1,'192.168.0.102'),(375,'2020-11-01 13:16:45','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDIyOTQwMCwiZXhwIjoxNjA0MjMzMDAwfQ.MnhLh5lyU7XPlwIt639I1rt6IV-UT9MAyK0DOn1V52M',4,'192.168.11.130'),(376,'2020-11-01 13:26:41','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMjk5OTcsImV4cCI6MTYwNDIzMzU5N30.H-2spisznt1M4Y0_Sh80rsmAkdujGWfkU0ebkr7ypDw',1,'192.168.11.130'),(377,'2020-11-01 13:59:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMzE5NTAsImV4cCI6MTYwNDIzNzU1MH0.Xw8N2mIxpaeykejTrmIsnW21Vz2Ke1hGo3IoIzZQS3Y',1,'192.168.0.102'),(378,'2020-11-01 14:39:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMzQzNDQsImV4cCI6MTYwNDIzOTk0NH0.d6OEMMz4d4HM_Ai7JtaW-EVYIgfJggHJ2KyKlhGaoiA',1,'192.168.0.102'),(379,'2020-11-01 14:52:50','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMzUxNjYsImV4cCI6MTYwNDIzODc2Nn0.KGapCgO2MLDF8fJvC_1ffTgrG1y_xn69kRexYwkPlcM',1,'192.168.11.130'),(380,'2020-11-01 15:16:12','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMzY1NzUsImV4cCI6MTYwNDI0MjE3NX0.zoxYSCfQ-eB2RQJ-Z3gT6oJ10gwnQ0dTbYWCtdc2HCg',1,'192.168.0.102'),(381,'2020-11-01 15:37:47','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjgzLCJ1c2VybmFtZSI6ImthcmkiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyMzc4NjMsImV4cCI6MTYwNDI0MTQ2M30.SVrZ4fQjhMizJztv2vwrYHcvyC26P7Mp_0A1PnAF4gg',83,'192.168.11.130'),(382,'2020-11-01 15:38:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjM3OTA4LCJleHAiOjE2MDQyNDM1MDh9.sw05GsF8TZuWfjDa1GvVs23neGrexp5u3oiht-ftvGg',84,'192.168.0.102'),(383,'2020-11-01 15:47:21','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyMzg0MzcsImV4cCI6MTYwNDI0MjAzN30.4K-MZfglSqFC2LPMJMWWutovqFpn0mG1o7hnpCZ2BIc',28,'192.168.11.130'),(384,'2020-11-01 15:49:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDIzODU0NCwiZXhwIjoxNjA0MjQ0MTQ0fQ.7Fh14RHSrNLRBn5gDjPTxFDDhmKwGzE_aYYVQufkKzM',4,'192.168.0.102'),(385,'2020-11-01 15:50:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyMzg2MDksImV4cCI6MTYwNDI0NDIwOX0.6h7juXnou7OsZhqe7Oqbwg0Tg0lbFA26cqJkp0qprGk',1,'192.168.0.102'),(386,'2020-11-01 15:51:20','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjM4NjgyLCJleHAiOjE2MDQyNDQyODJ9.jmDdZMfzejmB-szPeLRahH8j-K-NeePFHl-ClahPWYI',84,'192.168.0.102'),(387,'2020-11-01 15:52:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjgzLCJ1c2VybmFtZSI6ImthcmkiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyMzg3MjksImV4cCI6MTYwNDI0MjMyOX0.fh-ozlsmxJNPQ_LpeUtEpYc8LZO4gUKICzfyx1_e2Mo',83,'192.168.11.130'),(388,'2020-11-01 15:58:32','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDIzOTExNSwiZXhwIjoxNjA0MjQ0NzE1fQ.lAMyfiJH0Vv5A9W1Pu545xWYkxrpKc669tTFOhmqGdg',4,'192.168.0.102'),(389,'2020-11-01 16:09:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjM5NzY4LCJleHAiOjE2MDQyNDUzNjh9.6jza4ff0GD6RkBEtQOTREOPVuY6ZYS-N1J1WgRL-shQ',84,'192.168.0.102'),(390,'2020-11-01 16:10:27','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjgzLCJ1c2VybmFtZSI6ImthcmkiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyMzk4MjMsImV4cCI6MTYwNDI0MzQyM30.FxC3wo4B5G6gGbfFeShAM97sY4yXCWF-lpfuQURumw0',83,'192.168.11.130'),(391,'2020-11-01 16:14:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDAwNTUsImV4cCI6MTYwNDI0MzY1NX0.BKBkWuOB_q9_ExFgl3ZKAk41O-nTwvi4bpaedmsYgkQ',1,'192.168.11.130'),(392,'2020-11-01 16:20:31','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjgzLCJ1c2VybmFtZSI6ImthcmkiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNDA0MjcsImV4cCI6MTYwNDI0NDAyN30.pazTz3pZfqAHLaDUkELAUNMuJc0e2DA7cEn_9CH7MO4',83,'192.168.11.130'),(393,'2020-11-01 16:22:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjQwNTYxLCJleHAiOjE2MDQyNDYxNjF9.Dy5FYGOWyxdlPH2-4fXiT3YPwLCW_SlF7W39YR19TT8',5,'192.168.0.102'),(394,'2020-11-01 16:24:47','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDA2ODMsImV4cCI6MTYwNDI0NDI4M30.6jfei8ETiABdrVPhju8NflskSYbbNXTKqdgrc8_q1z8',1,'192.168.11.130'),(395,'2020-11-01 16:27:51','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI0MDg3NCwiZXhwIjoxNjA0MjQ2NDc0fQ.VmPW56WfequGKM-WFqK676WbecAOhAJk90GKvIHN9y0',85,'192.168.0.102'),(396,'2020-11-01 16:34:46','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDEyODEsImV4cCI6MTYwNDI0NDg4MX0.3zODpajX2SEuVtvokrrgabWUiG0rKnfugPPvSMJJN54',1,'192.168.11.130'),(397,'2020-11-01 16:51:38','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDIyOTQsImV4cCI6MTYwNDI0NTg5NH0.dZ8_GQ3tqXQU9XWv1ChNpptlDVp3K3C6WFMKNQL4MIs',1,'192.168.11.130'),(398,'2020-11-01 16:54:59','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDI0OTUsImV4cCI6MTYwNDI0NjA5NX0.-CdYCIiLVgoUaHwpi6IevK1VzDNv4Rau47pWG_Plo8Q',1,'192.168.11.130'),(399,'2020-11-01 16:58:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI0MjcwMSwiZXhwIjoxNjA0MjQ2MzAxfQ.wgdx2KpKoKi3OJYqrme0n6Qilc7Pgdc9OcloJx9okmg',4,'192.168.11.130'),(400,'2020-11-01 17:00:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjgzLCJ1c2VybmFtZSI6ImthcmkiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNDI4MjIsImV4cCI6MTYwNDI0NjQyMn0._J76hJBP9BJ64NnsgYce991MBbke3vD8nXnVWSFxoCQ',83,'192.168.11.130'),(401,'2020-11-01 17:06:51','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjQzMjA2LCJleHAiOjE2MDQyNDY4MDZ9.wkW2-uiiJRFdQsMg4MSAyE8jQTArjkmF89MeC6moAgo',84,'192.168.11.130'),(402,'2020-11-01 17:23:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI0NDE3OCwiZXhwIjoxNjA0MjQ3Nzc4fQ.82yMTjHPt28nCMJa-E4CZ1_dOXAn1J_vO7xtQ68-5rA',4,'192.168.11.130'),(403,'2020-11-01 17:23:14','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg2LCJ1c2VybmFtZSI6ImFzZGYiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNDQxOTAsImV4cCI6MTYwNDI0Nzc5MH0.JvXpt627aRO02tpBYnFfe7QLcEGjTMKgUgNfGy1S5KY',86,'192.168.11.130'),(404,'2020-11-01 17:27:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDQ0MjEsImV4cCI6MTYwNDI0ODAyMX0.pgRWd0akDi_YqE-zVUEU2A5OyvZNDbXKH3KE0248jgQ',1,'192.168.11.130'),(405,'2020-11-01 17:54:00','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDYwMzYsImV4cCI6MTYwNDI0OTYzNn0.aZMhAePSdiqJr3KOHMJ1dJGmU2yula_SxchOB-VVtps',1,'192.168.11.130'),(406,'2020-11-01 18:12:31','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjc0LCJ1c2VybmFtZSI6InBlbmNobyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI0NzE0NiwiZXhwIjoxNjA0MjUwNzQ2fQ.bcM0oCw_o_DKuBBQQ5rUoxWI1a2ZDMPL6I_5n4UN568',74,'192.168.11.130'),(407,'2020-11-01 18:13:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI0NzE4OCwiZXhwIjoxNjA0MjUyNzg4fQ.bYqR-bpuDcc4p52iU2fOnDFBYhooqeMq-jttQBWOFlI',4,'192.168.0.102'),(408,'2020-11-01 18:13:58','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjQ3MjQwLCJleHAiOjE2MDQyNTI4NDB9.DGiAUzgCG88eFJbHph5q63Su18-hykF4mouWVkBG0Ic',84,'192.168.0.102'),(409,'2020-11-01 18:15:25','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI0NzMyNywiZXhwIjoxNjA0MjUyOTI3fQ.WqHY3I_jC4yllJNYRq2Xx82Xwz-croO_kQ7_JZ22vzI',85,'192.168.0.102'),(410,'2020-11-01 18:17:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjQ3NDQyLCJleHAiOjE2MDQyNTMwNDJ9.BojsuK5_rqJIv7SYeKo89KbHul_FtN1dR5eqsx0Sqmo',84,'192.168.0.102'),(411,'2020-11-01 18:18:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI0NzUzNiwiZXhwIjoxNjA0MjUzMTM2fQ.RFOci2aeD9ejerMofIYj8gZ0cVQbXrtz_-Qytgam3bE',85,'192.168.0.102'),(412,'2020-11-01 18:19:23','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDc1NTgsImV4cCI6MTYwNDI1MTE1OH0.SyZhSE-xshqtiAx1xcskcBzLAtwnB7BOpr93AqwboxU',1,'192.168.11.130'),(413,'2020-11-01 18:21:34','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDc2ODksImV4cCI6MTYwNDI1MTI4OX0.Jo3qVx1Ft5QPiBx7gMSez2A9EY3-3LdeKOXOUiAKeXE',1,'192.168.11.130'),(414,'2020-11-01 18:22:29','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDc3NTEsImV4cCI6MTYwNDI1MzM1MX0.62Diz3i0Ws7HThTRQebz8Jj91fAOowYfz9WiXiYQT6s',1,'192.168.0.102'),(415,'2020-11-01 18:23:51','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjQ3ODI2LCJleHAiOjE2MDQyNTE0MjZ9.e46THXhoZ1G4OlIS5TlAuBdYPzseD51bY5izdto3oQo',5,'192.168.11.130'),(416,'2020-11-01 18:26:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI0Nzk2NSwiZXhwIjoxNjA0MjUzNTY1fQ.ZmS86ov97WnEHVkjDBlXhAgYKhSKEopQYn2LuiAcpOo',85,'192.168.0.102'),(417,'2020-11-01 18:51:01','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNDk0NTYsImV4cCI6MTYwNDI1MzA1Nn0.mWRHH5c-oOPDKwXCsKrHzAv4mWXqS3db704jx_PfHXg',1,'192.168.11.130'),(418,'2020-11-01 19:08:08','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNTA0OTAsImV4cCI6MTYwNDI1NjA5MH0.gcBlHD_9mSIfBq5Be_G2ZVt3oMXMU_TxEwIQQ3N1kl0',1,'192.168.0.102'),(419,'2020-11-01 19:09:32','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjUwNTc0LCJleHAiOjE2MDQyNTYxNzR9.6ZTx4bFMdh6HnOOMXJrGvprmE4jUVe8mdu5JqM-DFRU',5,'192.168.0.102'),(420,'2020-11-01 19:10:29','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNTA2MzEsImV4cCI6MTYwNDI1NjIzMX0.6RMCMu3CAL7t3lpVA5UH6bwssV1o6OXd3L7L89yXGoY',1,'192.168.0.102'),(421,'2020-11-01 19:11:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjUwNjkwLCJleHAiOjE2MDQyNTYyOTB9.YeBH7XuAzpsVzzndEf_OXszKL0-HEnq9yN81FehuK0A',5,'192.168.0.102'),(422,'2020-11-01 19:12:38','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI1MDc2MCwiZXhwIjoxNjA0MjU2MzYwfQ.pyJa4_H_k5zJaehZfOar2ngA52BWV_mhjYmVNS4-tHs',85,'192.168.0.102'),(423,'2020-11-01 19:13:51','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNTA4MzMsImV4cCI6MTYwNDI1NjQzM30.JZNUbkuVWbPNg5rCYWrTkzBJ6dGHDqp4XRpwndL8Q8Y',1,'192.168.0.102'),(424,'2020-11-01 19:14:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI1MDg4MSwiZXhwIjoxNjA0MjU2NDgxfQ.ux3awHdR4GajdlhcEvE9ZEPPdOO4u9Vxa-RbMo2Ff48',85,'192.168.0.102'),(425,'2020-11-01 19:17:13','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjUxMDM1LCJleHAiOjE2MDQyNTY2MzV9.9ii8KV6Df9wzmtxx9pgDgzSNM-QtNZC_JXvFo8gYIk4',84,'192.168.0.102'),(426,'2020-11-01 19:25:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjUxNTIyLCJleHAiOjE2MDQyNTcxMjJ9.UnC00xogNnOeAkhZBM0u6bQsmzbtUB1cCj0HX7NKz2s',5,'192.168.0.102'),(427,'2020-11-01 19:28:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNTE3MDEsImV4cCI6MTYwNDI1NzMwMX0.VrKBSzBH5eO2WNiS85RWN3ZuOOlYu1qc4v7lQOKHCok',1,'192.168.0.102'),(428,'2020-11-01 19:35:57','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjUyMTYwLCJleHAiOjE2MDQyNTc3NjB9.gS8VXSSHwxDH8ZHnanDsNoynb-1c-Jmmi4Ye83gzUJM',5,'192.168.0.102'),(429,'2020-11-01 19:37:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI1MjI1MCwiZXhwIjoxNjA0MjU3ODUwfQ.m4C1aC8MtPycWTDCnxwbOqgDaN2nUyy8-VeerMY5TbU',4,'192.168.0.102'),(430,'2020-11-01 19:42:10','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI1MjUzMiwiZXhwIjoxNjA0MjU4MTMyfQ.EOuj7ckhmipiWNn5pmz35gS2vM7kwmpYkf_ml23SWTE',85,'192.168.0.102'),(431,'2020-11-01 19:43:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjUyNjA0LCJleHAiOjE2MDQyNTgyMDR9.RCD4SGf0-72TMz7d1cpU_oNszbNadWch0jj1J_ZYB8I',5,'192.168.0.102'),(432,'2020-11-01 19:50:50','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNTMwNTIsImV4cCI6MTYwNDI1ODY1Mn0.Lw6bPyPCC3ZaX6nlFLLP9c7gThkn8SnBaknm6SwmH8c',1,'192.168.0.102'),(433,'2020-11-01 20:35:05','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI1NTcwNywiZXhwIjoxNjA0MjYxMzA3fQ.oJKUYO_q6eXtVRsLv0O0VhN9BPb-ub0SXhu52eHSdSA',85,'192.168.0.102'),(434,'2020-11-01 20:40:00','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNTYwMDMsImV4cCI6MTYwNDI2MTYwM30.cggKSYEZBxYcwx7GVyQUSQdl0wxMQvTs1FyXDiRD734',2,'192.168.0.102'),(435,'2020-11-01 20:50:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjc2LCJ1c2VybmFtZSI6IlZpY3RvcmlhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjU2NjU2LCJleHAiOjE2MDQyNjIyNTZ9.qHZDXq902XbeQnqb_4L2VDBUInbG_MU-3dJRilFgJbc',76,'192.168.0.102'),(436,'2020-11-01 20:56:52','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNTcwMTUsImV4cCI6MTYwNDI2MjYxNX0.IeNGLL5pYkAjAOf-HHRAIxxY3PrD0eXT5oYjbwoDKX0',1,'192.168.0.102'),(437,'2020-11-01 20:57:29','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjU3MDUxLCJleHAiOjE2MDQyNjI2NTF9.SVAx9VE5jpHIhc9DAmw9izF8Z_iFtANOXo6KtwCNPCw',84,'192.168.0.102'),(438,'2020-11-01 20:58:36','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI1NzExOCwiZXhwIjoxNjA0MjYyNzE4fQ.nbePXXonBpejEcwuqcjZ3jlN_llXI-oQPhCF5r9ArjI',4,'192.168.0.102'),(439,'2020-11-01 20:59:45','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjU3MTg3LCJleHAiOjE2MDQyNjI3ODd9.fv3vj6zUAg0iO5WjsIZT0gQ4KHnC6GBuS7QND8lYHAY',84,'192.168.0.102'),(440,'2020-11-01 21:01:16','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjc2LCJ1c2VybmFtZSI6IlZpY3RvcmlhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjU3Mjc5LCJleHAiOjE2MDQyNjI4Nzl9.RxVBuGPNwSmwwDpdh46o6Hits_D7eGa4eAq3eCuLy1Y',76,'192.168.0.102'),(441,'2020-11-01 21:02:38','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjU3MzYwLCJleHAiOjE2MDQyNjI5NjB9.ZEAswAH5s2rWE-SK7pKn1RanW1gK2va7VWGiw3YnMd0',5,'192.168.0.102'),(442,'2020-11-01 21:24:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg3LCJ1c2VybmFtZSI6Ik1hcmsiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNTg2NjEsImV4cCI6MTYwNDI2NDI2MX0.TBbhCLVkyyynsp8hiKDdcn9xYRmUSwnC-ypsj3un-So',87,'192.168.0.102'),(443,'2020-11-01 21:29:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg4LCJ1c2VybmFtZSI6Ik1hcmlhLkQuQi4iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNTg5NDQsImV4cCI6MTYwNDI2NDU0NH0.XdmuVy3FQMHFdt8JMTHUo_YUHTMpKHGnQdkiWim1Ovg',88,'192.168.0.102'),(444,'2020-11-01 21:40:14','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNTk2MTcsImV4cCI6MTYwNDI2NTIxN30.LE4v7WbtSpYGVX1TpmkztGjU6d51tONqd5MXHMLa2kc',2,'192.168.0.102'),(445,'2020-11-01 21:41:08','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg4LCJ1c2VybmFtZSI6Ik1hcmlhLkQuQi4iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNTk2NzAsImV4cCI6MTYwNDI2NTI3MH0.i3gGgrFK9A9wWOq9sEbr4WeTk07HFOak3o3X_C3zszQ',88,'192.168.0.102'),(446,'2020-11-01 21:46:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI2MDAwMSwiZXhwIjoxNjA0MjY1NjAxfQ.K5hRomcybzLk9s13BKYbILDpXDU7q9u9wu8JZoGZwRU',85,'192.168.0.102'),(447,'2020-11-01 22:13:30','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI2MTYxMiwiZXhwIjoxNjA0MjY3MjEyfQ.Nz_-rbpQDAB0Rpp783QEluFqQXXfleAV3L7IvK1G680',85,'192.168.0.102'),(448,'2020-11-02 00:02:18','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNjgxNDAsImV4cCI6MTYwNDI3Mzc0MH0.KuewPb0f4RcMNoYU8-hfg4GuLvGzEE8k6D_KnZHVHYg',2,'192.168.0.102'),(449,'2020-11-02 00:04:26','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg3LCJ1c2VybmFtZSI6Ik1hcmsiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNjgyNjgsImV4cCI6MTYwNDI3Mzg2OH0.F8JNwFqQ8JzoIuK5A_4Sn9YyBt5Wn4tKdYz4-PIWqKQ',87,'192.168.0.102'),(450,'2020-11-02 00:07:28','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNjg0NTAsImV4cCI6MTYwNDI3NDA1MH0.SK-2ySwhjKkA4iZJObKGCzvnAh0oMIyGGCs8COgW5yM',2,'192.168.0.102'),(451,'2020-11-02 00:07:59','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI2ODQ4MSwiZXhwIjoxNjA0Mjc0MDgxfQ.Dgy3T0xLEIl2E_vzZyUEpZ1hmk77MT2G0USlCzJoExs',4,'192.168.0.102'),(452,'2020-11-02 00:09:45','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjMsInVzZXJuYW1lIjoiR29zaGV0byIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI2ODU4OCwiZXhwIjoxNjA0Mjc0MTg4fQ.JetDMo-YSLio_7fTDqesc4EqIQKARu9_ojjDNHTsd-8',3,'192.168.0.102'),(453,'2020-11-02 00:14:10','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjY4ODUyLCJleHAiOjE2MDQyNzQ0NTJ9.OhGNaUALXAK0yNKUaichwLX-8GTcMogJZsf11PqGw2k',5,'192.168.0.102'),(454,'2020-11-02 00:16:45','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNjkwMDgsImV4cCI6MTYwNDI3NDYwOH0.isKqlgDibnWOP8qQQ0ZqeiBNFacsZO6_d12zdWy3zAc',1,'192.168.0.102'),(455,'2020-11-02 00:17:24','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg4LCJ1c2VybmFtZSI6Ik1hcmlhLkQuQi4iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNjkwNDYsImV4cCI6MTYwNDI3NDY0Nn0.jPJ7tWqQ6Iqtx86bzzC2bzom5fxGYIeetiKzJ5fpQNw',88,'192.168.0.102'),(456,'2020-11-02 00:31:19','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQyNjk4ODEsImV4cCI6MTYwNDI3NTQ4MX0.KuW0BYLHTnvVAnY8J98BLlVhbViBAcD6JuMymgPY-f4',1,'192.168.0.102'),(457,'2020-11-02 00:33:16','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjc2LCJ1c2VybmFtZSI6IlZpY3RvcmlhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjY5OTk4LCJleHAiOjE2MDQyNzU1OTh9.3Z3BXiogzehthR3Q9JtgSq7g-HxNo0FSkSvs1904lpg',76,'192.168.0.102'),(458,'2020-11-02 00:51:24','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjcxMDg3LCJleHAiOjE2MDQyNzY2ODd9.8J4fAuT7V_NB52fC66XuiNQbtHC9FWYvR7sEtXkwQlg',84,'192.168.0.102'),(459,'2020-11-02 00:52:17','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI3MTE0MCwiZXhwIjoxNjA0Mjc2NzQwfQ.YG_SFhjQYYZIn-5MuNCt5tYBKYjXVZUapfRXfuApuao',85,'192.168.0.102'),(460,'2020-11-02 00:54:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNzEyNDksImV4cCI6MTYwNDI3Njg0OX0.U_cyNBr5Irqw1YijQZgtDSsv7dzHNNocG7kCT78ujZg',2,'192.168.0.102'),(461,'2020-11-02 01:04:53','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MjcxODk1LCJleHAiOjE2MDQyNzc0OTV9.hkDo8gTkXNLTmhtQuVmHyU2WuwZZMaw2KrQW7hO-wvo',84,'192.168.0.102'),(462,'2020-11-02 01:07:40','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDI3MjA2MiwiZXhwIjoxNjA0Mjc3NjYyfQ.wlBGto3XZeBE0YgCG2st6RxHK9yV7BRsH226PAyHG4o',85,'192.168.0.102'),(463,'2020-11-02 01:08:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjI4LCJ1c2VybmFtZSI6ImFsYWJhbGEiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQyNzIxMzYsImV4cCI6MTYwNDI3NzczNn0.vyv29yBVrxS0A7NYpnT8qopb6XgF78W6tug4GbEy7WU',28,'192.168.0.102'),(464,'2020-11-02 09:20:47','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXJuYW1lIjoiUGVzaG8iLCJpc0FkbWluIjowLCJpYXQiOjE2MDQzMDE2NDksImV4cCI6MTYwNDMwNzI0OX0.g_4VAxCrhEkeMmy9g8PHdtDSTY1NkDBh5oauadESP_s',2,'192.168.0.102'),(465,'2020-11-02 09:23:12','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg1LCJ1c2VybmFtZSI6IlN0cmFkaXZhcml1cyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDMwMTc5NCwiZXhwIjoxNjA0MzA3Mzk0fQ.o6Rc6C2RMQJ2XOglg1XvjhErdgJqdzr0qjtOSXWkSec',85,'192.168.0.102'),(466,'2020-11-02 09:23:37','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg3LCJ1c2VybmFtZSI6Ik1hcmsiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQzMDE4MTgsImV4cCI6MTYwNDMwNzQxOH0.CBIUM9P0ZF7Jq92NryJu6cPdlTrikpzu2o7zc_pw57c',87,'192.168.0.102'),(467,'2020-11-02 09:24:30','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjc2LCJ1c2VybmFtZSI6IlZpY3RvcmlhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzAxODcxLCJleHAiOjE2MDQzMDc0NzF9.bwRLmP40sJ4i_I5B_GpKa18Ps2TeMV53gLN1SY8kqec',76,'192.168.0.102'),(468,'2020-11-02 09:25:49','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzAxOTUwLCJleHAiOjE2MDQzMDc1NTB9.H2t1IUCv8ffu9H4U2LxUMzjW9ULiNOVxNLEUT5prNf4',84,'192.168.0.102'),(469,'2020-11-02 09:29:12','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg0LCJ1c2VybmFtZSI6IkxhdXJlYXRlIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzAyMTUzLCJleHAiOjE2MDQzMDc3NTN9.XuES2Ruh_5HmRWBmuNN0DkxUUFFdfJu7tGRV4MFfu6A',84,'192.168.0.102'),(470,'2020-11-02 09:47:34','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMDMyNTAsImV4cCI6MTYwNDMwNjg1MH0.NIp3dPIcGscEg4PBMlp2o_6qqKHVjAfidkfd_2Qhmyc',1,'192.168.11.130'),(471,'2020-11-02 10:06:25','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg5LCJ1c2VybmFtZSI6Imdvc2hvIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzA0MzgxLCJleHAiOjE2MDQzMDc5ODF9.tFPfLXOy6Y3gEQqt6ddthpRE45YTDJNL9v4y9yljqlQ',89,'192.168.11.130'),(472,'2020-11-02 10:14:55','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMDQ4OTEsImV4cCI6MTYwNDMwODQ5MX0.4OBefDR48nu9-CHr_p3ou-LScH-qbY6vGATrogLj6eI',1,'192.168.11.130'),(473,'2020-11-02 10:18:42','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg5LCJ1c2VybmFtZSI6Imdvc2hvIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzA1MTE4LCJleHAiOjE2MDQzMDg3MTh9.FNuUps76Mf4tmqcb_hRwDZ4lNRQLjLeOrgEbzjJ_qKs',89,'192.168.11.130'),(474,'2020-11-02 10:29:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjkwLCJ1c2VybmFtZSI6IkRhbmllbCIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDMwNTc5MCwiZXhwIjoxNjA0MzA5MzkwfQ.Ozhxv02kayhEVIADEWE00EEg7DB4ny1GDuG5-i6cQq0',90,'192.168.11.130'),(475,'2020-11-02 11:07:15','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjkxLCJ1c2VybmFtZSI6Imd1aGFobyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDMwODAzMSwiZXhwIjoxNjA0MzExNjMxfQ.nQUfZU_VwYmfZ1OUaH9anSsrpZg6t9O4JQbX9XGFloA',91,'192.168.11.130'),(476,'2020-11-02 11:14:43','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDMwODQ3OSwiZXhwIjoxNjA0MzEyMDc5fQ.LH3_WYGP1Kb1uvAZbcHM1WNswTngaqM5gCOArnlQuYI',4,'192.168.11.130'),(477,'2020-11-02 11:19:40','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMDg3NzYsImV4cCI6MTYwNDMxMjM3Nn0.mcxca5ylIK8ks9X4zEdSNPFEFQpOkS0URR7AxB7xXyY',1,'192.168.11.130'),(478,'2020-11-02 11:27:48','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMDkyNjQsImV4cCI6MTYwNDMxMjg2NH0.eOtAMvjoj717n9R0XYSp6Fam-6FzN1nZLHL0-34gC28',1,'192.168.11.130'),(479,'2020-11-02 12:21:54','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMTI1MTAsImV4cCI6MTYwNDMxNjExMH0.fAXvaJgjZxnuMztJzfig_aKEqwzB4pqCvgDHzqIiTv4',1,'192.168.11.130'),(480,'2020-11-02 12:24:22','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjkyLCJ1c2VybmFtZSI6Im9nb25vbW1vIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzEyNjU3LCJleHAiOjE2MDQzMTYyNTd9.FbsucETqgO0zOhbU3flmFSJnWoSjBoAbquVSMD15jUw',92,'192.168.11.130'),(481,'2020-11-02 12:28:43','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjkzLCJ1c2VybmFtZSI6Iml2YW5za2kiLCJpc0FkbWluIjowLCJpYXQiOjE2MDQzMTI5MTksImV4cCI6MTYwNDMxNjUxOX0.D-qWdtnTpuvrecJzghp7X11llwk-q7CN4ZYtVX4lfIw',93,'192.168.11.130'),(482,'2020-11-02 12:32:41','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMTMxNTcsImV4cCI6MTYwNDMxNjc1N30.JFbwAAaYwjs5s9sdWrvJRtlXKSSoAVVIORKQMttJVow',1,'192.168.11.130'),(483,'2020-11-02 12:40:40','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjk0LCJ1c2VybmFtZSI6ImVkbyIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDMxMzYzNSwiZXhwIjoxNjA0MzE3MjM1fQ.Hf5CrknPOuixsB-ACfD7BClj86w48LxuWMksm5qPYac',94,'192.168.11.130'),(484,'2020-11-02 12:44:18','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMTM4NTQsImV4cCI6MTYwNDMxNzQ1NH0.YqOVG0C53DoTvxVvuQdcczjpQEJhCIxLTg2TIVopJ0Q',1,'192.168.11.130'),(485,'2020-11-02 13:00:02','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMTQ3OTgsImV4cCI6MTYwNDMxODM5OH0.XD0eh_eNuMkTm-qB2Pk_zTngcqeVIf2KPP8Mbg4ORAA',1,'192.168.11.130'),(486,'2020-11-02 13:01:12','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMTQ4NjcsImV4cCI6MTYwNDMxODQ2N30.GqwoiX-GSrDaBhtmkUck9P7SKTrEUTrMXMPEKmLoNVw',1,'192.168.11.130'),(487,'2020-11-02 13:32:12','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjk1LCJ1c2VybmFtZSI6InVzZXIxNiIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDMxNjcyOCwiZXhwIjoxNjA0MzIwMzI4fQ.gKcuBAn7uKZnnCdE3avl1D9tdz_tiAvXfgqiemdC6B8',95,'192.168.11.130'),(488,'2020-11-02 14:09:36','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMTg5NzEsImV4cCI6MTYwNDMyMjU3MX0.Rde57-qhOZ8tLKtSW6PnSwtmKcgDhm8d64f8nExPwFg',1,'192.168.11.130'),(489,'2020-11-02 14:46:40','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjkyLCJ1c2VybmFtZSI6Im9nb25vbW1vIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzIxMTk2LCJleHAiOjE2MDQzMjQ3OTZ9.xaB37qzQ209Xz4w-vheg2srBqITd4Y2IBixRqYaURxg',92,'192.168.11.130'),(490,'2020-11-02 14:59:39','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMjE5NzUsImV4cCI6MTYwNDMyNTU3NX0.2vjN4SSSfltcOAFPa4PancNOjVRn4XuMVe6tou6uAiU',1,'192.168.11.130'),(491,'2020-11-02 15:37:57','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg5LCJ1c2VybmFtZSI6Imdvc2hvIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzI0MjcyLCJleHAiOjE2MDQzMjc4NzJ9.hP1txrvuIBSEUS1X5HfgGZ4f7QZjXN4ogS4SVWADrpA',89,'192.168.11.130'),(492,'2020-11-02 16:14:07','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoiQW50aG9ueSIsImlzQWRtaW4iOjAsImlhdCI6MTYwNDMyNjQ0MywiZXhwIjoxNjA0MzMwMDQzfQ.Rm5VCB69rxe7SE6n-OUkH9Y-GfEv2m61jFNfsSaaa1s',4,'192.168.11.130'),(493,'2020-11-02 18:48:21','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMzU2OTcsImV4cCI6MTYwNDMzOTI5N30.lz4ocjAy4Hdl3wXdgT1HLMrzwzmeoN-XlxtahtJ7WDA',1,'192.168.11.130'),(494,'2020-11-02 19:09:36','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMzY5NzIsImV4cCI6MTYwNDM0MDU3Mn0.gbVeag_UjoGJGK9K3emnLwO45BRHM4qno_turCrxS7Y',1,'192.168.11.130'),(495,'2020-11-02 19:38:18','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg5LCJ1c2VybmFtZSI6Imdvc2hvIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzM4Njk0LCJleHAiOjE2MDQzNDIyOTR9.I4Oo465gY64_3v_exlusFLWdlOQKzccGg5_IUcGiapo',89,'192.168.11.130'),(496,'2020-11-02 19:38:52','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMzg3MjgsImV4cCI6MTYwNDM0MjMyOH0.tTdP6AfD8iehDCBoIVXlOzlwzeT6ZBVcrjQTOvKC91E',1,'192.168.11.130'),(497,'2020-11-02 19:39:20','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjg5LCJ1c2VybmFtZSI6Imdvc2hvIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzM4NzU2LCJleHAiOjE2MDQzNDIzNTZ9.WYNcxTIvUY_H81Gq-v4-lV_q_ExNr3M7svhijc71-PI',89,'192.168.11.130'),(498,'2020-11-02 19:42:04','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzMzg5MTksImV4cCI6MTYwNDM0MjUxOX0.src5cdKjv5AlhAz0blgzdnmMCJHBV8r-xnze9kTLa0Q',1,'192.168.11.130'),(499,'2020-11-02 19:57:03','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjkyLCJ1c2VybmFtZSI6Im9nb25vbW1vIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA0MzM5ODE5LCJleHAiOjE2MDQzNDM0MTl9.PT9UUhqHrTxOyDeKSDMaW_Oqem2-iN9n45VeizwYtKA',92,'192.168.11.130'),(500,'2020-11-03 00:56:33','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJpc0FkbWluIjoxLCJpYXQiOjE2MDQzNTc3OTUsImV4cCI6MTYwNDM2MzM5NX0.Y0AHeJ_s5LXrSnbBay-GzIrL-e46IhLrsYMnGiM-Xt8',1,'192.168.0.102'),(501,'2020-11-10 20:54:35','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoiQ2xlb3BhdHJhIiwiaXNBZG1pbiI6MCwiaWF0IjoxNjA1MDM0NDc4LCJleHAiOjE2MDUwNDAwNzh9.1o7jkR0saxtmiwnW51uH6k-v1EKKhTt8H-6qsLPO_aY',5,'130.204.202.97');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statuses`
--

LOCK TABLES `statuses` WRITE;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` VALUES (1,'Free'),(2,'Borrowed'),(3,'Unlisted');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `update_date` datetime DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT 0,
  `is_deleted` tinyint(1) DEFAULT 0,
  `points` int(11) DEFAULT 0,
  `level_id` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_levels1_idx` (`level_id`),
  CONSTRAINT `fk_users_levels1` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','$2b$10$chFFIvS7jzdwYRGhbjDPkutpFe4Sqd/KJDpFN/pAA0qRbeFNCOnpi','admin@teleriklibrary.com','admin.png','2020-10-09 12:18:41',NULL,1,0,196,2),(2,'Pesho','$2b$10$jrWd57YIkxGN2PIK5y0wRe5B6PG4Z2VNWE9Jo/Tw2eGfwiUkB9Zp2','pesho@gmail.com','1604256249084_dsds.jpeg','2020-10-09 14:27:54','2020-11-01 20:44:06',0,0,124,2),(3,'Gosheto','$2b$10$swxAGzV3bQLK0/6y/UoYguGNMODwLcfF.yJmmH2L0c2CMUgyzMVPa','gosheto@gmail.com','1603990428292_big_5539e2d61de04456e1d88dd6f3c201c8.jpg','2020-10-09 14:47:06','2020-11-01 18:11:25',0,0,57,2),(4,'Anthony','$2b$10$5CucB5iGijUyakomk4ndV.kJC8uf5HJLDvW4lFm4GTowKM8k9CQRu','anto@gmail.com','1604239642145_Hyper-Light-Drifter-Wallpapers-Wallpaper-Cave.jpg','2020-10-09 19:36:01','2020-11-01 16:07:19',0,0,1848,5),(5,'Cleopatra','$2b$10$UjcWlQOMnw9kbFxiFOWxIe97fPCyP0A6smhYjXbevBLbz3z6VCrDq','cleo@gmail.com','1603987214321_pexels-photo-3755021.jpg','2020-10-09 21:21:31','2020-11-01 18:23:43',0,0,237,3),(6,'John','veronika','John@gmail.com','https://clinicforspecialchildren.org/wp-content/uploads/2016/08/avatar-placeholder.gif','2020-10-19 15:28:04','2020-10-19 15:36:13',0,1,0,1),(7,'John2','$2b$10$48tDD5au0rZlH3s.7pUgR.RAPqK6Xk.xObFihF3DR14HjpEXDbhN6','John2@gmail.com',NULL,'2020-10-20 03:53:01',NULL,0,1,0,1),(8,'Dilan','$2b$10$3HPj4wQQz4wvjEpN5Xq3ouhV1PnEa0R6jpN7eoXOqiDpqG3XoI5GK','Dilan@gmail.com',NULL,'2020-10-20 03:57:13',NULL,0,1,0,1),(9,'kristi','$2b$10$NtQm11x8Cl0qBTJGXG.w6e67Ohn02UMqDiSyDps/WgkG/K5VebSJO','kristi@gmail.com',NULL,'2020-10-20 04:02:52',NULL,0,0,0,1),(10,'admin111','$2b$10$7lGdvb4Is1WNiYSnTV1NXuTr3cSdXzLTFKZIdnohdxcTbY5GKWUYy','admin@admin.com',NULL,'2020-10-20 12:35:25','2020-11-01 12:33:41',0,1,0,1),(11,'admin11111','$2b$10$xFll9pu0HGJAFNj6SpsRs.C18sLNokY9BNMbyUXb5AAzb5Pu041KG','admin111@admin.com',NULL,'2020-10-20 12:38:28',NULL,0,1,0,1),(12,'Boris','$2b$10$wY8nlMPIPwrmmblREd5A/uAloDUA2dEZQWsb4yZaBpNBKlqtw424m',NULL,NULL,'2020-10-20 13:54:15',NULL,0,0,0,1),(13,'AnthoynT','$2b$10$nlCbOfULetLTR/94BJRmFuBcaJ921mjx4xzBsrMZx9eHmm3Cl025m',NULL,NULL,'2020-10-20 14:21:13',NULL,0,1,0,1),(14,'Cassete','$2b$10$vUf.YpxMKPlhXPUsxN922e5KYTW90QkBZf1wrLwYvW.cPM53FmwyO','casete@email.com',NULL,'2020-10-20 14:23:54',NULL,0,0,0,1),(15,'antoniii','$2b$10$PPThzXfGKMkZRG0Pa9HDU.vr2Hfgg.7QTPIM7o7n64GngZdXKv//S',NULL,NULL,'2020-10-20 14:37:32',NULL,0,1,0,1),(16,'sdadasda','$2b$10$Gar5thBvFEp2e6zi.23W.u/f2O6eU6Du/YnYksXZajSb27laLtLBi',NULL,NULL,'2020-10-20 14:38:26',NULL,0,1,0,1),(17,'aaa','$2b$10$WUTHV6nGU0N/B27C1ysat..JrpVq2JmwuW18NhaIqrLHPY8hq9pFm',NULL,NULL,'2020-10-20 18:22:17',NULL,0,1,0,1),(18,'antchev','$2b$10$r8ob.a6xbO9Df8gFr5F4RexcJ.X0Y167sOimc5RzPl.k8zGrG5w9a','antchev@antchev.com','1604237121258_images.jpg','2020-10-25 13:38:03','2020-11-02 18:51:10',1,0,0,1),(19,'asd','$2b$10$KNJ0HGdNpBpDwwryLdgc6uw8.xV0OTRSHQygEfLBYKVlJy7SjuPlO',NULL,NULL,'2020-10-25 13:46:57',NULL,0,1,0,1),(20,'asdasd','newpass','a@a.com','test','2020-10-25 13:48:55','2020-10-29 17:35:00',0,1,0,1),(21,'ivanivanov','$2b$10$A5eLFd7vD935bEjCBWVnoeZhSOcQRQQzifbmopQ7XrAtqSv5.icT2',NULL,NULL,'2020-10-25 13:51:09',NULL,0,0,0,1),(22,'asdasdasdasd','$2b$10$CdddYN7JGHet5DlJuvB1IeHM.oOck/GO1g6oazsGEW7CQIPFi9z9G',NULL,NULL,'2020-10-25 13:56:10',NULL,0,1,0,1),(23,'asd11','$2b$10$P.qbw./ewBde0Gsv4WPRie0UKGdz77lxqzHAMfsZ6F61YIaXeZa7i',NULL,NULL,'2020-10-25 14:03:39',NULL,0,1,0,1),(24,'asd22','$2b$10$6qLK0bgQjfOIDW3yV64H.OhwX5DJlhKaKXhPi/jxav29JM/KPYIBe',NULL,NULL,'2020-10-25 14:05:14',NULL,0,1,0,1),(25,'asd33','$2b$10$u6cIdHB2YAiB6Ops0EnqHOz2YiyuWOmj02fonqDh.HqjguCnuoiMy',NULL,NULL,'2020-10-25 14:05:44',NULL,0,1,0,1),(26,'nikolay','$2b$10$7a/XKp2RKhX1mw.FU.hW0uHxeNnRgW4Et2oQLXK.aH9a5/iLU50ni',NULL,NULL,'2020-10-25 16:01:33',NULL,0,0,0,1),(27,'nikolay123','$2b$10$lSr/EWfq2cBWcfzC4nMn2eQe2v0YKSsi2DcIA3s29Lac7euXSYGc.',NULL,NULL,'2020-10-25 16:02:50',NULL,0,0,0,1),(28,'alabala','$2b$10$M8dkGd8OFDfbtx7Sx4fsoOYF1Fl5dazf5E0wazMkCGl/txZreGQy2','alabala@alabala.com','1604303716485_49x58PIC8esuucDCvbavj_PIC2018.png_wh860.png','2020-10-25 16:04:21','2020-11-02 09:55:20',0,0,13,1),(29,'alabala1','$2b$10$i8sPENP0U5zul4MX7FnKmeuYG9QXlH7JGW2n2/3dJyF9mR6nVXuNi','alabam@gmail.com','1604166830383_ux.PNG','2020-10-25 16:07:44','2020-10-31 19:59:23',1,1,0,1),(30,'alabala11','$2b$10$gLbAC7WDnqtXXWn4czdNVe6v1CmSo8QNZucqXh2cMcwwojX3oD.bW',NULL,NULL,'2020-10-25 16:08:34',NULL,0,1,0,1),(31,'asdfgh','$2b$10$L76TIMXPlaQDVK/E3wVVsuZwh3foa3D6NtghMH5oEEyvuAkQo6bzi',NULL,NULL,'2020-10-25 16:22:13',NULL,0,1,0,1),(32,'Venera','$2b$10$XtxqNk6Y63WRvVQ1GhdNd.CcVRhWLQGX2V0LzhvnEzvRd5yigDee6','venera@gmail.com',NULL,'2020-10-25 17:12:34',NULL,0,0,0,1),(33,'lifestyle','$2b$10$2oSGBsudQmMZuHEUqRKeu..IciGda4W8p8pyeV1BG1gvy6YpaPGpS',NULL,NULL,'2020-10-25 19:42:51',NULL,0,0,0,1),(34,'lima','$2b$10$eeq6VJNl/jXL3tktXtuLvOlfAE7L4TvTGtyi.8y/F8omQZkmzQcze',NULL,NULL,'2020-10-25 19:48:43',NULL,0,1,0,1),(35,'lima123','$2b$10$Qbah9ltv3N8GkWed5Cd2rekuW4T2Qi3tvUIojPqwGtr.MEfVnK1OS',NULL,NULL,'2020-10-25 19:49:51',NULL,0,1,0,1),(36,'lima1','$2b$10$aRhGscO9gyYQT/lpGtyBwOMY4viwCGUGWtpFjr71n7RKxEtXah89i',NULL,NULL,'2020-10-25 19:51:16',NULL,0,1,0,1),(37,'lima12','$2b$10$QfF6b7CMd38DL9s0810EwuRZp1Rk9mAI2rRyCqSZifqboWMQy6FBi',NULL,NULL,'2020-10-25 19:52:28',NULL,0,1,0,1),(38,'lima1234','$2b$10$g9gU/48Tz3WTtmwERHqfwOBU3bkl88vjlQtFHpVdDW5clsXwp68XC',NULL,NULL,'2020-10-25 19:54:48',NULL,0,1,0,1),(39,'lima12345','$2b$10$6HU.qW1w9TueEtDsqmitgehTXVLKoFY30Sb1KPQG5PfZDAW.llyti',NULL,NULL,'2020-10-25 19:55:33',NULL,0,1,0,1),(40,'lima555','$2b$10$YA05MWj369QMyWr3DUvXw.W16y6rgpAOjkgcGpFxCr1s6IFQzwLAS',NULL,NULL,'2020-10-25 20:02:25',NULL,0,1,0,1),(41,'lima666','$2b$10$4USD54JFk8HCS4Uvr4HfLuuyirF53iowDksQTzNH9GLAwGy.s0lCa',NULL,NULL,'2020-10-25 20:07:29',NULL,0,1,0,1),(42,'lima777','$2b$10$rFAznxnyTUZ.V04DpaAlcevR5h6bJVIC.x4PjpLy/uVOzqQf6154S',NULL,NULL,'2020-10-25 20:08:45',NULL,0,1,0,1),(43,'lima888','$2b$10$YsQflNhnKa/vg3s.zWxiA./QQC6ShuA66tOVAs5s2XKY..3AWSnNq',NULL,NULL,'2020-10-25 20:15:24',NULL,0,1,0,1),(44,'lima999','$2b$10$WH3Bicma48Ovm6R3vr4z3u4ugGq5pBwqjzDsLAOxwxgaogmsBnpIm',NULL,NULL,'2020-10-25 20:22:21',NULL,0,1,0,1),(45,'lima11','$2b$10$LGnjitpAQZVCZFXndfifPeBd3Hiq6arneY7zA/4mMI35g0fqfFn4u',NULL,NULL,'2020-10-25 20:27:35',NULL,0,1,0,1),(46,'lima222','$2b$10$kBYTXSCe0h6jZgJL9pWMTelUIqstOCx4eGZSWfM.4crIwlytUTvOy',NULL,NULL,'2020-10-25 20:28:32',NULL,0,1,0,1),(47,'lima444','$2b$10$jpT.Scdiow2hwfSd8m6n9enNfC0Nwf392tsyngWZepATx3IwsTdP.',NULL,NULL,'2020-10-25 20:31:38',NULL,0,1,0,1),(48,'lima1111','$2b$10$OmzJbXLsO1TrfZLuiyUeGu.AAyZJcFlE/H.T22gm/FUE.eIjGgnzW',NULL,NULL,'2020-10-25 20:34:09',NULL,0,1,0,1),(49,'lima9999','$2b$10$FokfU0ss1ZNoWMNJnFQup.ASv3T1tldug0i9LYmVUFTqrPQSB6wQa',NULL,NULL,'2020-10-25 20:47:03',NULL,0,1,0,1),(50,'lima1234567','$2b$10$dmcHmKbXYHGGRcHvVMDVu.1rTKNoaVLNGLSWHS13.GfNgEacrslGu',NULL,NULL,'2020-10-25 21:10:02',NULL,0,1,0,1),(51,'lima999999','$2b$10$SgZ3stMV9nfZHXBfVnBajO.xwBuhkJCIaXjqeekQkHqbXGB.YgMyS',NULL,NULL,'2020-10-25 21:10:35',NULL,0,1,0,1),(52,'lima1010','$2b$10$wXqL7Xxf9bI48Gf5PZlFpush0EAHgjWx.jpmaf8FWryhIoykWpev.',NULL,NULL,'2020-10-25 21:11:13',NULL,0,1,0,1),(53,'lovejoy','$2b$10$F5b8Avb3BhSPfH3FbyXxG.WDd3faPz2tnPsgjACj.pKYNqxqobZju',NULL,NULL,'2020-10-26 10:54:19',NULL,0,0,0,1),(54,'lima111111','$2b$10$/rSKtvY2CCez9.46nHI5t.4BSUX9iZCPeZd/QbR3ZsHQITtoU9Riq',NULL,NULL,'2020-10-26 10:57:49',NULL,0,1,0,1),(55,'qwerty','$2b$10$LFRbnjAHr3jkfSscdiFvmuawT7nQdxTEm5EX0uFCNOYsI.gDMWAkq',NULL,NULL,'2020-10-26 11:01:31',NULL,0,0,0,1),(56,'qwerty1','$2b$10$M9yGkDh4cVZBazYgmgaaee92Nln1CdyPANn/Xe2M8cu04mF1Q1PU6',NULL,NULL,'2020-10-26 11:01:53',NULL,0,1,0,1),(57,'qwerty222','$2b$10$D.HS.Wdi/wua.W/eUwtQq.tHBo3j6NET54xyDGrvE74ZX41nF2TB.',NULL,NULL,'2020-10-26 11:05:17',NULL,0,1,0,1),(58,'alabala111','$2b$10$NkZCNpxZK7t7Ii5ssrHkzuaAXoPyQmWBHpqwOKtZz0/EnEaxgQM3C',NULL,NULL,'2020-10-26 11:55:27',NULL,0,1,0,1),(59,'alabala11111','$2b$10$xeHK/ox1FKgzIK.7GyN6huYDIDbJ5BZCKYWVDwjCOVJtlxhpQvWk6',NULL,NULL,'2020-10-26 11:56:55',NULL,0,1,0,1),(60,'asdf26','$2b$10$OhF/j/3RLO8dnS7y2Ffk4uStMSFdPQDbM.t7bg4XZeQGroUSMPlWe',NULL,NULL,'2020-10-26 11:57:57','2020-11-01 13:19:07',0,1,0,1),(61,'lima111','$2b$10$7ehZGyoCMVUjvnPmp3OQ6Ogjv0ViekPXn4Px7E/S0ATwJrj/5SwPS',NULL,NULL,'2020-10-26 12:15:41',NULL,0,1,0,1),(62,'lima789','$2b$10$iTHuxXlwxvCrsVcPNZoMSuXy7xj9hRjvWVKqAK.Y2VRSRIiUQ00N.',NULL,NULL,'2020-10-26 12:16:14',NULL,0,0,0,1),(63,'admin12345','$2b$10$mNaRc2hRbZMKyiq7FGsCoOQ4q5wK1/oWWq860H91SFOJ0IYF.f9Cq',NULL,NULL,'2020-10-26 18:25:47',NULL,0,1,0,1),(64,'lifestyle123','$2b$10$gEiTx8/o8knwdt7akfW/R.Ekci45kLFhi7jHP06lVfKTjVfChGN3a',NULL,NULL,'2020-10-28 11:39:35',NULL,0,1,0,1),(65,'lifestyle11','$2b$10$tYqkYH5tiQZc/aPcnkVzhertc6zkTk8cD.olwQ7hhoOpu.5afm8V2',NULL,'1604237179801_UCL.jpg','2020-10-28 11:40:31','2020-11-01 15:26:24',0,0,72,2),(66,'Bubamara','$2b$10$K0pq3so3gNoKC1.flAkon./mHPucFTpSC10PjRy6lC/qeh1d7YGfa',NULL,NULL,'2020-10-28 13:59:52',NULL,0,0,20,1),(67,'asd123456','$2b$10$xp7crKcFuHyp7fAKWdTzRO01.gueTeE4xq75RQ/TqB6K/frVAEOn6',NULL,NULL,'2020-10-28 18:55:31',NULL,0,1,0,1),(68,'MariiaPetrovaMimailova','$2b$10$JMJCW4260npMtJnYcbQmO.bF1.Bwl33u/vjP22n29lrxr0IarHGuG','mariia@gmail.com',NULL,'2020-10-29 15:55:01',NULL,0,1,0,1),(69,'marina','$2b$10$qQTg29G9brLXRIe8.hgDx.wwoxFqvn7DnOext6F5XLeWIARUHtVMS','mariika@gmail.com',NULL,'2020-10-31 20:36:02',NULL,0,0,0,1),(70,'kitty','$2b$10$c6SIu09GMAKN/et/fKRS3.rk0Sr6Khpqt8aKeZIgAvw40sQy6jnzy','kitty@gmail.com',NULL,'2020-10-31 20:38:34',NULL,0,0,0,1),(71,'Mario','$2b$10$TquGrFrpgYdW3BbkfNRH3eDTAYS3nJcBR8VqhNOxQCaCKFvIHbdye','email@email.com','1604170106164_Hyper-Light-Drifter-Wallpapers-Wallpaper-Cave.jpg','2020-10-31 20:48:24',NULL,0,0,0,1),(72,'Marrsss','$2b$10$Gikw7SYQLUGCunBOe65hRu2eQj/0vyEKTrOCB17tRLdJ49.aFFK82','marr@gmail.com','1604170185649_80562758_2809211382463578_2572062244909088768_o.jpg','2020-10-31 20:49:18','2020-10-31 20:49:43',0,1,0,1),(73,'The Laureate','$2b$10$zJBN9RKqPFpmGmquu1sXmOeivdJOp7TMMuzh7l43FquALW5ZnD1vi','laureatei@yahoo.com',NULL,'2020-11-01 12:17:11',NULL,0,1,0,1),(74,'pencho','$2b$10$6HepJpEDtp4XFLJFDS3lg.957bOmUnTU4g.AbP6Ipyj4pq61NxolK',NULL,'1604247298348_champion-league-soccer-ball.jpg','2020-11-01 12:37:51','2020-11-01 18:15:02',0,0,61,2),(75,'joro','$2b$10$3Q4.AVRHz/qWtPvpcNn2.uCoUFqjqDS5t6sroRTQEmEv6jp2u3YXu',NULL,'1604227454718_epl_logo.jpg','2020-11-01 12:44:19',NULL,0,0,0,1),(76,'Victoria','$2b$10$ps7EoY5MC/ZMbLcoF2JYQ.MmyCseiGOIXcfyPh1NW5FPB7.i4aF.S','viki@gmail.com','1604229166895_vikito.PNG','2020-11-01 13:12:44',NULL,0,0,21,1),(77,'marianaDB','$2b$10$/.MM5s2Huf/X22s/p5SaB.DJuriIMOkE66X7x7FwV6/IqgVH16nyW',NULL,NULL,'2020-11-01 14:31:06',NULL,0,1,0,1),(78,'MongoDB','$2b$10$jo63lF2d2hz88M227I3RO.bNLW5.upZEs1Kzso5HGLI5CN3qGpPly',NULL,NULL,'2020-11-01 14:31:50',NULL,0,1,0,1),(79,'MoongoBG','$2b$10$ej8zy.1lCKR4myrtU7F85OMp4JdBZscCr6Z8C1VmM.TsjrCDB1AAS',NULL,NULL,'2020-11-01 14:33:48',NULL,0,1,0,1),(80,'MangoBg','$2b$10$tyQOcNFf3I.wdrOxiCC9zew3Z9UANnT8Rmutg06EMXEBear2P0uiG',NULL,NULL,'2020-11-01 14:34:00',NULL,0,1,0,1),(82,'mariomario','$2b$10$TUbjtueGYYiOI5TU8vA7zecQzRIyYqFbvjRlmtO4./BTd30gP.7j6',NULL,'1604234378588_vikito.PNG','2020-11-01 14:39:36',NULL,0,1,0,1),(83,'kari','$2b$10$GYatFQAb9NrB7FMBEAF69uJpMd.qsI5gdQ6f04SUcbeOBnuQpKOzi','kari@kari.com','1604238022028_cat-154642_640.png','2020-11-01 15:37:46','2020-11-01 16:34:22',0,0,190,2),(84,'Laureate','$2b$10$m3e5oKu40UyyrXKsGIJLfeSX4VvVok5OgLnM5ieWoJkacx5NYYBaG','laureate@yahoo.com','1604237897292_istockphoto-496916317-612x612.jpg','2020-11-01 15:38:15',NULL,0,0,430,3),(85,'Stradivarius','$2b$10$d.tK5tGWjg7Bb0S/oAXzWey2kO6fqWhR.qapZ2CKVTN8JD4UpgMFS','blondi@gmail.com','1604240913030_writer.jpg','2020-11-01 16:27:50','2020-11-01 16:28:30',0,0,274,3),(86,'asdf','$2b$10$brp27to94JzyonibvDoDEuGhFRyWwCoGWEpei9vEBX8/Mu.CGm5xq','asfasfasfa@gmail.com',NULL,'2020-11-01 17:23:13',NULL,0,0,0,1),(87,'Mark','$2b$10$KSZwlbnQ1r3eRV3e/LeWAOvVzTxI2LVhtfBRlDym..ppY5wS0KCym','face@book.com','1604258691137_mark.jpg','2020-11-01 21:24:18','2020-11-01 21:24:48',0,0,0,1),(88,'Maria.D.B.','$2b$10$esZSKnzpP1fvnQ4EFhYQheodn7KPiSpHtqpeONfivtKDD2iC4QeYq',NULL,'1604258953898_pexels-photo-4101141.jpeg','2020-11-01 21:29:00','2020-11-01 21:29:11',0,0,20,1),(89,'gosho','$2b$10$v7KQvqYQfyuZ882CO4cFYOVtUm0j8PjzQ.UL7mjm5J0V1i6ESKeZy','gosho@gosho.com','1604304512113_407.jpg','2020-11-02 10:06:24','2020-11-02 10:08:36',0,0,58,2),(90,'Daniel','$2b$10$XkrGL3HS2rAT6GUy0aaVauMt6jpx/qIQQGb3dPIGZ2WLeFyY5K7fe',NULL,NULL,'2020-11-02 10:29:53',NULL,0,0,20,1),(91,'guhaho','$2b$10$nZ0RDmeVf5mkQq46erxM2eCFGyswaLY5l6Snyw.PbJeHbTofuC98e','guhaho@guhaho.com','1604308122115_theDude.jpg','2020-11-02 11:07:14','2020-11-02 11:08:40',0,0,21,1),(92,'ogonommo','$2b$10$uPhuj20P8B2nk/2WFnKZsu4LtsnhD3rOU7dA9Px3mukYzP6SBnRxi','ogonommo@gmail.com',NULL,'2020-11-02 12:24:21',NULL,0,0,62,2),(93,'ivanski','$2b$10$ng7mncsZWJTMU49skp5c0uJrnIvUEEs/k3fg15bDRgGLHky86tdPK',NULL,NULL,'2020-11-02 12:28:42',NULL,0,0,30,1),(94,'edo','$2b$10$ci9QnbKjsiNWP1FCvI8WfeVUuWMreVivZ9tXroJ5MrSIENVoAPfAG','edo@abv.bg',NULL,'2020-11-02 12:40:38',NULL,0,1,10,1),(95,'user16','$2b$10$U6nuQqol0qQVm411MD/.zuiX1tlNhYu62TIbnLup8A8rdLVSZWrja',NULL,NULL,'2020-11-02 13:32:11',NULL,0,0,10,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_active_bans`
--

DROP TABLE IF EXISTS `v_active_bans`;
/*!50001 DROP VIEW IF EXISTS `v_active_bans`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_active_bans` AS SELECT 
 1 AS `id`,
 1 AS `description`,
 1 AS `fromDate`,
 1 AS `toDate`,
 1 AS `userId`,
 1 AS `username`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_avg_rating`
--

DROP TABLE IF EXISTS `v_avg_rating`;
/*!50001 DROP VIEW IF EXISTS `v_avg_rating`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_avg_rating` AS SELECT 
 1 AS `bookId`,
 1 AS `rating`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_bans`
--

DROP TABLE IF EXISTS `v_bans`;
/*!50001 DROP VIEW IF EXISTS `v_bans`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_bans` AS SELECT 
 1 AS `id`,
 1 AS `description`,
 1 AS `fromDate`,
 1 AS `toDate`,
 1 AS `userId`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_books`
--

DROP TABLE IF EXISTS `v_books`;
/*!50001 DROP VIEW IF EXISTS `v_books`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_books` AS SELECT 
 1 AS `id`,
 1 AS `title`,
 1 AS `author`,
 1 AS `year`,
 1 AS `cover`,
 1 AS `description`,
 1 AS `createDate`,
 1 AS `updateDate`,
 1 AS `genreId`,
 1 AS `languageId`,
 1 AS `statusId`,
 1 AS `genre`,
 1 AS `language`,
 1 AS `status`,
 1 AS `rating`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_borrows`
--

DROP TABLE IF EXISTS `v_borrows`;
/*!50001 DROP VIEW IF EXISTS `v_borrows`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_borrows` AS SELECT 
 1 AS `id`,
 1 AS `fromDate`,
 1 AS `toDate`,
 1 AS `userId`,
 1 AS `bookId`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_comments`
--

DROP TABLE IF EXISTS `v_comments`;
/*!50001 DROP VIEW IF EXISTS `v_comments`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_comments` AS SELECT 
 1 AS `id`,
 1 AS `content`,
 1 AS `createDate`,
 1 AS `updateDate`,
 1 AS `editReason`,
 1 AS `reviewId`,
 1 AS `userId`,
 1 AS `userName`,
 1 AS `userAvatar`,
 1 AS `userLevel`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_levels`
--

DROP TABLE IF EXISTS `v_levels`;
/*!50001 DROP VIEW IF EXISTS `v_levels`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_levels` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `requiredPoints`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_likes`
--

DROP TABLE IF EXISTS `v_likes`;
/*!50001 DROP VIEW IF EXISTS `v_likes`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_likes` AS SELECT 
 1 AS `id`,
 1 AS `reviewId`,
 1 AS `userId`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_likes_by_review`
--

DROP TABLE IF EXISTS `v_likes_by_review`;
/*!50001 DROP VIEW IF EXISTS `v_likes_by_review`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_likes_by_review` AS SELECT 
 1 AS `reviewId`,
 1 AS `likes`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ratings`
--

DROP TABLE IF EXISTS `v_ratings`;
/*!50001 DROP VIEW IF EXISTS `v_ratings`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ratings` AS SELECT 
 1 AS `id`,
 1 AS `rating`,
 1 AS `createDate`,
 1 AS `updateDate`,
 1 AS `bookId`,
 1 AS `userId`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_reviews`
--

DROP TABLE IF EXISTS `v_reviews`;
/*!50001 DROP VIEW IF EXISTS `v_reviews`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_reviews` AS SELECT 
 1 AS `id`,
 1 AS `content`,
 1 AS `createDate`,
 1 AS `updateDate`,
 1 AS `editReason`,
 1 AS `bookId`,
 1 AS `bookTitle`,
 1 AS `bookAuthor`,
 1 AS `rating`,
 1 AS `userId`,
 1 AS `userName`,
 1 AS `userAvatar`,
 1 AS `userLevel`,
 1 AS `likes`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_sessions`
--

DROP TABLE IF EXISTS `v_sessions`;
/*!50001 DROP VIEW IF EXISTS `v_sessions`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_sessions` AS SELECT 
 1 AS `id`,
 1 AS `loginDate`,
 1 AS `authToken`,
 1 AS `userId`,
 1 AS `ip`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_users`
--

DROP TABLE IF EXISTS `v_users`;
/*!50001 DROP VIEW IF EXISTS `v_users`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_users` AS SELECT 
 1 AS `id`,
 1 AS `username`,
 1 AS `email`,
 1 AS `avatar`,
 1 AS `createDate`,
 1 AS `updateDate`,
 1 AS `isAdmin`,
 1 AS `points`,
 1 AS `level`,
 1 AS `levelId`,
 1 AS `lastSeen`,
 1 AS `isBanned`,
 1 AS `banToDate`,
 1 AS `banDescription`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'teleriklibrary'
--

--
-- Dumping routines for database 'teleriklibrary'
--
/*!50003 DROP FUNCTION IF EXISTS `has_reviewed` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `has_reviewed`(user_id INT, book_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE hasReviewed BOOL;
    SET hasReviewed = 0;
    SELECT 	EXISTS(	SELECT * FROM v_reviews AS r
					WHERE `user_id` = r.userId
                    AND `book_id` = r.bookId) 
			INTO hasReviewed ;
    RETURN hasReviewed;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_admin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `is_admin`(user_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE isAdmin BOOL;
    SET isAdmin = 0;
    SELECT 	EXISTS(	SELECT * FROM v_users AS u
					WHERE `user_id` = u.id
                    AND u.isAdmin = 1) 
			INTO isAdmin ;
    RETURN isAdmin;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_banned` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `IS_BANNED`(user_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE isBanned BOOL;
    SET isBanned = 0;
    SELECT 	EXISTS(	SELECT * FROM v_active_bans AS ab 
					WHERE `user_id` = ab.userId) 
			INTO isBanned ;
    RETURN isBanned;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `is_read_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` FUNCTION `is_read_by_user`(user_id INT, book_id INT) RETURNS tinyint(1)
BEGIN
    DECLARE isRead BOOL;
    SET isRead = 0;
    SELECT EXISTS(	SELECT * FROM v_borrows AS b
					WHERE `user_id` = b.userId
                    AND `book_id` = b.bookId
                    AND b.toDate IS NOT NULL) 
			INTO isRead;
    RETURN isRead;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ban_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `ban_user`(IN user_id INT, IN reason TEXT, IN days INT)
BEGIN
	DECLARE banned BOOL DEFAULT 0;
	SELECT is_banned(user_id) INTO banned;
    
    IF banned THEN
		UPDATE bans
		SET bans.to_date = DATE_ADD(bans.to_date, INTERVAL IFNULL(days, 1) DAY)
		WHERE bans.id = (SELECT id FROM v_active_bans ab WHERE ab.userId = user_id);
    ELSE
		INSERT INTO bans (description, from_date, to_date, user_id)
		VALUES (reason, NOW(), DATE_ADD(NOW(), INTERVAL IFNULL(days, 1) DAY), user_id);
	END IF;
    
    CALL calculate_points(user_id, 'ban');
    CALL get_user_by('id', user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `borrow_book` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `borrow_book`(IN user_id INT, IN book_id INT)
BEGIN
	INSERT INTO borrows (from_date, user_id, book_id)
    VALUES (NOW(), user_id, book_id);
	UPDATE books b SET b.status_id = 2
    WHERE b.id = book_id;
    CALL get_book(book_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calculate_level` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `calculate_level`(IN user_id INT)
BEGIN

	UPDATE users AS u
	SET u.level_id = (
		SELECT l.id 
        FROM levels l 
        WHERE u.points >= l.required_points 
        ORDER BY required_points DESC 
        LIMIT 1 
    )
	WHERE u.id = user_id;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calculate_points` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `calculate_points`(IN user_id INT, IN action VARCHAR(20))
BEGIN
	DECLARE points_ INT DEFAULT 0;
    SET points_ = (SELECT points from v_users u WHERE u.id = user_id);
    
    SET points_ = (
    
	CASE action
		WHEN 'review' THEN points_ + 10
        WHEN 'delete_review' THEN points_ - 10
		WHEN 'return' THEN points_ + 10
		WHEN 'like' THEN points_ + 1
        WHEN 'delete_like' THEN points_ - 1
		WHEN 'ban' THEN points_ - points_ * 0.1
	END
    );

	UPDATE users AS u
	SET u.points = points_
	WHERE u.id = user_id;
    
    CALL calculate_level(user_id);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_book` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `create_book`(IN title VARCHAR(100), IN author VARCHAR(100), 
							IN year INT, IN cover BLOB, IN description TEXT, 
                            IN genre_id INT, IN language_id INT)
BEGIN
	INSERT INTO books (title, author, year, cover,
						description, create_date, genre_id,
                        language_id, status_id)
    VALUES (title, author, year, cover, 
			description, NOW(), genre_id, 
            language_id, 1);

    CALL get_book(LAST_INSERT_ID());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_comment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `create_comment`(IN content TEXT, IN user_id INT, IN review_id INT)
BEGIN
	INSERT INTO comments (content, create_date, user_id, review_id)
    VALUES (content, NOW(), user_id, review_id);

    CALL get_comment(LAST_INSERT_ID());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_review` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `create_review`(IN content TEXT, IN user_id INT, IN book_id INT)
BEGIN
	INSERT INTO reviews (content, create_date, user_id, book_id)
    VALUES (content, NOW(), user_id, book_id);
	
    CALL calculate_points(user_id, 'review');
    CALL get_review(LAST_INSERT_ID());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_session` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `create_session`(IN user_id INT, IN auth_token VARCHAR(255), IN ip VARCHAR(46))
BEGIN
	INSERT INTO sessions (login_date, user_id, auth_token, ip)
    VALUES (NOW(), user_id, auth_token, ip);

    CALL get_session(LAST_INSERT_ID());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `create_user`(IN username VARCHAR(255), 
													IN password VARCHAR(255), 
													IN email VARCHAR(255), 
                                                    IN avatar VARCHAR(255))
BEGIN
	INSERT INTO users (username, password, email, avatar, create_date)
    VALUES (username, password, email, avatar, NOW());

    CALL get_user_by('id', LAST_INSERT_ID());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `create_user_admin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `create_user_admin`(IN username VARCHAR(255), IN password VARCHAR(255), 
                                            IN email VARCHAR(255), IN avatar VARCHAR(255))
BEGIN
	INSERT INTO users (username, password, email, avatar, create_date)
    VALUES (username, password, email, avatar, NOW());

    CALL get_user_by('id', LAST_INSERT_ID());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_book` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `delete_book`(IN book_id INT)
BEGIN
    UPDATE books b
    SET b.is_deleted = 1, 
		b.status_id = 3
    WHERE b.id = book_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_comment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `delete_comment`(IN comment_id INT)
BEGIN
    UPDATE comments c
    SET c.is_deleted = 1
    WHERE c.id = comment_id;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_like` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `delete_like`(IN user_id INT, IN review_id INT)
BEGIN
    UPDATE likes l
    SET l.is_deleted = 1
    WHERE l.user_id = user_id AND l.review_id = review_id;
    
    CALL calculate_points((SELECT r.user_id FROM reviews r WHERE r.id = review_id), 'delete_like');
    CALL get_review(review_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_rating` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `delete_rating`(IN user_id INT, IN book_id INT)
BEGIN
    UPDATE ratings r
    SET r.is_deleted = 1
    WHERE r.user_id = user_id AND r.book_id = book_id;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_review` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `delete_review`(IN review_id INT)
BEGIN
    UPDATE reviews r
    SET r.is_deleted = 1
    WHERE r.id = review_id;
    
    CALL calculate_points((SELECT r.user_id FROM reviews r WHERE r.id = review_id), 'delete_review');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `delete_user`(IN user_id INT)
BEGIN
    UPDATE users u
    SET u.is_deleted = 1
    WHERE u.id = user_id;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_active_bans` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_active_bans`()
BEGIN
	SELECT userId, toDate FROM v_active_bans;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_active_borrow` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_active_borrow`(IN user_id INT, IN book_id INT)
BEGIN
	SELECT * FROM v_borrows b
    WHERE 	b.userId = user_id
    AND		b.bookId = book_id
    AND		b.toDate IS NULL;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_books` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_books`(IN page_number INT, IN per_page INT, 
													IN sort_by VARCHAR(15), IN sort_order VARCHAR(5))
BEGIN
    DECLARE _offset INT DEFAULT 0;
    DECLARE _order VARCHAR(20) DEFAULT 'title asc';
    
    SET _offset = (page_number - 1) * per_page;
    SET _order = CONCAT(sort_by, ' ', sort_order);
    
	SET @sql_ = CONCAT('SELECT * FROM v_books ORDER BY ', _order, ' LIMIT ', per_page, ' OFFSET ', _offset);
    
	PREPARE stmt FROM @sql_;
	EXECUTE stmt;    
    DEALLOCATE PREPARE stmt;
    
    SELECT count(*) FROM v_books INTO @count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_borrows` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_borrows`()
BEGIN
	SELECT * FROM v_borrows;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_genres` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_genres`()
BEGIN
	SELECT * FROM genres g ORDER BY g.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_languages` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_languages`()
BEGIN
	SELECT * FROM languages l ORDER BY l.name;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_levels` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_levels`()
BEGIN
	SELECT * FROM levels;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_reviews` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_reviews`()
BEGIN
	SELECT * FROM v_reviews;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_statuses` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_statuses`()
BEGIN
	SELECT * FROM statuses;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_all_users`(IN page_number INT, IN per_page INT, 
													IN sort_by VARCHAR(15), IN sort_order VARCHAR(5))
BEGIN
    DECLARE _offset INT DEFAULT 0;
    DECLARE _order VARCHAR(20) DEFAULT 'username asc';
    
    SET _offset = (page_number - 1) * per_page;
    SET _order = CONCAT(sort_by, ' ', sort_order);
    
	SET @sql_ = CONCAT('SELECT * FROM v_users ORDER BY ', _order, ' LIMIT ', per_page, ' OFFSET ', _offset);
    
	PREPARE stmt FROM @sql_;
	EXECUTE stmt;    
    DEALLOCATE PREPARE stmt;
    
    SELECT count(*) FROM v_users INTO @count;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_book` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_book`(IN book_id INT)
BEGIN
	SELECT * FROM v_books 
    WHERE id = book_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_borrows_by_book` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_borrows_by_book`(IN book_id INT)
BEGIN
	SELECT * FROM v_borrows b
    WHERE b.bookId = book_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_borrows_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_borrows_by_user`(IN user_id INT)
BEGIN
	SELECT bor.userId AS userId, bor.bookId AS bookId, bor.fromDate AS fromDate, bor.toDate AS toDate, b.title AS bookTitle, b.author AS bookAuthor
    FROM
        ((SELECT * from v_borrows ORDER BY id DESC LIMIT 18446744073709551615) AS `bor`
         JOIN `v_books` `b` ON (`b`.`id` = `bor`.`bookId`))
	WHERE bor.userId = user_id
    GROUP BY bookId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_comment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_comment`(IN comment_id INT)
BEGIN
	SELECT * FROM v_comments
    WHERE id = comment_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_comments_by_review` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_comments_by_review`(IN review_id INT)
BEGIN
	SELECT * FROM v_comments c
    WHERE c.reviewId = review_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_comments_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_comments_by_user`(IN user_id INT)
BEGIN
	SELECT * FROM v_comments c
    WHERE c.userId = user_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_dashboard_data` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_dashboard_data`()
BEGIN
	SELECT * FROM v_books b ORDER BY b.rating desc LIMIT 6;
    SELECT * FROM v_books b ORDER BY b.createDate desc LIMIT 6;
	SELECT * FROM v_reviews r ORDER BY r.likes desc LIMIT 3;
    SELECT * FROM v_users u WHERE isBanned = 0 ORDER BY u.points desc LIMIT 5;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_like` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_like`(IN user_id INT, IN review_id INT)
BEGIN
	SELECT * FROM v_likes l
    WHERE 	l.userId = user_id
    AND		l.reviewId = review_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_likes_by_review` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_likes_by_review`(IN review_id INT)
BEGIN
	SELECT * FROM v_likes l
    WHERE l.reviewId = review_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_likes_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_likes_by_user`(IN user_id INT)
BEGIN
	SELECT * FROM v_likes l
    WHERE l.userId = user_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_ratings_by_book` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_ratings_by_book`(IN book_id INT)
BEGIN
	SELECT * FROM v_ratings r
    WHERE r.bookId = book_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_ratings_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_ratings_by_user`(IN user_id INT)
BEGIN
	SELECT * FROM v_ratings r
    WHERE r.userId = user_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_review` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_review`(IN review_id INT)
BEGIN
	SELECT * FROM v_reviews 
    WHERE id = review_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_reviews_by_book` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_reviews_by_book`(IN book_id INT)
BEGIN
	SELECT * FROM v_reviews r
    WHERE r.bookId = book_id
    ORDER BY r.likes = (SELECT MAX(likes) FROM v_reviews rev WHERE rev.bookId = book_id) desc, r.createDate desc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_reviews_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_reviews_by_user`(IN user_id INT)
BEGIN
	SELECT * FROM v_reviews r
    WHERE r.userId = user_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_session` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_session`(IN session_id INT)
BEGIN
	SELECT * FROM v_sessions
    WHERE id = session_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_sessions_by_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_sessions_by_user`(IN user_id INT)
BEGIN
	SELECT * FROM v_sessions s
    WHERE s.userId = user_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_by` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_user_by`(IN col_name VARCHAR(255), IN term VARCHAR(255))
BEGIN
	SET @sql_ = CONCAT('SELECT * FROM v_users WHERE ', col_name, ' = ', '"', term, '"');
	PREPARE stmt FROM @sql_;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_user_password` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `get_user_password`(IN username VARCHAR(45))
BEGIN
	SELECT u.password AS password
    FROM users u
    WHERE u.username = username AND u.is_deleted = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `like_review` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `like_review`(IN user_id INT, IN review_id INT)
BEGIN
	INSERT INTO likes (user_id, review_id)
    VALUES (user_id, review_id);

	CALL calculate_points((SELECT r.user_id FROM reviews r WHERE r.id = review_id), 'like');
    CALL get_review(review_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `rate_book` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `rate_book`(IN user_id INT, IN book_id INT, IN rating INT)
BEGIN
    DECLARE ratingId INT;
	
    SELECT id FROM ratings AS r
	WHERE `user_id` = r.user_id
	AND `book_id` = r.book_id
    AND `is_deleted` = 0
    INTO ratingId;
	
    IF ratingId IS NULL 
    THEN 
		INSERT INTO ratings (rating, create_date, user_id, book_id)
		VALUES (rating, NOW(), user_id, book_id);
	ELSE
		UPDATE ratings AS r
		SET r.rating = rating,
			r.update_date = NOW()
		WHERE 	r.user_id = user_id
        AND 	r.book_id = book_id;		
	END IF;
    
    CALL get_book(book_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `remove_ban` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `remove_ban`(IN user_id INT)
BEGIN

	UPDATE bans
    SET bans.to_date = NOW()
    WHERE bans.id = (SELECT id FROM v_active_bans ab WHERE ab.userId = user_id);

    CALL get_user_by('id', user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `return_book` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `return_book`(IN user_id INT, IN book_id INT)
BEGIN
	UPDATE borrows AS bo
    SET    bo.to_date = NOW()
	WHERE  bo.user_id = user_id 
    AND    bo.book_id = book_id
    AND    bo.to_date IS NULL;
    
    UPDATE books AS b 
    SET    b.status_id = 1
    WHERE  b.id = book_id;
    
    CALL calculate_points(user_id, 'return');
    CALL   get_book(book_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_books` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `search_books`(IN page_number INT, IN per_page INT,
										IN sort_by VARCHAR(15), IN sort_order VARCHAR(5),
										IN title VARCHAR(100), IN author VARCHAR(100), IN year INT,
										IN genreId INT, IN languageId INT, IN statusId INT,
                                        IN rating INT)
BEGIN
    DECLARE _offset INT DEFAULT 0;
    DECLARE _order VARCHAR(20) DEFAULT 'title asc';
    SET _offset = (page_number - 1) * per_page;
    SET _order = CONCAT(sort_by, ' ', sort_order);
    
    IF (title IS NULL AND author IS NULL AND year IS NULL AND genreId IS NULL 
		AND languageId IS NULL AND statusId IS NULL AND rating IS NULL) THEN
        CALL get_all_books(page_number, per_page, sort_by, sort_order);
    ELSE
		SET @sql_base = CONCAT('SELECT * FROM v_books WHERE');
        SET @sql_base_count = CONCAT('SELECT count(*) FROM v_books WHERE');
        
        SET @sql_ = '';
        SET @first_ = 1;
        IF title IS NOT NULL THEN
			SET @sql_ = CONCAT(@sql_, ' title LIKE ', '\"%', title, '%\"');
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			END IF;
		END IF;
        IF author IS NOT NULL THEN
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			ELSE
				SET @sql_ = CONCAT(@sql_, ' AND');
			END IF;
			SET @sql_ = CONCAT(@sql_, ' author LIKE ', '\"%', author, '%\"');
		END IF;
        IF year IS NOT NULL THEN
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			ELSE
				SET @sql_ = CONCAT(@sql_, ' AND');
			END IF;
			SET @sql_ = CONCAT(@sql_, ' year LIKE ', '\"%', year, '%\"');
		END IF;
        IF genreId IS NOT NULL THEN
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			ELSE
				SET @sql_ = CONCAT(@sql_, ' AND');
			END IF;
			SET @sql_ = CONCAT(@sql_, ' genreId = ', genreId);
		END IF;
        IF languageId IS NOT NULL THEN
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			ELSE
				SET @sql_ = CONCAT(@sql_, ' AND');
			END IF;
			SET @sql_ = CONCAT(@sql_, ' languageId = ', languageId);
		END IF;
        IF statusId IS NOT NULL THEN
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			ELSE
				SET @sql_ = CONCAT(@sql_, ' AND');
			END IF;
			SET @sql_ = CONCAT(@sql_, ' statusId = ', statusId);
		END IF;
        IF rating IS NOT NULL THEN
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			ELSE
				SET @sql_ = CONCAT(@sql_, ' AND');
			END IF;
			SET @sql_ = CONCAT(@sql_, ' rating >= ', rating);
		END IF;
        
		SET @sql_pagination = CONCAT(@sql_base, @sql_, ' ORDER BY ', _order, ' LIMIT ', per_page, ' OFFSET ', _offset);
		SET @sql_count = CONCAT(@sql_base_count, @sql_, ' INTO @count;');
		
		PREPARE stmt_p FROM @sql_pagination;
		EXECUTE stmt_p;
		PREPARE stmt_c FROM @sql_count;
		EXECUTE stmt_c;
		
		DEALLOCATE PREPARE stmt_p;
		DEALLOCATE PREPARE stmt_c;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_book_by` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `search_book_by`(IN col_name VARCHAR(45), IN term VARCHAR(45))
BEGIN
	SET @sql_ = CONCAT('SELECT * FROM v_books WHERE ', col_name, ' LIKE ', '"%', term, '%"');
	PREPARE stmt FROM @sql_;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_users` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `search_users`(IN page_number INT, IN per_page INT,
										IN sort_by VARCHAR(15), IN sort_order VARCHAR(5),
                                        IN username VARCHAR(100), IN is_admin TINYINT,
										IN is_banned TINYINT, IN level_id VARCHAR(45))
BEGIN
    DECLARE _offset INT DEFAULT 0;
    DECLARE _order VARCHAR(20) DEFAULT 'username asc';
    SET _offset = (page_number - 1) * per_page;
    SET _order = CONCAT(sort_by, ' ', sort_order);
    
    IF (username IS NULL AND is_admin IS NULL AND is_banned IS NULL AND level_id IS NULL) THEN
        CALL get_all_users(page_number, per_page, sort_by, sort_order);
    ELSE
		SET @sql_base = CONCAT('SELECT * FROM v_users WHERE');
        SET @sql_base_count = CONCAT('SELECT count(*) FROM v_users WHERE');
        
        SET @sql_ = '';
        SET @first_ = 1;
        IF username IS NOT NULL THEN
			SET @sql_ = CONCAT(@sql_, ' username LIKE ', '\"%', username, '%\"');
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			END IF;
		END IF;
        IF is_admin IS NOT NULL THEN
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			ELSE
				SET @sql_ = CONCAT(@sql_, ' AND');
			END IF;
			SET @sql_ = CONCAT(@sql_, ' isAdmin = ', is_admin);
		END IF;
        IF is_banned IS NOT NULL THEN
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			ELSE
				SET @sql_ = CONCAT(@sql_, ' AND');
			END IF;
			SET @sql_ = CONCAT(@sql_, ' isBanned = ', is_banned);
		END IF;
        IF level_id IS NOT NULL THEN
            IF @first_ IS TRUE THEN
				SET @first_ = 0;
			ELSE
				SET @sql_ = CONCAT(@sql_, ' AND');
			END IF;
			SET @sql_ = CONCAT(@sql_, ' levelId >= ', level_id);
		END IF;
        
		SET @sql_pagination = CONCAT(@sql_base, @sql_, ' ORDER BY ', _order, ' LIMIT ', per_page, ' OFFSET ', _offset);
		SET @sql_count = CONCAT(@sql_base_count, @sql_, ' INTO @count;');

		PREPARE stmt_p FROM @sql_pagination;
		EXECUTE stmt_p;
		PREPARE stmt_c FROM @sql_count;
		EXECUTE stmt_c;
		
		DEALLOCATE PREPARE stmt_p;
		DEALLOCATE PREPARE stmt_c;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `toggle_admin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `toggle_admin`(IN user_id INT)
BEGIN
	UPDATE users AS u
	SET u.is_admin = NOT u.is_admin
	WHERE u.id = user_id;

    CALL get_user_by('id', user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_book` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `update_book`(IN book_id INT, IN title VARCHAR(100), IN author VARCHAR(100), 
							IN year INT, IN cover BLOB, IN description TEXT, 
                            IN genre_id INT, IN language_id INT, IN status_id INT)
BEGIN
	UPDATE books AS b
	SET b.title = title,
		b.author = author,
        b.year = year,
        b.cover = cover,
        b.description = description,
		b.update_date = NOW(),
        b.genre_id = genre_id,
        b.language_id = language_id,
        b.status_id = status_id
	WHERE b.id = book_id;

    CALL get_book(book_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_comment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `update_comment`(IN comment_id INT, IN content TEXT, IN edit_reason TINYTEXT)
BEGIN
	UPDATE comments AS c
	SET c.content = content,
		c.edit_reason = edit_reason,
		c.update_date = NOW()
	WHERE c.id = comment_id;

    CALL get_comment(comment_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_review` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `update_review`(IN review_id INT, IN content TEXT, IN edit_reason TINYTEXT)
BEGIN
	UPDATE reviews AS r
	SET r.content = content,
		r.edit_reason = edit_reason,
		r.update_date = NOW()
	WHERE r.id = review_id;

    CALL get_review(review_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`telerik`@`%` PROCEDURE `update_user`(IN user_id INT, IN username VARCHAR(255), IN password VARCHAR(255), IN email VARCHAR(255), IN avatar VARCHAR(255))
BEGIN
	UPDATE users AS u
	SET u.username = COALESCE(username, u.username),
		u.password = COALESCE(password, u.password),
        u.email = COALESCE(email, u.email),
        u.avatar = COALESCE(avatar, u.avatar),
		u.update_date = NOW()
	WHERE u.id = user_id;

    CALL get_user_by('id', user_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `v_active_bans`
--

/*!50001 DROP VIEW IF EXISTS `v_active_bans`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_active_bans` AS select `b`.`id` AS `id`,`b`.`description` AS `description`,`b`.`from_date` AS `fromDate`,`b`.`to_date` AS `toDate`,`b`.`user_id` AS `userId`,`u`.`username` AS `username` from (`bans` `b` join `users` `u` on(`b`.`user_id` = `u`.`id`)) where current_timestamp() between `b`.`from_date` and `b`.`to_date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_avg_rating`
--

/*!50001 DROP VIEW IF EXISTS `v_avg_rating`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_avg_rating` AS select `r`.`book_id` AS `bookId`,avg(`r`.`rating`) AS `rating` from `ratings` `r` where `r`.`is_deleted` = 0 group by `r`.`book_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_bans`
--

/*!50001 DROP VIEW IF EXISTS `v_bans`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_bans` AS select `bans`.`id` AS `id`,`bans`.`description` AS `description`,`bans`.`from_date` AS `fromDate`,`bans`.`to_date` AS `toDate`,`bans`.`user_id` AS `userId` from `bans` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_books`
--

/*!50001 DROP VIEW IF EXISTS `v_books`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_books` AS select `b`.`id` AS `id`,`b`.`title` AS `title`,`b`.`author` AS `author`,`b`.`year` AS `year`,`b`.`cover` AS `cover`,`b`.`description` AS `description`,`b`.`create_date` AS `createDate`,`b`.`update_date` AS `updateDate`,`b`.`genre_id` AS `genreId`,`b`.`language_id` AS `languageId`,`b`.`status_id` AS `statusId`,`g`.`name` AS `genre`,`l`.`name` AS `language`,`s`.`name` AS `status`,round(ifnull(`avg`.`rating`,0),2) AS `rating` from ((((`books` `b` join `genres` `g` on(`b`.`genre_id` = `g`.`id`)) join `languages` `l` on(`b`.`language_id` = `l`.`id`)) join `statuses` `s` on(`b`.`status_id` = `s`.`id`)) left join `v_avg_rating` `avg` on(`b`.`id` = `avg`.`bookId`)) where `b`.`is_deleted` = 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_borrows`
--

/*!50001 DROP VIEW IF EXISTS `v_borrows`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_borrows` AS select `borrows`.`id` AS `id`,`borrows`.`from_date` AS `fromDate`,`borrows`.`to_date` AS `toDate`,`borrows`.`user_id` AS `userId`,`borrows`.`book_id` AS `bookId` from `borrows` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_comments`
--

/*!50001 DROP VIEW IF EXISTS `v_comments`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_comments` AS select `teleriklibrary`.`comments`.`id` AS `id`,`teleriklibrary`.`comments`.`content` AS `content`,`teleriklibrary`.`comments`.`create_date` AS `createDate`,`teleriklibrary`.`comments`.`update_date` AS `updateDate`,`teleriklibrary`.`comments`.`edit_reason` AS `editReason`,`teleriklibrary`.`comments`.`review_id` AS `reviewId`,`teleriklibrary`.`comments`.`user_id` AS `userId`,`u`.`username` AS `userName`,`u`.`avatar` AS `userAvatar`,`u`.`level` AS `userLevel` from (`teleriklibrary`.`comments` left join `teleriklibrary`.`v_users` `u` on(`teleriklibrary`.`comments`.`user_id` = `u`.`id`)) where `teleriklibrary`.`comments`.`is_deleted` = 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_levels`
--

/*!50001 DROP VIEW IF EXISTS `v_levels`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_levels` AS select `levels`.`id` AS `id`,`levels`.`name` AS `name`,`levels`.`required_points` AS `requiredPoints` from `levels` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_likes`
--

/*!50001 DROP VIEW IF EXISTS `v_likes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_likes` AS select `likes`.`id` AS `id`,`likes`.`review_id` AS `reviewId`,`likes`.`user_id` AS `userId` from `likes` where `likes`.`is_deleted` = 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_likes_by_review`
--

/*!50001 DROP VIEW IF EXISTS `v_likes_by_review`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_likes_by_review` AS select `l`.`review_id` AS `reviewId`,count(`l`.`review_id`) AS `likes` from `likes` `l` where `l`.`is_deleted` <> 1 group by `l`.`review_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ratings`
--

/*!50001 DROP VIEW IF EXISTS `v_ratings`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ratings` AS select `ratings`.`id` AS `id`,`ratings`.`rating` AS `rating`,`ratings`.`create_date` AS `createDate`,`ratings`.`update_date` AS `updateDate`,`ratings`.`book_id` AS `bookId`,`ratings`.`user_id` AS `userId` from `ratings` where `ratings`.`is_deleted` = 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_reviews`
--

/*!50001 DROP VIEW IF EXISTS `v_reviews`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_reviews` AS select `teleriklibrary`.`reviews`.`id` AS `id`,`teleriklibrary`.`reviews`.`content` AS `content`,`teleriklibrary`.`reviews`.`create_date` AS `createDate`,`teleriklibrary`.`reviews`.`update_date` AS `updateDate`,`teleriklibrary`.`reviews`.`edit_reason` AS `editReason`,`teleriklibrary`.`reviews`.`book_id` AS `bookId`,`b`.`title` AS `bookTitle`,`b`.`author` AS `bookAuthor`,`r`.`rating` AS `rating`,`teleriklibrary`.`reviews`.`user_id` AS `userId`,`u`.`username` AS `userName`,`u`.`avatar` AS `userAvatar`,`u`.`level` AS `userLevel`,ifnull(`l`.`likes`,0) AS `likes` from ((((`teleriklibrary`.`reviews` left join `teleriklibrary`.`v_likes_by_review` `l` on(`teleriklibrary`.`reviews`.`id` = `l`.`reviewId`)) left join `teleriklibrary`.`v_users` `u` on(`teleriklibrary`.`reviews`.`user_id` = `u`.`id`)) left join `teleriklibrary`.`v_books` `b` on(`teleriklibrary`.`reviews`.`book_id` = `b`.`id`)) left join `teleriklibrary`.`v_ratings` `r` on(`teleriklibrary`.`reviews`.`user_id` = `r`.`userId` and `teleriklibrary`.`reviews`.`book_id` = `r`.`bookId`)) where `teleriklibrary`.`reviews`.`is_deleted` = 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_sessions`
--

/*!50001 DROP VIEW IF EXISTS `v_sessions`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_sessions` AS select `sessions`.`id` AS `id`,`sessions`.`login_date` AS `loginDate`,`sessions`.`auth_token` AS `authToken`,`sessions`.`user_id` AS `userId`,`sessions`.`ip` AS `ip` from `sessions` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_users`
--

/*!50001 DROP VIEW IF EXISTS `v_users`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`telerik`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_users` AS select `u`.`id` AS `id`,`u`.`username` AS `username`,`u`.`email` AS `email`,`u`.`avatar` AS `avatar`,`u`.`create_date` AS `createDate`,`u`.`update_date` AS `updateDate`,`u`.`is_admin` AS `isAdmin`,`u`.`points` AS `points`,`l`.`name` AS `level`,`u`.`level_id` AS `levelId`,`ses`.`loginDate` AS `lastSeen`,(select `IS_BANNED`(`u`.`id`)) AS `isBanned`,`bans`.`toDate` AS `banToDate`,`bans`.`description` AS `banDescription` from (((`teleriklibrary`.`users` `u` join `teleriklibrary`.`levels` `l` on(`l`.`id` = `u`.`level_id`)) left join `teleriklibrary`.`v_active_bans` `bans` on(`bans`.`userId` = `u`.`id`)) left join (select `s`.`id` AS `id`,`s`.`loginDate` AS `loginDate`,`s`.`authToken` AS `authToken`,`s`.`userId` AS `userId`,`s`.`ip` AS `ip` from (select `v_sessions`.`id` AS `id`,`v_sessions`.`loginDate` AS `loginDate`,`v_sessions`.`authToken` AS `authToken`,`v_sessions`.`userId` AS `userId`,`v_sessions`.`ip` AS `ip` from `teleriklibrary`.`v_sessions` order by `v_sessions`.`loginDate` desc limit 1000000000000) `s` group by `s`.`userId`) `ses` on(`ses`.`userId` = `u`.`id`)) where `u`.`is_deleted` = 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-11 21:12:17
