import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import authRouter from './routes/auth-router.js';
import usersRouter from './routes/users-router.js';
import booksRouter from './routes/books-router.js';
import homeRouter from './routes/home-router.js';
import authController from './controllers/auth-controller.js';
import userController from './controllers/users-controller.js';
import utils from './common/util.js';

import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import { seedMemoryStorage, pullFreshBanStatuses } from './common/common.js';

import {
  notFound,
  productionErrors,
  developmentErrors,
  catchErrors,
} from './handlers/error-handler.js';

import dotenv from 'dotenv'; //process.env.PASS

dotenv.config();

const app = express();
passport.use(jwtStrategy);

app.use(cors());
app.use(helmet());
app.use(bodyParser.json());
app.use(passport.initialize());

app.use('/images/', express.static(path.resolve(__dirname + '/../public/uploads/')));

app.listen(process.env.PORT, (err) => {
  if (err) {
    console.log('could not start');
  } else {
    seedMemoryStorage();
    setInterval(() => pullFreshBanStatuses(), +process.env.CHECK_BAN_INTERVAL); //get banned users from db every minute
    console.log(`Listening on port ${process.env.PORT}`);
  }
});

app.use('/home', homeRouter);
app.use('/session', authRouter);
app.use('/users', authController.authenticate, usersRouter);
app.use('/books', authController.authenticate, booksRouter);

/*testing upload cover image*/

app.post(
  '/upload',
  authController.authenticate,
  utils.upload.single('file'),
  async (req, res) => {
    const fileName = req.file.filename;
    res.status(201).json({ filename: fileName });
  },
);

//enums
app.use('/enums',  catchErrors(userController.getEnums));

app.use(notFound);

if (process.env.STAGE === 'development') {
  app.use(developmentErrors);
} else if (process.env.STAGE === 'production') {
  app.use(productionErrors);
}
