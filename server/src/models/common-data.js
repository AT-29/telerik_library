import pool from './pool.js';

const getAllBorrows = async () => {
  const sql = 'CALL get_all_borrows()';
  const result = await pool.query(sql);
  return result?.[0];
};

const getAllGenres = async () => {
  const sql = 'CALL get_all_genres()';
  const result = await pool.query(sql);
  return result?.[0];
};

const getAllLanguages = async () => {
  const sql = 'CALL get_all_languages()';
  const result = await pool.query(sql);
  return result?.[0];
};

const getAllLevels = async () => {
  const sql = 'CALL get_all_levels()';
  const result = await pool.query(sql);
  return result?.[0];
};

const getAllStatuses = async () => {
  const sql = 'CALL get_all_statuses()';
  const result = await pool.query(sql);
  return result?.[0];
};


const getBorrowsByBook = async (bookId) => {
  const sql = 'CALL get_borrows_by_book( ? )';
  const result = await pool.query(sql, [bookId]);
  return result?.[0];
};

const getBorrowsByUser = async (user_id) => {
  const sql = 'CALL get_borrows_by_user( ? )';
  const result = await pool.query(sql, [user_id]);
  return result?.[0];
};

const getCommentsByUser = async (user_id) => {
  const sql = 'CALL get_comments_by_user( ? )';
  const result = await pool.query(sql, [user_id]);
  return result?.[0];
};


const getLike = async (user_id, review_id) => {
  const sql = 'CALL get_like( ? )';
  const result = await pool.query(sql, [user_id, review_id]);
  return result?.[0];
};

const deleteRating = async (user_id, review_id) => {
  const sql = 'CALL delete_rating( ? )';
  const result = await pool.query(sql, [user_id, review_id]);
  return result?.[0];
};
const getDashBoardData = async () =>{
  const sql = 'CALL get_dashboard_data()';
  const result = await pool.query(sql);
  return result;
};


export default {
  deleteRating,
  getLike,
  getCommentsByUser,
  getBorrowsByUser,
  getBorrowsByBook,
  getAllBorrows,
  getAllGenres,
  getAllLanguages,
  getAllLevels,
  getAllStatuses,
  getDashBoardData,
};
