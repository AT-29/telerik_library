import pool from './pool.js';

const getAllReviews = async (user_id) => {
  const sql = 'CALL get_all_reviews( ? )';
  const result = await pool.query(sql, [user_id]);
  return result?.[0];
};
//is this real?
const searchReviewBy = async (column, filter) => {
  const sql = 'CALL search_reviews_by(?, ?)';
  const result = await pool.query(sql, [column, filter]);
  return result?.[0];
};

const getReview = async (id) => {
  const sql = 'CALL get_review(?)';
  const result = await pool.query(sql, [id]);
  return result?.[0]?.[0];
};

const getReviewsByBook = async (id) => {
  const sql = 'CALL get_reviews_by_book(?)';
  const result = await pool.query(sql, [id]);
  return result?.[0];
};

const getReviewsByUser = async (id) => {
  const sql = 'CALL get_reviews_by_user(?)';
  const result = await pool.query(sql, [id]);
  return result?.[0];
};

const createReview = async (content, userId, bookId) => {
  const sql = 'CALL create_review(?, ?, ?)';
  const result = await pool.query(sql, [content, userId, bookId]);
  return result?.[0]?.[0];
};

const updateReview = async (reviewId, content, reason) => {
  const sql = 'CALL update_review(?, ?, ?)';
  const result = await pool.query(sql, [reviewId, content, reason]);
  return result?.[0]?.[0];
};

const deleteReview = async (reviewId) => {
  const sql = 'CALL delete_review(?)';
  await pool.query(sql, [reviewId]);
  return reviewId;
};

const getLikesByReview = async (likeId) => {
  const sql = 'CALL get_likes_by_review(?)';
  const result = await pool.query(sql, [likeId]);
  return result?.[0];
};
const getLike = async (userId, reviewId) => {
  const sql = 'CALL get_like(?, ?)';
  const result = await pool.query(sql, [userId, reviewId]);
  return result?.[0]?.[0];
};

const likeReview = async (userId, reviewId) => {
  const sql = 'CALL like_review(?, ?)';
  const result = await pool.query(sql, [userId, reviewId]);
  return result?.[0]?.[0];
};

const deleteLike = async (userId, likeId) => {
  const sql = 'CALL delete_like(?, ?)';
  const result = await pool.query(sql, [userId, likeId]);
  return result?.[0]?.[0];
};

const hasReviewed = async (userId, bookId) => {
  const sql = 'SELECT has_reviewed(?, ?) AS hasReviewed';
  const result = await pool.query(sql, [userId, bookId]);
  return result?.[0]?.hasReviewed;
};

export default { hasReviewed, deleteLike, likeReview, getLike, getLikesByReview, getReviewsByUser, getAllReviews, searchReviewBy, getReviewsByBook, createReview, updateReview, deleteReview, getReview };
