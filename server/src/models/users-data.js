import pool from './pool.js';

const createUser = async (username, hashedPassword, email, avatar) => {
  const sql = 'CALL create_user(?, ?, ?, ?)';
  const user = await pool.query(sql, [username, hashedPassword, email, avatar]);

  return user?.[0]?.[0];
};
const searchUsers = async (page, perPage, sortBy, sortOrder, username, isAdmin, isBanned, levelId) =>{
  const sql = 'CALL search_users(?, ?, ?, ?, ?, ?, ?, ?); SELECT @count;';
  const result = await pool.query(sql, [page, perPage, sortBy, sortOrder, username, isAdmin, isBanned, levelId]);
  const totalCount = result[2][0]['@count'];
  return {
    totalCount: totalCount,
    data: result?.[0],
    page: page,
    perPage: perPage,
  };
};

const getUserBy = async (colum, value) => {
  const sql = 'CALL get_user_by(?, ?)';
  const user = await pool.query(sql, [colum, value]);
  return user?.[0]?.[0];
};

const getUserPassword = async (username) => {
  const sql = 'CALL get_user_password(?)';
  const result = await pool.query(sql, [username]);
  return result?.[0]?.[0]?.password;
};

const updateUser = async (userId, username, password, email, avatar) => {
  const sql = 'CALL update_user(?, ?, ?, ?, ?)';
  const user = await pool.query(sql, [userId, username, password, email, avatar]);
  return user?.[0]?.[0];
};

const deleteUser = async (userId) => {
  const sql = 'CALL delete_user(?)';
  await pool.query(sql, [userId]);
  return userId;
};

const banUser = async (userId, reason, days) => {
  const sql = 'CALL ban_user(?, ?, ?)';
  const user = await pool.query(sql, [userId, reason, days]);
  return user?.[0]?.[0];
};

const toggleAdmin = async (userId) => {
  const sql = 'CALL toggle_admin(?)';
  const user = await pool.query(sql, [userId]);
  return user?.[0]?.[0];
};


const getLikesByUser = async (userId) => {
  const sql = 'CALL get_likes_by_user(?)';
  const result = await pool.query(sql, [userId]);
  return result?.[0];
};

const getBorrowsByUser = async (userId) => {
  const sql = 'CALL get_borrows_by_user(?)';
  const result = await pool.query(sql, [userId]);
  return result?.[0];
};

const getRatingsByUser = async (id) => {
  const sql = 'CALL get_ratings_by_user(?)';
  const result = await pool.query(sql, [id]);
  return result?.[0];
};

const isBanned = async (id) => {
  const sql = 'SELECT is_banned(?) AS isBanned';
  const result = await pool.query(sql, [id]);
  return result?.[0]?.isBanned;// returns 0 or 1 
};

const removeBan = async (id) => {
  const sql = 'CALL remove_ban(?)';
  const result = await pool.query(sql, [id]);
  return result?.[0]?.[0];
};

const isAdmin = async (id) => {
  const sql = 'SELECT is_admin(?) AS isAdmin';
  const result = await pool.query(sql, [id]);
  return result?.[0]?.isAdmin; // returns 0 or 1 
};

const getActiveBans = async () => {
  const sql = 'CALL get_active_bans()';
  const result = await pool.query(sql);
  const data = result?.[0];
  const map = [...data].reduce((acc, item) => acc.set(item.userId, item.toDate), new Map());
  return map;
};

export default {
  searchUsers,
  getBorrowsByUser,
  getActiveBans,
  removeBan,
  isBanned,
  isAdmin,
  getLikesByUser,
  createUser,
  getUserBy,
  updateUser,
  deleteUser,
  banUser,
  toggleAdmin,
  getRatingsByUser,
  getUserPassword,
};

