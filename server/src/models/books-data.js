import pool from './pool.js';

const getAllBooks = async (page, perPage, sortBy, sortOrder) => {
  const sql = ` CALL get_all_books(?, ?, ?, ?); 
                SELECT @count;`;
  const result = await pool.query(sql, [page, perPage, sortBy, sortOrder]);
  const totalCount = result[2][0]['@count'];

  return {
    totalCount: totalCount,
    data: result?.[0],
    page: page,
    perPage: perPage,
  };
};

const getBook = async (id) => {
  const sql = 'CALL get_book(?)';
  const result = await pool.query(sql, [id]);
  return result?.[0]?.[0];
};

const searchBooks = async (page, perPage, sortBy, sortOrder, title, author, year, genreId, languageId, statusId, rating) => {
  const sql = ` CALL search_books(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); 
                SELECT @count;`;
  const result = await pool.query(sql, [page, perPage, sortBy, sortOrder, title, author, year, genreId, languageId, statusId, rating]);
  const totalCount = result[2][0]['@count'];

  return {
    totalCount: totalCount,
    data: result?.[0],
    page: page,
    perPage: perPage,
  };
};

const searchBookBy = async (column, filter) => {
  const sql = 'CALL search_book_by(?, ?)';
  const result = await pool.query(sql, [column, filter]);
  return result?.[0];
};

const deleteBook = async (id) => {
  const sql = 'CALL delete_book(?)';
  await pool.query(sql, [id]);
  return id;
};

const getRatingsByBook = async (id) => {
  const sql = 'CALL get_ratings_by_book(?)';
  const result = await pool.query(sql, [id]);
  return result?.[0]?.[0];
};

const borrowBook = async (userId, bookId) => {
  const sql = 'CALL borrow_book(?, ?)';
  const result = await pool.query(sql, [userId, bookId]);
  return result?.[0]?.[0];
};

const returnBook = async (userId, bookId) => {
  const sql = 'CALL return_book(?, ?)';
  const result = await pool.query(sql, [userId, bookId]);
  return result?.[0]?.[0];
};

const rateBook = async (userId, bookId, rating) => {
  const sql = 'CALL rate_book(?, ?, ?)';
  const result = await pool.query(sql, [userId, bookId, rating]);
  return result?.[0]?.[0];
};

const createBook = async (title, author, year, cover, description, genreId, languageId) => {
  const sql = 'CALL create_book(?, ?, ?, ?, ?, ?, ?)';
  const result = await pool.query(sql, [title, author, year, cover, description, genreId, languageId]);
  return result?.[0]?.[0];
};

const updateBook = async (bookId, title, author, year, cover, description, genreId, languageId, statusId) => {
  const sql = 'CALL update_book(?, ?, ?, ?, ?, ?, ?, ?, ?)';
  const result = await pool.query(sql, [bookId, title, author, year, cover, description, genreId, languageId, statusId]);
  return result?.[0]?.[0];
};

const isReadByUser = async (userId, bookId) => {
  const sql = 'SELECT is_read_by_user(?, ?) AS isRead';
  const result = await pool.query(sql, [userId, bookId]);
  return result?.[0].isRead;

};

const getActiveBorrow = async (userId, bookId) => {
  const sql = 'CALL get_active_borrow( ?, ? )';
  const result = await pool.query(sql, [userId, bookId]);
  return result?.[0]?.[0];
};

export default {
  getActiveBorrow,
  searchBooks,
  isReadByUser,
  getAllBooks,
  searchBookBy,
  getBook,
  createBook,
  updateBook,
  deleteBook,
  getRatingsByBook,
  borrowBook,
  returnBook,
  rateBook,
};
