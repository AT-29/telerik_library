import pool from './pool.js';

const getComment = async (commentId) => {
  const sql = 'CALL get_comment(?)';
  const result = await pool.query(sql, [commentId]);
  return result?.[0]?.[0];
};

const searchCommentsBy = async (column, filter) => {
  const sql = 'CALL search_comments_by(?, ?)';
  const result = await pool.query(sql, [column, filter]);
  return result?.[0];
};

const getCommentsByReview = async (reviewId) => {
  const sql = 'CALL get_comments_by_review(?)';
  const result = await pool.query(sql, [reviewId]);
  return result?.[0];
};

const getCommentsByUser = async (userId) => {
  const sql = 'CALL get_comments_by_user(?)';
  const result = await pool.query(sql, [userId]);
  return result?.[0];
};

const createComment = async (content, userId, reviewId) => {
  const sql = 'CALL create_comment(?, ?, ?)';
  const result = await pool.query(sql, [content, userId, reviewId]);
  return result?.[0]?.[0];
};

const updateComment = async (commentId, content, reason) => {
  const sql = 'CALL update_comment(?, ?, ?)';
  const result = await pool.query(sql, [commentId, content, reason]);
  return result?.[0]?.[0];
};

const deleteComment = async (commentId) => {
  const sql = 'CALL delete_comment(?)';
  await pool.query(sql, [commentId]);
  return commentId;
};

export default { getCommentsByUser, getCommentsByReview, searchCommentsBy, deleteComment, updateComment, createComment, getComment };
