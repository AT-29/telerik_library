import mariadb from 'mariadb';
import dotenv from 'dotenv'; //process.env.PASS

dotenv.config();

const pool = mariadb.createPool({
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  connectionLimit: 10,
  multipleStatements: true,
});
export default pool;



