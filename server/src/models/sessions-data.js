import pool from './pool.js';

const createSession = async (userId, authToken, ip) => {
  const sql = 'CALL create_session(?, ?, ?)';
  const result = await pool.query(sql, [userId, authToken, ip]);
  return result?.[0]?.[0];
};

const closeSession = async (userId) => {
  const sql = 'CALL close_session(?)';
  const result = await pool.query(sql, [userId]);
  return result?.[0]?.[0];
};

const getSession = async (sessionId) => {
  const sql = 'CALL create_session(?)';
  const result = await pool.query(sql, [sessionId]);
  return result?.[0]?.[0];
};

const getSessionsByUser = async (userId) => {
  const sql = 'CALL get_sessions_by_user(?)';
  const result = await pool.query(sql, [userId]);
  return result?.[0]?.[0];
};

export default { createSession, getSession, getSessionsByUser, closeSession };
