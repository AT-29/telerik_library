import commonService from '../services/common-service.js';
import commonData from '../models/common-data.js';
import userService from '../services/user-service.js';
import userData from '../models/users-data.js';

const enumData = {
  minId: 1,
  minPage: 1,
  minRating: 1,
  maxRating: 5,
  minDays: 1,
  defaultPage: 1,
  defaultPerPage: 12,
  defaultSortBy: 'title',
  defaultSortOrder: 'asc',
  sortByValues: new Set(['title', 'author', 'year', 'createDate', 'genreId', 'languageId', 'statusId']),
  sortOrderValues: new Set(['asc', 'desc']),
  defaultSortUserBy: 'username',
  sortByUserValues: new Set(['username', 'createDate', 'points', 'level', 'isAdmin', 'isBanned']),
  userNameMinLength: 3,
  userNameMaxLength: 25,
  passwordMinLength: 3,
  passwordMaxLength: 25,
  titleMinLength: 3,
  titleMaxLength: 100,
  authorMinLength: 3,
  authorMaxLength: 100,
  reviewMinLength: 5,
  reviewMaxLength: 2000,
  reasonMinLength: 5,
  reasonMaxLength: 60,
  descriptionMinLength: 15,
  descriptionMaxLength: 2000,
  editReasonMinLength: 5,
  editReasonnMaxLength: 500,
};

const seedMemoryStorage = async () => {
  const genres = await commonService.getAllGenres(commonData)();
  const languages = await commonService.getAllLanguages(commonData)();
  const statuses = await commonService.getAllStatuses(commonData)();
  const activeBans = await userService.getActiveBans(userData)();

  const levels = await commonService.getAllLevels(commonData)();

  enumData.languages = languages;
  enumData.genres = genres;
  enumData.statuses = statuses;
  enumData.activeBans = activeBans;
  enumData.levels = levels;

};

const pullFreshBanStatuses = async () => {
  enumData.activeBans = await userService.getActiveBans(userData)();
};

const formatDate = (dateString) => {
  return new Date(dateString).toLocaleDateString('en-US', {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour:'numeric',
    minute:'numeric',
  });
};

export { enumData, seedMemoryStorage, pullFreshBanStatuses, formatDate };
