import validator from 'validator';
import utils from './util.js';
import { enumData } from './common.js';


const validateParam = (type, value) => {
  switch (type) {
    case 'id':
    case 'userId':
      utils.predicate(
        value,
        `ID should be A valid integer greater or equal to ${enumData.minId}`,
        (id) => typeof +id === 'number',
        (id) => Number.isInteger(+id),
        (id) => +id >= enumData.minId);
      break;
    case 'page':
    case 'perPage':
     !value || utils.predicate(
        value,
        `Page should be A valid integer greater or equal to ${enumData.minPage}`,
        (page) => typeof +page === 'number',
        (page) => Number.isInteger(+page),
        (page) => +page >= enumData.minPage);
      break;
    case 'sortBy':
      !value || utils.predicate(
        value,
        `Sorting column should be one of '${[...enumData.sortByValues].join("','")}'`,
        (sortBy) => typeof sortBy === 'string',
        (sortBy) => enumData.sortByValues.has(sortBy));
      break;
    case 'sortOrder':
      !value || utils.predicate(
        value,
        'Sorting order should be "asc" or "desc"',
        (sortOrder) => typeof sortOrder === 'string',
        (sortOrder) => enumData.sortOrderValues.has(sortOrder));
      break;
    case 'rating':
      utils.predicate(
        value,
        `Rating should be а valid integer between ${enumData.minRating} and ${enumData.maxRating}`,
        (rating) => typeof +rating === 'number',
        (rating) => Number.isInteger(+rating),
        (rating) => rating >= enumData.minRating && rating <= enumData.maxRating);
      break;

    case 'username':
      utils.predicate(
        value,
        `Username should be a string in the range between ${enumData.userNameMinLength} and ${enumData.userNameMaxLength} characters long`,
        (username) => username,
        (username) => typeof username === 'string',
        (username) => username.trim(),
        (username) => username.length >= enumData.userNameMinLength,
        (username) => username.length <= enumData.userNameMaxLength);
      break;

    case 'password':
      utils.predicate(
        value,
        `Password should be a string in the range between ${enumData.passwordMinLength} and ${enumData.passwordMaxLength} characters long`,
        (password) => password,
        (password) => typeof password === 'string',
        (password) => password.trim(),
        (password) => password.length >= enumData.passwordMinLength,
        (password) => password.length <= enumData.passwordMaxLength);
      break;

    case 'email':
     !value || utils.predicate(
        value,
        'Email should be in a valid email format',
        (email) => validator.isEmail(email));
      break;
    case 'content':
      utils.predicate(
        value,
        `Review content should be a string in the range between ${enumData.reviewMinLength} and ${enumData.reviewMaxLength} characters longs`,
        (review) => review,
        (review) => typeof review === 'string',
        (review) => review.trim(),
        (review) => review.length >= enumData.reviewMinLength,
        (review) => review.length <= enumData.reviewMaxLength);
      break;

    case 'reason':
      !value || utils.predicate(
        value,
        `Reason should be a string in range between ${enumData.reasonMinLength} and ${enumData.reasonMaxLength}`,
        (reason) => reason,
        (reason) => typeof reason === 'string',
        (reason) => reason.trim(),
        (reason) => reason.length >= enumData.reasonMinLength,
        (reason) => reason.length <= enumData.reasonMaxLength);
      break;
    case 'days':
      !value || utils.predicate(
        +value,
        `Days should be a valid integer greater or equal to ${enumData.minDays} or empty`,
        (days) => days,
        (days) => typeof days === 'number',
        (days) => Number.isInteger(days),
        (days) => days >= enumData.minDays);
      break;

    case 'title':
      utils.predicate(
        value,
        `Book title should be a string in the range between ${enumData.titleMinLength} and ${enumData.titleMaxLength} characters long`,
        (title) => title,
        (title) => typeof title === 'string',
        (title) => title.trim(),
        (title) => title.length >= enumData.titleMinLength,
        (title) => title.length <= enumData.titleMaxLength);
      break;

    case 'author':
      utils.predicate(
        value,
        `Author name should be a string in the range between ${enumData.authorMinLength} and ${enumData.authorMaxLength} characters long`,
        (author) => author,
        (author) => typeof author === 'string',
        (author) => author.trim(),
        (author) => author.length >= enumData.authorMinLength,
        (author) => author.length <= enumData.authorMaxLength);
      break;

    case 'year':
      utils.predicate(
        value,
        'Year should be a valid integer',
        (year) => typeof +year === 'number',
        (year) => Number.isInteger(+year));
      break;
    case 'cover':
       utils.predicate(
         value,
        'Book cover should be a valid url adress',
        (cover) => cover,
      );
      break;
      case 'avatar':
        !value || utils.predicate(
          value,
          'Avatar should be a valid url adress',
          (avatar) => avatar,
          (avatar) => typeof avatar === 'string',
        );
        break;
    case 'description':
      !value || utils.predicate(
        value,
        `Book description should be a string in the range between ${enumData.descriptionMinLength} and ${enumData.descriptionMaxLength} characters long`,
        (description) => description,
        (description) => typeof description === 'string',
        (description) => description.trim(),
        (description) => description.length >= enumData.descriptionMinLength,
        (description) => description.length <= enumData.descriptionMaxLength);
      break;
    case 'editReason':
      !value || utils.predicate(
        value,
        `Edit reason should be a string in the range between ${enumData.editReasonMinLength} and ${enumData.editReasonnMaxLength} characters long`,
        (editReason) => editReason,
        (editReason) => typeof editReason === 'string',
        (editReason) => editReason.trim(),
        (editReason) => editReason.length >= enumData.editReasonMinLength,
        (editReason) => editReason.length <= enumData.editReasonnMaxLength);
      break;
    case 'languageId':
      utils.predicate(
        value,
        `Language id should be a valid integer greater than ${enumData.minId} from a selection of languages`,
        (languageId) => typeof +languageId === 'number',
        (languageId) => Number.isInteger(+languageId),
        (languageId) => +languageId >= enumData.minId,
        (languageId) => enumData.languages.find(item => +item.id === +languageId));
      break;
    case 'genreId':
      utils.predicate(
        value,
        `Genre id should be a valid integer greater than ${enumData.minId} from a selection of genres`,
        (genreId) => typeof +genreId === 'number',
        (genreId) => Number.isInteger(+genreId),
        (genreId) => +genreId >= enumData.minId,
        (genreId) => enumData.genres.find(item => +item.id === +genreId));
      break;
    case 'statusId':
      !value || utils.predicate(
        value,
        `Status id should be a valid integer greater than ${enumData.minId} from a selection of statuses`,
        (statusId) => typeof +statusId === 'number',
        (statusId) => Number.isInteger(+statusId),
        (statusId) => +statusId >= enumData.minId,
        (statusId) => enumData.statuses.find(item => +item.id === +statusId));
      break;
    default:
      break;
  }
};

const Schema = {
  ID:['id'],
  User: ['username', 'password', 'email'],
  UpdateUser: ['email','avatar'],
  Session: ['username', 'password'],
  Book: [
    'title',
    'author',
    'year',
    'cover',
    'description',
    'genreId',
    'languageId',
  ],
  Review: ['content', 'editReason'],
  Comment: ['content', 'editReason'],
  Rating: ['rating'],
  Ban: ['reason', 'days'],
  Pagination:['page','perPage','sortBy','sortOrder'],
};

const validateByScheme = (params,section='body',mode=false) => {
  return async (req, res, next) => {
    await params.map((param) => {
      if (param in req[section]) {
        validateParam(param, req[section][param]);
      }else{
        if(mode==='required'){
          throw new Error(`${param} is required in ${section}!`);
        }
      }
    });
    return next();
  };
};



export { validateParam, validateByScheme, Schema };
