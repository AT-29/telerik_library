import { networkInterfaces } from 'os';
import multer from 'multer';
// import FTPStorage from 'multer-ftp';
import { extname } from 'path';
import dotenv from 'dotenv'; //process.env.PASS

dotenv.config();

const predicate = (value, errorMsg, ...callbacks) => {
  if (!callbacks.every((fn) => fn(value))) {
    throw new Error(errorMsg + ' instead received: ' + value);
  }
};

const getLocalIP = () => {
  const interfaces = networkInterfaces();
  for (const devName in interfaces) {
    const iface = interfaces[devName];
    for (let i = 0; i < iface.length; i++) {
      const alias = iface[i];
      if (
        alias.family === 'IPv4' &&
        alias.address !== '127.0.0.1' &&
        !alias.internal
      )
        return alias.address;
    }
  }
  return '0.0.0.0';
};

const extractBySchema = (data, schema) => {
  return Object.keys(data).reduce((acc, key) => {
    if (key in schema) {
      acc[key] = data[key];
    }
    return acc;
  }, {});
};

/*Local storage multer settings */
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/uploads');
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

/*Remote storage multer settings */
// const ftpStorage = new FTPStorage({ 
//   ftp: {
//     host: process.env.FTP_SERVER,
//     secure: false,
//     user: process.env.FTP_SERVER_USERNAME,
//     password: process.env.FTP_SERVER_PASSWORD,
//   },
//   destination: (req, file, options, cb) => {
//     const { fileName } = req.body;
//     cb(null, fileName);
//   },
// });

const checkFileType = (file, cb) => {
  const filetypes = /jpeg|jpg|png|gif/;
  const ext = filetypes.test(extname(file.originalname).toLowerCase());
  const mimetype = filetypes.test(file.mimetype);
  try {
    if (mimetype && ext) {
      return cb(null, true);
    } else {
      throw new Error('Images Only');
    }
  } catch (err) {
    console.log(err.message);
  }
};

const upload = multer({
  // storage: ftpStorage,
  storage: storage,
  limits: { fileSize: 1000000 },
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
});

export default { predicate, getLocalIP, extractBySchema, upload };
