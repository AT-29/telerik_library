const createSession = (sessionData) => {
  return async (userId, authToken, ip) => {
    return await sessionData.createSession(userId, authToken, ip);
  };
};
const closeSession = (sessionData) => {
  return async (userId) => {
    return await sessionData.closeSession(userId);
  };
};
const getSession = (sessionData) => {
  return async (sessionId) => {
    return await sessionData.getSession(sessionId);
  };
};

const getSessionsByUser = (sessionData) => {
  return async (userId) => {
    return await sessionData.getSessionsByUser(userId);
  };
};

export default { createSession, getSession, getSessionsByUser, closeSession };