import { serviceErrors } from '../handlers/error-handler.js';
import bcrypt from 'bcrypt';

const getUserBy = (usersData) => {
  return async (column, id) => {
    const user = await usersData.getUserBy(column, id);
    if (!user) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, item: null };
    } else {
      return { error: null, item: user };
    }
  };
};

// const getAllUsers = (usersData) => {
//   return async (filter) => {
//     return filter
//       ? await usersData.searchBy('name', filter)
//       : await usersData.getAllUsers();
//   };
// };



const createUser = (usersData) => {
  return async (username, password, email, avatar) => {
    const existingUserName = await usersData.getUserBy('username', username);
    if (existingUserName) {
      return {
        error: serviceErrors.DUPLICATE_USERNAME_RECORD.code,
        item: null,
      };
    }
    if (email) {
      const existingEmail = await usersData.getUserBy('email', email);
      if (existingEmail) {
        return { error: serviceErrors.DUPLICATE_EMAIL_RECORD.code, item: null };
      }
    }
    const hash = await bcrypt.hash(password, 10);
    const user = await usersData.createUser(username, hash, email, avatar);

    return { error: null, item: user };
  };
};

const updateUser = (usersData) => {
  return async (id, username, password, email, avatar, userId, isAdmin) => {
    if (!isAdmin) {
      if (id !== userId) {
        return {
          error: serviceErrors.OPERATION_NOT_PERMITTED.code,
          item: null,
        };
      }
    }

    const hash =  password ? await bcrypt.hash(password, 10) : null;

    const user = await usersData.updateUser(
      id,
      username,
      hash,
      email,
      avatar
    );
    return { error: null, item: user };
  };
};

const deleteUser = (usersData) => {
  return async (id) => {
    const user = await usersData.deleteUser(id);
    return { error: null, item: user };
  };
};
const isBanned = (usersData) => {
  return async (userId) => {
    const status = await usersData.isBanned(userId);
    if (status === true) {
      return { error: serviceErrors.RESOURCE_IS_FORBIDDEN.code, item: null };
    }
    return { error: null, item: status };
  };
};

const banUser = (usersData) => {
  return async (userId, reason, days) => {
    const user = await usersData.banUser(userId, reason, days);
    return { error: null, item: user };
  };
};
const removeBan = (usersData) => {
  return async (userId) => {
    const user = await usersData.removeBan(userId);
    return { error: null, item: user };
  };
};

const getActiveBans = (usersData) => {
  return async () => {
    const bans = await usersData.getActiveBans();
    return bans;
  };
};
/*
username is not case sensitive
password is case sensitive
*/
const logInByUserName = (usersData) => {
  return async (username, password) => {
    const hashedPassword = await usersData.getUserPassword(username);

    if (!hashedPassword || !(await bcrypt.compare(password, hashedPassword))) {
      return { error: serviceErrors.INVALID_SIGNIN.code, item: null };
    }

    const user = await usersData.getUserBy('username', username);

    return {
      error: null,
      item: user,
    };
  };
};

const toggleAdmin = (usersData) => {
  return async (userId) => {
    const user = await usersData.toggleAdmin(userId);
    return { error: null, item: user };
  };
};

const getRatingsByUser = (usersData) => {
  return async (userId) => {
    const rating = await usersData.getRatingsByUser(userId);
    return { error: null, item: rating };
  };
};

const getLikesByUser = (usersData) => {
  return async (userId) => {
    const likes = await usersData.getLikesByUser(userId);
    return { error: null, item: likes };
  };
};
const getBorrowsByUser = (usersData) => {
  return async (userId) => {
    const borrows = await usersData.getBorrowsByUser(userId);
    return { error: null, item: borrows };
  };
};

const searchUsers = (usersData) => {
  return async (
    page,
    perPage,
    sortBy,
    sortOrder,
    username,
    isAdmin,
    isBanned,
    levelId
  ) => {
    return await usersData.searchUsers(
      page,
      perPage,
      sortBy,
      sortOrder,
      username,
      isAdmin,
      isBanned,
      levelId
    );
  };
};

export default {
  getBorrowsByUser,
  getLikesByUser,
  isBanned,
  getActiveBans,
  removeBan,
  getRatingsByUser,
  toggleAdmin,
  getUserBy,
  searchUsers,
  createUser,
  logInByUserName,
  updateUser,
  banUser,
  deleteUser,
};
