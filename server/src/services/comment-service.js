import { serviceErrors } from '../handlers/error-handler.js';

const getComment = (commentData) => {
  return async (commentId) => {
    const comment = await commentData.getComment(commentId);
    if (!comment) return { item: null, error: serviceErrors.RECORD_NOT_FOUND.code };
    return { error: null, item: comment };
  };
};

const getCommentsByReview = (commentData) => {
  return async (reviewId) => {
    const comment = await commentData.getCommentsByReview(reviewId);
    if (!comment) return { item: null, error: serviceErrors.RECORD_NOT_FOUND.code };
    return { error: null, item: comment };
  };
};

const getCommentsByUser = (commentData) => {
  return async (userId) => {
    const comment = await commentData.getCommentsByUser(userId);
    if (!comment) return { item: null, error: serviceErrors.RECORD_NOT_FOUND.code };
    return { error: null, item: comment };
  };
};

const createComment = (commentData, reviewData) => {
  return async (content, userId, reviewId) => {
    const review = await reviewData.getReview(reviewId);
    if (!review) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, item: null };
    }
    const comment = await commentData.createComment(content, userId, reviewId);
    if (!comment) return { item: null, error: serviceErrors.OPERATION_NOT_PERMITTED.code };
    return { error: null, item: comment };
  };
};

const updateComment = (commentData) => {
  return async (commentId, content, reason, userId, isAdmin) => {
    const comment = await commentData.getComment(commentId);
    if (!comment) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, item: null };
    }

    if (!isAdmin) {
      if (comment.userId !== userId) {
        return {
          error: serviceErrors.OPERATION_NOT_PERMITTED.code,
          item: null,
        };
      }
    }
    const updatedComment = await commentData.updateComment(commentId, content, reason);
    if (!updatedComment) return { item: null, error: serviceErrors.OPERATION_NOT_PERMITTED.code };
    return { error: null, item: updatedComment };
  };
};


const deleteComment = (commentData) => {
  return async (commentId, userId, isAdmin) => {

    const comment = await commentData.getComment(commentId);

    if (!comment) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, item: null };
    }

    if (!isAdmin) {
      if (comment.userId !== userId) {
        return {
          error: serviceErrors.OPERATION_NOT_PERMITTED.code,
          item: null,
        };
      }
    }


    const deletedCommentId = await commentData.deleteComment(commentId);
    if (!deletedCommentId) return { item: null, error: serviceErrors.RECORD_NOT_FOUND.code };
    return { error: null, item: deletedCommentId };
  };
};

export default { getCommentsByUser, getComment, getCommentsByReview, createComment, updateComment, deleteComment };