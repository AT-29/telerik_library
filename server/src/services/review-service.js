import { serviceErrors } from '../handlers/error-handler.js';

const getAllReviews = (reviewData) => {
  return async () => {
    const reviews =  await reviewData.getAllReviews();
    return reviews;
  };
};

const getReview = (reviewData) => {
  return async (id) => {
    const review = await reviewData.getReview(id);
    if (!review) {
      return { item: null, error: serviceErrors.RECORD_NOT_FOUND.code };
    }
    return { error: null, item: review };
  };
};

const getReviewsByBook = (reviewData) => {
  return async (id) => {
    const review = await reviewData.getReviewsByBook(id);
    if (!review)
      return { item: null, error: serviceErrors.RECORD_NOT_FOUND.code };
    return { error: null, item: review };
  };
};

const getReviewsByUser = (reviewData) => {
  return async (id) => {
    const review = await reviewData.getReviewsByUser(id);
    if (!review)
      return { item: null, error: serviceErrors.RECORD_NOT_FOUND.code };
    return { error: null, item: review };
  };
};

const getLikesByReview = (reviewData) => {
  return async (id) => {
    const likes = await reviewData.getLikesByReview(id);
    if (!likes)
      return { item: null, error: serviceErrors.RECORD_NOT_FOUND.code };
    return { error: null, item: likes };
  };
};
const hasReviewed = (reviewData) => {
  return async (userId, bookId) => {
    const hasReviewed = await reviewData.hasReviewed(userId,bookId);
    return { error: null, item: hasReviewed };
  };
};
const createReview = (reviewData, bookData) => {
  return async (content, userId, bookId) => {
    const isRead = await bookData.isReadByUser(userId,bookId);
   
    if(!isRead){
      return {error: serviceErrors.BOOK_NOT_READ.code, item: null };
    }
    const hasReviewed = await reviewData.hasReviewed(userId, bookId);

    if (hasReviewed) {
      return { error: serviceErrors.BOOK_ALREADY_REVIEWED.code, item: null };
    }
    const review = await reviewData.createReview(content, userId, bookId);

    if (!review) {
      return { error: serviceErrors.OPERATION_NOT_PERMITTED.code, item: null };
    }
    return { error: null, item: review };
  };
};

const updateReview = (reviewData) => {
  return async (reviewId, content, editReason, userId, isAdmin) => {
    const review = await reviewData.getReview(reviewId);

    if (!review) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, item: null };
    }
    if (!isAdmin) {
      if (review.userId !== userId) {
        return {
          error: serviceErrors.OPERATION_NOT_PERMITTED.code,
          item: null,
        };
      }
    }

    const updatedReview = await reviewData.updateReview(
      reviewId,
      content,
      editReason);
    if (!updatedReview) {
      return { item: null, error: serviceErrors.OPERATION_NOT_PERMITTED.code };
    }
    return { error: null, item: updatedReview };
  };
};

const deleteReview = (reviewData) => {
  return async (reviewId, userId, isAdmin) => {
    const review = await reviewData.getReview(reviewId);
    if (!review) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, item: null };
    }
    if (!isAdmin) {
      if (review.userId !== userId) {
        return {
          error: serviceErrors.OPERATION_NOT_PERMITTED.code,
          item: null,
        };
      }
    }
    const deletedReview = await reviewData.deleteReview(reviewId);
    return { error: null, item: deletedReview };
  };
};
const toggleLikeReview = (reviewData) => {
  return async (userId, reviewId) => {
    const review = await reviewData.getReview(reviewId);
    if (!review) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, item: null };
    }
    const liked = await reviewData.getLike(userId, reviewId);

    if (liked){
      const dec = await reviewData.deleteLike(userId, reviewId);
      if (!dec) {
        return { item: null, error: serviceErrors.OPERATION_NOT_PERMITTED.code };
      }
      return { error: null, item: -1 };
    }else{
      const inc = await reviewData.likeReview(userId, reviewId);
      if (!inc) {
        return { item: null, error: serviceErrors.OPERATION_NOT_PERMITTED.code };
      }
      return { error: null, item: +1 };
    }
 
   
  };
};

const likeReview = (reviewData) => {
  return async (userId, reviewId) => {
    const review = await reviewData.getReview(reviewId);
    if (!review) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, item: null };
    }
    const liked = await reviewData.getLike(userId, reviewId);
    if (liked) {
      return { error: serviceErrors.OPERATION_NOT_PERMITTED.code, item: null };
    }
    const like = await reviewData.likeReview(userId, reviewId);
    if (!like) {
      return { item: null, error: serviceErrors.OPERATION_NOT_PERMITTED.code };
    }
    return { error: null, item: like };
  };
};

const deleteLike = (reviewData) => {
  return async (userId, reviewId) => {
    const review = await reviewData.getReview(reviewId);
    if (!review) {
      return { error: serviceErrors.RECORD_NOT_FOUND.code, item: null };
    }
    const liked = await reviewData.getLike(userId, reviewId);
    if (!liked) {
      return { error: serviceErrors.DELETE_NOT_PERMITTED.code, item: null };
    }
    const like = await reviewData.deleteLike(userId, reviewId);
    if (!like) {
      return { item: null, error: serviceErrors.OPERATION_NOT_PERMITTED.code };
    }
    return { error: null, item: like };
  };
};

export default {
  toggleLikeReview,
  getLikesByReview,
  likeReview,
  deleteLike,
  getReviewsByUser,
  getAllReviews,
  getReviewsByBook,
  createReview,
  updateReview,
  deleteReview,
  getReview,
  hasReviewed,  
};
