import { serviceErrors } from '../handlers/error-handler.js';

const getBook = (bookData) => {
  return async (id) => {
    const book = await bookData.getBook(id);
    if (!book) {
      return { item: null, error: serviceErrors.RECORD_NOT_FOUND.code };
    }
    return { error: null, item: book };
  };
};

const getRatingsByBook = (bookData) => {
  return async (id) => {
    const { error, item } = await getBook(bookData)(id);
    if (error) {
      return { error, item };
    }
    const rating = await bookData.getRatingsByBook(id);
    if (typeof rating === 'undefined') {
      return { item: null, error: serviceErrors.RECORD_NOT_RATED.code };
    }
    return { error: null, item: await bookData.getRatingsByBook(id) };
  };
};

const getAllFoundBooks = (bookData) => {
  return async (page, perPage, sortBy, sortOrder) => {
    return await bookData.getAllBooks(page, perPage, sortBy, sortOrder);
  };
};

const searchBooks = (bookData) => {
  return async (
    page,
    perPage,
    sortBy,
    sortOrder,
    title,
    author,
    year,
    genreId,
    languageId,
    statusId,
    rating
  ) => {
    return await bookData.searchBooks(
      page,
      perPage,
      sortBy,
      sortOrder,
      title,
      author,
      year,
      genreId,
      languageId,
      statusId,
      rating
    );
  };
};

const deleteBook = (bookData) => {
  return async (id) => {
    const { item, error } = await getBook(bookData)(id);
    if (error) {
      return { error, item };
    }
    return { error: null, item: await bookData.deleteBook(id) };
  };
};

const borrowBook = (bookData) => {
  return async (userId, bookId) => {
    const { error, item } = await getBook(bookData)(bookId);
    if (error) {
      return { error, item: null };
    }
    if (item.status !== 'Free') {
      return { error: serviceErrors.RECORD_NOT_AVAILABLE.code, item: null };
    }
    return { error: null, item: await bookData.borrowBook(userId, bookId) };
  };
};

const returnBook = (bookData) => {
  return async (userId, bookId) => {

    const result = await getActiveBorrow(bookData)(userId, bookId);

    if (!result.item){
      return { item: null, error: serviceErrors.RESOURCE_IS_FORBIDDEN.code };
    }
    const returnedBook = await bookData.returnBook(userId, bookId);
    return { error: null, item: returnedBook };
  };
};

const rateBook = (bookData) => {
  return async (userId, bookId, rating) => {
    const isRead = await bookData.isReadByUser(userId, bookId);
    if (!isRead) {
      return { error: serviceErrors.RESOURCE_IS_FORBIDDEN.code, item: null };
    }
    const bookRating = await bookData.rateBook(userId, bookId, rating);
    return { error: null, item: bookRating };
  };
};

const createBook = (bookData) => {
  return async (
    title,
    author,
    year,
    cover,
    description,
    genreId,
    languageId
  ) => {
    return await bookData.createBook(
      title,
      author,
      year,
      cover,
      description,
      genreId,
      languageId
    );
  };
};

const updateBook = (bookData) => {
  return async (
    bookId,
    title,
    author,
    year,
    cover,
    description,
    genreId,
    languageId,
    statusId
  ) => {
    const { error } = await getBook(bookData)(bookId);
    if (error) {
      return { error, item: null };
    }
    const updatedBook = await bookData.updateBook(
      bookId,
      title,
      author,
      year,
      cover,
      description,
      genreId,
      languageId,
      statusId
    );
    if (!updatedBook) {
      return { error: serviceErrors.OPERATION_NOT_PERMITTED.code, item: null };
    }
    return { error: null, item: updatedBook };
  };
};

const getActiveBorrow = (bookData) => {
  return async (userId, bookId) => {
    const borrow = await bookData.getActiveBorrow(userId, bookId);
    return { error: null, item: borrow };
  };
};

const getAllBorrows = (bookData) => {
  return async () => {
    return await bookData.getAllBorrows();
  };
};
const isReadByUser = (bookData) => {
  return async (userId, bookId) => {
    const isRead = await bookData.isReadByUser(userId, bookId);
    return { error: null, item: isRead };
  };
};

export default {
  isReadByUser,
  searchBooks,
  getAllBorrows,
  getActiveBorrow,
  createBook,
  updateBook,
  getBook,
  getAllFoundBooks,
  deleteBook,
  borrowBook,
  returnBook,
  rateBook,
  getRatingsByBook,
};
