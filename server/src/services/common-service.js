const getAllLanguages = (commonData) => {
    return async () => {
        return await commonData.getAllLanguages();
    };
};

const getAllGenres = (commonData) => {
    return async () => {
        return await commonData.getAllGenres();
    };
};

const getAllStatuses = (commonData) => {
    return async () => {
        return await commonData.getAllStatuses();
    };
};
const getAllLevels = (commonData) => {
    return async () => {
        return await commonData.getAllLevels();
    };
};
const getDashBoardData = (commonData) =>{
    return async () =>{
        return await commonData.getDashBoardData();
    }
}   


export default { getAllLanguages, getAllGenres, getAllStatuses, getAllLevels, getDashBoardData };