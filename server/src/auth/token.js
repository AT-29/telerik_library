import dotenv from 'dotenv'; //process.env.PASS
import jwt from 'jsonwebtoken';

dotenv.config();

const createToken = (payload) => {
    const token = jwt.sign(payload, process.env.PRIVATE_KEY, { expiresIn: +process.env.TOKEN_LIFETIME });
    return token;
};

export default createToken;