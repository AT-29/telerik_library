import dotenv from 'dotenv'; //process.env.PASS
import passportJwt from 'passport-jwt';

dotenv.config();

const options = {
    secretOrKey: process.env.PRIVATE_KEY,
    jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
    const userData = {
        id: payload.sub,
        username: payload.username,
        isAdmin: payload.isAdmin,
    };
    
    // userData will be set as `req.user` in the `next` middleware

    done(null, userData);
});


export default jwtStrategy;
