import express from 'express';
import { catchErrors } from '../handlers/error-handler.js';
import homeController from '../controllers/home-controller.js';

  
const homeRouter = new express.Router();

//home
homeRouter.get('/',  catchErrors(homeController.getDashBoardData)); //get books, reviews and users at home

export default homeRouter;
