import express from 'express';
import { catchErrors } from '../handlers/error-handler.js';
import authController from '../controllers/auth-controller.js';
import reviewController from '../controllers/review-controller.js';
import userController from '../controllers/users-controller.js';
import {validateByScheme, Schema} from '../common/validations.js';


const usersRouter = new express.Router();

//users  
usersRouter.post('/', catchErrors(validateByScheme(Schema.User,'body','required')), authController.isAdmin, catchErrors(authController.registerUser)); //admin create user
usersRouter.get('/search',catchErrors(validateByScheme(Schema.ID,'params')), authController.isAdmin, catchErrors(userController.searchUsers)); // only admin can get all users ?, later
usersRouter.get('/:id',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(userController.getUserById)); //only admin can get user by id ?

usersRouter.put('/:id',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(validateByScheme(Schema.UpdateUser,'body','required')),catchErrors(validateByScheme(['avatar'],'body')), catchErrors(userController.updateUser)); //only admin can update user by id ?
usersRouter.delete('/:id',catchErrors(validateByScheme(Schema.ID,'params')), authController.isAdmin, catchErrors(userController.deleteUser)); //only admin can delete user

//reviews, likes, ratings by user

usersRouter.get('/:id/borrows',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(userController.getBorrowsByUser)); // get borrows by user 
usersRouter.get('/:id/reviews',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(reviewController.getReviewsByUser)); // get reviews by user 
usersRouter.get('/:id/likes',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(userController.getLikesByUser)); // get likes by user 
usersRouter.get('/:id/ratings',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(userController.getRatingsByUser)); // get reviews by user 
usersRouter.get('/:id/comments',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(reviewController.getCommentsByUser)); // get comments by user 



/* Admin routes: */
//bans
usersRouter.post('/:id/ban',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(validateByScheme(Schema.Ban,'body','required')),  authController.isAdmin, catchErrors(userController.banUser)); //only admin can ban user
usersRouter.put('/:id/ban',catchErrors(validateByScheme(Schema.ID,'params')), authController.isAdmin, catchErrors(userController.removeBan)); // will remove ban from user
//make admin/revoke admin
usersRouter.put('/:id/toggleadmin',catchErrors(validateByScheme(Schema.ID,'params')), authController.isAdmin, catchErrors(userController.toggleAdmin)); //only admin can delete user

export default usersRouter;
