import express from 'express';
import { catchErrors } from '../handlers/error-handler.js';
import bookController from '../controllers/book-controller.js';
import authController from '../controllers/auth-controller.js';
import reviewController from '../controllers/review-controller.js';
import { validateByScheme, Schema } from '../common/validations.js';
import userController from '../controllers/users-controller.js';

  
const booksRouter = new express.Router();

//books
booksRouter.get('/', catchErrors(validateByScheme(Schema.Pagination,'query')), catchErrors(bookController.getAllBooks)); //get all books
booksRouter.get('/search', catchErrors(validateByScheme(Schema.Pagination,'query')), catchErrors(bookController.searchBooks)); //search all books
booksRouter.get('/:id', catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(bookController.getBook)); //get book by id
booksRouter.get('/:id/reviewed', catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(reviewController.hasReviewed)); //get is reviewd by user
booksRouter.get('/:id/read', catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(bookController.isReadByUser)); //get isRead by user

booksRouter.delete('/:id', catchErrors(validateByScheme(Schema.ID,'params')), authController.isAdmin, catchErrors(bookController.deleteBook)); //delete book by id
booksRouter.post('/', catchErrors(validateByScheme(Schema.Book,'body','required')), authController.isAdmin,  catchErrors(bookController.createBook)); // create book 
booksRouter.put('/:id',catchErrors(validateByScheme(Schema.ID,'params')),  authController.isAdmin, catchErrors(validateByScheme(Schema.Book,'body','required')), catchErrors(bookController.updateBook)); //update book

//borrows
booksRouter.get('/:id/borrowing/active',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(bookController.getActiveBorrow)); // get borrows by user 

booksRouter.post('/:id/borrowing',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(userController.banStatus), catchErrors(bookController.borrowBook)); // borrow book
booksRouter.put('/:id/borrowing',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(bookController.returnBook)); // return book

//reviews
booksRouter.get('/reviews',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(reviewController.getAllReviews)); //read all reviews of all books
booksRouter.get('/:id/reviews/:id', catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(reviewController.getReview)); //get a review by id
booksRouter.post('/:id/reviews/',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(validateByScheme(Schema.Review,'body','required')), catchErrors(userController.banStatus), catchErrors(reviewController.createReview)); //submit a review
booksRouter.put('/:id/reviews/:id',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(validateByScheme(Schema.Review,'body','required')), catchErrors(userController.banStatus), catchErrors(reviewController.updateReview)); //update review   
booksRouter.delete('/:id/reviews/:id',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(userController.banStatus), catchErrors(reviewController.deleteReview)); //delete review
booksRouter.get('/:id/reviews',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(reviewController.getReviewsByBook)); //read reviews of book
booksRouter.get('/:id/reviews/:id/like',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(reviewController.getLikesByReview)); //like review
//booksRouter.post('/:id/reviews/:id/like',catchErrors(validateByScheme(Schema.ID,'params')),  catchErrors(reviewController.toggleLikeReview)); //toggle like review allowed if banned
booksRouter.post('/:id/reviews/:id/like',catchErrors(validateByScheme(Schema.ID,'params')),  catchErrors(reviewController.likeReview)); //like review allowed if banned
booksRouter.delete('/:id/reviews/:id/like',catchErrors(validateByScheme(Schema.ID,'params')),  catchErrors(reviewController.deleteLike)); //delete like allowed if banned

//ratings
booksRouter.get('/:id/ratings/',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(bookController.getRatingsByBook)); //read rating of book
booksRouter.post('/:id/ratings/',catchErrors(validateByScheme(Schema.ID,'params')), catchErrors(validateByScheme(Schema.Rating,'body','required')), catchErrors(userController.banStatus), catchErrors(bookController.rateBook)); //rate book

//comments
booksRouter.get('/:id/reviews/:id/comments',catchErrors(validateByScheme(Schema.ID,'params')),  catchErrors(reviewController.getCommentsByReview)); //get comments by review id
booksRouter.get('/:id/reviews/:id/comments/:id',catchErrors(validateByScheme(Schema.ID,'params')),  catchErrors(reviewController.getComment)); //get comment by id
booksRouter.post('/:id/reviews/:id/comments',catchErrors(validateByScheme(Schema.ID,'params')),  catchErrors(userController.banStatus), catchErrors(validateByScheme(Schema.Comment,'body','required')), catchErrors(reviewController.createComment)); //submit comment on review
booksRouter.put('/:id/reviews/:id/comments/:id',catchErrors(validateByScheme(Schema.ID,'params')),  catchErrors(userController.banStatus), catchErrors(validateByScheme(Schema.Comment,'body','required')), catchErrors(reviewController.updateComment)); //update comment by id
booksRouter.delete('/:id/reviews/:id/comments/:id',catchErrors(validateByScheme(Schema.ID,'params')),  catchErrors(userController.banStatus), catchErrors(reviewController.deleteComment)); //delete comment by id



export default booksRouter;
