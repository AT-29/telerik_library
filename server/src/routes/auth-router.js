import express from 'express';
import { catchErrors } from '../handlers/error-handler.js';
import authController from '../controllers/auth-controller.js';
import { validateByScheme, Schema } from '../common/validations.js';

const authRouter = new express.Router();

//auth
authRouter.post('/registration', catchErrors(validateByScheme(Schema.User,'body','required')), catchErrors(authController.registerUser)); //admin create user user 
authRouter.post('/', catchErrors(validateByScheme(Schema.Session,'body','required')), catchErrors(authController.logInUserByUsername), catchErrors(authController.createSession)); //login user
authRouter.delete('/', authController.logOut, catchErrors(authController.closeSession)); //logout user

export default authRouter;
