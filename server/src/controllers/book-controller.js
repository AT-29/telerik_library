import bookService from '../services/book-service.js';
import bookData from '../models/books-data.js';

import { validateParam } from '../common/validations.js';
import { serviceErrorStack } from '../handlers/error-handler.js';
import { enumData } from '../common/common.js';

const getBook = async (req, res) => {
  const id = +req.params.id;
  const { error, item } = await bookService.getBook(bookData)(id);
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: `This Book ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).json(item);
};

const getRatingsByBook = async (req, res) => {
  const id = +req.params.id;
  const { error, item } = await bookService.getRatingsByBook(bookData)(id);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({
      error: `This Book's rating is ${serviceErrorStack.get(error).message}`,
    });
  }
  res.status(200).json(item);
};

const getAllBooks = async (req, res) => {
  const page = +req.query.page || enumData.defaultPage;
  const perPage = +req.query.perPage || enumData.defaultPerPage;
  const sortBy = req.query.sortBy || enumData.defaultSortBy;
  const sortOrder = req.query.sortOrder || enumData.defaultSortOrder;

  const books = await bookService.getAllFoundBooks(bookData)(
    page,
    perPage,
    sortBy,
    sortOrder,
  );

  res.status(200).json(books);
};

const searchBooks = async (req, res) => {
  const page = +req.query.page || enumData.defaultPage;
  const perPage = +req.query.perPage || enumData.defaultPerPage;
  const sortBy = req.query.sortBy || enumData.defaultSortBy;
  const sortOrder = req.query.sortOrder || enumData.defaultSortOrder;
  const title = req.query.title || null;
  const author = req.query.author || null;
  const year = req.query.year || null;
  const genreId = req.query.genreId || null;
  const languageId = req.query.languageId || null;
  const statusId = req.query.statusId || null;
  const rating = req.query.rating || null;

  const books = await bookService.searchBooks(bookData)(
    page,
    perPage,
    sortBy,
    sortOrder,
    title,
    author,
    year,
    genreId,
    languageId,
    statusId,
    rating
  );
  res.status(200).json(books);
};

const deleteBook = async (req, res) => {
  const id = +req.params.id;
  const { error, item } = await bookService.deleteBook(bookData)(id);
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: `This book ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).json({ message: 'you have deleted ' + item });
};

const borrowBook = async (req, res) => {
  const userId = +req.user.id;
  const bookId = +req.params.id;
  validateParam('id', userId);

  const { error, item } = await bookService.borrowBook(bookData)(
    userId,
    bookId
  );
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: `This book ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).json({
    message: `You have borrowed the book ${item.title} by ${item.author}`,
  });
};

const returnBook = async (req, res) => {

  const userId = +req.user.id;
  const bookId = +req.params.id;
  validateParam('id', userId);

  const { error, item } = await bookService.returnBook(bookData)(
    userId,
    bookId
  );

  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: `This book ${serviceErrorStack.get(error).message}` });
  } else {
    res.status(200).send({
      message: `You have returned the book ${item.title} by ${item.author}`,
    });
  }
};

const rateBook = async (req, res) => {
  const userId = +req.user.id;
  const bookId = +req.params.id;
  const rating = +req.body.rating;

  validateParam('id', userId);
  const { error, item } = await bookService.rateBook(bookData)(
    userId,
    bookId,
    rating
  );
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: `This book ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).json({
    message: `You have rated book ${item.title} with score ${rating}`,
  });
};

const isReadByUser = async (req, res) => {
  const userId = +req.user.id;
  const bookId = +req.params.id;

  validateParam('id', userId);
  const { error, item } = await bookService.isReadByUser(bookData)(
    userId,
    bookId
  );
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: `This book ${serviceErrorStack.get(error).message}` });
  }
  return res.status(200).json(item);
};

const getActiveBorrow = async (req, res) =>{
  const userId = +req.user.id;
  const bookId = +req.params.id;

  validateParam('id', userId);

  const { error, item } = await bookService.getActiveBorrow(bookData)(
    userId,
    bookId
  );
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: `This book ${serviceErrorStack.get(error).message}` });
  }
  return res.status(200).json(item);
};


const createBook = async (req, res) => {
  const {
    title,
    author,
    year,
    cover,
    description,
    genreId,
    languageId,
  } = req.body;
  const createdBook = await bookService.createBook(bookData)(
    title,
    author,
    year || null,
    cover || null,
    description || null,
    genreId || null,
    languageId || null
  );
  res.status(201).send(createdBook);
};

const updateBook = async (req, res) => {
  const bookId = +req.params.id;
  const {
    title,
    author,
    year,
    cover,
    description,
    genreId,
    languageId,
    statusId,
  } = req.body;

  const { error, item } = await bookService.updateBook(bookData)(
    bookId,
    title,
    author,
    year || null,
    cover || null,
    description || null,
    genreId || null,
    languageId || null,
    statusId || null
  );
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: `This book ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send(item);
};

export default {
  createBook,
  updateBook,
  getAllBooks,
  searchBooks,
  getBook,
  deleteBook,
  borrowBook,
  returnBook,
  rateBook,
  getRatingsByBook,
  isReadByUser,
  getActiveBorrow,
};
