import createToken from '../auth/token.js';
import { serviceErrorStack, serviceErrors } from '../handlers/error-handler.js';
import passport from 'passport';
import usersData from '../models/users-data.js';
import userService from '../services/user-service.js';

import sessionService from '../services/session-service.js';
import sessionData from '../models/sessions-data.js';
import utils from '../common/util.js';

const authenticate = async (req, res, next) => {  
  passport.authenticate('jwt', { session: false }, async (error, token) => {
    if (error || !token) {
      return res
        .status(401)
        .json({ error: 'Unauthorized, invalid credentials.' });
    }
    req.user = token;
    next();
  })(req, res, next);
};
const isAdmin = (req, res, next) => {
  if (req.user && req.user.isAdmin === 1) {
    next();
  } else {
    res.status(serviceErrors.RESOURCE_IS_FORBIDDEN.status).send({
      error: serviceErrors.RESOURCE_IS_FORBIDDEN.message,
    });
  }
};

const registerUser = async (req, res) => {
  const { username, password, email, avatar } = req.body;

  const data = await userService.createUser(usersData)(
    username,
    password,
    email || null,
    avatar || null,
  );

  const user = data.item;
  const error = data.error;

  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: serviceErrorStack.get(error).message });
  }

  res.status(201).send(user);
};

const logInUserByUsername = async (req, res, next) => {
  const { username, password } = req.body;

  const data = await userService.logInByUserName(usersData)(username, password);
  const user = data.item;
  const error = data.error;

  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: serviceErrorStack.get(error).message });
  } else {
    const payload = {
      sub: user.id,
      username: user.username,
      isAdmin: user.isAdmin,
    };
    const token = createToken(payload);

    req.user = { data: user, token: token};

    next();
  }
};

const logOut = async (req, res) => {
  req.logout();
  res.status(200).send({ message: 'Good bye. We will miss you :(' });
};

const createSession = async (req, res) => {
  const user = req.user.data;
  const token = req.user.token;
  // const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  const session = await sessionService.createSession(sessionData)(
    user.id,
    token,
    utils.getLocalIP()
  );

  res.status(200).send({ user: user, token: token, sessionId: session.id });
};

const closeSession = async (req, res, next) => {
  const id = +req.user.id;
  await sessionService.createSession(sessionData)(id);
  next();
};

const getSession = async (req, res) => {
  const sessionId = 'someid';
  const session = await sessionService.getSession(sessionData)(sessionId);
  res.status(200).json(session);
};

const getSessionByUser = async (req, res) => {
  const session = await sessionService.getSessionByUser(sessionData)(
    req.user.id
  );
  res.status(200).json(session);
};

export default {
  closeSession,
  createSession,
  getSession,
  getSessionByUser,
  registerUser,
  logInUserByUsername,
  logOut,
  authenticate,
  isAdmin,
};

