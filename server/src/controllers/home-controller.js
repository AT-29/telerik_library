
import commonData from '../models/common-data.js';
import commonService from '../services/common-service.js';
// import { enumData } from '../common/common.js';

const getDashBoardData = async (req, res) => {
  const data = await commonService.getDashBoardData(commonData)();
  res.status(200).json(data);
};

export default { getDashBoardData };
