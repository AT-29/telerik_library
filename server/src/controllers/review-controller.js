import { validateParam } from '../common/validations.js';
import reviewService from '../services/review-service.js';
import reviewData from '../models/reviews-data.js';
import commentService from '../services/comment-service.js';
import commentData from '../models/comments-data.js';
import bookData from '../models/books-data.js';

import {serviceErrorStack} from '../handlers/error-handler.js';



const getAllReviews = async (req, res) => {
  const formatted = await reviewService.getAllReviews(reviewData)();
  res.status(200).json(formatted);
};

const getReview = async (req, res) => {
  const id = +req.params.id;
  const { error, item } =  await reviewService.getReview(reviewData)(id);
  if(serviceErrorStack.has(error)){
    return res.status(serviceErrorStack.get(error).status).send({ error:`This Review is ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send(item);
};

const getReviewsByBook = async (req, res) => {
  const id = +req.params.id;
  const { error, item } =  await reviewService.getReviewsByBook(reviewData)(id);
  if(serviceErrorStack.has(error)){
      return res.status(serviceErrorStack.get(error).status).send({ error:`This Review is ${serviceErrorStack.get(error).message}` });
    }
  res.status(200).send(item);
};

const getReviewsByUser = async (req, res) => {
  const id = +req.params.id;
  const { error, item } = await reviewService.getReviewsByUser(reviewData)(id);
  if(serviceErrorStack.has(error)){
      return res.status(serviceErrorStack.get(error).status).send({ error:`This Review is ${serviceErrorStack.get(error).message}` });
   }
    res.status(200).send(item);
};

const getLikesByReview = async (req, res) => {
  const id = +req.params.id;
  const { error, item } = await reviewService.getLikesByReview(reviewData)(id);
  if(serviceErrorStack.has(error)){
     return res.status(serviceErrorStack.get(error).status).send({ error:`This Review is ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send(item);
};

const createReview = async (req, res) => {
  const userId = +req.user.id;
  const bookId = +req.params.id;
  const { content } = req.body;
  validateParam('id', userId);

  const { error, item } = await reviewService.createReview(reviewData,bookData)(content, userId, bookId);
  if(serviceErrorStack.has(error)){
    return res.status(serviceErrorStack.get(error).status).send({ error:`This Book is ${serviceErrorStack.get(error).message}` });
  }
  res.status(201).send({ message:'You have submited a review', item});
};

const updateReview = async (req, res) => {
  const userId = +req.user.id;
  const isAdmin = +req.user.isAdmin;
  const reviewId = +req.params.id;
  const content = req.body.content;
  const editReason = req.body.editReason || null;
  const { error, item } = await reviewService.updateReview(reviewData)( reviewId, content, editReason, userId,isAdmin);
  if(serviceErrorStack.has(error)){
    return res.status(serviceErrorStack.get(error).status).send({ error:`This Review is ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send({ message:'You have updated your review',item});
};

const deleteReview = async (req, res) => {
  const id = +req.params.id;
  const userId = +req.user.id;
  const isAdmin = req.user.isAdmin;
  validateParam('id', userId);
  const { error, item } = await reviewService.deleteReview(reviewData)(id, userId, isAdmin);
  if(serviceErrorStack.has(error)){
    return res.status(serviceErrorStack.get(error).status).send({ error:`This Review is ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send({ message: 'you have deleted ' + item });
};

const toggleLikeReview = async (req, res) => {
  const userId = +req.user.id;
  const reviewId = +req.params.id;
  validateParam('id', userId);
  const { error, item } = await reviewService.toggleLikeReview(reviewData)(userId, reviewId);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: `This Like is ${serviceErrorStack.get(error).message}` });
  }
  res.status(201).send({ message: item });
};


const likeReview = async (req, res) => {
  const userId = +req.user.id;
  const reviewId = +req.params.id;
  validateParam('id', userId);

  const { error } = await reviewService.likeReview(reviewData)(userId, reviewId);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: `This Like is ${serviceErrorStack.get(error).message}` });
  }
  res.status(201).send({ message: 1 });
};

const deleteLike = async (req, res) => {
  const userId = +req.user.id;
  const reviewId = +req.params.id;
  validateParam('id', userId);

  const { error } = await reviewService.deleteLike(reviewData)(userId, reviewId);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: `${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send({ message: -1 });
};

const getCommentsByReview = async (req, res) => {
  const id = +req.params.id;
  const { error, item } = await commentService.getCommentsByReview(commentData)(id);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: `This Review is ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send(item);
};

const getCommentsByUser = async (req, res) => {
  const id = +req.params.id;
  const { error, item } = await commentService.getCommentsByUser(commentData)(id);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: `This User is ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send(item);
};

const getComment = async (req, res) => {
  const id = +req.params.id;
  const { error, item } = await commentService.getComment(commentData)(id);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: `This Comment is ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send(item);
};

const createComment = async (req, res) => {
  const userId = +req.user.id;
  const reviewId = +req.params.id;
  const { content } = req.body;
  validateParam('id', userId);

  const { error, item } = await commentService.createComment(commentData, reviewData)(content, userId, reviewId);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: `This Review is ${serviceErrorStack.get(error).message}` });
  }
  res.status(201).send(item);
};

const updateComment = async (req, res) => {
  const userId = +req.user.id;
  const isAdmin = +req.user.isAdmin;
  const reviewId = +req.params.id;
  const content = req.body.content;
  const editReason = req.body.editReason || null;
 
  const { error, item } = await commentService.updateComment(commentData)(reviewId, content, editReason, userId, isAdmin);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: `This Comment ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send(item);
};

const deleteComment = async (req, res) => {
  const id = +req.params.id;
  const userId = +req.user.id;
  const isAdmin = +req.user.isAdmin;

  const { error, item } = await commentService.deleteComment(commentData)(id, userId, isAdmin);
  if (serviceErrorStack.has(error)) {
    return res.status(serviceErrorStack.get(error).status).send({ error: `This Comment is ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send({ message: 'You have deleted ' + item });
};

const hasReviewed = async (req, res) => {
  const userId = +req.user.id;
  const bookId = +req.params.id;

  validateParam('id', userId);
  const { error, item } = await reviewService.hasReviewed(reviewData)(
    userId,
    bookId,
  );

  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: `This book ${serviceErrorStack.get(error).message}` });
  }
  
  return res.status(200).json(item);
};
export default {
  toggleLikeReview,
  hasReviewed,
  getCommentsByUser,
  createComment,
  updateComment,
  deleteComment,
  getComment,
  getCommentsByReview,
  likeReview, deleteLike,
  getAllReviews,
  createReview,
  updateReview,
  deleteReview,
  getReviewsByBook,
  getReviewsByUser,
  getReview,
  getLikesByReview,
};
