import userService from '../services/user-service.js';
import usersData from '../models/users-data.js';

import { serviceErrorStack, serviceErrors } from '../handlers/error-handler.js';
import { enumData, formatDate } from '../common/common.js';

// const getAllUsers = async (req, res) => {
//   const { search } = req.query;
//   const users = await userService.getAllUsers(usersData)(search);
//   res.status(200).send(users);
// };

const searchUsers = async (req,res)=>{

  const page = +req.query.page || enumData.defaultPage;
  const perPage = +req.query.perPage || enumData.defaultPerPage;
  const sortBy = req.query.sortBy || enumData.defaultSortUserBy;
  const sortOrder = req.query.sortOrder || enumData.defaultSortOrder;
  const username = req.query.username || null;
  const isAdmin = req.query.isAdmin || null;
  const isBanned = req.query.isBanned || null;
  const levelId = req.query.levelId || null;

  const users = await userService.searchUsers(usersData)(
    page,
    perPage,
    sortBy,
    sortOrder,
    username,
    isAdmin,
    isBanned,
    levelId,
  );
  res.status(200).json(users);

};



const getUserById = async (req, res) => {
  const id = +req.params.id;
  const data = await userService.getUserBy(usersData)('id',id);
  const user = data.item;
  const error = data.error;
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(user);
};

const updateUser = async (req, res) => {
  const id = +req.params.id;
  const userId = +req.user.id;
  const isAdmin = +req.user.isAdmin;
  const { username, password, email, avatar } = req.body;
  
  const data = await userService.updateUser(usersData)(
    id,
    username || null,
    password || null,
    email || null,
    avatar || null,
    userId,
    isAdmin,
  );
  const user = data.item;
  const error = data.error;
 
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(user);
};

const toggleAdmin = async (req, res) => {
  const id = +req.params.id;

  const data = await userService.toggleAdmin(usersData)(id);
  const user = data.item;
  const error = data.error;
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(user);
};

const deleteUser = async (req, res) => {
  const id = +req.params.id;

  const data = await userService.deleteUser(usersData)(id);
  const user = data.item;
  const error = data.error;
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error))
      .send({ error: serviceErrorStack.get(error).message });
  } else {
    res
      .status(200)
      .send({ message: 'You have deleted user with username' + user });
  }
};

const banUser = async (req, res) => {
  const id = +req.params.id;
  const reason = req.body.reason || null;
  const days = req.body.days || null;
  const data = await userService.banUser(usersData)(id, reason, days);
  const user = data.item;
  const error = data.error;

  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(user);
};

const isBanned = async (req, res) => {
  const id = +req.user.id;

  const data = await userService.isBanned(usersData)(id);
  const status = data.item;
  const error = data.error;

  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json({ banned: status });
};

const banStatus = async (req, res, next) => {
  const id = req.user.id;

  const banned = enumData.activeBans.has(id);
  if (banned) {
    return res
      .status(serviceErrors.BANNED_USER.status)
      .send({
        error:
          serviceErrors.BANNED_USER.message +
          ' until ' +
          formatDate(enumData.activeBans.get(id)),
      });
  }
  next();
};

const getRatingsByUser = async (req, res) => {
  const id = +req.params.id;

  const { error, item } = await userService.getRatingsByUser(usersData)(id);
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({
        error: `This Users's rating is ${
          serviceErrorStack.get(error).message
        }`,
      });
  }
  res.status(200).json(item);
};

const getActiveBans = async (req, res) => {
  const data = await userService.getActiveBans(usersData)();
  const user = data.item;
  const error = data.error;
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(user);
};

const removeBan = async (req, res) => {
  const id = +req.params.id;

  const data = await userService.removeBan(usersData)(id);
  const user = data.item;
  const error = data.error;

  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: serviceErrorStack.get(error).message });
  }
  res.status(200).json(user);
};

const getLikesByUser = async (req, res) => {
  const id = +req.params.id;

  const { error, item } = await userService.getLikesByUser(usersData)(id);
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: `This Review ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send(item);
};
const getBorrowsByUser = async (req, res) => {
  const id = +req.params.id;
  const { error, item } = await userService.getBorrowsByUser(usersData)(id);
  if (serviceErrorStack.has(error)) {
    return res
      .status(serviceErrorStack.get(error).status)
      .send({ error: ` ${serviceErrorStack.get(error).message}` });
  }
  res.status(200).send(item);
};

const getEnums = async (req, res) => {
  res.status(200)
    .send({
      genres: enumData.genres,
      languages: enumData.languages,
      levels: enumData.levels,
    });
};

export default {
  getEnums,
  getLikesByUser,
  getBorrowsByUser,
  getActiveBans,
  removeBan,
  isBanned,
  searchUsers,
  getUserById,
  banUser,
  banStatus,
  deleteUser,
  updateUser,
  getRatingsByUser,
  toggleAdmin,
};
