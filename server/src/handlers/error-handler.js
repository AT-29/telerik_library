const catchErrors = (fn) => {
  return (req, res, next) => {
    return fn(req, res, next).catch(next);
  };
};

const notFound = (req, res, next) => {
  const error = new Error('Not Found');
  error.status = 404;
  next(error);
};

const serviceErrors = {
  /** Such a record does not exist (when it is expected to exist) */
  RECORD_NOT_FOUND: {
    code: 1,
    status: 404,
    message: 'not found!',
  },
  /** The requirements do not allow more than one of username  resource */
  DUPLICATE_USERNAME_RECORD: {
    code: 2,
    status: 409,
    message: 'Name not available!',
  },
  /** The requirements do not allow more than one of email  resource */
  DUPLICATE_EMAIL_RECORD: {
    code: 3,
    status: 409,
    message: 'Email not available!',
  },
  /** The requirements do not allow such an operation */
  OPERATION_NOT_PERMITTED: {
    code: 4,
    status: 500,
    message: 'Not allowed!',
  },
  /** username/password mismatch */
  INVALID_SIGNIN: {
    code: 5,
    status: 500,
    message: 'Invalid username/password',
  },
  RECORD_NOT_AVAILABLE: {
    code: 6,
    status: 409,
    message: 'is not available!',
  },
  RECORD_NOT_RATED: {
    code: 7,
    status: 409,
    message: 'not assigned!',
  },
  RECORD_GONE: {
    code: 8,
    status: 410,
    message: 'Item no longer available!',
  },
  DELETE_NOT_PERMITTED: {
    code: 9,
    status: 403,
    message: 'You cannot delete!',
  },
  RESOURCE_IS_FORBIDDEN:{
    code:10,
    status:403,
    message: 'action is forbidden.',
  },
  BANNED_USER: {
    code: 11,
    status: 403,
    message: 'You are banned',
  },
  BOOK_NOT_READ: {
    code: 12,
    status: 403,
    message: 'still not read by you.',
  },
  BOOK_ALREADY_REVIEWED: {
    code: 13,
    status: 403,
    message: 'already reviewed by you.',
  },
  PLACEHOLDER_ERROR: {
    code: 14,
    status: 500,
    message: 'Lorem ispum error',
  },
};

const serviceErrorStack = Object.values(serviceErrors).reduce((acc, item) => {
  acc.set(item.code, { status: item.status, message: item.message });
  return acc;
}, new Map);

const productionErrors = (err, req, res, next) => {
  res.status(err.status || 500).send({ error: err.message });
};

const developmentErrors = (err, req, res, next) => {
  err.stack = err.stack || '';
  const errorDetails = {
    error: err.message,
    status: err.status,
    stackHighlighted: err.stack.split('\n').reduce((acc, item, index) => {
      acc[index] = item.trim();
      return acc;
    }, {}),
  };
  res.status(err.status || 500).send(errorDetails);
};

export {
  catchErrors,
  notFound,
  serviceErrors,
  productionErrors,
  developmentErrors,
  serviceErrorStack,
};
